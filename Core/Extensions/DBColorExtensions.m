//
//  DBColorExtensions.m
//  File version: 1.0.1
//  Last modified: 06/24/2016
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBColorExtensions.h"

@implementation UIColor (DBColorExtensions)

- (double)R
{
    const CGFloat *components = CGColorGetComponents(self.CGColor);
    return components[0];
}

- (double)G
{
    const CGFloat *components = CGColorGetComponents(self.CGColor);
    return components[1];
}

- (double)B
{
    const CGFloat *components = CGColorGetComponents(self.CGColor);
    return components[2];
}

- (double)alpha
{
    return CGColorGetAlpha(self.CGColor);
}

+ (UIColor *)coloreR:(NSUInteger)R G:(NSUInteger)G B:(NSUInteger)B
{
    return [self coloreR:R G:G B:B conAlpha:1.0];
}

+ (UIColor *)coloreR:(NSUInteger)R G:(NSUInteger)G B:(NSUInteger)B conAlpha:(double)alpha
{
    return [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:alpha];
}

+ (UIColor *)coloreConStringaHex:(NSString *)stringaHex
{
    NSString *colorString = [[stringaHex stringByReplacingOccurrencesOfString:@"#" withString:@""] uppercaseString];
    
    CGFloat alpha = 1.0;
    CGFloat red = 0.0;
    CGFloat blue = 0.0;
    CGFloat green = 0.0;
    
    switch ([colorString length]) {
        case 0:
            // @""
            break;
            
        case 3:
            // #RGB
            alpha = 1.0;
            red   = [self colorComponentFrom:colorString start:0 length:1];
            green = [self colorComponentFrom:colorString start:1 length:1];
            blue  = [self colorComponentFrom:colorString start:2 length:1];
            break;
            
        case 4:
            // #ARGB
            alpha = [self colorComponentFrom:colorString start:0 length:1];
            red   = [self colorComponentFrom:colorString start:1 length:1];
            green = [self colorComponentFrom:colorString start:2 length:1];
            blue  = [self colorComponentFrom:colorString start:3 length:1];
            break;
            
        case 6:
            // #RRGGBB
            alpha = 1.0;
            red   = [self colorComponentFrom:colorString start:0 length:2];
            green = [self colorComponentFrom:colorString start:2 length:2];
            blue  = [self colorComponentFrom:colorString start:4 length:2];
            break;
            
        case 8:
            // #AARRGGBB
            alpha = [self colorComponentFrom:colorString start:0 length:2];
            red   = [self colorComponentFrom:colorString start:2 length:2];
            green = [self colorComponentFrom:colorString start:4 length:2];
            blue  = [self colorComponentFrom:colorString start:6 length:2];
            break;
            
        default:
            DBLog(@"stringaHex non valida: ", stringaHex);
            break;
    }
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+ (CGFloat)colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length
{
    NSString *substring = [string substringWithRange:NSMakeRange(start, length)];
    NSString *fullHex = (length == 2) ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString:fullHex] scanHexInt:&hexComponent];
    return hexComponent / 255.0;
}

+ (UIColor *)color:(UIColor *)color withBrightness:(CGFloat)brightness
{
    CGFloat hue, saturation, oldBrightness, alpha;
    
    BOOL success = [color getHue:&hue saturation:&saturation brightness:&oldBrightness alpha:&alpha];
    if (!success) {
        return color;
    }
    
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:alpha];
}

+ (UIColor *)colorWithBlack:(CGFloat)black alpha:(CGFloat)alpha
{
    return [UIColor colorWithWhite:(1.0 - black) alpha:alpha];
}

+ (BOOL)isColorLight:(UIColor *)color
{
    if (!color) {
        return YES;
    }
    
    CGFloat colorBrightness = 0;
    
    CGColorSpaceRef colorSpace = CGColorGetColorSpace(color.CGColor);
    CGColorSpaceModel colorSpaceModel = CGColorSpaceGetModel(colorSpace);
    
    if (colorSpaceModel == kCGColorSpaceModelRGB) {
        // Colori RGB
        const CGFloat *componentColors = CGColorGetComponents(color.CGColor);
        colorBrightness = ((componentColors[0] * 299.0) + (componentColors[1] * 587.0) + (componentColors[2] * 114.0)) / 1000.0;
    }
    else {
        // Colori monocromatici
        [color getWhite:&colorBrightness alpha:0];
    }
    
    return (colorBrightness >= 0.5f) ? YES : NO;
}

+ (BOOL)isColorDark:(UIColor *)color
{
    return ![self isColorLight:color];
}

+ (UIColor *)blendColor:(UIColor *)color withColor:(UIColor *)otherColor alpha:(CGFloat)alpha
{
    if (!color) {
        color = [UIColor clearColor];
    }
    
    if (!otherColor) {
        return color;
    }
    
    alpha = mantieniAllInterno(alpha, 0.0, 1.0);
    CGFloat beta = 1.0 - alpha;
    
    CGFloat r1, g1, b1, a1, r2, g2, b2, a2;
    [color getRed:&r1 green:&g1 blue:&b1 alpha:&a1];
    [otherColor getRed:&r2 green:&g2 blue:&b2 alpha:&a2];
    
    CGFloat newRed   = r1 * beta + r2 * alpha;
    CGFloat newGreen = g1 * beta + g2 * alpha;
    CGFloat newBlue  = b1 * beta + b2 * alpha;
    CGFloat newAlpha = a1 * beta + a2 * alpha;
    
    return [UIColor colorWithRed:newRed green:newGreen blue:newBlue alpha:newAlpha];
}

@end
