//
//  DBLocalizationExtensions.h
//  File version: 1.0.1
//  Last modified: 09/20/2016
//
//  Created by Davide Balistreri on 04/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBLocalization.h"

@interface DBLocalization (DBLocalizationExtensions)

/// Localizza il contenuto dell'oggetto in base ai dati precompilati o alla stringa -localizedText se è già stata impostata.
+ (void)localizeObject:(UIResponder *)object;

/// Localizza il contenuto degli oggetti in base ai dati precompilati o alla stringa -localizedText se è già stata impostata.
+ (void)localizeObjects:(NSArray *)objects;

@end


@interface UILabel (DBLocalizationExtensions)

/// Localizza e imposta il testo della Label. Sarà possibile recuperare l'identifier utilizzato da qui.
@property (strong, nonatomic) NSString *localizedText;

@end


@interface UITextField (DBLocalizationExtensions)

/// Localizza e imposta il testo della TextField. Sarà possibile recuperare l'identifier utilizzato da qui.
@property (strong, nonatomic) NSString *localizedText;

/// Localizza e imposta il testo del placeholder della TextField. Sarà possibile recuperare l'identifier utilizzato da qui.
@property (strong, nonatomic) NSString *localizedPlaceholder;

@end


@interface UITextView (DBLocalizationExtensions)

/// Localizza e imposta il testo della TextView. Sarà possibile recuperare l'identifier utilizzato da qui.
@property (strong, nonatomic) NSString *localizedText;

@end


@interface UIButton (DBLocalizationExtensions)

/// Localizza e imposta il testo del Button. Sarà possibile recuperare l'identifier utilizzato da qui.
@property (strong, nonatomic) NSString *localizedText;

@end


@interface UINavigationItem (DBLocalizationExtensions)

/// Localizza e imposta il titolo della NavigationBar. Sarà possibile recuperare l'identifier utilizzato da qui.
@property (strong, nonatomic) NSString *localizedTitle;

@end
