//
//  DBImageExtensions.h
//  File version: 1.0.0
//  Last modified: 03/27/2016
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"
#import "DBViewExtensions.h"

@interface UIImage (DBImageExtensions)

+ (nullable UIImage *)unisciImmagineSopra:(nullable UIImage *)immagineSopra conImmagineSotto:(nullable UIImage *)immagineSotto conTrasparenza:(BOOL)trasparenza;

+ (nonnull UIImage *)safeImageNamed:(nullable NSString *)name
    NS_SWIFT_NAME(safe(named:));

NS_ASSUME_NONNULL_BEGIN

#if DBFRAMEWORK_TARGET_IOS

/**
 * Returns an empty image.
 */
+ (nonnull UIImage *)image;

- (UIImage *)immagineRidimensionataConRisoluzione:(CGFloat)risoluzione;
- (UIImage *)immagineRidimensionataConRisoluzioneMinima:(CGFloat)risoluzione;
- (UIImage *)immagineRitagliataConRect:(CGRect)rect;
- (UIImage *)immagineRuotataDiGradi:(CGFloat)gradi;
- (UIImage *)immagineRicolorataComeMaschera:(UIColor *)colore;

+ (UIImage *)iconaTabBarConNome:(NSString *)nome;
+ (UIImage *)iconaTabBarConNome:(NSString *)nome conFullscreen:(BOOL)fullscreen;
+ (UIImage *)iconaTabBarConNome:(NSString *)nome conFullscreen:(BOOL)fullscreen conPadding:(CGFloat)padding;
+ (UIImage *)iconaTabBar:(UIImage *)image conFullscreen:(BOOL)fullscreen conPadding:(CGFloat)padding;

+ (UIImage *)creaImmagineConColore:(UIColor *)colore conDimensioni:(CGSize)dimensioni
    DBFRAMEWORK_DEPRECATED(0.5.2, "utilizza il metodo +[UIImage immagineConColore:dimensioni:]");
+ (UIImage *)creaImmagineConColore:(UIColor *)colore conImmagineMaschera:(UIImage *)immagine
    DBFRAMEWORK_DEPRECATED(0.5.2, "utilizza il metodo +[UIImage immagineConColore:immagineMaschera:]");

+ (UIImage *)immagineConColore:(UIColor *)colore dimensioni:(CGSize)dimensioni;
+ (UIImage *)immagineConColore:(UIColor *)colore immagineMaschera:(UIImage *)immagine;
+ (UIImage *)immagineConColori:(NSArray<UIColor *> *)colori direzione:(DBLayoutDirection)direzione dimensioni:(CGSize)dimensioni;

+ (UIImage *)catturaFrameVideoConURL:(NSURL *)URL;
+ (UIImage *)catturaFrameVideoConURL:(NSURL *)URL secondi:(NSTimeInterval)secondi;

+ (UIImage *)catturaSchermata:(UIView *)schermata;
+ (UIImage *)immagineSchermata:(UIView *)schermata;

#else

+ (UIImage *)immagineSpecchiataOrizzontalmente:(UIImage *)immagine;
- (UIImage *)immagineSpecchiataOrizzontalmente;

#endif

NS_ASSUME_NONNULL_END

@end
