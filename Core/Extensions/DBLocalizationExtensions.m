//
//  DBLocalizationExtensions.m
//  File version: 1.0.1
//  Last modified: 09/20/2016
//
//  Created by Davide Balistreri on 04/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBLocalizationExtensions.h"
#import "DBObjectExtensions.h"
#import "DBViewExtensions.h"
#import "DBRuntimeHandler.h"

@implementation DBLocalization (DBLocalizationExtensions)

+ (void)localizeObject:(UIResponder *)object
{
    __weak id weakObject = object;
    
    if ([weakObject respondsToSelector:@selector(localizedText)]) {
        NSString *localizedText = [weakObject localizedText];
        if ([NSString isNotEmpty:localizedText]) {
            // Riutilizzo il localizedText dell'oggetto
            [weakObject setLocalizedText:localizedText];
            return;
        }
    }
    
    if ([weakObject isKindOfClass:[UILabel class]]) {
        [weakObject setLocalizedText:[weakObject text]];
    }
    else if ([weakObject isKindOfClass:[UITextField class]]) {
        [weakObject setLocalizedText:[weakObject text]];
        [weakObject setLocalizedPlaceholder:[weakObject placeholder]];
    }
    else if ([weakObject isKindOfClass:[UITextView class]]) {
        [weakObject setLocalizedText:[weakObject text]];
    }
    else if ([weakObject isKindOfClass:[UIButton class]]) {
        [weakObject setLocalizedText:[weakObject text]];
    }
    else if ([weakObject isKindOfClass:[UISegmentedControl class]]) {
        UISegmentedControl *segmentedControl = weakObject;
        for (NSUInteger counter = 0; counter < segmentedControl.numberOfSegments; counter++) {
            NSString *title = [segmentedControl titleForSegmentAtIndex:counter];
            [segmentedControl setTitle:DBLocalizedString(title) forSegmentAtIndex:counter];
        }
    }
}

+ (void)localizeObjects:(NSArray *)objects
{
    for (id object in objects) {
        [self localizeObject:object];
    }
}

@end


@implementation UILabel (DBLocalizationExtensions)

- (NSString *)localizedText
{
    return [self oggettoAssociatoConSelector:@selector(localizedText)];
}

- (void)setLocalizedText:(NSString *)localizedText
{
    self.text = DBLocalizedString(localizedText);
    [self associaOggettoStrong:localizedText conSelector:@selector(localizedText)];
}

@end


@implementation UITextField (DBLocalizationExtensions)

- (NSString *)localizedText
{
    return [self oggettoAssociatoConSelector:@selector(localizedText)];
}

- (void)setLocalizedText:(NSString *)localizedText
{
    self.text = DBLocalizedString(localizedText);
    [self associaOggettoStrong:localizedText conSelector:@selector(localizedText)];
}

- (NSString *)localizedPlaceholder
{
    return [self oggettoAssociatoConSelector:@selector(localizedPlaceholder)];
}

- (void)setLocalizedPlaceholder:(NSString *)localizedPlaceholder
{
    self.placeholder = DBLocalizedString(localizedPlaceholder);
    [self associaOggettoStrong:localizedPlaceholder conSelector:@selector(localizedPlaceholder)];
}

@end


@implementation UITextView (DBLocalizationExtensions)

- (NSString *)localizedText
{
    return [self oggettoAssociatoConSelector:@selector(localizedText)];
}

- (void)setLocalizedText:(NSString *)localizedText
{
    self.text = DBLocalizedString(localizedText);
    [self associaOggettoStrong:localizedText conSelector:@selector(localizedText)];
}

@end


@implementation UIButton (DBLocalizationExtensions)

- (NSString *)localizedText
{
    return [self oggettoAssociatoConSelector:@selector(localizedText)];
}

- (void)setLocalizedText:(NSString *)localizedText
{
    self.text = DBLocalizedString(localizedText);
    [self associaOggettoStrong:localizedText conSelector:@selector(localizedText)];
}

@end


@implementation UINavigationItem (DBLocalizationExtensions)

- (NSString *)localizedTitle
{
    return [self oggettoAssociatoConSelector:@selector(localizedTitle)];
}

- (void)setLocalizedTitle:(NSString *)localizedTitle
{
    self.title = DBLocalizedString(localizedTitle);
    [self associaOggettoStrong:localizedTitle conSelector:@selector(localizedTitle)];
}

@end
