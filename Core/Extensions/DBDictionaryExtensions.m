//
//  DBDictionaryExtensions.m
//  File version: 1.0.1
//  Last modified: 03/26/2016
//
//  Created by Davide Balistreri on 03/01/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBDictionaryExtensions.h"
#import "DBObjectExtensions.h"
#import "DBStringExtensions.h"

@implementation NSDictionary (DBDictionaryExtensions)

/**
 * Metodo che restituisce un dizionario vuoto se quello passato non è valido.
 */
+ (NSDictionary *)safeDictionary:(NSDictionary *)dictionary
{
    return ([NSDictionary isEmpty:dictionary]) ? [NSDictionary new] : dictionary;
}

- (id)safeValueForKey:(NSString *)key
{
    @try {
        id value = [self valueForKey:key];
        
        if ([NSObject isNotNull:value]) {
            return value;
        }
        
        return nil;
    }
    @catch (NSException *exception) {
        return nil;
    }
}

- (id)safeObjectForKey:(NSString *)key
{
    return [self safeValueForKey:key];
}

- (NSDictionary *)safeDictionaryForKey:(NSString *)key
{
    @try {
        id value = [self objectForKey:key];
        
        if ([NSObject isNotNull:value]) {
            if ([value isKindOfClass:[NSDictionary class]]) {
                return value;
            }
        }
        
        return [NSDictionary dictionary];
    }
    @catch (NSException *exception) {
        return [NSDictionary dictionary];
    }
}

- (NSArray *)safeArrayForKey:(NSString *)key
{
    @try {
        id value = [self objectForKey:key];
        
        if ([NSObject isNotNull:value]) {
            if ([value isKindOfClass:[NSArray class]]) {
                return value;
            }
        }
        
        return [NSArray array];
    }
    @catch (NSException *exception) {
        return [NSArray array];
    }
}

- (NSString *)safeStringForKey:(NSString *)key
{
    @try {
        id value = [self valueForKey:key];
        
        if ([NSObject isNotNull:value]) {
            if ([value isKindOfClass:[NSString class]]) {
                return [NSString safeString:value];
            } else if ([value respondsToSelector:@selector(stringValue)]) {
                return [NSString safeString:[value stringValue]];
            }
        }
    }
    @catch (NSException *exception) {
    }
    
    return @"";
}

- (NSNumber *)safeNumberForKey:(NSString *)key
{
    @try {
        id value = [self valueForKey:key];
        
        if ([NSObject isNotNull:value]) {
            if ([value isKindOfClass:[NSNumber class]]) {
                return value;
            }
        }
    }
    @catch (NSException *exception) {}
    
    return [NSNumber new];
}

- (BOOL)safeBoolForKey:(NSString *)key
{
    @try {
        id value = [self valueForKey:key];
        
        if ([NSObject isNotNull:value]) {
            return [value boolValue];
        }
        
        return NO;
    }
    @catch (NSException *exception) {
        return NO;
    }
}

- (int)safeIntForKey:(NSString *)key
{
    @try {
        id value = [self valueForKey:key];
        
        if ([NSObject isNotNull:value]) {
            return [value intValue];
        }
        
        return 0;
    }
    @catch (NSException *exception) {
        return 0;
    }
}

- (NSInteger)safeIntegerForKey:(NSString *)key
{
    @try {
        id value = [self valueForKey:key];
        
        if ([NSObject isNotNull:value]) {
            return [value integerValue];
        }
        
        return 0;
    }
    @catch (NSException *exception) {
        return 0;
    }
}

- (float)safeFloatForKey:(NSString *)key
{
    @try {
        id value = [self valueForKey:key];
        
        if ([NSObject isNotNull:value]) {
            return [value floatValue];
        }
        
        return 0.0f;
    }
    @catch (NSException *exception) {
        return 0.0f;
    }
}

- (double)safeDoubleForKey:(NSString *)key
{
    @try {
        id value = [self valueForKey:key];
        
        if ([NSObject isNotNull:value]) {
            return [value doubleValue];
        }
        
        return 0.0;
    }
    @catch (NSException *exception) {
        return 0.0;
    }
}

- (void)safeSetValue:(id)value forKey:(NSString *)key
{
    @try {
        [self setValue:value forKey:key];
    } @catch (NSException *exception) {}
}

- (void)safeSetObject:(id)object forKey:(NSString *)key
{
    [self safeSetValue:object forKey:key];
}

- (void)setBool:(BOOL)value forKey:(NSString *)key
{
    @try {
        [self setValue:[NSNumber numberWithBool:value] forKey:key];
    } @catch (NSException *exception) {}
}

- (void)setInt:(int)value forKey:(NSString *)key
{
    @try {
        [self setValue:[NSNumber numberWithInt:value] forKey:key];
    } @catch (NSException *exception) {}
}

- (void)setInteger:(NSInteger)value forKey:(NSString *)key
{
    @try {
        [self setValue:[NSNumber numberWithInteger:value] forKey:key];
    } @catch (NSException *exception) {}
}

- (void)setFloat:(float)value forKey:(NSString *)key
{
    @try {
        [self setValue:[NSNumber numberWithFloat:value] forKey:key];
    } @catch (NSException *exception) {}
}

- (void)setDouble:(double)value forKey:(NSString *)key
{
    @try {
        [self setValue:[NSNumber numberWithDouble:value] forKey:key];
    } @catch (NSException *exception) {}
}

@end
