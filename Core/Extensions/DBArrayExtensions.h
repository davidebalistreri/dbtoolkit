//
//  DBArrayExtensions.h
//  File version: 1.0.0
//  Last modified: 03/01/2016
//
//  Created by Davide Balistreri on 03/01/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

// MARK: - NSArray

@interface NSArray (DBArrayExtensions)

+ (instancetype)safeArrayWithObject:(id)object;

+ (instancetype)safeArrayWithArray:(NSArray *)array;


- (BOOL)objectAtIndexExists:(NSInteger)index;

- (id)safeObjectAtIndex:(NSUInteger)index;

+ (instancetype)arrayWithArrays:(NSArray *)primoArray, ... NS_REQUIRES_NIL_TERMINATION;

- (NSArray *)arrayByRemovingObject:(id)anObject;

@end


// MARK: - NSMutableArray

@interface NSMutableArray (DBArrayExtensions)

/**
 * Istanzia un mutable array contenente N oggetti NSNull.
 */
+ (instancetype)arrayConDimensione:(NSUInteger)dimensione;

- (void)safeAddObject:(id)object;
- (void)addBool:(BOOL)value;
- (void)addInt:(int)value;
- (void)addInteger:(NSInteger)value;
- (void)addFloat:(float)value;
- (void)addDouble:(double)value;
- (void)removeFirstObject;
- (void)moveObjectFromIndex:(NSUInteger)from toIndex:(NSUInteger)to;

/// Aggiunge o rimuove l'oggetto se è già contenuto nell'Array.
- (void)toggleObject:(id)object;

- (void)removeObjectsNotInArray:(NSArray *)otherArray;

@end
