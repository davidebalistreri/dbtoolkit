//
//  DBStringExtensions.m
//  File version: 1.1.3
//  Last modified: 09/20/2016
//
//  Created by Davide Balistreri on 02/04/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBStringExtensions.h"
#import "DBFramework.h"

#import <CommonCrypto/CommonCrypto.h>

@implementation NSString (DBStringExtensions)

+ (NSString *)stringaConData:(NSData *)data
{
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+ (NSString *)stringaConOggetto:(id)oggetto
{
    if ([oggetto respondsToSelector:@selector(description)]) {
        return [NSString safeString:[NSString stringWithFormat:@"%@", [oggetto description]]];
    }
    else {
        return [NSString safeString:[NSString stringWithFormat:@"%@", oggetto]];
    }
}

+ (NSString *)stringaConBool:(BOOL)value
{
    return (value) ? @"YES" : @"NO";
}

+ (NSString *)stringaConInt:(int)value
{
    return [NSString stringWithFormat:@"%d", value];
}

+ (NSString *)stringaConInteger:(NSInteger)value
{
    return [NSString stringWithFormat:@"%ld", (long)value];
}

+ (NSString *)stringaConFloat:(float)value
{
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setUsesSignificantDigits:YES];
    [formatter setDecimalSeparator:@"."];
    return [formatter stringFromNumber:[NSNumber numberWithFloat:value]];
}

+ (NSString *)stringaConDouble:(double)value
{
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setUsesSignificantDigits:YES];
    [formatter setDecimalSeparator:@"."];
    return [formatter stringFromNumber:[NSNumber numberWithDouble:value]];
}

+ (NSString *)stringaConArray:(NSArray *)array
{
    NSMutableString *stringa = [NSMutableString string];
    
    for (id oggetto in array) {
        [stringa appendFormat:@"%@", oggetto];
    }
    
    return stringa;
}

+ (NSString *)stringaConOggetti:(id)primoOggetto, ... NS_REQUIRES_NIL_TERMINATION
{
    NSMutableString *stringa = [NSMutableString string];
    
    va_list listaOggetti;
    va_start(listaOggetti, primoOggetto);
    
    id oggetto = primoOggetto;
    while (oggetto) {
        [stringa appendFormat:@"%@", oggetto];
        oggetto = va_arg(listaOggetti, id);
    }
    
    va_end(listaOggetti);
    
    return stringa;
}

+ (NSString *)stringWithVariableArgumentsList:(va_list)list firstArgument:(id)argument
{
    NSMutableString *string = [NSMutableString string];
    
    while (argument) {
        [string appendFormat:@"%@", argument];
        argument = va_arg(list, id);
    }
    
    return string;
}

/**
 * Metodo che restituisce una stringa vuota se la stringa passata non è valida.
 */
+ (NSString *)safeString:(NSString *)string
{
    return ([NSString isEmpty:string]) ? @"" : string;
}

/**
 * Metodo che restituisce una stringa vuota se la stringa passata non è valida,
 * altrimenti se la stringa è valida pulisce eventuali caratteri vuoti all'inizio e alla fine.
 */
+ (NSString *)cleanString:(NSString *)string
{
    return ([NSString isEmpty:string]) ? @"" : string.trimmedString;
}

+ (NSString *)cleanStringFromHTML:(NSString *)htmlString
{
    NSData *data = [htmlString dataUsingEncoding:NSUnicodeStringEncoding];
    NSDictionary *options = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
    
    NSString *cleanString = attributedString.string;
    return [NSString cleanString:cleanString];
}

- (NSString *)safeSubstringFromIndex:(NSUInteger)from
{
    @try {
        return [self substringFromIndex:from];
    } @catch (NSException *exception) {
        return @"";
    }
}

- (NSString *)safeSubstringToIndex:(NSUInteger)to
{
    @try {
        return [self substringToIndex:to];
    } @catch (NSException *exception) {
        return @"";
    }
}

- (NSString *)safeSubstringWithRange:(NSRange)range
{
    @try {
        return [self substringWithRange:range];
    } @catch (NSException *exception) {
        return @"";
    }
}

- (BOOL)isNotEqualToString:(NSString *)string
{
    return ![self isEqualToString:string];
}

- (BOOL)isSimilarToString:(NSString *)string
{
    return ([NSString similarityOfString:self withString:string] < 0.5) ? NO : YES;
}

// Punteggi algoritmo
const CGFloat kScoreCharMissing     = 1.0;
const CGFloat kScoreCharDifferent   = 0.8;
const CGFloat kScoreCharMoved       = 0.2;
const CGFloat kScoreCharAlmostEqual = 0.1;
const CGFloat kScoreCharEqual       = 0.0;

/**
 * Restituisce la percentuale di somiglianza tra due stringhe.
 */
+ (CGFloat)similarityOfString:(NSString *)string withString:(NSString *)otherString
{
    CGFloat difference = 0.0;
    
    // Conto i caratteri mancanti
    difference = kScoreCharMissing * abs(string.length - otherString.length);
    
    for (NSInteger counter = 0; counter < MIN(string.length, otherString.length); counter++) {
        unichar char1 = [string characterAtIndex:counter];
        unichar char2 = [otherString characterAtIndex:counter];
        
        CGFloat score = [self similarityOfChar:char1 withOtherChar:char2];
        if (score == kScoreCharDifferent) {
            // Controllo i caratteri circostanti
            CGFloat scorePre = kScoreCharMissing;
            if (counter-1 >= 0) {
                scorePre = [self similarityOfChar:char1 withOtherChar:[otherString characterAtIndex:counter-1]];
            }
            
            CGFloat scorePost = kScoreCharMissing;
            if (counter+1 < otherString.length) {
                scorePost = [self similarityOfChar:char1 withOtherChar:[otherString characterAtIndex:counter+1]];
            }
            
            if (fmin(scorePre, scorePost) <= kScoreCharAlmostEqual) {
                // Uno dei caratteri circostanti è simile
                score = kScoreCharMoved;
            }
        }
        
        difference += score;
    }
    
    // Calcolo la percentuale di similitudine
    return 1.0 - (difference / fmax(string.length, otherString.length));
}

+ (CGFloat)similarityOfChar:(unichar)aChar withOtherChar:(unichar)anotherChar
{
    if (aChar == anotherChar) {
        // Carattere uguale
        return kScoreCharEqual;
    }
    else if (tolower(aChar) == tolower(anotherChar)) {
        // Carattere quasi uguale
        return kScoreCharAlmostEqual;
    }
    else {
        // Carattere diverso, controllo se assomiglia ai caratteri vicini
        return kScoreCharDifferent;
    }
}

- (BOOL)contieneCaratteri:(NSString *)caratteri
{
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:caratteri];
    NSRange range = [self rangeOfCharacterFromSet:set];
    
    return (range.location == NSNotFound) ? NO : YES;
}

- (BOOL)contieneCaratteriDiversiDa:(NSString *)caratteri
{
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:caratteri];
    set = [set invertedSet];
    NSRange range = [self rangeOfCharacterFromSet:set];
    
    return (range.location == NSNotFound) ? NO : YES;
}

- (BOOL)contieneStringa:(NSString *)stringa
{
    return [self containsString:stringa];
}

- (BOOL)contieneStringaCaseInsensitive:(NSString *)stringa
{
    return [self.lowercaseString containsString:stringa.lowercaseString];
}

#if __IPHONE_OS_VERSION_MIN_REQUIRED < 80000

- (BOOL)containsString:(NSString *)aString
{
    return [self rangeOfString:aString].location != NSNotFound;
}

#endif

- (NSString *)trimmedString
{
    NSString *stringa = [NSString stringWithString:self];
    return [stringa stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (NSArray *)occorrenzeSottostringa:(NSString *)sottostringa inStringa:(NSString *)stringa
{
    // Check
    if ([NSString isEmpty:stringa] || [NSString isEmpty:sottostringa]) {
        return [NSArray array];
    }
    
    NSMutableArray *occorrenze = [NSMutableArray array];
    NSString *ricerca = stringa;
    NSUInteger location = 0;
    
    while (ricerca.length > 0) {
        NSRange range = [ricerca rangeOfString:sottostringa];
        
        if (range.location == NSNotFound) break;
        if (!occorrenze) occorrenze = [NSMutableArray array];
        
        NSRange realRange = range;
        realRange.location += location;
        
        NSValue *valueRange = [NSValue valueWithRange:realRange];
        [occorrenze addObject:valueRange];
        
        location = realRange.location + realRange.length;
        ricerca = [ricerca safeSubstringFromIndex:range.location + range.length];
    }
    
    return occorrenze;
}

- (NSArray *)occorrenzeSottostringa:(NSString *)sottostringa
{
    return [NSString occorrenzeSottostringa:sottostringa inStringa:self];
}

- (NSRange)primaOccorrenzaSottostringa:(NSString *)sottostringa partendoDa:(NSInteger)caratterePartenza
{
    // Check
    if (caratterePartenza >= self.length) {
        return NSMakeRange(0, 0);
    }
    
    NSString *stringa = [self safeSubstringFromIndex:caratterePartenza];
    NSArray *occorrenze = [NSString occorrenzeSottostringa:sottostringa inStringa:stringa];
    
    if ([NSArray isEmpty:occorrenze]) {
        return NSMakeRange(0, 0);
    }
    
    NSValue *valueRange = [occorrenze firstObject];
    
    NSRange range = [valueRange rangeValue];
    range.location += caratterePartenza;
    return range;
}

- (NSRange)ultimaOccorrenzaSottostringa:(NSString *)sottostringa partendoDa:(NSInteger)caratterePartenza
{
    // Check
    if (caratterePartenza >= self.length) {
        return NSMakeRange(0, 0);
    }
    
    NSString *stringa = [self safeSubstringFromIndex:caratterePartenza];
    NSArray *occorrenze = [NSString occorrenzeSottostringa:sottostringa inStringa:stringa];
    
    if ([NSArray isEmpty:occorrenze]) {
        return NSMakeRange(0, 0);
    }
    
    NSValue *valueRange = [occorrenze lastObject];
    
    NSRange range = [valueRange rangeValue];
    range.location += caratterePartenza;
    return range;
}

- (NSRange)primaOccorrenzaSottostringa:(NSString *)sottostringa
{
    return [self primaOccorrenzaSottostringa:sottostringa partendoDa:0];
}

- (NSRange)ultimaOccorrenzaSottostringa:(NSString *)sottostringa
{
    return [self ultimaOccorrenzaSottostringa:sottostringa partendoDa:0];
}

/**
 * Metodo per calcolare l'altezza di un testo in base al font.
 */
#if DBFRAMEWORK_TARGET_IOS
- (CGFloat)altezzaConFont:(UIFont *)font conDimensioniMassime:(CGSize)dimensioniMassime
#else
- (CGFloat)altezzaConFont:(NSFont *)font conDimensioniMassime:(CGSize)dimensioniMassime
#endif
{
    NSStringDrawingOptions options = NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin;
    
    NSDictionary *attr = @{NSFontAttributeName:font};
    CGRect frame = [self boundingRectWithSize:dimensioniMassime options:options attributes:attr context:nil];
    
    return frame.size.height;
}

- (NSString *)stringaBinaria:(NSInteger)value
{
    int bytes = 2;
    int byteBlock = 8; // Bits per byte
    int totalBits = bytes * byteBlock; // Total bits
    int binaryDigit = totalBits; // Which digit are we processing
    
    // C array - storage plus one for null
    char ndigit[totalBits + 1];
    
    while (binaryDigit-- > 0) {
        // Set digit in array based on rightmost bit
        ndigit[binaryDigit] = (value & 1) ? '1' : '0';
        
        // Shift incoming value one to right
        value >>= 1;
    }
    
    // Append null
    ndigit[totalBits] = 0;
    
    // Creo la stringa binaria
    return [NSString stringWithUTF8String:ndigit];
}

- (NSString *)stringaBinariaInversa:(NSInteger)value
{
    NSString *binary = [self stringaBinaria:value];
    
    // Inverto i caratteri della stringa (così il primo bit sarà il primo carattere, e il sedicesimo sarà l'ultimo)
    NSString *reversedBinary = @"";
    
    for (NSInteger counter = 0; counter < binary.length; counter++) {
        NSRange range = NSMakeRange(binary.length - counter - 1, 1);
        reversedBinary = [reversedBinary stringByAppendingString:[binary substringWithRange:range]];
    }
    
    return reversedBinary;
}

- (BOOL)checkBit:(NSInteger)bit stringaBinaria:(NSString *)stringaBinaria
{
    NSRange range = NSMakeRange(bit, 1);
    NSString *stringaBit = [stringaBinaria substringWithRange:range];
    
    return [stringaBit isEqualToString:@"1"] ? YES : NO;
}

- (BOOL)isMailValida
{
    BOOL stricterFilter = NO;
    
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isEmailValida
{
    DBFRAMEWORK_DEPRECATED_LOG(0.5.0, "utilizza -[NSString isMailValida]");
    return [self isMailValida];
}

+ (NSString *)stringaRandomConStringaCaratteri:(NSString *)stringaCaratteri conNumeroCaratteri:(NSUInteger)numeroCaratteri
{
    NSString *stringaRandom = @"";
    int maxIndex = (int) [stringaCaratteri length];
    
    for (NSInteger counter = 0; counter < numeroCaratteri; counter++) {
        stringaRandom = [stringaRandom stringByAppendingFormat:@"%C", [stringaCaratteri characterAtIndex:arc4random_uniform(maxIndex) % maxIndex]];
    }
    
    stringaRandom = [NSString stringWithFormat:@"%@", stringaRandom];
    return stringaRandom;
}

+ (NSString *)stringaRandomConNumeroCaratteri:(NSUInteger)numeroCaratteri
{
    NSString *stringaCaratteri = @"0123456789abcdefghijklmnopqrstuvwxyz";
    return [self stringaRandomConStringaCaratteri:stringaCaratteri conNumeroCaratteri:numeroCaratteri];
}

+ (NSString *)stringaMD5:(NSString *)stringa
{
    if ([NSString isEmpty:stringa]) {
        return stringa;
    }
    
    const char *pointer = [stringa UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(pointer, (CC_LONG)strlen(pointer), md5Buffer);
    
    NSMutableString *string = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [string appendFormat:@"%02x", md5Buffer[i]];
    }
    
    return string;
}

- (NSString *)stringByFormattingAsCreditCardNumber
{
    // Preparo la stringa ad essere formattata
    NSString *safeString = [self stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSMutableString *mutableString = [safeString mutableCopy];
    
    // Aggiungo uno spazio ogni 4 caratteri, ma non alla fine della stringa
    int caratteri = parseInt(mutableString.length) - 1;
    caratteri = mantieniAllInterno(caratteri, 0, caratteri);
    
    // Tengo conto di quanti spazi ho aggiunto
    int spazi = 0;
    
    for (int posizione = 4; posizione <= caratteri; posizione += 4) {
        // Inserisco lo spazio
        [mutableString insertString:@" " atIndex:posizione + spazi];
        spazi++;
    }
    
    return [NSString stringWithString:mutableString];
}

+ (NSString *)stringWithJSONObject:(id)jsonObject
{
    if ([NSJSONSerialization isValidJSONObject:jsonObject]) {
        NSError *error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:jsonObject options:NSJSONWritingPrettyPrinted error:&error];
        NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        return string;
    }
    
    return nil;
}

@end



@implementation NSMutableString (DBStringExtensions)

- (void)appendLine
{
    [self appendString:@"\n"];
}

- (void)appendStringLine:(NSString *)aString
{
    [self appendString:aString];
    [self appendLine];
}

- (void)appendFormatLine:(NSString *)format, ...
{
    va_list args;
    va_start(args, format);
    
    NSString *string = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    
    [self appendStringLine:string];
}

@end


@implementation NSMutableParagraphStyle (DBStringExtensions)

+ (NSMutableParagraphStyle *)paragraphStyleConInterlinea:(CGFloat)interlinea
{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    [paragraphStyle applicaInterlinea:interlinea];
    return paragraphStyle;
}

- (void)applicaInterlinea:(CGFloat)interlinea
{
    self.lineSpacing = interlinea;
    // self.maximumLineHeight = interlinea;
}

@end


@implementation NSAttributedString (DBStringExtensions)

+ (instancetype)attributedStringWithHTML:(NSString *)htmlString
{
    NSString *safeString = [NSString safeString:htmlString];
    
    NSData *data = [safeString dataUsingEncoding:NSUnicodeStringEncoding];
    NSDictionary *options = @{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)};
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
    return attributedString;
}

+ (instancetype)attributedStringConStringa:(NSString *)stringa
{
    return [[NSMutableAttributedString alloc] initWithString:[NSString safeString:stringa]];
}

+ (instancetype)localizedAttributedStringConStringa:(NSString *)stringa fontBase:(UIFont *)fontBase coloreBase:(UIColor *)coloreBase stringaAttributed:(NSString *)stringaAttributed fontAttributed:(UIFont *)fontAttributed coloreAttributed:(UIColor *)coloreAttributed
{
    return [self attributedStringConStringa:DBLocalizedString(stringa) fontBase:fontBase coloreBase:coloreBase stringaAttributed:DBLocalizedString(stringaAttributed) fontAttributed:fontAttributed coloreAttributed:coloreAttributed];
}

+ (instancetype)attributedStringConStringa:(NSString *)stringa fontBase:(UIFont *)fontBase coloreBase:(UIColor *)coloreBase stringaAttributed:(NSString *)stringaAttributed fontAttributed:(UIFont *)fontAttributed coloreAttributed:(UIColor *)coloreAttributed
{
    if (!fontBase) fontBase = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    if (!coloreBase) coloreBase = [UIColor blackColor];
    
    stringa = [NSString safeString:stringa];
    stringaAttributed = [NSString safeString:stringaAttributed];
    if (!fontAttributed) fontAttributed = fontBase;
    if (!coloreAttributed) coloreAttributed = coloreBase;
    
    NSDictionary *base = @{NSForegroundColorAttributeName:coloreBase, NSFontAttributeName:fontBase};
    NSDictionary *attributed = @{NSForegroundColorAttributeName:coloreAttributed, NSFontAttributeName:fontAttributed};
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:stringa attributes:base];
    
    NSRange range = [stringa rangeOfString:stringaAttributed];
    [attributedString setAttributes:attributed range:range];
    
    return attributedString;
}

@end


@implementation NSMutableAttributedString (DBStringExtensions)

- (void)applicaInterlinea:(CGFloat)interlinea
{
    [self applicaInterlinea:interlinea conRange:NSMakeRange(0, self.string.length)];
}

- (void)applicaInterlinea:(CGFloat)interlinea conRange:(NSRange)range
{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle paragraphStyleConInterlinea:interlinea];
    NSDictionary *attributes = @{NSParagraphStyleAttributeName:paragraphStyle};
    [self addAttributes:attributes range:range];
}

@end


@implementation UITextField (DBStringExtensions)

- (NSString *)cleanText
{
    NSString *cleanText = [NSString cleanString:self.text];
    return cleanText;
}

@end
