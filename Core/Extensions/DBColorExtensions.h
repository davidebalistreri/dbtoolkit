//
//  DBColorExtensions.h
//  File version: 1.0.1
//  Last modified: 06/24/2016
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@interface UIColor (DBColorExtensions)

@property (nonatomic, readonly) double R;
@property (nonatomic, readonly) double G;
@property (nonatomic, readonly) double B;
@property (nonatomic, readonly) double alpha;

+ (UIColor *)coloreR:(NSUInteger)R G:(NSUInteger)G B:(NSUInteger)B;
+ (UIColor *)coloreR:(NSUInteger)R G:(NSUInteger)G B:(NSUInteger)B conAlpha:(double)alpha;
+ (UIColor *)coloreConStringaHex:(NSString *)stringaHex;

+ (UIColor *)color:(UIColor *)color withBrightness:(CGFloat)brightness;

+ (UIColor *)colorWithBlack:(CGFloat)black alpha:(CGFloat)alpha;

/// Restituisce YES se la luminosità del colore supera una determinata soglia media.
+ (BOOL)isColorLight:(UIColor *)color;
/// Restituisce YES se la luminosità del colore è al di sotto di una determinata soglia media.
+ (BOOL)isColorDark:(UIColor *)color;

+ (UIColor *)blendColor:(UIColor *)color withColor:(UIColor *)otherColor alpha:(CGFloat)alpha;

@end
