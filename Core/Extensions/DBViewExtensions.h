//
//  DBViewExtensions.h
//  File version: 1.1.4
//  Last modified: 09/20/2016
//
//  Created by Davide Balistreri on 05/30/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"
#import "DBCompatibility.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wnullability-completeness"


// MARK: - View

enum {
#if DBFRAMEWORK_TARGET_IOS
    /// Adatta le dimensioni della vista alla superview
    DBViewAdattaDimensioni = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight,
#else
    /// Adatta le dimensioni della vista alla superview
    DBViewAdattaDimensioni = NSViewWidthSizable|NSViewHeightSizable,
#endif
    /// Deprecated.
    DBAutoresizeAdattaDimensioni = DBViewAdattaDimensioni
};


#if DBFRAMEWORK_TARGET_IOS
enum {
    DBViewAnimationDefault = UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState
};
#endif


/**
 * Direzioni da utilizzare come parametro per le funzioni.
 */
typedef NS_ENUM(NSUInteger, DBLayoutDirection) {
    /// Verso l'alto
    DBViewLayoutDirectionTop,
    /// Verso sinistra
    DBViewLayoutDirectionLeft,
    /// Verso destra
    DBViewLayoutDirectionRight,
    /// Verso il basso
    DBViewLayoutDirectionBottom,
    /// Verso l'alto a sinistra
    DBViewLayoutDirectionTopLeft,
    /// Verso l'alto a destra
    DBViewLayoutDirectionTopRight,
    /// Verso il basso a sinistra
    DBViewLayoutDirectionBottomLeft,
    /// Verso il basso a destra
    DBViewLayoutDirectionBottomRight,
    /// Unknown
    DBViewLayoutDirectionUnknown = 0
};


/// Effetti da applicare alle View.
typedef NS_ENUM(NSUInteger, DBViewVisualEffect) {
    /// Nessun effetto.
    DBViewVisualEffectNone = 0,
    /// Sfocatura scura.
    DBViewVisualEffectDarkBlur,
    /// Sfocatura chiara.
    DBViewVisualEffectLightBlur,
    /// Sfocatura molto chiara.
    DBViewVisualEffectExtraLightBlur
};


@interface UIView (DBViewExtensions)

- (void)removeAllSubviews;
- (void)removeAllConstraints;

- (BOOL)isDescendantOfViewWithClass:(Class)klass;
- (__kindof UIView *)superviewWithClass:(Class)klass;
- (__kindof UIView *)topmostSuperview;

- (void)bringToFront;
- (void)sendToBack;

@property (weak, nonatomic, readonly) __kindof UIViewController *parentViewController;


#if DBFRAMEWORK_TARGET_IOS

- (void)setVisualEffect:(DBViewVisualEffect)visualEffect;

- (void)aggiungiOmbraConColore:(UIColor *)colore conSpessore:(CGFloat)spessore;
- (void)rimuoviOmbra;

- (void)aggiungiBordoConColore:(UIColor *)colore conSpessore:(CGFloat)spessore;
- (void)rimuoviBordo;

- (void)aggiungiAngoliArrotondatiConRaggio:(CGFloat)raggio;
- (void)aggiungiAngoliArrotondatiCerchio;
- (void)rimuoviAngoliArrotondati;

- (void)setShouldRasterize:(BOOL)shouldRasterize;

- (UIImage *)catturaSchermata;
- (UIImage *)immagineSchermata;

- (UIScrollView *)creaScrollView;

- (UIView *)maskViewWithSubviews:(NSArray<UIView *> *)subviews;
- (UIImage *)maskImageWithSubviews:(NSArray<UIView *> *)subviews;

#else

@property (strong, nonatomic) NSColor *backgroundColor;
@property (nonatomic) BOOL clipsToBounds;
@property (nonatomic) CGFloat alpha;
@property (nonatomic) BOOL opaque;

- (void)disableSubviews:(BOOL)disable;

#endif

@end


#if DBFRAMEWORK_TARGET_IOS

// MARK: - TableView

@interface UITableView (DBViewExtensions)

/// Il contentSize che comprende tutte le celle della TableView, gli header e i footer.
@property (readonly, nonatomic) CGSize contentSizeReale;

/// Aggiorna tutte le sezioni della TableView con o senza animazione.
- (void)reloadData:(BOOL)animated;

/// Reloads the data with an animation, and calls the completionBlock when done
- (void)reloadDataWithDuration:(CGFloat)duration completionBlock:(void(^)(void))completionBlock;

- (void)scrollToBottom:(BOOL)animated;

/**
 * Permette di animare l'aggiornamento di layout della TableView, e di eseguire un blocco di codice al termine dell'animazione.
 *
 * NB: Questo metodo non deve essere richiamato se il dataSource della TableView è cambiato e non sono state aggiornate le sezioni della tabella, altrimenti verrà generata un'eccezione.
 */
- (void)aggiornaLayoutTableViewConCompletionBlock:(void(^)(void))completionBlock;

/// Crea una UIView replicando il contenuto di questa UITableView.
- (UIView *)creaView;

@end


// MARK: - CollectionView

@interface UICollectionView (DBViewExtensions)

/// Il contentSize che comprende tutte le celle della CollectionView.
@property (readonly, nonatomic) CGSize contentSizeReale;

/// Aggiorna tutte le sezioni della CollectionView con o senza animazione.
- (void)reloadData:(BOOL)animazione;

@end


// MARK: - ScrollView

@interface UIScrollView (DBViewExtensions)

/// La porzione attualmente visibile sullo schermo.
@property (readonly) CGRect visibleRect;

/// Crea una UIScrollView con il contenuto della vista specificata e la aggiunge al suo interno.
+ (UIScrollView *)creaScrollViewConVista:(UIView *)vista;

/// Il contentSize che comprende tutti gli elementi della ScrollView.
@property (readonly) CGSize contentSizeIdeale;

/// Adatta il contentSize della ScrollView in base al suo contenuto.
- (void)adattaContenuto;

/// Adatta solo l'altezza del contentSize della ScrollView in base al suo contenuto.
- (void)adattaAltezzaContenuto;

/// Adatta solo la larghezza del contentSize della ScrollView in base al suo contenuto.
- (void)adattaLarghezzaContenuto;

- (void)scrollToTop:(BOOL)animated;
- (void)scrollToBottom:(BOOL)animated;

/// Funzione che imposta il content offset assicurando che non si ecceda il content size
- (void)safeSetContentOffset:(CGPoint)contentOffset;

@end


// MARK: - Label

@interface UILabel (DBViewExtensions)

- (void)setAttributedTextConStringa:(NSString *)stringa
                      conColoreBase:(UIColor *)coloreBase
               conStringaAttributed:(NSString *)stringaAttributed
                conColoreAttributed:(UIColor *)coloreAttributed;

- (void)setAttributedTextConStringa:(NSString *)stringa
                        conFontBase:(UIFont *)fontBase
                      conColoreBase:(UIColor *)coloreBase
               conStringaAttributed:(NSString *)stringaAttributed
                  conFontAttributed:(UIFont *)fontAttributed
                conColoreAttributed:(UIColor *)coloreAttributed;

- (void)applicaInterlinea:(CGFloat)interlinea;

@end

#endif


// MARK: - TextField

@interface UITextField (DBViewExtensions)

#if DBFRAMEWORK_TARGET_IOS

@property (strong, nonatomic) UIFont *placeholderTextFont;
@property (strong, nonatomic) UIColor *placeholderTextColor;

+ (void)correzioneLagiOS8
DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza il metodo +[DBUtility correzioneLagInputFields]");

#else

@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSAttributedString *attributedText;
@property (strong, nonatomic) NSString *placeholder;

- (void)applicaInterlinea:(CGFloat)interlinea;

#endif

@end


// MARK: - TextView

@interface UITextView (DBViewExtensions)

#if DBFRAMEWORK_TARGET_IOS

/// Padding minimo consigliato
- (void)applicaPaddingMinimo;

- (void)applicaPadding:(CGFloat)padding;
- (void)applicaPaddingInset:(UIEdgeInsets)paddingInset;

- (void)applicaInterlinea:(CGFloat)interlinea;

#else

@property (strong, nonatomic) NSString *text;

#endif

@end


// MARK: - Button

@interface UIButton (DBViewExtensions)

@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSAttributedString *attributedText;

#if DBFRAMEWORK_TARGET_IOS
@property (strong, nonatomic) UIColor *textColor;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) UIImage *backgroundImage;
#endif

@end


// MARK: - GestureRecognizer

#if DBFRAMEWORK_TARGET_IOS

@interface UIGestureRecognizer (DBViewExtensions)

- (void)invalidateGesture;

@end

#endif


// MARK: - Storyboard

@interface UIStoryboard (DBViewExtensions)

+ (UIStoryboard *)safeStoryboardWithName:(NSString *)name bundle:(NSBundle *)storyboardBundleOrNil;

/// Instantiates and returns the view controller which identifier is equal to the specified class.
- (__kindof UIViewController * __nullable)instantiateViewControllerWithClass:(nullable Class)klass;

@end


// MARK: - Font

@interface UIFont (DBViewExtensions)

/// Returns the system font if a font with the specified name doesn't exist.
+ (UIFont *)safeFontWithName:(NSString *)fontName size:(CGFloat)fontSize;

@end

#pragma clang diagnostic pop
