//
//  DBStringExtensions.h
//  File version: 1.1.3
//  Last modified: 09/20/2016
//
//  Created by Davide Balistreri on 02/04/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"
#import "DBCompatibility.h"
#import "Objectify.h"

@interface NSString (DBStringExtensions)

NS_ASSUME_NONNULL_BEGIN

+ (NSString *)stringaConData:(NSData *)data;

+ (NSString *)stringaConOggetto:(id)oggetto;
+ (NSString *)stringaConBool:(BOOL)value;
+ (NSString *)stringaConInt:(int)value;
+ (NSString *)stringaConInteger:(NSInteger)value;
+ (NSString *)stringaConFloat:(float)value;
+ (NSString *)stringaConDouble:(double)value;
+ (NSString *)stringaConArray:(NSArray *)array;

+ (NSString *)stringaConOggetti:(id)primoOggetto, ... NS_REQUIRES_NIL_TERMINATION;
+ (NSString *)stringWithVariableArgumentsList:(va_list)list firstArgument:(id)argument;

/**
 * Metodo che restituisce una stringa vuota se la stringa passata non è valida.
 */
+ (nonnull NSString *)safeString:(nullable NSString *)string
    NS_SWIFT_NAME(safe(_:));

/**
 * Metodo che restituisce una stringa vuota se la stringa passata non è valida,
 * altrimenti se la stringa è valida pulisce eventuali caratteri vuoti all'inizio e alla fine.
 */
+ (nonnull NSString *)cleanString:(nullable NSString *)string;

+ (nonnull NSString *)cleanStringFromHTML:(nullable NSString *)htmlString;

- (nonnull NSString *)safeSubstringFromIndex:(NSUInteger)from;
- (nonnull NSString *)safeSubstringToIndex:(NSUInteger)to;
- (nonnull NSString *)safeSubstringWithRange:(NSRange)range;

/**
 * Metodo che restituisce YES se la stringa passata è diversa da quella dell'istanza.
 */
- (BOOL)isNotEqualToString:(NSString *)string;

/**
 * Restituisce la percentuale di somiglianza tra due stringhe.
 */
+ (CGFloat)similarityOfString:(NSString *)string withString:(NSString *)otherString;

/**
 * Restituisce YES se la stringa specificata è simile a quella dell'istanza.
 * <p>Se più del 50% dei caratteri sono diversi, le stringhe non sono considerate simili.</p>
 */
- (BOOL)isSimilarToString:(NSString *)string;

- (BOOL)contieneCaratteri:(NSString *)caratteri;
- (BOOL)contieneCaratteriDiversiDa:(NSString *)caratteri;
- (BOOL)contieneStringa:(NSString *)stringa;
- (BOOL)contieneStringaCaseInsensitive:(NSString *)stringa;

#if __IPHONE_OS_VERSION_MIN_REQUIRED < 80000
- (BOOL)containsString:(NSString *)aString;
#endif

@property (readonly, copy) NSString *trimmedString;

+ (NSArray *)occorrenzeSottostringa:(NSString *)sottostringa inStringa:(NSString *)stringa;
- (NSArray *)occorrenzeSottostringa:(NSString *)sottostringa;
- (NSRange)primaOccorrenzaSottostringa:(NSString *)sottostringa partendoDa:(NSInteger)caratterePartenza;
- (NSRange)ultimaOccorrenzaSottostringa:(NSString *)sottostringa partendoDa:(NSInteger)caratterePartenza;
- (NSRange)primaOccorrenzaSottostringa:(NSString *)sottostringa;
- (NSRange)ultimaOccorrenzaSottostringa:(NSString *)sottostringa;

/**
 * Metodo per calcolare l'altezza di un testo in base al font.
 */
#if DBFRAMEWORK_TARGET_IOS
- (CGFloat)altezzaConFont:(UIFont *)font conDimensioniMassime:(CGSize)dimensioniMassime;
#else
- (CGFloat)altezzaConFont:(NSFont *)font conDimensioniMassime:(CGSize)dimensioniMassime;
#endif

- (NSString *)stringaBinaria:(NSInteger)value;
- (NSString *)stringaBinariaInversa:(NSInteger)value;
- (BOOL)checkBit:(NSInteger)bit stringaBinaria:(NSString *)stringaBinaria;

- (BOOL)isMailValida;
- (BOOL)isEmailValida DBFRAMEWORK_DEPRECATED(0.5.0, "utilizza -[NSString isMailValida]");
+ (NSString *)stringaRandomConStringaCaratteri:(NSString *)stringaCaratteri conNumeroCaratteri:(NSUInteger)numeroCaratteri;
+ (NSString *)stringaRandomConNumeroCaratteri:(NSUInteger)numeroCaratteri;
+ (NSString *)stringaMD5:(NSString *)stringa;

/// Formatta la stringa come il numero delle carte di credito
- (NSString *)stringByFormattingAsCreditCardNumber;

+ (NSString *)stringWithJSONObject:(id)jsonObject;

NS_ASSUME_NONNULL_END

@end


@interface NSMutableString (DBStringExtensions)

- (void)appendLine;

- (void)appendStringLine:(nonnull NSString *)aString;

- (void)appendFormatLine:(nonnull NSString *)format, ... NS_FORMAT_FUNCTION(1,2);

@end


@interface NSMutableParagraphStyle (DBStringExtensions)

+ (nonnull NSMutableParagraphStyle *)paragraphStyleConInterlinea:(CGFloat)interlinea;

- (void)applicaInterlinea:(CGFloat)interlinea;

@end


@interface NSAttributedString (DBStringExtensions)

NS_ASSUME_NONNULL_BEGIN

+ (instancetype)attributedStringWithHTML:(nullable NSString *)htmlString;

+ (instancetype)attributedStringConStringa:(nullable NSString *)stringa;

+ (instancetype)attributedStringConStringa:(nullable NSString *)stringa
                                  fontBase:(nullable UIFont *)fontBase
                                coloreBase:(nullable UIColor *)coloreBase
                         stringaAttributed:(nullable NSString *)stringaAttributed
                            fontAttributed:(nullable UIFont *)fontAttributed
                          coloreAttributed:(nullable UIColor *)coloreAttributed;

+ (instancetype)localizedAttributedStringConStringa:(nullable NSString *)stringa
                                           fontBase:(nullable UIFont *)fontBase
                                         coloreBase:(nullable UIColor *)coloreBase
                                  stringaAttributed:(nullable NSString *)stringaAttributed
                                     fontAttributed:(nullable UIFont *)fontAttributed
                                   coloreAttributed:(nullable UIColor *)coloreAttributed;

NS_ASSUME_NONNULL_END

@end


@interface NSMutableAttributedString (DBStringExtensions)

- (void)applicaInterlinea:(CGFloat)interlinea;
- (void)applicaInterlinea:(CGFloat)interlinea conRange:(NSRange)range;

@end


@interface UITextField (DBStringExtensions)

/// Returns the text displayed by the text field, cleaned with -[NSString cleanString:]
@property (nonnull, readonly) NSString *cleanText;

@end
