//
//  DBTableViewController.h
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 01/12/2015
//
//  Creato da Davide Balistreri il 18/02/2015
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import <UIKit/UIKit.h>

@class DBDataSource;

@protocol DBTableViewControllerDelegate <NSObject>

@optional

- (void)tableViewRefreshControl;
- (void)tableViewDidSelectItem:(id)item atIndexPath:(NSIndexPath *)indexPath;

@end


/**
 UITableViewController esteso per creare uno standard di funzionamento e per semplificare alcuni scenari ricorrenti.
 
 Quando il DBTableViewController verrà inizializzato, verranno eseguiti in sequenza i metodi <b>impostaSchermata</b>,
 <b>localizzaSchermata</b>, <b>aggiornaSchermata:animazione</b> e <b>aggiornaDataSource</b>, per permettere allo sviluppatore di costruire la schermata per intero.
 
 Espone una variabile <b>didPerformFirstLoad</b> che indica se la schermata è stata inizializzata completamente da quando è stata creata.
 
 Inoltre permette di utilizzare il metodo <b>aggiornaDataSource</b> per aggiornare i dati che saranno mostrati nella schermata: ad esempio
 per scaricare nuove informazioni da un web service.
 
 Se dopo l'inizializzazione verrà cambiata la localizzazione di sistema attraverso la classe <i>DBLocalization</i>,
 verrà automaticamente eseguito il metodo <b>localizzaSchermata</b>.
 */
@interface DBTableViewController : UITableViewController <DBTableViewControllerDelegate>

@property (weak, nonatomic) id<DBTableViewControllerDelegate> delegate;

@property (strong, nonatomic) NSIndexPath *selectedIndex;
@property (strong, nonatomic) id dataSource;

- (void)mostraRefreshControl:(BOOL)mostra;
- (void)abilitaScrollToTop:(BOOL)abilita;

/// Variabile che indica se l'utente non ha ancora impostato/modificato il dataSource (ad esempio per mostrare il refreshControl).
@property (nonatomic, readonly) BOOL isAspettandoDataSource;


/**
 Boolean che indica se la schermata è stata inizializzata completamente da quando è stata creata.
 
 Viene impostato a <b>YES</b> quando il ViewController ha terminato l'inizializzazione, è stato
 localizzato e aggiornato, ed è visibile sullo schermo.
 */
@property (nonatomic, readonly) BOOL didPerformFirstLoad;

/**
 Metodo da utilizzare per inizializzare tutti i componenti della schermata:
 ad esempio i frame delle subviews, o le immagini e i testi che non verranno cambiati.
 
 Viene richiamato automaticamente quando il ViewController ha terminato l'inizializzazione.
 
 Deve essere eseguito un'unica volta.
 */
- (void)impostaSchermata;

/**
 Metodo per distruggere la schermata e le eventuali risorse attive.
 
 Viene richiamato automaticamente quando il ViewController ha iniziato la deallocazione.
 
 Deve essere eseguito un'unica volta.
 */
- (void)distruggiSchermata;

/**
 Metodo da utilizzare per la localizzazione di tutti i testi della schermata.
 
 Viene richiamato automaticamente dopo l'esecuzione del metodo <b>impostaSchermata</b>,
 e verrà richiamato ogni volta che verrà cambiata la lingua attarverso la classe <i>DBLocalization</i>,
 per facilitare l'aggiornamento di tutti i testi con la nuova lingua impostata.
 */
- (void)localizzaSchermata;

/**
 Metodo deprecato, utilizza <b>aggiornaSchermata:animazione</b>.
 */
- (void)aggiornaSchermata __attribute__((unavailable("Metodo deprecato, utilizza -aggiornaSchermata:animazione.")));

/**
 Metodo da utilizzare quando bisogna aggiornare i dati visualizzati sulla schermata, con o senza animazione.
 
 Viene richiamato automaticamente quando il ViewController ha terminato l'inizializzazione,
 dopo l'esecuzione dei metodi <b>impostaSchermata</b> e <b>localizzaSchermata</b>.
 */
- (void)aggiornaSchermata:(BOOL)animazione;

/**
 Metodo da utilizzare quando bisogna aggiornare i dati che saranno mostrati nella schermata: ad esempio
 per scaricare nuove informazioni da un web service.
 */
- (void)aggiornaDataSource;

/**
 * Delegate della TableView, è stato esteso per far sì che passi automaticamente l'oggetto cliccato al delegate di questa classe (se è presente).
 *
 * Per avere questa funzionalità, ricordati sempre di chiamare [super tableView:didSelectRowAtIndexPath:] nelle classi che estenderanno da questa!
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

@end
