//
//  DBNavigationController.m
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 01/12/2015
//
//  Creato da Davide Balistreri il 30/03/2015
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import "DBNavigationController.h"
#import "DBToolkit.h"

@implementation DBNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self impostaSchermata];
    [self localizzaSchermata];
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    // Retrocompatibilità
    [self aggiornaSchermata];
#pragma clang diagnostic pop
}

- (void)dealloc
{
    [self distruggiSchermata];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.didPerformFirstLoad = YES;
}

/**
 * Metodo per inizializzare la schermata. Viene richiamato automaticamente alla fine del viewDidLoad, e deve essere eseguito solo un'unica volta.
 *
 * Ricordati sempre di chiamare [super impostaSchermata] nelle classi che estenderanno da questa.
 */
- (void)impostaSchermata
{
    // Ascolto le notifiche per i cambi di localizzazione
    if ([self respondsToSelector:@selector(localizzaSchermata)])
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localizzaSchermata) name:kNotificaLinguaCorrenteAggiornata object:nil];
}

/**
 * Metodo per distruggere la schermata. Viene richiamato automaticamente al dealloc, e deve essere eseguito solo un'unica volta.
 *
 * Ricordati sempre di chiamare [super distruggiSchermata] nelle classi che estenderanno da questa!
 */
- (void)distruggiSchermata
{
    // Rimuovo il listener per i cambi di localizzazione
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/**
 * Metodo per impostare tutti i testi della schermata correttamente localizzati. Viene richiamato automaticamente alla fine del viewDidLoad e verrà richiamato ogni volta che verrà cambiata la lingua tramite la classe DBLocalization.
 *
 * NB: Non verrà richiamato automaticamente il metodo aggiornaSchermata.
 *
 * Ricordati sempre di chiamare [super localizzaSchermata] nelle classi che estenderanno da questa!
 */
- (void)localizzaSchermata
{
    
}

/**
 * Metodo per aggiornare i dati mostrati dalla schermata. Viene richiamato automaticamente alla fine del viewDidLoad.
 *
 * Ricordati sempre di chiamare [super aggiornaSchermata] nelle classi che estenderanno da questa!
 */
- (void)aggiornaSchermata DEPRECATED_ATTRIBUTE
{
    // Retrocompatibilità
    [self aggiornaSchermata:NO];
}

/**
 * Metodo per aggiornare i dati mostrati dalla schermata, con o senza animazione. Viene richiamato automaticamente alla fine del viewDidLoad.
 *
 * Ricordati sempre di chiamare [super aggiornaSchermata:animazione] nelle classi che estenderanno da questa!
 */
- (void)aggiornaSchermata:(BOOL)animazione
{
    
}

@end
