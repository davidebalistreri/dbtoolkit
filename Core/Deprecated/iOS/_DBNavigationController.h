//
//  DBNavigationController.h
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 01/12/2015
//
//  Creato da Davide Balistreri il 30/03/2015
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import <UIKit/UIKit.h>

@interface DBNavigationController : UINavigationController

/// Variabile che indica se la schermata è stata inizializzata e aggiornata almeno una volta
@property (nonatomic) BOOL didPerformFirstLoad;

/**
 * Metodo per inizializzare la schermata. Viene richiamato automaticamente alla fine del viewDidLoad, e deve essere eseguito solo un'unica volta.
 *
 * Ricordati sempre di chiamare [super impostaSchermata] nelle classi che estenderanno da questa.
 */
- (void)impostaSchermata;

/**
 * Metodo per distruggere la schermata. Viene richiamato automaticamente al dealloc, e deve essere eseguito solo un'unica volta.
 *
 * Ricordati sempre di chiamare [super distruggiSchermata] nelle classi che estenderanno da questa!
 */
- (void)distruggiSchermata;

/**
 * Metodo per impostare tutti i testi della schermata correttamente localizzati. Viene richiamato automaticamente alla fine del viewDidLoad e verrà richiamato ogni volta che verrà cambiata la lingua tramite la classe DBLocalization.
 *
 * NB: Non verrà richiamato automaticamente il metodo aggiornaSchermata.
 *
 * Ricordati sempre di chiamare [super localizzaSchermata] nelle classi che estenderanno da questa!
 */
- (void)localizzaSchermata;

/**
 * Metodo per aggiornare i dati mostrati dalla schermata. Viene richiamato automaticamente alla fine del viewDidLoad.
 *
 * Ricordati sempre di chiamare [super aggiornaSchermata] nelle classi che estenderanno da questa!
 */
- (void)aggiornaSchermata DEPRECATED_ATTRIBUTE;

/**
 * Metodo per aggiornare i dati mostrati dalla schermata, con o senza animazione. Viene richiamato automaticamente alla fine del viewDidLoad.
 *
 * Ricordati sempre di chiamare [super aggiornaSchermata:animazione] nelle classi che estenderanno da questa!
 */
- (void)aggiornaSchermata:(BOOL)animazione;

@end
