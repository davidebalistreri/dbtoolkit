//
//  DBPopupController.m
//  File version: 1.0.1
//  Last modified: 12/07/2015
//
//  Created by Davide Balistreri on 08/13/2015
//  Copyright 2013-2016 © Davide Balistreri. All rights reserved.
//

#import "DBPopupController.h"
#import "DBFramework.h"

CGFloat kAnimazionePopupDurata  = 0.6;
CGFloat kAnimazionePopupZoomIn  = 0.88;
CGFloat kAnimazionePopupZoomOut = 0.98;

@interface DBPopupController ()

@property (strong, nonatomic) NSMutableArray *popups;
@property (strong, nonatomic) UIWindow *popupWindow;

+ (instancetype)sharedInstance;
- (void)apriPopupControllerWindowConRootViewController:(id)rootViewController;
- (void)chiudiPopupControllerWindow;

@end


@interface DBPopup ()

@property (strong, nonatomic) UIView *superview;
@property (strong, nonatomic) UIView *view;
@property (strong, nonatomic) UIView *containerView;

@property (strong, nonatomic) UIViewController *viewController;
@property (nonatomic) CGRect frameOriginale;

@property (copy) DBPopupControllerBlock aperturaBlock;
@property (copy) DBPopupControllerBlock chiusuraBlock;

+ (instancetype)popup;

@end


@implementation DBPopup

+ (instancetype)popup
{
    DBPopup *popup = [DBPopup new];
    DBPopupController *sharedInstance = [DBPopupController sharedInstance];
    [sharedInstance.popups addObject:popup];
    return popup;
}

- (void)setBackground:(DBPopupBackgroundStyle)background
{
    _background = background;
    
    [self.containerView setVisualEffect:DBViewVisualEffectNone];
    
    switch (background) {
        case DBPopupBackgroundStyleOpaco:
            self.containerView.backgroundColor = [self.tintColor colorWithAlphaComponent:1.0];
            break;
        case DBPopupBackgroundStyleSemiTrasparente:
            self.containerView.backgroundColor = [self.tintColor colorWithAlphaComponent:0.9];
            break;
        case DBPopupBackgroundStyleTrasparente:
            self.containerView.backgroundColor = [UIColor clearColor];
            break;
        case DBPopupBackgroundStyleEffettoSfocato:
        default:
            [self.containerView setVisualEffect:DBViewVisualEffectDarkBlur];
            break;
    }
}

- (void)setPadding:(CGFloat)padding
{
    _padding = padding;
    
    // Aggiungo il padding
    UIView *popupView = self.viewController.view;
    popupView.constraintLarghezza -= padding * 2.0;
    popupView.constraintAltezza   -= padding * 2.0;
}

@synthesize tintColor = _tintColor;

- (UIColor *)tintColor
{
    return (_tintColor) ? _tintColor : [DBUtility tintColor];
}

- (void)setTintColor:(UIColor *)tintColor
{
    _tintColor = tintColor;
    
    // Aggiorno il colore del background
    self.background = _background;
}

+ (void)chiudiPopup:(DBPopup *)popup
{
    if (popup) {
        DBPopupController *sharedInstance = [DBPopupController sharedInstance];
        
        // Da gestire
        // [viewController viewWillDisappear:YES];
        
        // Animazione
        [UIView animateWithDuration:(kAnimazionePopupDurata / 2.0) animations:^{
            popup.view.transform = CGAffineTransformMakeScale(kAnimazionePopupZoomOut, kAnimazionePopupZoomOut);
            popup.containerView.alpha = 0.0;
            
        } completion:^(BOOL finished) {
            // Da gestire
            // [viewController viewDidDisappear:YES];
            
            [popup.containerView removeFromSuperview];
            popup.containerView = nil;
            popup.viewController = nil;
            
            if ([popup.superview isEqual:sharedInstance.popupWindow]) {
                [sharedInstance chiudiPopupControllerWindow];
            }
            
            if (popup.chiusuraBlock) {
                popup.chiusuraBlock();
            }
            
            [sharedInstance.popups removeObject:popup];
        }];
    }
}

- (void)chiudiPopup
{
    [DBPopup chiudiPopup:self];
}

@end


@implementation DBPopupController

+ (instancetype)sharedInstance
{
    @synchronized(self) {
        DBPopupController *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedPopupController"];
        
        if (!sharedInstance) {
            sharedInstance = [DBPopupController new];
            sharedInstance.popups = [NSMutableArray array];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedPopupController"];
        }
        
        return sharedInstance;
    }
}

- (UIWindow *)popupWindow
{
    if (!_popupWindow) {
        _popupWindow = [UIWindow new];
        _popupWindow.backgroundColor = [UIColor clearColor];
        _popupWindow.windowLevel = UIWindowLevelStatusBar;
    }
    
    _popupWindow.dimensioni = [DBGeometry dimensioniSchermo];
    return _popupWindow;
}

- (void)apriPopupControllerWindowConRootViewController:(id)rootViewController
{
    [self.popupWindow makeKeyAndVisible];
    self.popupWindow.rootViewController = rootViewController;
}

- (void)chiudiPopupControllerWindow
{
    if (self.popups.count <= 1) {
        [self.popupWindow setHidden:YES];
        self.popupWindow.rootViewController = nil;
    }
}

#pragma mark - Popup personalizzati

/**
 * Controlla se ci sono popup attualmente aperti.
 */
+ (BOOL)popupAperti
{
    DBPopupController *sharedInstance = [DBPopupController sharedInstance];
    return (sharedInstance.popups.count > 1) ? YES : NO;
}

/**
 * Crea un popup con il contenuto del ViewController.
 * @param viewController Il ViewController che occuperà la vista del popup.
 */
+ (DBPopup *)popupViewController:(UIViewController *)viewController
{
    return [self popupViewController:viewController conAperturaBlock:nil conChiusuraBlock:nil];
}

/**
 * Crea un popup con il contenuto del ViewController, permettendo di eseguire una parte di codice
 * quando il popup viene mostrato sullo schermo, e/o quando viene chiuso.
 * @param viewController Il ViewController che occuperà la vista del popup.
 * @param aperturaBlock  Il codice da eseguire quando il popup viene aperto.
 * @param chiusuraBlock  Il codice da eseguire quando il popup viene chiuso.
 */
+ (DBPopup *)popupViewController:(UIViewController *)viewController conAperturaBlock:(DBPopupControllerBlock)aperturaBlock conChiusuraBlock:(DBPopupControllerBlock)chiusuraBlock
{
    if (!viewController) {
        return nil;
    }
    
    DBPopupController *sharedInstance = [DBPopupController sharedInstance];
    UIWindow *window = sharedInstance.popupWindow;
    
    DBPopup *popup = [DBPopup popup];
    popup.superview = window;
    popup.viewController = viewController;
    popup.aperturaBlock = aperturaBlock;
    popup.chiusuraBlock = chiusuraBlock;
    
    // Sposto tutto il contenuto del popupViewController all'interno di una più piccola popupView
    // e oscuro/sfoco le parti esterne per far risaltare il popup
    UIView *popupView = viewController.view;
    popupView.clipsToBounds = YES;

    // Creo la vista principale, la ContainerView
    UIView *containerView = [UIView new];
    containerView.frame = window.bounds;
    containerView.clipsToBounds = YES;
    
    popup.containerView = containerView;
    popup.view = popupView;

    // Padding di default
    popup.padding = 20.0;
    
    // Background di default
    popup.background = DBPopupBackgroundStyleEffettoSfocato;
    
    [containerView addSubview:popupView];
    [window addSubview:containerView];
    
    // Layout
    popup.view.autoresizesSubviews = NO;
    
    // Grafica
    popupView.alpha = 0.0;
    containerView.alpha = 0.0;
    
    // Ok, tutto pronto
    [sharedInstance apriPopupControllerWindowConRootViewController:viewController];
    
    // Animazione
    // Ridimensiono la Container View per l'animazione
    popupView.transform = CGAffineTransformMakeScale(kAnimazionePopupZoomIn, kAnimazionePopupZoomIn);
    
    // Da gestire
    // [viewController viewWillAppear:YES];
    
    // Fade-in e zoom-in del contenuto del popup
    
    // Animazione più elaborata
    // [UIView animateWithDuration:kAnimazionePopupDurata delay:(kAnimazionePopupDurata / 5.0) usingSpringWithDamping:5 initialSpringVelocity:10 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    
    // Animazione meno complessa
    [UIView animateWithDuration:(kAnimazionePopupDurata / 2.0) animations:^{
        containerView.alpha = 1.0;
        popupView.alpha = 1.0;
        popupView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        // Da gestire
        // [viewController viewDidAppear:YES];
        
        if (popup.aperturaBlock) {
            popup.aperturaBlock();
        }
    }];
    
    return popup;
}

/**
 * Chiude il popup specificato ed esegue il chiusuraBlock (se presente).
 */
+ (void)chiudiPopup:(DBPopup *)popup
{
    if (popup) {
        [popup chiudiPopup];
    }
}

/**
 * Chiude il popup del ViewController specificato ed esegue il chiusuraBlock (se presente).
 */
+ (void)chiudiPopupViewController:(UIViewController *)viewController
{
    DBPopupController *sharedInstance = [DBPopupController sharedInstance];
    
    for (DBPopup *popup in sharedInstance.popups) {
        if ([popup.viewController isEqual:viewController]) {
            [popup chiudiPopup];
            break;
        }
    }
}

/**
 * Chiude il popup del ViewController specificato ed esegue il chiusuraBlock specificato.
 */
+ (void)chiudiPopupViewController:(UIViewController *)viewController conChiusuraBlock:(DBPopupControllerBlock)chiusuraBlock
{
    DBPopupController *sharedInstance = [DBPopupController sharedInstance];
    
    for (DBPopup *popup in sharedInstance.popups) {
        if ([popup.viewController isEqual:viewController]) {
            popup.chiusuraBlock = chiusuraBlock;
            [popup chiudiPopup];
            break;
        }
    }
}

/**
 * Chiude tutti i popup aperti, eseguendo i loro chiusuraBlock (se presenti).
 */
+ (void)chiudiTuttiPopups
{
    DBPopupController *sharedInstance = [DBPopupController sharedInstance];
    
    for (DBPopup *popup in sharedInstance.popups) {
        [popup chiudiPopup];
    }
}

@end
