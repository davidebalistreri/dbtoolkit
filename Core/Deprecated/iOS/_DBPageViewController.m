//
//  DBPageViewController.m
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 01/12/2015
//
//  Creato da Davide Balistreri il 10/10/2014
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import "DBPageViewController.h"

#import "DBToolkit.h"

@interface DBPageViewController () <UIPageViewControllerDelegate, UIPageViewControllerDataSource>

@property (nonatomic) BOOL initialLoading;

@property (nonatomic) NSUInteger selectedPage;
@property (nonatomic) NSUInteger indexPendingMostraPagina;

@property (nonatomic) CGRect oldFrame;

@property (strong, nonatomic) UIGestureRecognizer *gestureRecognizer;
@property (weak, nonatomic) UIScrollView *scrollView;

@end


@implementation DBPageViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.initialLoading = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Inizializzo il PageViewController
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageViewController.delegate = self;
    
    [self.view addSubview:self.pageViewController.view];
    
    self.gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(azioneGestureRecognizer:)];
    [self.view addGestureRecognizer:self.gestureRecognizer];
    
    for (id subview in self.pageViewController.view.subviews) {
        if ([subview isKindOfClass:[UIScrollView class]]) {
            self.scrollView = subview;
        }
    }
    
    // [self.pageViewController.view addSubview:self.pageControl];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    CGRect newFrame = self.view.bounds;
    
    if (self.initialLoading || !CGRectEqualToRect(self.oldFrame, self.view.bounds)) {
        
        if (self.initialLoading) {
            // Inizializzazione completa, eseguo i task che erano in attesa
            self.initialLoading = NO;
            [self mostraViewControllerConIndex:self.indexPendingMostraPagina animazione:NO];
        }
        
        self.oldFrame = self.view.bounds;
        
        // Imposto il PageControl
        newFrame = self.pageControl.frame;
        newFrame.size.width = self.view.bounds.size.width;
        newFrame.origin.x = 0.0;
        newFrame.origin.y = self.view.bounds.size.height - newFrame.size.height - newFrame.size.height;
        self.pageControl.frame = newFrame;
        self.pageControl.currentPageIndicatorTintColor = (self.tintColor) ? self.tintColor : [DBToolkit tintColor];
        
        [self.pageControl removeTarget:self action:@selector(pageControl:) forControlEvents:UIControlEventTouchUpInside];
        [self.pageControl addTarget:self action:@selector(pageControl:) forControlEvents:UIControlEventTouchUpInside];
    }
    
     self.pageViewController.view.frame = self.view.bounds;
}

@synthesize dataSource = _dataSource;
- (void)setDataSource:(NSArray *)dataSource
{
    _dataSource = dataSource;
    self.pageControl.numberOfPages = [dataSource count];
    
    if (self.isScorrimentoInvertito) {
        // Inverto l'ordine degli elementi nell'array
        _dataSource = [[dataSource reverseObjectEnumerator] allObjects];
        self.selectedPage = [dataSource count] - 1;
    }
    
    self.scrollView.bounces = ([dataSource count] < 2) ? NO : YES;
    
    if ([dataSource count] == 0) {
        self.pageViewController.dataSource = nil;
        return;
    }
    
    self.pageViewController.dataSource = self;
    
    self.pageControl.hidden = ([dataSource count] > 1) ? NO : YES;
    
    // Seleziono il primo ViewController
    NSInteger indexPrimoViewController = (self.isScorrimentoInvertito) ? [self.dataSource count] - 1 : 0;
    UIViewController *viewController = [self viewControllerAtIndex:indexPrimoViewController];
    NSArray *viewControllers = @[viewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

- (void)aggiungiViewController:(UIViewController *)viewController
{
    NSMutableArray *dataSource = [NSMutableArray arrayWithArray:self.dataSource];
    [dataSource addObject:viewController];
    self.dataSource = dataSource;
}

- (void)rimuoviViewControllers
{
    self.dataSource = [NSArray array];
}

@synthesize selectedPage = _selectedPage;
- (void)setSelectedPage:(NSUInteger)selectedPage
{
    _selectedPage = selectedPage;
    self.pageControl.currentPage = selectedPage;
}

- (void)pageControl:(UIPageControl *)sender
{
    [self mostraViewControllerConIndex:sender.currentPage animazione:!self.isSenzaAnimazione];
}

- (void)mostraViewControllerConIndex:(NSUInteger)index animazione:(BOOL)animazione
{
    // Fix viewDidLoad
    if (self.initialLoading) {
        self.indexPendingMostraPagina = index;
        return;
    }
    
    if ([self.dataSource count] == 0) return;
    if (![self.dataSource objectAtIndexExists:index]) return;
    
    NSUInteger trueIndex = index;
    if (self.isScorrimentoInvertito) trueIndex = [self.dataSource count] - 1 - trueIndex;
    
    UIPageViewControllerNavigationDirection direction = (self.selectedPage < trueIndex) ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse;
    
    UIViewController *viewController = [self viewControllerAtIndex:trueIndex];
    NSArray *viewControllers = @[viewController];
    [self.pageViewController setViewControllers:viewControllers direction:direction animated:animazione completion:nil];
    self.selectedPage = trueIndex;
}

#pragma mark - Page view controller data source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSInteger currentIndex = [self.dataSource indexOfObject:viewController];
    NSInteger nextIndex = currentIndex - 1;
    
    if (![self.dataSource objectAtIndexExists:nextIndex]) return nil;
    return [self viewControllerAtIndex:nextIndex];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSInteger currentIndex = [self.dataSource indexOfObject:viewController];
    NSInteger nextIndex = currentIndex + 1;
    
    if (![self.dataSource objectAtIndexExists:nextIndex]) return nil;
    return [self viewControllerAtIndex:nextIndex];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    UIViewController *current = [pageViewController.viewControllers firstObject];
    self.selectedPage = [self.dataSource indexOfObject:current];
    
    // Indice che tiene conto della storyboard invertita
    NSUInteger trueIndex = self.selectedPage;
    if (self.isScorrimentoInvertito) trueIndex = [self.dataSource count] - 1 - trueIndex;
    
    if ([self.delegate respondsToSelector:@selector(pageViewDidShowItem:atIndex:)])
        [self.delegate pageViewDidShowItem:current atIndex:trueIndex];
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    UIViewController *viewController = [self.dataSource safeObjectAtIndex:index];
    viewController.view.frame = self.view.bounds;
    viewController.view.layer.masksToBounds = YES;
    return viewController;
}

- (void)azioneGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
    if ([self.dataSource count] == 0) return;
    id item = [self.dataSource safeObjectAtIndex:self.selectedPage];
    
    if ([self.delegate respondsToSelector:@selector(pageViewDidSelectItem:atIndex:)])
        [self.delegate pageViewDidSelectItem:item atIndex:self.selectedPage];
}

@end
