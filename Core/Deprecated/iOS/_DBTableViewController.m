//
//  DBTableViewController.m
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 01/12/2015
//
//  Creato da Davide Balistreri il 18/02/2015
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import "DBTableViewController.h"
#import "DBToolkit.h"

@interface DBTableViewController ()

/// Variabile che tiene conto di quanto tempo è passato da quando la vista è stata caricata a quando è stato aggiornato il dataSource
@property (nonatomic) NSTimeInterval timeViewDidLoad;
@property (nonatomic) BOOL checkTimeViewDidLoad;

@property (nonatomic) BOOL isInViewDidLoad;

@end


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

/*
 Nascondo il warning per la mancata implementazione del metodo aggiornaDataSource:
 così se lo sviluppatore non lo implementa non verrà richiamato
 */

@implementation DBTableViewController
#pragma clang diagnostic pop

- (void)viewDidLoad
{
    // Il ViewController ancora non è stato inizializzato completamente
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    if (!_didPerformFirstLoad) {
        // L'inizializzazione del ViewController è quasi completata
        
        //        // Se il dataSource ancora non è arrivato, mostro il refreshControl
        //        if (!self.dataSource) {
        //            [self.refreshControl beginRefreshing];
        //        }
        
        //        self.timeViewDidLoad = [NSDate timeIntervalSinceReferenceDate];
        
        // Eseguo i metodi implementati dallo sviluppatore
        [self privateImpostaSchermata];
        [self privateLocalizzaSchermata];
        [self privateAggiornaSchermata:NO];
        if (!self.dataSource) [self privateAggiornaDataSource];
    }
    else {
        [self privateAggiornaSchermata:animated];
    }
    
    // Se il refreshControl era presente, in questo modo continuerà la sua animazione
    if ([self.refreshControl isRefreshing]) {
        // CGPoint offset = self.tableView.contentOffset;
        [self.refreshControl beginRefreshing];
        // self.tableView.contentOffset = offset;
    }
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    // Inizializzazione del ViewController completata
    _didPerformFirstLoad = YES;
    
    [super viewDidAppear:animated];
}

- (void)dealloc
{
    [self privateDistruggiSchermata];
}

#pragma mark - Metodi privati

- (void)privateImpostaSchermata
{
    // Inizializzo il TableViewController
    self.tableView.scrollsToTop = YES;
    
    // Se il delegate non è ancora stato impostato
    if (!self.delegate) self.delegate = self;
    
    // Ascolto le notifiche per i cambi di localizzazione
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(privateLocalizzaSchermata) name:kNotificaLinguaCorrenteAggiornata object:nil];
    
    // Eseguo il metodo implementato dallo sviluppatore
    [self impostaSchermata];
}

- (void)privateDistruggiSchermata
{
    // Rimuovo il listener per i cambi di localizzazione
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // Eseguo il metodo implementato dallo sviluppatore
    [self distruggiSchermata];
}

- (void)privateLocalizzaSchermata
{
    // Eseguo il metodo implementato dallo sviluppatore
    [self localizzaSchermata];
}

- (void)privateAggiornaSchermata:(BOOL)animazione
{
    //    if (self.isInViewDidLoad || self.isAspettandoDataSource)
    //        return;
    //
    //    // Se questo metodo è stato richiamato entro un secondo dal viewDidLoad, tolgo il refreshControl senza eseguire l'animazione
    //    NSTimeInterval actualTime = [NSDate timeIntervalSinceReferenceDate] - 1;
    //    if (self.checkTimeViewDidLoad && actualTime < self.timeViewDidLoad) {
    //        self.tableView.contentOffset = CGPointZero;
    //        self.checkTimeViewDidLoad = NO;
    //    }
    
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
    
    // L'utente ha richiesto di aggiornare la schermata, diciamo che non sta più aspettando il dataSource
    // self.isAspettandoDataSource = NO;
    
    // Eseguo il metodo implementato dallo sviluppatore
    [self aggiornaSchermata:animazione];
}

- (void)privateAggiornaDataSource
{
    if ([self respondsToSelector:@selector(aggiornaDataSource)]) {
        
        if (!self.dataSource) {
            // Mostro il RefreshControl
            [self.refreshControl beginRefreshing];
            
            if (!_didPerformFirstLoad) {
                // Fix per mostrarlo anche quando la TableView non è stata caricata
                self.tableView.contentOffset = CGPointMake(-1, -1);
            }
        }
        
        // Eseguo il metodo implementato dallo sviluppatore
        [self aggiornaDataSource];
    }
}

#pragma mark - Metodi da implementare dallo sviluppatore

- (void)impostaSchermata
{
    // Da implementare dallo sviluppatore
}

- (void)distruggiSchermata
{
    // Da implementare dallo sviluppatore
}

- (void)localizzaSchermata
{
    // Da implementare dallo sviluppatore
}

- (void)aggiornaSchermata:(BOOL)animazione
{
    // Da implementare dallo sviluppatore
}

- (void)abilitaScrollToTop:(BOOL)abilita
{
    if (!self.tableView.superview) return;
    
    for (UIScrollView *view in [self.tableView.superview subviews]) {
        if ([view isMemberOfClass:[UIScrollView class]]) {
            [view setScrollsToTop:NO];
        }
    }
    
    self.tableView.scrollsToTop = abilita;
}

- (void)mostraRefreshControl:(BOOL)mostra
{
    if (mostra && !self.refreshControl) {
        self.refreshControl = [UIRefreshControl new];
        [self.refreshControl addTarget:self action:@selector(tableViewRefreshControl) forControlEvents:UIControlEventValueChanged];
    }
    else if (!mostra) {
        [self.refreshControl removeTarget:self action:@selector(tableViewRefreshControl) forControlEvents:UIControlEventValueChanged];
        self.refreshControl = nil;
    }
}

@synthesize dataSource = _dataSource;
- (void)setDataSource:(id)dataSource
{
    _dataSource = dataSource;
    
    // L'utente ha impostato/modificato il dataSource
    // _isAspettandoDataSource = NO;
}

- (void)tableViewRefreshControl
{
    [self privateAggiornaDataSource];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.dataSource && [self.dataSource isMemberOfClass:[DBDataSource class]]) {
        // Gestisco le sezioni del DBDataSource
        return [self.dataSource numeroSezioni];
    }
    else if ([self.dataSource isKindOfClass:[NSArray class]]) {
        // Considero il dataSource una sezione unica
        return 1;
    }
    
    // Caso ancora non contemplato
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.dataSource && [self.dataSource isMemberOfClass:[DBDataSource class]]) {
        // Gestisco le sezioni del DBDataSource
        return [self.dataSource numeroOggettiNellaSezioneConIndice:section];
    }
    else if ([self.dataSource isKindOfClass:[NSArray class]]) {
        // Considero il dataSource una sezione unica
        return [self.dataSource count];
    }
    
    // Caso ancora non contemplato
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (self.dataSource && [self.dataSource isMemberOfClass:[DBDataSource class]]) {
        // Gestisco le sezioni del DBDataSource
        return [self.dataSource nomeSezioneConIndice:section];
    }
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Da implementare dallo sviluppatore
    return [UITableViewCell new];
}

/**
 * Delegate della TableView, è stato esteso per far sì che passi automaticamente l'oggetto cliccato al delegate di questa classe (se è presente).
 *
 * Per avere questa funzionalità, ricordati sempre di chiamare [super tableView:didSelectRowAtIndexPath:] nelle classi che estenderanno da questa!
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id item = nil;
    
    if (self.dataSource && [self.dataSource isMemberOfClass:[DBDataSource class]]) {
        // Gestisco le sezioni del DBDataSource
        item = [self.dataSource oggettoConIndice:indexPath.row nellaSezioneConIndice:indexPath.section];
    }
    else if ([self.dataSource isKindOfClass:[NSArray class]]) {
        // Considero il dataSource una sezione unica
        item = [self.dataSource safeObjectAtIndex:indexPath.row];
    }
    
    self.selectedIndex = indexPath;
    
    if ([self.delegate respondsToSelector:@selector(tableViewDidSelectItem:atIndexPath:)]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.delegate tableViewDidSelectItem:item atIndexPath:indexPath];
        });
    }
}

@end
