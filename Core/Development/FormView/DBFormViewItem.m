//
//  DBFormViewItem.m
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 29/03/2016
//
//  Creato da Davide Balistreri il 29/03/2016
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import "DBFormViewItem.h"
#import "DBFormView.h"
#import "DBGeometry.h"

@implementation DBFormViewItem

+ (instancetype)itemConCampo:(UIView *)vista
{
    DBFormViewItem *item = [self new];
    item.campo = vista;
    item.altezzaFissata = vista.altezza;
    return item;
}

- (void)rimuoviDalForm
{
    [self.form rimuoviCampo:self];
}

@end
