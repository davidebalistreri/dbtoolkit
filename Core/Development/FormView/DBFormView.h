//
//  DBFormView.h
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 29/03/2016
//
//  Creato da Davide Balistreri il 29/03/2016
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import "DBFoundation.h"

// CLASSE IN VIA DI SVILUPPO

#import "DBFormViewItem.h"

/**
 * Questa classe permette di creare un Form con campi dinamici e personalizzati.
 */
@interface DBFormView : UITableView

#pragma mark Gestione campi del Form

/// Gli oggetti DBFormItem di ogni campo aggiunto al Form
@property (strong, nonatomic, readonly) NSArray<DBFormViewItem *> *campi;

- (void)aggiungiCampo:(UIView *)campo;
- (void)rimuoviCampo:(UIView *)campo;

@end
