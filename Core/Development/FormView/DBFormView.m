//
//  DBForm.m
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 29/03/2016
//
//  Creato da Davide Balistreri il 29/03/2016
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import "DBFormView.h"
#import "DBTableViewHandler.h"
#import "DBGeometry.h"
#import "DBViewExtensions.h"

#pragma mark - DBFormHandler implementazione

@interface DBFormView () <DBTableViewHandlerDelegate>

// @property (strong, nonatomic) DBDataSource *campi;

@end


@implementation DBFormView

#pragma mark - Inizializzazione

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self inizializza];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self inizializza];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self inizializza];
    }
    return self;
}

- (void)inizializza
{
    if (!self.campi) {
        self.campi = [NSArray array];
        
        self.delegate = self;
        self.dataSource = self;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
}

- (void)dealloc
{
    // Da fare
}

//- (void)didMoveToSuperview
//{
//    [super didMoveToSuperview];
//    
//    if (self.superview) {
//        [self impostaContenuto];
//        self.frameOriginale = self.frame;
//        self.superview.clipsToBounds = NO;
//    }
//}

//- (void)setTableView:(UITableView *)tableView
//{
//    if (_tableView) {
//        // Rimuovo i riferimenti alla vecchia TableView
//        _tableView.delegate = nil;
//        _tableView.dataSource = nil;
//    }
//    
//    tableView.delegate = self;
//    tableView.dataSource = self;
//    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    
//    _tableView = tableView;
//}

@synthesize campi = _campi;
- (void)setCampi:(NSArray<DBFormViewItem *> *)campi
{
    _campi = campi;
}

#pragma mark Gestione campi del Form

- (void)aggiungiCampo:(UIView *)campo
{
    DBFormViewItem *oggetto = [DBFormViewItem itemConCampo:campo];
    oggetto.form = self;
    
    NSMutableArray *campi = [self.campi mutableCopy];
    [campi addObject:oggetto];
    self.campi = campi;
    
    [self reloadData];
}

- (void)rimuoviCampo:(UIView *)campo
{
    for (DBFormViewItem *oggetto in self.campi) {
        if ([oggetto.campo isEqual:campo]) {
            // Trovato
            NSMutableArray *campi = [self.campi mutableCopy];
            [campi removeObject:oggetto];
            self.campi = campi;
            
            [self reloadData];
        }
    }
}

#pragma mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.campi.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBFormViewItem *oggetto = self.campi[indexPath.row];
    
    if ([oggetto.campo isKindOfClass:[UITableViewCell class]]) {
        return UITableViewAutomaticDimension;
    }
    
    return oggetto.altezzaFissata;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBFormViewItem *oggetto = self.campi[indexPath.row];
    
    UITableViewCell *cella = nil;
    
    if ([oggetto.campo isKindOfClass:[UITableViewCell class]]) {
        // La vista è già una UITableViewCell
        cella = oggetto.campo;
    }
    else {
        cella = [UITableViewCell new];
        
        // Quando le celle non sono più visibili sullo schermo vengono deallocate
        // La superview di ogni vista viene quindi automaticamente resettata
        UIView *campo = oggetto.campo;
        [cella addSubview:campo];
    }
    
    cella.selectionStyle = UITableViewCellSelectionStyleNone;
    cella.clipsToBounds = YES;
    
    return cella;
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    DBFormHandlerItem *oggetto = [self.dataSource oggettoConIndexPath:indexPath];
//    UIView *vista = oggetto.vista;
//    
//    if ([oggetto.vista isKindOfClass:[UITableViewCell class]] == NO) {
//        vista.larghezza = tableView.larghezza;
//        vista.altezza = [self tableView:tableView heightForRowAtIndexPath:indexPath];
//    }
//}

@end
