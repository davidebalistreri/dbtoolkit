//
//  DBNetworking.h
//  Versione file: 1.1.0
//  Ultimo aggiornamento: 11/12/2015
//
//  Creato da Davide Balistreri il 15/03/2015
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import <UIKit/UIKit.h>
#import "DBObject.h"

@interface DBNetworking : NSObject

/**
 * CompletionBlock per le richieste web.
 * @param error L'errore, se assente, indica che la chiamata è andata a buon fine.
 * @param responseObject L'oggetto ricevuto in risposta dal web service.
 */
typedef void (^DBNetworkingCompletionBlock)(NSError *error, id responseObject);

/**
 * CompletionBlock per le richieste web.
 * @param error L'errore, se assente, indica che la richiesta è terminata con successo.
 */
typedef void (^DBNetworkingSuccessBlock)(NSError *error);


/**
 * Attiva il debug delle chiamate nella console.
 * @param debug Disattivo di default.
 */
+ (void)impostaDebug:(BOOL)debug;

/**
 * Se attivo, ogni nuova chiamata con lo stesso URL annullerà le chiamate precedenti. Altrimenti, ogni nuova chiamata rimarrà in attesa della risposta della prima.
 * @param cancellaRichiestePrecedenti Disattivo di default.
 */
+ (void)impostaCancellaRichiestePrecedenti:(BOOL)cancellaRichiestePrecedenti;

/**
 * @param headers Il Dictionary contenente tutti gli Header da aggiungere ad ogni chiamata effettuata.
 */
+ (void)impostaHeaderGlobali:(NSDictionary *)headers;

/**
 * Restituisce gli Header che vengono attualmente aggiunti ad ogni chiamata effettuata.
 */
+ (NSDictionary *)headerGlobali;


/**
 * Boolean che indica se il dispositivo è connesso ad internet.
 */
+ (BOOL)checkConnessioneInternet;


+ (void)HTMLGETConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock;


+ (void)JSONGETConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock;

+ (void)JSONPOSTConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock;

+ (void)JSONDELETEConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock;

+ (void)JSONPUTConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock;


+ (void)GETImmagineConURLString:(NSString *)URLString conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock;

+ (void)GETImmagineConURLString:(NSString *)URLString sovrascrivi:(BOOL)sovrascrivi conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock;

+ (void)GETFileConURLString:(NSString *)URLString conDownloadPath:(NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi conSuccessBlock:(DBNetworkingSuccessBlock)successBlock;

+ (void)GETFileConURLString:(NSString *)URLString conDownloadPath:(NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi conProgressHUD:(BOOL)progressHUDAbilitato messaggioProgressHUD:(NSString *)messaggioProgressHUD conSuccessBlock:(DBNetworkingSuccessBlock)successBlock;

@end


/**
 * La risposta server alle chiamate DBNetworking restituita nei completionBlock.
 */
@interface DBNetworkingResponse : DBObject

/**
 * L'oggetto di risposta restituito dal server.
 */
@property (strong, nonatomic, nullable) id responseObject;

/**
 * L'eventuale errore restituito dal server.
 */
@property (strong, nonatomic, nullable) NSError *error;

/**
 * Il codice di risposta restituito dal server.
 */
@property (nonatomic) NSInteger statusCode;

@end


/**
 * Deprecato: Ora viene utilizzato il Reachability integrato in AFNetworking.
 * Utilizzare +[DBNetworking checkConnessioneInternet]
 *
 * @interface Reachability (DBNetworking)
 *
 * + (BOOL)checkConnessioneInternet;
 *
 * @end
 */


@interface UIImageView (DBNetworking)

- (void)caricaImmagineConURLString:(nonnull NSString *)URLString conActivityIndicator:(nullable UIActivityIndicatorView *)activityIndicator;
- (void)caricaImmagineConURLString:(nonnull NSString *)URLString conActivityIndicator:(nullable UIActivityIndicatorView *)activityIndicator conCompletionBlock:(nullable void (^)(BOOL finished))completionBlock;

@end
