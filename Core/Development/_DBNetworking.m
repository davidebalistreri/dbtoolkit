//
//  DBNetworking.m
//  Versione file: 1.0.2
//  Ultimo aggiornamento: 11/12/2015
//
//  Creato da Davide Balistreri il 15/03/2015
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import "DBNetworking.h"
#import "DBToolkit.h"

#import <AFNetworking/AFNetworking.h>
#import "UIImageView+AFNetworking.h"

#pragma mark - DBNetworkingRequest Class

@interface DBNetworkingRequest : NSObject

// Dati della richiesta
@property (strong, nonatomic) NSString *metodo;
@property (strong, nonatomic) NSString *contentType;
@property (strong, nonatomic) NSString *URLString;
@property (strong, nonatomic) NSDictionary *parametri;
@property (strong, nonatomic) NSDictionary *headers;

// Oggetto richiesta
@property (strong, nonatomic) NSURLSessionDataTask *requestTask;
@property (nonatomic) NSTimeInterval timestamp;

@property (nonatomic) BOOL debug;

@property (copy) DBNetworkingCompletionBlock completionBlock;

// Dati della risposta
@property (strong, nonatomic) NSError *error;
@property (strong, nonatomic) id responseObject;

/// Override per confrontare due oggetti DBNetworkingRequest
- (BOOL)isEqual:(id)object;

@end

@implementation DBNetworkingRequest

/// Override per confrontare due oggetti DBNetworkingRequest
- (BOOL)isEqual:(id)object
{
    if (![object isKindOfClass:[DBNetworkingRequest class]])
        return NO;
    
    DBNetworkingRequest *richiesta = (DBNetworkingRequest *)object;
    
    if (![richiesta.metodo      isEqual:self.metodo])      return NO;
    if (![richiesta.contentType isEqual:self.contentType]) return NO;
    if (![richiesta.URLString   isEqual:self.URLString])   return NO;
    if (![richiesta.parametri   isEqual:self.parametri])   return NO;
    if (![richiesta.headers     isEqual:self.headers])     return NO;
    
    return YES;
}

@end


@implementation DBNetworking

#pragma mark - DBNetworking's additions

BOOL dbNetworkingDebug = NO;

+ (void)impostaDebug:(BOOL)debug
{
    @synchronized(self) {
        dbNetworkingDebug = debug;
    }
}

bool dbNetworkingCancelPreviousRequests = NO;

+ (void)impostaCancellaRichiestePrecedenti:(BOOL)cancellaRichiestePrecedenti
{
    @synchronized(self) {
        dbNetworkingCancelPreviousRequests = cancellaRichiestePrecedenti;
    }
}

NSDictionary *dbNetworkingHeaders;

+ (void)impostaHeaderGlobali:(NSDictionary *)headers
{
    @synchronized(self) {
        dbNetworkingHeaders = headers;
    }
}

+ (NSDictionary *)headerGlobali
{
    @synchronized(self) {
        return dbNetworkingHeaders;
    }
}

NSMutableArray *dbNetworkingRequests;

+ (BOOL)attendiRichiesteSimili:(DBNetworkingRequest *)richiestaOriginale
{
    @synchronized(self) {
        // Prendo le richieste ancora in attesa (escludendo quella originale)
        NSMutableArray *richiesteAttive = [[self filtraRichiesteSimili:richiestaOriginale] mutableCopy];
        
        if (dbNetworkingCancelPreviousRequests && [richiesteAttive count] > 0) {
            // Annullo tutte le altre richieste
            while ([richiesteAttive count] > 0) {
                DBNetworkingRequest *richiesta = [richiesteAttive firstObject];
                [richiesta.requestTask cancel];
                
                if (richiesta.completionBlock)
                    richiesta.completionBlock([NSError new], nil);
                
                [richiesteAttive removeObject:richiesta];
                [dbNetworkingRequests removeObject:richiesta];
            }
        }
        else if ([richiesteAttive count] > 0) {
            // La richiesta deve restare in attesa di quelle precedenti
            return YES;
        }
        
        // La richiesta può essere effettuata
        return NO;
    }
}

/**
 Restituisce un'array di richieste simili a quella passata (non includendola nell'array restituito).
 @param richiestaOriginale La richiesta originale per effettuare il filtro.
 */
+ (NSArray *)filtraRichiesteSimili:(DBNetworkingRequest *)richiestaOriginale
{
    // Prendo le altre richieste ancora in attesa con lo stesso URLString
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"URLString == %@", richiestaOriginale.URLString];
    NSMutableArray *richiesteAttive = [[dbNetworkingRequests filteredArrayUsingPredicate:predicate] mutableCopy];
    
    // Escludo quella originale
    [richiesteAttive removeObject:richiestaOriginale];
    
    // Confronto più nel dettaglio le richieste ancora in attesa
    NSMutableArray *richiesteSimili = [NSMutableArray array];
    for (DBNetworkingRequest *richiesta in richiesteAttive) {
        if ([richiesta isEqual:richiestaOriginale]) {
            [richiesteSimili addObject:richiesta];
        }
    }
    
    return richiesteSimili;
}

+ (DBNetworkingRequest *)aggiungiRichiestaConMetodo:(NSString *)metodo conContentType:(NSString *)contentType conURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    @synchronized(self) {
        if (!dbNetworkingRequests) {
            // E' la prima richiesta
            dbNetworkingRequests = [NSMutableArray array];
        }
        
        // Aggiungo la richiesta
        DBNetworkingRequest *richiesta = [DBNetworkingRequest new];
        richiesta.metodo = metodo;
        richiesta.contentType = contentType;
        richiesta.URLString = URLString;
        richiesta.parametri = parametri;
        richiesta.headers = [dbNetworkingHeaders copy];
        richiesta.debug = dbNetworkingDebug;
        richiesta.timestamp = [NSDate timeIntervalSinceReferenceDate];
        richiesta.completionBlock = completionBlock;
        
        [dbNetworkingRequests addObject:richiesta];
        return richiesta;
    }
}

+ (void)terminaRichiesteSimili:(DBNetworkingRequest *)richiestaOriginale
{
    @synchronized(self) {
        // Prendo le altre richieste ancora in attesa
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:YES];
        NSArray *richiesteAttive = [self filtraRichiesteSimili:richiestaOriginale];
        richiesteAttive = [richiesteAttive arrayByAddingObject:richiestaOriginale];
        
        // Riordino per data
        richiesteAttive = [richiesteAttive sortedArrayUsingDescriptors:@[sortDescriptor]];
        
        NSMutableArray *mutableRichiesteAttive = [richiesteAttive mutableCopy];
        while ([mutableRichiesteAttive count] > 0) {
            DBNetworkingRequest *richiesta = [mutableRichiesteAttive firstObject];
            
            if (richiesta.completionBlock) {
                // richiesta.completionBlock(richiestaOriginale.error, [richiestaOriginale.responseObject copy]);
                richiesta.completionBlock(richiestaOriginale.error, richiestaOriginale.responseObject);
            }
            
            [mutableRichiesteAttive removeObject:richiesta];
            [dbNetworkingRequests removeObject:richiesta];
        }
    }
}

#pragma mark - Chiamate Web

+ (BOOL)checkConnessioneInternet
{
    return [[AFNetworkReachabilityManager sharedManager] isReachable];
}

+ (void)HTMLGETConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    [self eseguiRichiestaConMetodo:@"GET" conContentType:@"HTML" conURLString:URLString conParametri:parametri conCompletionBlock:completionBlock];
}

+ (void)JSONGETConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    [self eseguiRichiestaConMetodo:@"GET" conContentType:@"JSON" conURLString:URLString conParametri:parametri conCompletionBlock:completionBlock];
}

+ (void)JSONPOSTConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    [self eseguiRichiestaConMetodo:@"POST" conContentType:@"JSON" conURLString:URLString conParametri:parametri conCompletionBlock:completionBlock];
}

+ (void)JSONDELETEConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    [self eseguiRichiestaConMetodo:@"DELETE" conContentType:@"JSON" conURLString:URLString conParametri:parametri conCompletionBlock:completionBlock];
}

+ (void)JSONPUTConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    [self eseguiRichiestaConMetodo:@"PUT" conContentType:@"JSON" conURLString:URLString conParametri:parametri conCompletionBlock:completionBlock];
}

+ (void)eseguiRichiestaConMetodo:(NSString *)metodo conContentType:(NSString *)contentType conURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    // Check validità richiesta
    if ([NSString isEmpty:URLString]) {
        if (completionBlock) completionBlock([NSError new], nil);
        return;
    }
    
    // Creo e metto in coda la richiesta effettuata
    DBNetworkingRequest *richiesta = [self aggiungiRichiestaConMetodo:metodo conContentType:contentType conURLString:URLString conParametri:parametri conCompletionBlock:completionBlock];
    
    // Controllo se devo attendere la risposta delle precedenti richieste
    if ([self attendiRichiesteSimili:richiesta]) return;
    
    // Preparo la chiamata
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    if ([contentType isEqualToString:@"HTML"]) {
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html"]];
    }
    else if ([contentType isEqualToString:@"JSON"]) {
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json"]];
    }
    
    // Aggiungo l'identifier della lingua correntemente utilizzata dall'app
    // NB: Gli Header globali possono sovrascrivere questa impostazione automatica della lingua
    NSString *acceptLanguage = [DBLocalization identifierLinguaCorrente];
    [manager.requestSerializer setValue:acceptLanguage forHTTPHeaderField:@"Accept-Language"];
    
    // Aggiungo gli Header globali
    NSArray *allKeys = [richiesta.headers allKeys];
    NSArray *allValues = [richiesta.headers allValues];
    for (NSInteger counter = 0; counter < [allKeys count]; counter++) {
        [manager.requestSerializer setValue:allValues[counter] forHTTPHeaderField:allKeys[counter]];
    }
    
    NSMutableURLRequest *request = [manager.requestSerializer requestWithMethod:metodo URLString:URLString parameters:parametri error:nil];
    
    richiesta.requestTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        // Richiesta terminata
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSString *httpStatusCode = [NSString stringWithFormat:@"%d", (int)httpResponse.statusCode];
        
        // Parse della risposta
        if ([contentType isEqualToString:@"HTML"] && [responseObject isKindOfClass:[NSData class]]) {
            NSData *data = responseObject;
            responseObject = [NSString stringWithUTF8String:[data bytes]];
        }
        
        if (error == nil) {
            // La richiesta è andata a buon fine
            if (richiesta.debug) {
                NSString *stringaHeaders = ([NSDictionary isEmpty:richiesta.headers]) ? @"" : [NSString stringaConArray:@[@"\nHeaders: ", richiesta.headers]];
                NSString *stringaParametri = ([NSDictionary isEmpty:parametri]) ? @"" : [NSString stringaConArray:@[@"\nParameters: ", parametri]];
                DBLog(@"\n", metodo, @" URL: ", URLString, stringaHeaders, stringaParametri, @"\nSuccess (", httpStatusCode, @")\nResponse: ", responseObject);
            }
        }
        else {
            // Si è verificato un errore durante la richiesta
            if (richiesta.debug) {
                NSString *stringaHeaders = ([NSDictionary isEmpty:richiesta.headers]) ? @"" : [NSString stringaConArray:@[@"\nHeaders: ", richiesta.headers]];
                // AFNetworking 2.0:
                // NSString *stringaResponse = (responseObject) ? [NSString stringaConArray:@[@"Response: ", responseObject]] : [NSString stringaConData:responseData];
                
                // AFNetworking 3.0:
                NSString *stringaResponse = (responseObject) ? [NSString stringaConArray:@[@"Response: ", responseObject]] : @"";
                
                NSString *stringaParametri = ([NSDictionary isEmpty:parametri]) ? @"" : [NSString stringaConArray:@[@"\nParameters: ", parametri]];
                DBLog(@"\n", metodo, @" URL: ", URLString, stringaHeaders, stringaParametri, @"\nFailure (", httpStatusCode, @"): ", [error localizedDescription], @")\n", stringaResponse);
            }
            
            // Errore customizzato
            error = [NSError errorWithDomain:error.domain code:httpResponse.statusCode userInfo:error.userInfo];
        }
        
        richiesta.error = error;
        richiesta.responseObject = responseObject;
        [self terminaRichiesteSimili:richiesta];
    }];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [richiesta.requestTask resume];
}

+ (void)GETImmagineConURLString:(NSString *)URLString conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    [self GETImmagineConURLString:URLString sovrascrivi:NO conCompletionBlock:completionBlock];
}

+ (void)GETImmagineConURLString:(NSString *)URLString sovrascrivi:(BOOL)sovrascrivi conCompletionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    // Directory di default per il download delle immagini
    NSString *nomeImmagine = [URLString lastPathComponent];
    NSString *nomeCartellaImmagini = @"Images";
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *URLDocumenti = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *URLImmagini = [URLDocumenti URLByAppendingPathComponent:nomeCartellaImmagini isDirectory:YES];
    NSURL *URLImmagine = [URLImmagini URLByAppendingPathComponent:nomeImmagine];
    
    [self GETFileConURLString:URLString conDownloadPath:[URLImmagine path] sovrascrivi:sovrascrivi conSuccessBlock:^(NSError *error) {
        if (error) {
            completionBlock(error, nil);
        }
        else {
            UIImage *immagine = [UIImage imageWithContentsOfFile:[URLImmagine path]];
            completionBlock(nil, immagine);
        }
    }];
}

+ (void)GETFileConURLString:(NSString *)URLString conDownloadPath:(NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi conSuccessBlock:(DBNetworkingSuccessBlock)successBlock
{
    [self GETFileConURLString:URLString conDownloadPath:downloadPath sovrascrivi:sovrascrivi conProgressHUD:NO messaggioProgressHUD:nil conSuccessBlock:successBlock];
}

+ (void)GETFileConURLString:(NSString *)URLString conDownloadPath:(NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi conProgressHUD:(BOOL)progressHUDAbilitato messaggioProgressHUD:(NSString *)messaggioProgressHUD conSuccessBlock:(DBNetworkingSuccessBlock)successBlock
{
    if ([NSString isEmpty:URLString] || [NSString isEmpty:downloadPath]) {
        if (successBlock) successBlock([NSError new]);
        return;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Controllo se il file è già stato scaricato
    if (sovrascrivi == NO) {
        BOOL giaScaricato = [fileManager fileExistsAtPath:downloadPath];
        
        if (giaScaricato) {
            // Bisogna ritornare il file come responseObject!
            NSLog(@"Il file '%@' è già stato scaricato", [URLString lastPathComponent]);
            if (successBlock) successBlock(nil);
            return;
        }
    }
    
    // Controllo se la cartella di destinazione esiste
    NSString *cartellaDestinazione = [downloadPath stringByDeletingLastPathComponent];
    BOOL cartellaEsistente = NO;
    [fileManager fileExistsAtPath:cartellaDestinazione isDirectory:&cartellaEsistente];
    
    if (!cartellaEsistente) {
        NSError *error = nil;
        BOOL success = [fileManager createDirectoryAtPath:cartellaDestinazione withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error || !success) {
            DBLog(@"Non sono riuscito a creare la cartella per il download del file '", [downloadPath lastPathComponent], @"'.\n", [error localizedDescription]);
        }
        else {
            DBLog(@"Cartella di destinazione per il download del file '", [URLString lastPathComponent], @"' creata correttamente!");
        }
        
        // Escludo dal backup iCloud la cartella creata
        [fileManager escludiPathDaBackupiCloud:cartellaDestinazione];
    }
    
    // Scarico il file
    MBProgressHUD *progressHUD = nil;
    if (progressHUDAbilitato) progressHUD = [DBProgressHUD progressHUDConMessaggio:messaggioProgressHUD];
    if (progressHUDAbilitato) progressHUD.mode = MBProgressHUDModeDeterminateHorizontalBar;
    
    NSURL *URL = [NSURL URLWithString:URLString];
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    request.URL = URL;
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    request.timeoutInterval = 10.0;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:downloadPath append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"File '%@' scaricato correttamente nella cartella: '%@'", [URL lastPathComponent], downloadPath);
        [fileManager escludiURLDaBackupiCloud:[NSURL fileURLWithPath:downloadPath]];
        
        if (progressHUDAbilitato) [DBProgressHUD rimuoviProgressHUD:progressHUD];
        if (successBlock) successBlock(nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Si è verificato un errore durante il download del file '%@': %@", [URL lastPathComponent], [error localizedDescription]);
        [fileManager removeItemAtPath:downloadPath error:nil];
        
        if (progressHUDAbilitato) [DBProgressHUD rimuoviProgressHUD:progressHUD];
        if (successBlock) successBlock(error);
    }];
    
    if (progressHUDAbilitato) {
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            float progress = (parseFloat(totalBytesRead) / parseFloat(totalBytesExpectedToRead));
            progressHUD.progress = progress;
            
            NSLog(@"Download del file '%@': %d%%", [URL lastPathComponent], parseInt(progress * 100));
        }];
    }
    
    NSLog(@"Inizio il download del file '%@'", [URL lastPathComponent]);
    [[NSOperationQueue currentQueue] addOperation:operation];
}

@end


@implementation UIImageView (DBNetworking)

- (void)caricaImmagineConURLString:(NSString *)URLString conActivityIndicator:(UIActivityIndicatorView *)activityIndicator
{
    self.image = nil;
    
    // Check parametri non validi
    if (!URLString || [URLString isEmpty])
        return;
    
    // Avvio lo spinner di caricamento
    [activityIndicator startAnimating];
    
    // Scarico l'immagine o riutilizzo quella in cache
    NSURL *URL = [NSURL URLWithString:URLString];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:URL];
    __block UIImageView *blockImageView = self;
    
    [self setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        [blockImageView setImage:image];
        [activityIndicator stopAnimating];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        [activityIndicator stopAnimating];
    }];
}

- (void)caricaImmagineConURLString:(NSString *)URLString conActivityIndicator:(UIActivityIndicatorView *)activityIndicator conCompletionBlock:(void (^)(BOOL finished))completionBlock
{
    self.image = nil;
    
    // Check parametri non validi
    if (!URLString || [URLString isEmpty]) {
        if (completionBlock) completionBlock(NO);
        return;
    }
    
    // Avvio lo spinner di caricamento
    [activityIndicator startAnimating];
    
    // Scarico l'immagine o riutilizzo quella in cache
    NSURL *URL = [NSURL URLWithString:URLString];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:URL];
    __block UIImageView *blockImageView = self;
    
    [self setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        [blockImageView setImage:image];
        [activityIndicator stopAnimating];
        if (completionBlock) completionBlock(YES);
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        [activityIndicator stopAnimating];
        if (completionBlock) completionBlock(YES);
    }];
}

@end

/*
 // Codice per scaricare un file e utilizzare un MBProgressHUD
 
 - (void)scaricaFile:(NSURL *)URL conFilePath:(NSString *)filePath conCompletionBlock:(void (^)(BOOL finished))completionBlock;
 {
 UIView *view = [[[[AppDelegate sharedInstance] window] rootViewController] view];
 
 self.progressHUD = [MBProgressHUD new];
 self.progressHUD.mode = MBProgressHUDModeDeterminateHorizontalBar;
 self.progressHUD.frame = view.bounds;
 self.progressHUD.labelText = [Utility stringaLocalizzata:@"title_download"];
 [view addSubview:self.progressHUD];
 [self.progressHUD show:YES];
 
 NSURLRequest *request = [NSURLRequest requestWithURL:URL];
 AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
 operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
 
 [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
 NSLog(@"Successfully downloaded file to %@", filePath);
 [self.progressHUD hide:YES];
 if (completionBlock) completionBlock(YES);
 
 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
 NSLog(@"Error: %@", error);
 [self.progressHUD hide:YES];
 if (completionBlock) completionBlock(NO);
 }];
 
 [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
 float percentDone = ((float)((int)totalBytesRead) / (float)((int)totalBytesExpectedToRead));
 self.progressHUD.progress = percentDone;
 
 NSLog(@"Sent %d of %d bytes, %@", (int)totalBytesRead, (int)totalBytesExpectedToRead, filePath);
 }];
 
 [[NSOperationQueue currentQueue] addOperation:operation];
 }
 
 */
