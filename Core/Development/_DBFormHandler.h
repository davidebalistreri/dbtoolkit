//
//  DBFormHandler.h
//  Versione file: 0.1.0
//  Ultimo aggiornamento: 28/03/2016
//
//  Creato da Davide Balistreri il 28/03/2016
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import "DBFoundation.h"
#import "DBObject.h"

/**
 * Classe in sviluppo.
 */

//@class DBFormHandlerItem;
//@protocol DBFormHandlerDelegate;


/**
 * Questa classe permette di creare un Form con campi dinamici e personalizzati.
 */
@interface DBFormHandler : DBObject

#pragma mark - Metodi di classe

+ (instancetype)formConTableView:(UITableView *)tableView;
//+ (instancetype)formConTableView:(UITableView *)tableView conDelegate:(id<DBFormHandlerDelegate>)delegate;

#pragma mark - Metodi d'istanza

//@property (weak, nonatomic) id<DBFormHandlerDelegate> delegate;
@property (strong, nonatomic) UITableView *tableView;

- (void)distruggiHandler;

#pragma mark Gestione campi del Form

- (void)aggiungiVista:(UIView *)vista;

- (void)rimuoviVista:(UIView *)vista;

@end


//@interface DBFormHandlerItem : DBObject
//
//@property (strong, nonatomic) id vista;
//
//#pragma mark Gestione
//
//- (void)rimuoviDalForm;
//
//@end


//@protocol DBFormHandlerDelegate <NSObject>
//
//
//
//@end
