//
//  DBImageView.m
//  File version: 1.0.0
//  Last modified: 11/20/2016
//
//  Created by Davide Balistreri on 11/20/2016
//  Copyright 2013-2016 © Davide Balistreri. All rights reserved.
//

#import "DBImageView.h"
#import "DBAnimation.h"

@interface DBImageView ()

@property (strong, nonatomic) UIImage *pendingSetImageWithAnimation;

@end


@implementation DBImageView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.pendingSetImageWithAnimation && self.superview) {
        UIImage *image = self.pendingSetImageWithAnimation;
        self.pendingSetImageWithAnimation = nil;
        
        [self setImage:image withAnimation:[DBAnimation new]];
    }
}

- (void)setImage:(UIImage *)image
{
    [self setImage:image withAnimation:nil];
}

- (void)setImage:(UIImage *)image withAnimation:(DBAnimation *)animation
{
    if (animation == NO) {
        // No animation
        [super setImage:image];
        return;
    }
    
    if (!self.superview) {
        // Will set the image on next layout
        self.pendingSetImageWithAnimation = image;
        return;
    }
    
    // DBAnimation ancora non supporta le transition
    [super setImage:nil];
    
    [UIView transitionWithView:self duration:4.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [super setImage:image];
    } completion:nil];
}

@end
