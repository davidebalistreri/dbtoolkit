//
//  DBLogging.h
//  File version: 1.1.0
//  Last modified: 07/13/2017
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import <Foundation/Foundation.h>


// MARK: - Console logs

/**
 * This function print all arguments in the console log, without the need of formatting as a string before printing.
 */
#define DBLog(...) __DBLog(__VA_ARGS__, nil)
void __DBLog(id arguments, ...) NS_REQUIRES_NIL_TERMINATION;


/**
 * A more detailed version of DBLog: it also prints the function and the line that is logging.
 */
#define DBExtendedLog(...) __DBExtendedLog(__PRETTY_FUNCTION__, __LINE__, __VA_ARGS__, nil)
void __DBExtendedLog(const char *function, int line, id arguments, ...) NS_REQUIRES_NIL_TERMINATION;


/**
 * A cleaner log: it just prints all specified arguments, without the default timestamp.
 */
#define DBCleanLog(...) __DBCleanLog(__VA_ARGS__, nil)
void __DBCleanLog(id arguments, ...) NS_REQUIRES_NIL_TERMINATION;




// MARK: - Measurements

/**
 * Prints in the console log the exact execution time needed to perform the specified block.
 */
void DBLogMeasureBlock(void (^block)(void));


/**
 * Prints in the console log the exact execution time needed to perform the specified selector on the specified target.
 */
void DBLogMeasureSelector(id target, SEL selector);


/**
 * Call this to measure the execution time of everything you want.
 * Just remember to stop it somewhere to know how much time it took from A to B.
 */
void DBLogMeasurementStart(NSString *identifier);


/**
 * Stops the last started measurement and prints the execution time in the console log.
 */
void DBLogMeasurementStop(NSString *identifier);

