//
//  DBVersioning.h
//  File version: 1.0.0
//  Last modified: 09/10/2016
//
//  Created by Davide Balistreri on 03/26/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * Con questa classe è possibile tenere sotto controllo i cambiamenti del Framework e adattare il codice quando un modulo viene aggiornato.
 *
 * Per utilizzare questo sistema, dichiarare una costante con il numero di versione attualmente in uso, prima di importare il Framework:
 * #define DBFRAMEWORK_CURRENT_VERSION __DBFRAMEWORK_0_1_0
 *
 * Quando uscirà un aggiornamento del Framework, alcuni moduli potrebbero interrompere il supporto ad alcune funzioni: in questo modo sarà possibile ricevere un messaggio che indica quali moduli sono da aggiornare.
 */

/// Controlla se la versione specificata è minore di quella in uso (se dichiarata)
#define DBFRAMEWORK_VERSION_EARLIER_THAN(versione) \
    (defined(DBFRAMEWORK_CURRENT_VERSION) && DBFRAMEWORK_CURRENT_VERSION < versione)

/// Controlla se la versione specificata è maggiore di quella in uso (se dichiarata)
#define DBFRAMEWORK_VERSION_LATER_THAN(versione) \
    (defined(DBFRAMEWORK_CURRENT_VERSION) && DBFRAMEWORK_CURRENT_VERSION >= versione)


/**
 * Questa parte permette di tenere sotto controllo l'utilizzo di metodi non disponibili sulle versioni dei sistemi operativi supportati.
 */

#import <Availability.h>

#if __IPHONE_OS_VERSION_MIN_REQUIRED > __IPHONE_7_0
    #define __IPHONE_OS_VERSION_SOFT_MAX_REQUIRED __IPHONE_OS_VERSION_MIN_REQUIRED
#else
    #define __IPHONE_OS_VERSION_SOFT_MAX_REQUIRED __IPHONE_7_0
#endif

#if __MAC_OS_X_VERSION_MIN_REQUIRED > __MAC_10_9
    #define __MAC_OS_X_VERSION_SOFT_MAX_REQUIRED __MAC_OS_X_VERSION_MIN_REQUIRED
#else
    #define __MAC_OS_X_VERSION_SOFT_MAX_REQUIRED __MAC_10_9
#endif

#import "MJGAvailability.h"

/**
 * Permette di segnare i metodi come deprecati, e di avvertire lo sviluppatore in caso di utilizzo.
 */
#define DBFRAMEWORK_DEPRECATED(versione, messaggio) \
    __attribute__((deprecated(" Deprecato dalla versione " #versione ", " messaggio ".")))

/**
 * Permette di segnare i metodi come proibiti, e di bloccare la compilazione in caso di utilizzo.
 */
#define DBFRAMEWORK_FORBIDDEN(versione, messaggio) \
    __attribute__((unavailable(" Proibito dalla versione " #versione ", " messaggio ".")))

/**
 * Permette di stampare un log sulla console quando i metodi deprecati vengono richiamati,
 * intercettando automaticamente il nome del metodo sbagliato.
 */
#define DBFRAMEWORK_DEPRECATED_LOG(versione, messaggio) \
    NSLog(@"Stai utilizzando il metodo %s deprecato dalla versione %s: %s.\n\n", __PRETTY_FUNCTION__, #versione, messaggio)

/**
 * Permette di stampare un log sulla console quando i metodi deprecati vengono richiamati.
 */
#define DBFRAMEWORK_DEPRECATED_CUSTOM_LOG(metodo_deprecato, versione, messaggio) \
    NSLog(@"Stai utilizzando il metodo %s deprecato dalla versione %s: %s.\n\n", metodo_deprecato, #versione, messaggio)

/**
 * Permette di bypassare i warning per le parti di codice deprecate.
 */
#define DBFRAMEWORK_SUPPRESS_DEPRECATED_WARNING(code)                       \
    do {                                                                    \
        _Pragma("clang diagnostic push")                                    \
        _Pragma("clang diagnostic ignored \"-Wdeprecated-declarations\"")   \
        code;                                                               \
        _Pragma("clang diagnostic pop")                                     \
    } while(0)


/**
 * Costanti per le versioni del Framework rilasciate.
 *
 * Semantic versioning: MAJOR . MINOR . PATCH
 * MAJOR version when you make incompatible API changes,
 * MINOR version when you add functionality in a backwards-compatible manner, and
 * PATCH version when you make backwards-compatible bug fixes.
 */

#define __DBFRAMEWORK_0_9_0   900

#define __DBFRAMEWORK_0_8_0   800

#define __DBFRAMEWORK_0_7_3   703
#define __DBFRAMEWORK_0_7_2   702
#define __DBFRAMEWORK_0_7_1   701
#define __DBFRAMEWORK_0_7_0   700

#define __DBFRAMEWORK_0_6_5   605
#define __DBFRAMEWORK_0_6_4   604
#define __DBFRAMEWORK_0_6_3   603
#define __DBFRAMEWORK_0_6_2   602
#define __DBFRAMEWORK_0_6_1   601
#define __DBFRAMEWORK_0_6_0   600

#define __DBFRAMEWORK_0_6_0   600

#define __DBFRAMEWORK_0_5_1   501
#define __DBFRAMEWORK_0_5_0   500

#define __DBFRAMEWORK_0_4_1   401
#define __DBFRAMEWORK_0_4_0   400

#define __DBFRAMEWORK_0_3_2   302
#define __DBFRAMEWORK_0_3_1   301
#define __DBFRAMEWORK_0_3_0   300

#define __DBFRAMEWORK_0_2_0   200

#define __DBFRAMEWORK_0_1_3   103
#define __DBFRAMEWORK_0_1_2   102
#define __DBFRAMEWORK_0_1_1   101
#define __DBFRAMEWORK_0_1_0   100
