//
//  DBToolkit.h
//  File version: 1.0.0
//  Last modified: 07/26/2016
//
//  Created by Davide Balistreri on 03/07/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

// Retrocompatibility
#import "DBFramework.h"

#if DBFRAMEWORK_VERSION_EARLIER_THAN(__DBFRAMEWORK_0_4_0)
    #warning Bisogna adattare il codice DBToolkit alla nuova versione del Framework.
#endif


// MARK: - NSFileManager

@interface NSFileManager (DBToolkit)

/**
 * Restituisce l'URL della cartella dove salvare Documenti per l'app in uso.
 * <p>Su iOS punta alla cartella interna al pacchetto dell'App.</p>
 * <p>Su OSX punta ad una sottocartella con il nome dell'App, nella cartella Documenti dell'utente loggato.</p>
 */
+ (NSURL *)URLCartellaDocumenti;

/**
 * Restituisce l'URL della cartella dove salvare Documenti per l'app in uso.
 * <p>Su iOS punta alla cartella interna al pacchetto dell'App.</p>
 * <p>Su OSX punta ad una sottocartella con il nome dell'App, nella cartella Documenti dell'utente loggato.</p>
 */
- (NSURL *)URLCartellaDocumenti;

/// Analogo del metodo -[NSFileManager fileExistsAtPath:].
- (BOOL)fileExistsAtURL:(NSURL *)URL;

/// Analogo del metodo -[NSFileManager fileExistsAtPath:isDirectory:].
- (BOOL)fileExistsAtURL:(NSURL *)URL isDirectory:(BOOL *)isDirectory;

/// Esclude un file o una cartella dal backup automatico effettuato da iCloud
+ (BOOL)escludiURLDaBackupiCloud:(NSURL *)URL;

/// Esclude un file o una cartella dal backup automatico effettuato da iCloud
+ (BOOL)escludiPathDaBackupiCloud:(NSString *)path;

/// Esclude un file o una cartella dal backup automatico effettuato da iCloud
+ (BOOL)setSkipBackupAttributeToItemAtURL:(NSURL *)URL;

/// Esclude un file o una cartella dal backup automatico effettuato da iCloud
- (BOOL)escludiURLDaBackupiCloud:(NSURL *)URL;

/// Esclude un file o una cartella dal backup automatico effettuato da iCloud
- (BOOL)escludiPathDaBackupiCloud:(NSString *)path;

/// Esclude un file o una cartella dal backup automatico effettuato da iCloud
- (BOOL)setSkipBackupAttributeToItemAtURL:(NSURL *)URL;

@end


// MARK: - NSURL

@interface NSURL (DBToolkit)

+ (instancetype)safeFileURLWithPath:(NSString *)path;
+ (instancetype)safeFileURLWithPath:(NSString *)path isDirectory:(BOOL)isDirectory;

+ (instancetype)httpURLWithString:(NSString *)URLString;

@end


#if DBFRAMEWORK_TARGET_IOS

// MARK: - UINavigationController

@interface UINavigationController (DBToolkit)

- (void)nuovoStackConViewController:(UIViewController *)viewController animazione:(BOOL)animazione;

@end


// MARK: - UIBarButtonItem

@interface UIBarButtonItem (DBToolkit)

+ (UIBarButtonItem *)barButtonItemVuoto;

/// NB: Non è possibile impostare un BarButtonItem separatore come singolo elemento della NavigationBar
+ (UIBarButtonItem *)barButtonItemSeparatoreConLarghezza:(CGFloat)larghezza;

+ (UIBarButtonItem *)barButtonItemConImmagine:(UIImage *)immagine target:(id)target action:(SEL)action;
+ (UIBarButtonItem *)barButtonItemConStringa:(NSString *)stringa target:(id)target action:(SEL)action;
+ (UIBarButtonItem *)barButtonItemConStringaLocalizzata:(NSString *)stringa target:(id)target action:(SEL)action;

@end

#endif
