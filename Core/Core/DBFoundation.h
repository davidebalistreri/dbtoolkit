//
//  DBFoundation.h
//  File version: 1.1.0
//  Last modified: 04/18/2016
//
//  Created by Davide Balistreri on 12/15/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import <TargetConditionals.h>

/// Use this macro to compile the code only if the building target is iOS.
#define DBFRAMEWORK_TARGET_IOS (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)

/// Use this macro to compile the code only if the building target is OSX.
#define DBFRAMEWORK_TARGET_OSX !DBFRAMEWORK_TARGET_IOS


#if DBFRAMEWORK_TARGET_IOS
    #import <UIKit/UIKit.h>
#else
    #import <Cocoa/Cocoa.h>
#endif


#import "DBVersioning.h"
#import "DBLogging.h"
#import "DBMath.h"
#import "DBCentralManager.h"
