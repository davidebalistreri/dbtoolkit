//
//  DBCentralManager.h
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 09/17/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@interface DBCentralManager : NSObject

/**
 * Restituisce la risorsa condivisa salvata con l'identifier specificato.
 * @param identifier L'identifier univoco della risorsa.
 */
+ (id)getRisorsa:(NSString *)identifier;

/**
 * Salva la risorsa condivisa con l'identifier specificato.
 * @param risorsa    La risorsa da salvare.
 * @param identifier L'identifier univoco della risorsa.
 */
+ (void)setRisorsa:(id)risorsa conIdentifier:(NSString *)identifier;

@end
