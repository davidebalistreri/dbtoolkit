//
//  DBChangelog.h
//  File version: 1.0.0
//  Last modified: 07/26/2016
//
//  Created by Davide Balistreri on 06/23/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//


/**
 * 07-26-2016
 * VERSION 0.6.5
 *
 * - Project files recreated with new CocoaPods 1.0 specifications
 * - Added Example project
 * - Translated basic info in english
 */


/**
 * 07-25-2016
 * VERSION 0.6.4
 *
 * - Riorganizzata la struttura del progetto
 */


/**
 * 03-29-2016
 * VERSION 0.6.3
 *
 * - Aggiunta classe DBVersioning per tenere sotto controllo i cambiamenti del Framework nelle varie versioni
 * - Riorganizzata la struttura del progetto
 * - Spostati i metodi DBToolkit nelle nuove classi DBApplication, DBUtility, DBDeprecated, DBColorExtensions, DBImageExtensions
 * - Aggiunte nuove classi DBFoundation, DBMath, DBLog
 * - Aggiunto framework esterno MJGAvailability che permette di segnalare l'uso di metodi non disponibili su tutte le versioni del sistema operativo supportate
 */


/**
 * 03-26-2016
 * VERSION 0.6.2
 *
 * - DBNavigationController: aggiunto supporto ai completionBlock, ora è possibile pushare altri NavigationController (beta)
 * - DBTableViewController: aggiunto RefreshControl automatico
 * - DBTableViewHandler: aggiornamenti generali, la classe è ancora in beta
 * - DBParallaxView: ora è possibile applicare l'effetto parallasse anche a una View custom
 * - DBRuntimeHandler: aggiunti nuovi metodi
 * - UITextView: aggiunto supporto ai placeholder
 * - UIImage: nuovi metodi per manipolare le immagini
 * - Aggiunti alcuni metodi utili
 * - Miglioramenti e correzioni generali
 */


/**
 * 03-13-2016
 * VERSION 0.6.1
 *
 * - DBSlideMenu riscritto e migliorato, adesso non utilizza più le UIWindow
 * - DBRuntimeHandler permette di modificare il comportamento dell'applicazione a runtime
 * - DBButton aggiornato senza retrocompatibilità
 * - Aggiunti alcuni metodi utili, miglioramenti generali
 */


/**
 * 03-12-2016
 * VERSION 0.6.0.1
 *
 * - Alcuni miglioramenti
 */


/**
 * 03-11-2016
 * VERSION 0.6.0
 *
 * - Questa versione interrompe il supporto di alcune funzionalità DBViewController e di alcune vecchie classi
 * - Classi DBTabBarController, DBNavigationController, DBTableViewController riscritte da zero
 * - Ora il funzionamento di DBTableViewCell rispetta la struttura di del DBViewController
 * - DBTableViewHandler permette di semplificare la gestione di una TableView e del suo DataSource
 * - DBButton permette di modificare la grafica dei pulsanti, in futuro sarà esteso e potenziato
 * - DBAnimator permette di creare facilmente animazioni personalizzate tra due View
 * - DBGeometry adesso può gestire le Constraint dell'AutoLayout
 * - Aggiunti alcuni metodi e risolti alcuni problemi
 */


/**
 * 02-22-2016
 * VERSION 0.5.1.2
 *
 * - Aggiunta classe DBSlideshowView
 */


/**
 * 02-21-2016
 * VERSION 0.5.1
 *
 * - Aggiunta classe per controllare l'AutoLayout delle ImageView
 * - Migliorata la compatibilità tra iOS e OSX e ripulito il codice duplicato di alcune classi
 * - Aggiunti alcuni metodi e deprecati altri
 */


/**
 * 02-14-2016
 * VERSION 0.5.0
 *
 * - Numerosi aggiornamenti di supporto e compatibilità con OSX
 */
