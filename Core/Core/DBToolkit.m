//
//  DBToolkit.m
//  File version: 1.0.0
//  Last modified: 07/26/2016
//
//  Created by Davide Balistreri on 03/07/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBToolkit.h"

// MARK: - NSFileManager
#include <sys/xattr.h>

@implementation NSFileManager (DBToolkit)

+ (NSURL *)URLCartellaDocumenti
{
    return [[NSFileManager defaultManager] URLCartellaDocumenti];
}

- (NSURL *)URLCartellaDocumenti
{
    NSURL *URL = [[self URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
#if DBFRAMEWORK_TARGET_OSX
    URL = [URL URLByAppendingPathComponent:[DBApplication nomeApplicazione] isDirectory:YES];
    
    // Se la sottocartella con il nome dell'App non esiste, la creo
    if ([self fileExistsAtPath:URL.path isDirectory:nil] == NO) {
        [self createDirectoryAtURL:URL withIntermediateDirectories:YES attributes:nil error:nil];
    }
#endif
    return URL;
}

- (BOOL)fileExistsAtURL:(NSURL *)URL
{
    return [self fileExistsAtPath:URL.path];
}

- (BOOL)fileExistsAtURL:(NSURL *)URL isDirectory:(BOOL *)isDirectory
{
    return [self fileExistsAtPath:URL.path isDirectory:isDirectory];
}

+ (BOOL)escludiURLDaBackupiCloud:(NSURL *)URL
{
    return [[NSFileManager defaultManager] escludiURLDaBackupiCloud:URL];
}

+ (BOOL)escludiPathDaBackupiCloud:(NSString *)path
{
    return [[NSFileManager defaultManager] escludiURLDaBackupiCloud:[NSURL fileURLWithPath:path]];
}

+ (BOOL)setSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    return [[NSFileManager defaultManager] escludiURLDaBackupiCloud:URL];
}

- (BOOL)escludiURLDaBackupiCloud:(NSURL *)URL
{
    if (!URL || ![self fileExistsAtPath:[URL path]]) {
        // Il file specificato non esiste
        return NO;
    }
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
    
    if (error || !success) {
        NSLog(@"Si è verificato un errore durante l'esclusione del file %@ dal backup iCloud: %@", [URL lastPathComponent], error);
    }
    
    return success;
}

- (BOOL)escludiPathDaBackupiCloud:(NSString *)path
{
    return [self escludiURLDaBackupiCloud:[NSURL fileURLWithPath:path]];
}

- (BOOL)setSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    return [self escludiURLDaBackupiCloud:URL];
}

@end


@implementation NSURL (DBToolkit)

+ (instancetype)safeFileURLWithPath:(NSString *)path
{
    if ([NSString isNotEmpty:path]) {
        return [self fileURLWithPath:path];
    }
    
    return nil;
}

+ (instancetype)safeFileURLWithPath:(NSString *)path isDirectory:(BOOL)isDirectory
{
    if ([NSString isNotEmpty:path]) {
        return [self fileURLWithPath:path isDirectory:isDirectory];
    }
    
    return nil;
}

+ (instancetype)httpURLWithString:(NSString *)URLString
{
    NSURL *URL = [NSURL URLWithString:URLString];
    
    if (!URL.scheme) {
        NSString *httpURLString = [NSString stringWithFormat:@"http://%@", URLString];
        URL = [NSURL URLWithString:httpURLString];
    }
    
    return URL;
}

@end


#if DBFRAMEWORK_TARGET_IOS

// MARK: - UINavigationController

@implementation UINavigationController (DBToolkit)

- (void)nuovoStackConViewController:(UIViewController *)viewController animazione:(BOOL)animazione
{
    // Check
    if (!viewController)
        return;
    
    NSMutableArray *stack = [self.viewControllers mutableCopy];
    [stack insertObject:viewController atIndex:0];
    [self setViewControllers:stack];
    [self popToRootViewControllerAnimated:animazione];
}

@end


// MARK: - UIBarButtonItem

@implementation UIBarButtonItem (DBToolkit)

+ (UIBarButtonItem *)barButtonItemVuoto
{
    return [self barButtonItemConStringa:@"" target:nil action:nil];
}

+ (UIBarButtonItem *)barButtonItemSeparatoreConLarghezza:(CGFloat)larghezza
{
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    barButtonItem.width = larghezza;
    return barButtonItem;
}

+ (UIBarButtonItem *)barButtonItemConImmagine:(UIImage *)immagine target:(id)target action:(SEL)action
{
    CGFloat dimensioniNavigationBar = [UINavigationController new].navigationBar.frame.size.height;
    CGFloat dimensioniMassime = dimensioniNavigationBar / 2.0;
    CGRect frame = CGRectMake(0, 0, fmin(immagine.size.width, dimensioniMassime), fmin(immagine.size.height, dimensioniMassime));
    
    UIImage *immagineRidimensionata = [immagine immagineRidimensionataConRisoluzione:fmax(frame.size.width, frame.size.height)];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithImage:immagineRidimensionata style:UIBarButtonItemStylePlain target:target action:action];
    return barButtonItem;
}

+ (UIBarButtonItem *)barButtonItemConStringa:(NSString *)stringa target:(id)target action:(SEL)action
{
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:stringa style:UIBarButtonItemStylePlain target:target action:action];
    return barButtonItem;
}

+ (UIBarButtonItem *)barButtonItemConStringaLocalizzata:(NSString *)stringa target:(id)target action:(SEL)action
{
    return [self barButtonItemConStringa:DBLocalizedString(stringa) target:target action:action];
}

@end

#endif
