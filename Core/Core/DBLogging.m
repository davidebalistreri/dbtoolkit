//
//  DBLogging.m
//  File version: 1.1.0
//  Last modified: 07/13/2017
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBLogging.h"

#import "DBStringExtensions.h"
#import "DBObjectExtensions.h"
#import "DBThread.h"


@interface DBMeasurementLog : NSObject

@property (nonatomic) NSTimeInterval start;
@property (strong, nonatomic) NSString *identifier;

@end


@implementation DBMeasurementLog

+ (NSMutableArray *)sharedInstances
{
    @synchronized(self) {
        NSMutableArray *sharedInstances = [DBCentralManager getRisorsa:@"DBSharedMeasurementLog"];
        
        if (!sharedInstances) {
            sharedInstances = [NSMutableArray array];
            [DBCentralManager setRisorsa:sharedInstances conIdentifier:@"DBSharedMeasurementLog"];
        }
        
        return sharedInstances;
    }
}

@end


/**
 * This function print all arguments in the console log, without the need of formatting as a string before printing.
 */
void __DBLog(id arguments, ...)
{
    va_list list;
    va_start(list, arguments);
    
    NSString *string = [NSString stringWithVariableArgumentsList:list firstArgument:arguments];
    
    va_end(list);
    
    // Output
    NSLog(@"%@", string);
}


/**
 * A more detailed version of DBLog: it also prints the function and the line that is logging.
 */
void __DBExtendedLog(const char *function, int line, id arguments, ...)
{
    va_list list;
    va_start(list, arguments);
    
    NSString *string = [NSString stringWithVariableArgumentsList:list firstArgument:arguments];
    
    va_end(list);
    
    // Output
    NSLog(@"\n%s [Line %d]\n%@\n\n", function, line, string);
}


/**
 * A cleaner log: it just prints all specified arguments, without the default timestamp.
 */
void __DBCleanLog(id arguments, ...)
{
    va_list list;
    va_start(list, arguments);
    
    NSString *string = [NSString stringWithVariableArgumentsList:list firstArgument:arguments];
    
    va_end(list);
    
    // Output
    fprintf(stdout, "%s\n", [string UTF8String]);
}


/**
 * Prints in the console log the exact execution time needed to perform the specified block.
 */
void DBLogMeasureBlock(void(^block)(void))
{
    NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
    
    if (block) {
        block();
    }
    
    NSTimeInterval end = [NSDate timeIntervalSinceReferenceDate];
    
    NSTimeInterval executionTime = end - start;
    
    NSLog(@"^ Block execution time: %.10f seconds", executionTime);
}

/**
 * Prints in the console log the exact execution time needed to perform the specified selector on the specified target.
 */
void DBLogMeasureSelector(id target, SEL selector)
{
    NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
    
    if (target && selector) {
        [target safePerformSelector:selector];
    }
    
    NSTimeInterval end = [NSDate timeIntervalSinceReferenceDate];
    
    NSTimeInterval executionTime = end - start;
    
    NSLog(@"^ Selector execution time: %.10f seconds", executionTime);
}




/**
 * Call this to measure the execution time of everything you want.
 * Just remember to stop it somewhere to know how much time it took from A to B.
 */
void DBLogMeasurementStart(NSString *identifier)
{
    NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
    
    DBMeasurementLog *measurement = [DBMeasurementLog new];
    measurement.start = start;
    measurement.identifier = identifier;
    
    NSMutableArray *sharedInstances = [DBMeasurementLog sharedInstances];
    [sharedInstances addObject:measurement];
}


/**
 * Stops the last started measurement and prints the execution time in the console log.
 */
void DBLogMeasurementStop(NSString *identifier)
{
    NSTimeInterval end = [NSDate timeIntervalSinceReferenceDate];
    
    NSMutableArray *sharedInstances = [DBMeasurementLog sharedInstances];
    NSArray *reversedArray = [[sharedInstances reverseObjectEnumerator] allObjects];
    
    for (DBMeasurementLog *measurement in reversedArray) {
        if (measurement.identifier == identifier || [measurement.identifier isEqual:identifier]) {
            // Found
            NSTimeInterval executionTime = end - measurement.start;
            
            if ([NSString isNotEmpty:identifier]) {
                NSLog(@"^ Measurement '%@' execution time: %.10f seconds", identifier, executionTime);
            } else {
                NSLog(@"^ Measurement execution time: %.10f seconds", executionTime);
            }
            
            // Done
            [sharedInstances removeObject:measurement];
            return;
        }
    }
}
