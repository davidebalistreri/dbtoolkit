//
//  DBNavigationStoryController.h
//  File version: 1.1.0
//  Last modified: 03/28/2017
//
//  Created by Davide Balistreri on 05/05/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#import "DBViewController.h"
#import "DBNavigationStory.h"
#import "DBAnimation.h"


/**
 * NavigationStoryController its an object designed to manage multiple stacks of ViewControllers.
 * It can be considered as an evolution of the NavigationController.
 *
 * Each stack is defined as a <b>Story</b>.
 */

@interface DBNavigationStoryController : DBViewController

/// The NavigationStories currently on the story stack of the controller.
@property (strong, nonatomic, readonly) NSArray<__kindof DBNavigationStory *> *navigationStories;

/// The NavigationStory on top of the story stack of the controller.
@property (nonatomic, readonly) __kindof DBNavigationStory *presentedNavigationStory;


/// Block da eseguire al termine di un'operazione.
typedef void (^DBNavigationStoryControllerCompletionBlock)(void);

/**
 * Pushes a NavigationStory in the story stack and updates the display.
 *
 * If reuseExistingStory is active, and a Story with the same identifier already exists, it will be pushed instead.
 * If multiple existing Stories are found, the most recent will be used.
 */
- (DBNavigationStory *)pushNavigationStory:(DBNavigationStory *)navigationStory
                            withTransition:(DBAnimationTransition)transition
                        reuseExistingStory:(BOOL)reuseExistingStory
                           completionBlock:(DBNavigationStoryControllerCompletionBlock)completionBlock;

/**
 * Pops the top NavigationStory from the story stack and updates the display.
 */
- (void)popNavigationStoryWithTransition:(DBAnimationTransition)transition
                         completionBlock:(DBNavigationStoryControllerCompletionBlock)completionBlock;


/// Returns the NavigationStory with the specified identifier, if it exists in the story stack.
- (__kindof DBNavigationStory *)navigationStoryWithIdentifier:(NSString *)identifier;

/// Removes all NavigationStories from the stack, apart from the one currently displayed.
- (void)clearNavigationStories;

@end


@interface DBNavigationStoryController (Deprecated)

@property (readonly) UINavigationController *presentedNavigationController
    DBFRAMEWORK_DEPRECATED(0.7.2, "please manually get the NavigationController of the presented NavigationStory");

/**
 * Crea o riutilizza una NavigationStory.
 *
 * <p>Se una NavigationStory simile già esiste, essa verrà riutilizzata ripartendo dal suo primo ViewController.</p>
 * <p>Se invece non esiste, verrà creata e mostrata.</p>
 * <p>Le NavigationStory simili vengono discriminate in base alla classe del loro primo ViewController.</p>
 * <p>Per avere maggiore controllo, istanziare manualmente le NavigationStory e mostrarle attraverso il metodo
 * -[DBNavigationStoryController pushNavigationStory:withTransition:].</p>
 *
 * <br>Ogni ViewController delle NavigationStory apparterrà ad un NavigationController (quello specificato nel caso in cui la storia fosse stata
 * creata con un NavigationController, altrimenti un NavigationController creato automaticamente).
 * <p>Ogni ViewController avrà inoltre accesso al NavigationStoryController di appartenenza.</p>
 *
 * @param viewController Il primo ViewController della storia.
 *                       <p>Se viene passato un NavigationController, esso verrà automaticamente configurato
 *                       per far partire la storia dal suo primo ViewController.</p>
 * @param transition     L'effetto di transizione da utilizzare per il passaggio da una NavigationStory all'altra.
 * @param restoreSavedState Permette di riprendere una NavigationStory precedentemente istanziata.
 */
- (DBNavigationStory *)pushNavigationStoryWithViewController:(UIViewController *)viewController
                                              withTransition:(DBAnimationTransition)transition
                                           restoreSavedState:(BOOL)restoreSavedState
                                             completionBlock:(DBNavigationStoryControllerCompletionBlock)completionBlock
    DBFRAMEWORK_DEPRECATED(0.7.2, "please manually instantiate a NavigationStory and then push it through the NavigationStoryController");

- (void)pushNavigationStory:(DBNavigationStory *)navigationStory
             withTransition:(DBAnimationTransition)transition
            completionBlock:(DBNavigationStoryControllerCompletionBlock)completionBlock
    DBFRAMEWORK_DEPRECATED(0.7.2, "please use -[DBNavigationStoryController pushNavigationStory:withTransition:reuseExistingStory:completionBlock:");

@end
