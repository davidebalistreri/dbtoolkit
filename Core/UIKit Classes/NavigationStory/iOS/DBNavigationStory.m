//
//  DBNavigationStory.m
//  File version: 1.1.0
//  Last modified: 03/28/2017
//
//  Created by Davide Balistreri on 05/05/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBNavigationStory.h"
#import "DBNavigationController.h"
#import "DBObjectExtensions.h"

@implementation DBNavigationStory

// MARK: - Setup

+ (instancetype)navigationStoryWithViewController:(__kindof UIViewController *)viewController identifier:(NSString *)identifier
{
    // NavigationController
    UINavigationController *navigationController = nil;
    
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        navigationController = viewController;
    }
    else if ([viewController isKindOfClass:[UIViewController class]]) {
        // A NavigationStory always begins with a NavigationController
        navigationController = [DBNavigationController new];
        [navigationController pushViewController:viewController animated:NO];
        [navigationController setNavigationBarHidden:YES];
    }
    else {
        // Not valid
        return nil;
    }
    
    // Identifier
    if ([NSString isEmpty:identifier]) {
        // The story identifier wasn't specified
        
        // Using first view controller's class as identifier, as it's more unique than the NavigationController's class
        UIViewController *firstViewController = navigationController.viewControllers.firstObject;
        
        if (firstViewController) {
            identifier = NSStringFromClass([firstViewController class]);
        }
        else {
            // First view controller couldn't be found!
            
            // I will not use NavigationController's class as it may cause to unexpected behavior:
            // there may exist two navigation controllers with the same identifier but representing two different stories
            
            // This navigation story will not be reusable.
            // Please manually specify the identifier to make this navigation story reusable again.
        }
    }
    
    DBNavigationStory *navigationStory = [DBNavigationStory new];
    navigationStory.navigationController = navigationController;
    navigationStory.rootViewController = viewController;
    navigationStory.identifier = identifier;
    
    return navigationStory;
}

// MARK: - Public methods

@synthesize navigationController = _navigationController;
- (void)setNavigationController:(__kindof UINavigationController *)navigationController
{
    _navigationController = navigationController;
}

@synthesize rootViewController = _rootViewController;
- (void)setRootViewController:(__kindof UIViewController *)rootViewController
{
    _rootViewController = rootViewController;
}

@synthesize identifier = _identifier;
- (void)setIdentifier:(NSString *)identifier
{
    _identifier = identifier;
}

- (NSArray<UIViewController *> *)viewControllers
{
    return self.navigationController.viewControllers;
}

- (UIViewController *)presentedViewController
{
    return self.navigationController.viewControllers.lastObject;
}

@end


@implementation DBNavigationStory (Deprecated)

+ (DBNavigationStory *)navigationStoryWithNavigationController:(UINavigationController *)navigationController
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please use +[DBNavigationStory navigationStoryWithViewController:identifier:]");
    return [self navigationStoryWithViewController:navigationController identifier:nil];
}

@end
