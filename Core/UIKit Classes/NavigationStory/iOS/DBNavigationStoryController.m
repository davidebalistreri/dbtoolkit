//
//  DBNavigationStoryController.m
//  File version: 1.1.0
//  Last modified: 03/28/2017
//
//  Created by Davide Balistreri on 05/05/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBNavigationStoryController.h"

#import "DBNavigationController.h"
#import "DBViewExtensions.h"
#import "DBArrayExtensions.h"

@implementation DBNavigationStoryController

// MARK: - Inizializzazione

- (void)inizializzaSchermata
{
    self.view.backgroundColor = [UIColor clearColor];
    self.navigationStories = [NSMutableArray array];
}

// MARK: - Public methods

- (DBNavigationStory *)presentedNavigationStory
{
    return self.navigationStories.lastObject;
}

// MARK: - Private methods

@synthesize navigationStories = _navigationStories;
- (void)setNavigationStories:(NSArray<__kindof DBNavigationStory *> *)navigationStories
{
    _navigationStories = navigationStories;
}

- (void)removeNavigationStory:(DBNavigationStory *)navigationStory
{
    if (navigationStory) {
        // Remove from display
        UIViewController *viewController = navigationStory.navigationController;
        [viewController willMoveToParentViewController:nil];
        [viewController.view removeFromSuperview];
        [viewController removeFromParentViewController];
        
        // Remove from memory
        NSMutableArray *navigationStories = [self.navigationStories mutableCopy];
        [navigationStories removeObject:navigationStory];
        self.navigationStories = navigationStories;
    }
}

- (void)transitionFromNavigationStory:(DBNavigationStory *)from
                    toNavigationStory:(DBNavigationStory *)to
                       withTransition:(DBAnimationTransition)transition
                      completionBlock:(DBNavigationStoryControllerCompletionBlock)completionBlock
{
    UIViewController *fromViewController = from.navigationController;
    UIViewController *toViewController = to.navigationController;
    UIView *fromView = fromViewController.view;
    UIView *toView = toViewController.view;
    
    CGFloat duration = (fromViewController) ? 0.4 : 0.0;
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    
    toView.alpha = 0.0;
    toView.frame = self.view.bounds;
    toView.autoresizingMask = DBViewAdattaDimensioni;
    [self.view addSubview:toView];
    
    if (transition == DBAnimationTransitionZoom) {
        toView.transform = CGAffineTransformMakeScale(0.8, 0.8);
        duration = (fromViewController) ? 0.6 : 0.0;
    }
    
    // Notifications
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UINavigationControllerWillShowViewControllerNotification" object:toViewController];
    
    [UIView animateWithDuration:duration animations:^{
        toView.alpha = 1.0;
        
        if (transition == DBAnimationTransitionZoom) {
            toView.transform = CGAffineTransformIdentity;
            fromView.transform = CGAffineTransformMakeScale(1.2, 1.2);
            fromView.alpha = 0.0;
        }
        
    } completion:^(BOOL finished) {
        fromView.transform = CGAffineTransformIdentity;
        [fromView removeFromSuperview];
        
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
        
        // Notifications
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UINavigationControllerDidShowViewControllerNotification" object:toViewController];
        
        if (completionBlock) {
            completionBlock();
        }
    }];
}

// MARK: - Gestione della navigazione

- (DBNavigationStory *)pushNavigationStory:(DBNavigationStory *)navigationStory
                            withTransition:(DBAnimationTransition)transition
                        reuseExistingStory:(BOOL)reuseExistingStory
                           completionBlock:(DBNavigationStoryControllerCompletionBlock)completionBlock
{
    if (!navigationStory) {
        // Not valid
        return nil;
    }
    
    DBNavigationStory *fromNavigationStory = self.presentedNavigationStory;
    DBNavigationStory *toNavigationStory = navigationStory;
    
    if (reuseExistingStory) {
        DBNavigationStory *reusableNavigationStory = [self navigationStoryWithIdentifier:navigationStory.identifier];
        
        if (reusableNavigationStory) {
            // Reusing the existing NavigationStory
            toNavigationStory = reusableNavigationStory;
        }
    }
    
    if (fromNavigationStory == toNavigationStory) {
        // Pushing presented NavigationStory has no effect
        return fromNavigationStory;
    }
    
    // Reordering NavigationStories
    NSMutableArray *navigationStories = [self.navigationStories mutableCopy];
    [navigationStories removeObject:toNavigationStory];
    [navigationStories addObject:toNavigationStory];
    self.navigationStories = navigationStories;
    
    // Mostro la nuova NavigationStory
    [self transitionFromNavigationStory:fromNavigationStory toNavigationStory:toNavigationStory withTransition:transition completionBlock:^{
        
        if (completionBlock) {
            completionBlock();
        }
    }];
    
    return toNavigationStory;
}

- (void)popNavigationStoryWithTransition:(DBAnimationTransition)transition
                         completionBlock:(DBNavigationStoryControllerCompletionBlock)completionBlock
{
    // FIXME: TODO!
}

- (__kindof DBNavigationStory *)navigationStoryWithIdentifier:(NSString *)identifier
{
    // Reversed enumeration, to find the last pushed NavigationStory with the same identifier
    for (NSInteger counter = self.navigationStories.count - 1; counter >= 0; counter--) {
        DBNavigationStory *navigationStory = [self.navigationStories safeObjectAtIndex:counter];
        if ([navigationStory.identifier isEqualToString:identifier]) {
            return navigationStory;
        }
    }
    
    return nil;
}

- (void)clearNavigationStories
{
    @synchronized (self) {
        DBNavigationStory *presentedNavigationStory = self.presentedNavigationStory;
        NSArray *navigationStories = [NSArray arrayWithArray:self.navigationStories];
        
        for (DBNavigationStory *navigationStory in navigationStories) {
            if (navigationStory != presentedNavigationStory) {
                [self removeNavigationStory:navigationStory];
            }
        }
    }
}

@end


@implementation DBNavigationStoryController (Deprecated)

- (UINavigationController *)presentedNavigationController
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please manually get the NavigationController of the presented NavigationStory");
    return self.presentedNavigationStory.navigationController;
}

- (DBNavigationStory *)pushNavigationStoryWithViewController:(UIViewController *)viewController
                                              withTransition:(DBAnimationTransition)transition
                                           restoreSavedState:(BOOL)restoreSavedState
                                             completionBlock:(DBNavigationStoryControllerCompletionBlock)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please manually instantiate a NavigationStory and then push it through the NavigationStoryController");
    
    DBNavigationStory *navigationStory = [DBNavigationStory navigationStoryWithViewController:viewController identifier:nil];
    
    [self pushNavigationStory:navigationStory withTransition:transition reuseExistingStory:restoreSavedState completionBlock:completionBlock];
    
    return navigationStory;
}

- (void)pushNavigationStory:(DBNavigationStory *)navigationStory
             withTransition:(DBAnimationTransition)transition
            completionBlock:(DBNavigationStoryControllerCompletionBlock)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please use -[DBNavigationStoryController pushNavigationStory:withTransition:reuseExistingStory:completionBlock:");
    
    [self pushNavigationStory:navigationStory withTransition:transition reuseExistingStory:NO completionBlock:completionBlock];
}

@end
