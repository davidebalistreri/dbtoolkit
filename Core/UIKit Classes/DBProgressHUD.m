//
//  DBProgressHUD.m
//  File version: 1.1.0
//  Last modified: 02/14/2016
//
//  Created by Davide Balistreri on 03/27/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBProgressHUD.h"
#import "DBFramework.h"
#import "DBRuntimeHandler.h"

// MARK: - MBProgressHUD Category

@interface MBProgressHUD (DBProgressHUD)

@property (strong, nonatomic) UIView *vista;

@property (nonatomic) NSTimeInterval durata;
@property (nonatomic) BOOL ritardaSuccessivi;

@property (nonatomic) BOOL isToast;

@end


@implementation MBProgressHUD (DBProgressHUD)

- (UIView *)vista
{
    return [self oggettoAssociatoConSelector:@selector(vista)];
}

- (void)setVista:(UIView *)vista
{
    [self associaOggettoStrong:vista conSelector:@selector(vista)];
}

- (NSTimeInterval)durata
{
    NSNumber *oggetto = [self oggettoAssociatoConSelector:@selector(durata)];
    return [oggetto doubleValue];
}

- (void)setDurata:(NSTimeInterval)durata
{
    NSNumber *oggetto = [NSNumber numberWithDouble:durata];
    [self associaOggettoStrong:oggetto conSelector:@selector(durata)];
}

- (BOOL)ritardaSuccessivi
{
    NSNumber *oggetto = [self oggettoAssociatoConSelector:@selector(ritardaSuccessivi)];
    return [oggetto boolValue];
}

- (void)setRitardaSuccessivi:(BOOL)ritardaSuccessivi
{
    NSNumber *oggetto = [NSNumber numberWithBool:ritardaSuccessivi];
    [self associaOggettoStrong:oggetto conSelector:@selector(ritardaSuccessivi)];
}

- (BOOL)isToast
{
    NSNumber *oggetto = [self oggettoAssociatoConSelector:@selector(isToast)];
    return [oggetto boolValue];
}

- (void)setIsToast:(BOOL)isToast
{
    NSNumber *oggetto = [NSNumber numberWithBool:isToast];
    [self associaOggettoStrong:oggetto conSelector:@selector(isToast)];
}

@end


// MARK: - Estensione metodi di classe DBProgressHUD

@interface DBProgressHUD ()

+ (void)mostraToastInCoda;

@end


// MARK: - DBProgressHUDManager

@interface DBProgressHUDManager : NSObject <MBProgressHUDDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIFont *font;

@property (strong, nonatomic) NSMutableArray *progressHUDs;

@property (nonatomic) NSTimeInterval durataToasts;
@property (nonatomic) BOOL ritardaSuccessivi;

@end


@implementation DBProgressHUDManager

DBProgressHUDManager *dbSharedHUDManager;
+ (DBProgressHUDManager *)HUDManager
{
    @synchronized(self) {
        if (!dbSharedHUDManager) {
            dbSharedHUDManager = [DBProgressHUDManager new];
            dbSharedHUDManager.progressHUDs = [NSMutableArray array];
            dbSharedHUDManager.durataToasts = 1.8;
            dbSharedHUDManager.ritardaSuccessivi = YES;
            
            //            UIWindow *window = [UIWindow new];
            //            window.userInteractionEnabled = NO;
            //            window.windowLevel = 666;
            //            window.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
            //            window.rootViewController = [UIViewController new];
            //            window.autoresizesSubviews = YES;
            //            window.frame = [DBToolkit frameSchermo];
            //            [window makeKeyAndVisible];
            //
            // dbSharedHUDManager.window = window;
        }
        
        return dbSharedHUDManager;
    }
}

+ (UIWindow *)window
{
    // Da sistemare
    // return [DBProgressHUDManager HUDManager].window;
    // return (id)[DBToolkit presentedViewController].view;
    return [DBApplication topmostWindow];
}

+ (DBProgressHUD *)nuovoProgressHUD
{
    DBProgressHUDManager *manager = [self HUDManager];
    
    DBProgressHUD *progressHUD = [DBProgressHUD new];
    progressHUD.delegate = manager;
    
    [manager.progressHUDs addObject:progressHUD];
    return progressHUD;
}

+ (void)rimuoviProgressHUD:(DBProgressHUD *)progressHUD animazione:(BOOL)animazione
{
    DBProgressHUDManager *manager = [self HUDManager];
    progressHUD.delegate = manager;
    progressHUD.userInteractionEnabled = NO;
    [progressHUD hide:animazione];
}

+ (NSArray *)progressHUDs
{
    DBProgressHUDManager *manager = [self HUDManager];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isToast == %@", [NSNumber numberWithBool:NO]];
    NSArray *progressHUDs = [manager.progressHUDs filteredArrayUsingPredicate:predicate];
    return progressHUDs;
}

+ (NSArray *)toasts
{
    DBProgressHUDManager *manager = [self HUDManager];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isToast == %@", [NSNumber numberWithBool:YES]];
    NSArray *toasts = [manager.progressHUDs filteredArrayUsingPredicate:predicate];
    return toasts;
}

- (void)hudWasHidden:(DBProgressHUD *)hud
{
    //    UIView *vista = hud.vista;
    //
    //    // Se è l'ultimo ProgressHUD
    //    if (self.progressHUDs.count == 1) {
    //        if ([vista isEqual:self.window]) {
    //            self.window.hidden = YES;
    //        }
    //    }
    
    [hud removeFromSuperview];
    [self.progressHUDs removeObject:hud];
    
    if (hud.isToast) {
        [DBProgressHUD mostraToastInCoda];
    }
}

@end


// MARK: - Core

@implementation DBProgressHUD

+ (void)setFont:(UIFont *)font
{
    [[DBProgressHUDManager HUDManager] setFont:font];
}

// MARK: Toasts

+ (void)impostaDurataToasts:(NSTimeInterval)durata
{
    [[DBProgressHUDManager HUDManager] setDurataToasts:durata];
}

+ (void)impostaRitardaSuccessivi:(BOOL)ritardaSuccessivi
{
    [[DBProgressHUDManager HUDManager] setRitardaSuccessivi:ritardaSuccessivi];
}

+ (void)mostraToastInCoda
{
    NSMutableArray *toasts = [[DBProgressHUDManager toasts] mutableCopy];
    
    for (NSUInteger counter = 0; counter < toasts.count; counter++) {
        DBProgressHUD *toast = [toasts objectAtIndex:counter];
        
        if (!toast.superview) {
            [toast.vista addSubview:toast];
            [toast show:YES];
            [self rimuoviToast:toast conDelay:toast.durata];
        }
        
        if (toast.ritardaSuccessivi) {
            // Prima di aprire altri toast aspetto che si sia chiuso quello attualmente aperto
            return;
        }
        else {
            // Chiudo i toast eventualmente aperti prima di questo
            if (counter > 0) {
                DBProgressHUD *ultimoToast = [toasts objectAtIndex:counter - 1];
                [self rimuoviToast:ultimoToast conDelay:0];
                [toasts removeObject:ultimoToast];
                counter--;
            }
        }
    }
}

+ (DBProgressHUD *)toastConMessaggio:(NSString *)messaggio
{
    id vista = [DBProgressHUDManager window];
    NSTimeInterval durata = [[DBProgressHUDManager HUDManager] durataToasts];
    BOOL ritardaSuccessivi = [[DBProgressHUDManager HUDManager] ritardaSuccessivi];
    return [self toastConMessaggio:messaggio conVista:vista conDurata:durata ritardaSuccessivi:ritardaSuccessivi];
}

+ (DBProgressHUD *)toastConMessaggioLocalizzato:(NSString *)messaggio
{
    id vista = [DBProgressHUDManager window];
    NSTimeInterval durata = [[DBProgressHUDManager HUDManager] durataToasts];
    BOOL ritardaSuccessivi = [[DBProgressHUDManager HUDManager] ritardaSuccessivi];
    return [self toastConMessaggio:DBLocalizedString(messaggio) conVista:vista conDurata:durata ritardaSuccessivi:ritardaSuccessivi];
}

+ (DBProgressHUD *)toastConMessaggio:(NSString *)messaggio
                            conVista:(UIView *)vista
                           conDurata:(NSTimeInterval)durata
                   ritardaSuccessivi:(BOOL)ritardaSuccessivi
{
    DBProgressHUD *toast = [DBProgressHUDManager nuovoProgressHUD];
    toast.isToast = YES;
    
    toast.userInteractionEnabled = NO;
    toast.mode = MBProgressHUDModeCustomView;
    toast.detailsLabelFont = toast.labelFont;
    toast.detailsLabelText = [NSString safeString:messaggio];
    
    DBProgressHUDManager *manager = [DBProgressHUDManager HUDManager];
    if (manager.font) {
        toast.labelFont = manager.font;
        toast.detailsLabelFont = manager.font;
    }
    
    toast.vista = vista;
    toast.frame = vista.bounds;
    toast.durata = durata;
    toast.ritardaSuccessivi = ritardaSuccessivi;
    
    //    if ([vista isEqual:[DBProgressHUDManager window]]) {
    //        vista.hidden = NO;
    //        vista.userInteractionEnabled = NO;
    //    }
    
    // Mostro il toast, se non ce n'è già un altro attivo
    [self mostraToastInCoda];
    
    return toast;
}

+ (void)rimuoviToast:(DBProgressHUD *)toast conDelay:(NSTimeInterval)delay
{
    [self performBlockAfterDelay:delay block:^{
        [DBProgressHUDManager rimuoviProgressHUD:toast animazione:YES];
    }];
}

+ (void)rimuoviToasts
{
    for (DBProgressHUD *toast in [DBProgressHUDManager toasts]) {
        [DBProgressHUDManager rimuoviProgressHUD:toast animazione:YES];
    }
}


// MARK: ProgressHUDs

+ (DBProgressHUD *)progressHUD
{
    id vista = [DBProgressHUDManager window];
    return [self progressHUDConMessaggio:@"" conVista:vista];
}

+ (DBProgressHUD *)progressHUDConMessaggio:(NSString *)messaggio
{
    id vista = [DBProgressHUDManager window];
    return [self progressHUDConMessaggio:messaggio conVista:vista];
}

+ (DBProgressHUD *)progressHUDConMessaggioLocalizzato:(NSString *)messaggio
{
    id vista = [DBProgressHUDManager window];
    return [self progressHUDConMessaggio:DBLocalizedString(messaggio) conVista:vista];
}

/**
 * Crea un Progress HUD permettendo di impostare il messaggio e la vista su cui mostrarlo.
 */
+ (DBProgressHUD *)progressHUDConMessaggio:(NSString *)messaggio conVista:(UIView *)vista
{
    // Check
    if (!vista) {
        return nil;
    }
    
    DBProgressHUD *progressHUD = [DBProgressHUDManager nuovoProgressHUD];
    
    progressHUD.vista = vista;
    progressHUD.frame = vista.bounds;
    progressHUD.labelText = [NSString safeString:messaggio];
    
    DBProgressHUDManager *manager = [DBProgressHUDManager HUDManager];
    if (manager.font) {
        progressHUD.labelFont = manager.font;
    }
    
    // Mostro il ProgressHUD
    [vista addSubview:progressHUD];
    [progressHUD show:YES];
    
    return progressHUD;
}

+ (void)rimuoviProgressHUD:(DBProgressHUD *)progressHUD
{
    [DBProgressHUDManager rimuoviProgressHUD:progressHUD animazione:YES];
}

+ (void)rimuoviProgressHUDs
{
    for (DBProgressHUD *progressHUD in [DBProgressHUDManager progressHUDs]) {
        [DBProgressHUDManager rimuoviProgressHUD:progressHUD animazione:YES];
    }
}

- (void)rimuoviProgressHUD
{
    [DBProgressHUD rimuoviProgressHUD:self];
}

// MARK: - ProgressHUDs con Selector

+ (void)progressHUDConTarget:(id)target action:(SEL)action
{
    id vista = [DBProgressHUDManager window];
    [self progressHUDConMessaggio:@"" conVista:vista conTarget:target action:action];
}

+ (void)progressHUDConMessaggio:(NSString *)messaggio conTarget:(id)target action:(SEL)action
{
    id vista = [DBProgressHUDManager window];
    [self progressHUDConMessaggio:messaggio conVista:vista conTarget:target action:action];
}

+ (void)progressHUDConMessaggioLocalizzato:(NSString *)messaggio conTarget:(id)target action:(SEL)action
{
    id vista = [DBProgressHUDManager window];
    [self progressHUDConMessaggio:DBLocalizedString(messaggio) conVista:vista conTarget:target action:action];
}

/**
 * Crea un Progress HUD permettendo di impostare il messaggio e la vista su cui mostrarlo,
 * il quale sarà visualizzato solo per il tempo di esecuzione del Selector specificato.
 */
+ (void)progressHUDConMessaggio:(NSString *)messaggio conVista:(UIView *)vista conTarget:(id)target action:(SEL)action
{
    // Check
    if (!target || !action || !vista) return;
    if (![target respondsToSelector:action]) return;
    
    DBProgressHUD *progressHUD = [DBProgressHUD progressHUDConMessaggio:messaggio conVista:vista];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (target && action) [target action];
        [progressHUD hide:YES];
    });
}


// MARK: - ProgressHUDs con Block

+ (void)progressHUDConBlock:(dispatch_block_t)block
{
    id vista = [DBProgressHUDManager window];
    [self progressHUDConMessaggio:@"" conVista:vista conBlock:block];
}

+ (void)progressHUDConMessaggio:(NSString *)messaggio conBlock:(dispatch_block_t)block
{
    id vista = [DBProgressHUDManager window];
    [self progressHUDConMessaggio:messaggio conVista:vista conBlock:block];
}

+ (void)progressHUDConMessaggioLocalizzato:(NSString *)messaggio conBlock:(dispatch_block_t)block
{
    id vista = [DBProgressHUDManager window];
    [self progressHUDConMessaggio:DBLocalizedString(messaggio) conVista:vista conBlock:block];
}

/**
 * Crea un Progress HUD permettendo di impostare il messaggio e la vista su cui mostrarlo,
 * il quale sarà visualizzato solo per il tempo di esecuzione del Block specificato.
 */
+ (void)progressHUDConMessaggio:(NSString *)messaggio conVista:(UIView *)vista conBlock:(dispatch_block_t)block
{
    // Check
    if (!block || !vista) return;
    
    DBProgressHUD *progressHUD = [DBProgressHUD progressHUDConMessaggio:messaggio conVista:vista];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (block) block();
        [progressHUD hide:YES];
    });
}

@end
