//
//  DBProgressHUD.h
//  File version: 1.1.0
//  Last modified: 02/14/2016
//
//  Created by Davide Balistreri on 03/27/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"
#import "MBProgressHUD.h"

@interface DBProgressHUD : MBProgressHUD

+ (void)setFont:(UIFont *)font;

// MARK: - Toasts

+ (void)impostaDurataToasts:(NSTimeInterval)durata;
+ (void)impostaRitardaSuccessivi:(BOOL)ritardaSuccessivi;

+ (DBProgressHUD *)toastConMessaggio:(NSString *)messaggio;
+ (DBProgressHUD *)toastConMessaggioLocalizzato:(NSString *)messaggio;

/**
 * Crea un Toast permettendo di impostare il messaggio,
 * la vista su cui mostrarlo, la durata, e se i successivi Toast
 * dovranno attendere che questo sia sparito dalla schermata.
 */
+ (DBProgressHUD *)toastConMessaggio:(NSString *)messaggio
                            conVista:(UIView *)vista
                           conDurata:(NSTimeInterval)durata
                   ritardaSuccessivi:(BOOL)ritardaSuccessivi;

+ (void)rimuoviToasts;

// MARK: - ProgressHUDs

+ (DBProgressHUD *)progressHUD;
+ (DBProgressHUD *)progressHUDConMessaggio:(NSString *)messaggio;
+ (DBProgressHUD *)progressHUDConMessaggioLocalizzato:(NSString *)messaggio;

/**
 * Crea un Progress HUD permettendo di impostare il messaggio e la vista su cui mostrarlo.
 */
+ (DBProgressHUD *)progressHUDConMessaggio:(NSString *)messaggio conVista:(UIView *)vista;

+ (void)rimuoviProgressHUD:(DBProgressHUD *)progressHUD;
+ (void)rimuoviProgressHUDs;

- (void)rimuoviProgressHUD;

// MARK: - ProgressHUDs con Selector

+ (void)progressHUDConTarget:(id)target action:(SEL)action;
+ (void)progressHUDConMessaggio:(NSString *)messaggio conTarget:(id)target action:(SEL)action;
+ (void)progressHUDConMessaggioLocalizzato:(NSString *)messaggio conTarget:(id)target action:(SEL)action;

/**
 * Crea un Progress HUD permettendo di impostare il messaggio e la vista su cui mostrarlo,
 * il quale sarà visualizzato solo per il tempo di esecuzione del Selector specificato.
 */
+ (void)progressHUDConMessaggio:(NSString *)messaggio conVista:(UIView *)vista conTarget:(id)target action:(SEL)action;


// MARK: - ProgressHUDs con Block

+ (void)progressHUDConBlock:(dispatch_block_t)block;
+ (void)progressHUDConMessaggio:(NSString *)messaggio conBlock:(dispatch_block_t)block;
+ (void)progressHUDConMessaggioLocalizzato:(NSString *)messaggio conBlock:(dispatch_block_t)block;

/**
 * Crea un Progress HUD permettendo di impostare il messaggio e la vista su cui mostrarlo,
 * il quale sarà visualizzato solo per il tempo di esecuzione del Block specificato.
 */
+ (void)progressHUDConMessaggio:(NSString *)messaggio conVista:(UIView *)vista conBlock:(dispatch_block_t)block;

@end
