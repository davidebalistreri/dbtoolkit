//
//  DBPagerViewPage.m
//  File version: 1.0.0 beta
//  Last modified: 10/04/2016
//
//  Created by Davide Balistreri on 08/03/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBPagerViewPage.h"

#import "DBApplication.h"
#import "DBObjectExtensions.h"
#import "DBLocalization.h"

#import "DBPagerView.h"
#import "DBPagerViewTabItem.h"


@implementation DBPagerViewPage

// MARK: Class methods

+ (instancetype)pageWithView:(UIView *)view andTitle:(NSString *)title
{
    if (!view) {
        // Not valid
        return nil;
    }
    
    DBPagerViewPage *page = [DBPagerViewPage new];
    page.view = view;
    page.title = title;
    
    return page;
}

+ (instancetype)pageWithView:(UIView *)view andLocalizedTitle:(NSString *)localizedTitle
{
    NSString *title = DBLocalizedString(localizedTitle);
    return [self pageWithView:view andTitle:title];
}

+ (instancetype)pageWithViewController:(UIViewController *)viewController
{
    if (!viewController) {
        // Not valid
        return nil;
    }
    
    DBPagerViewPage *page = [DBPagerViewPage new];
    page.viewController = viewController;
    page.view = viewController.view;
    page.title = viewController.title;
    
    if ([NSString isEmpty:page.title]) page.title = viewController.navigationItem.title;
    if ([NSString isEmpty:page.title]) page.title = @" ";
    
    return page;
}

@synthesize view = _view;
- (void)setView:(id)view
{
    // Setup
    UIView *setupView = view;
    setupView.translatesAutoresizingMaskIntoConstraints = NO;
    
    _view = setupView;
}

// Private method
- (void)setTabItem:(__kindof DBPagerViewTabItem *)tabItem
{
    _tabItem = tabItem;
}

@synthesize viewController = _viewController;
- (void)setViewController:(id)viewController
{
    UIViewController *setupViewController = viewController;
    UIView *setupView = setupViewController.view;
    
    @try {
        // FIXME
        // Force call of viewDidLoad viewWillAppear viewDidAppear impostaSchermata LOCALIZZASCHERMATA aggiornaSchermata
        UIWindow *window = [DBApplication mainWindow];
        [window addSubview:setupView];
        [setupView layoutIfNeeded];
        [setupView removeFromSuperview];
        
    } @catch (NSException *exception) {
        NSLog(@"Error: %@", exception.description);
    }
    
    _viewController = setupViewController;
}

- (BOOL)isFirstPage
{
    if (self.parentPagerView && self.index == 0) {
        return YES;
    }
    
    return NO;
}

- (BOOL)isLastPage
{
    if ([NSArray isNotEmpty:self.parentPagerView.pages] && self.index == self.parentPagerView.pages.count - 1) {
        return YES;
    }
    
    return NO;
}

@end
