//
//  DBPagerView.m
//  File version: 1.0.0 beta
//  Last modified: 03/20/2018
//
//  Created by Davide Balistreri on 08/03/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBPagerView.h"

#import "DBArrayExtensions.h"
#import "DBColorExtensions.h"
#import "DBObjectExtensions.h"
#import "DBViewExtensions.h"
#import "DBGeometry.h"
#import "DBUtility.h"
#import "DBAnimation.h"
#import "DBLocalization.h"


// MARK: - TabView class

@interface TabView : UICollectionView

@property (weak, nonatomic) UICollectionView *contentView;

@end

@implementation TabView

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (CGRectContainsPoint(self.bounds, point)) {
        return self.contentView;
    }
    
    return [super hitTest:point withEvent:event];
}

@end


// MARK: - Page private methods

@interface DBPagerViewPage (PrivateMethods)

@property (nonatomic) __kindof DBPagerViewTabItem *tabItem;

@end


// MARK: - TabItem private methods

@interface DBPagerViewTabItem (PrivateMethods)

@property (nonatomic) CGSize realSize;
@property (nonatomic) CGSize displaySize;

@end


// MARK: - DBPagerView class

@interface DBPagerView () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate>

// Used to forward tap events to the tab view when its touches are redirected to the content view (when it is not scrollable)
@property (strong, nonatomic) UITapGestureRecognizer *tapRecognizer;

@property (strong, nonatomic) TabView *tabView;
@property (strong, nonatomic) UIView *pageIndicatorView;
@property (strong, nonatomic) UICollectionView *contentView;

@property (strong, nonatomic) NSArray<NSLayoutConstraint *> *constraints;

@property (nonatomic) CGRect currentBounds;

/// If the pager view is reloading its content, some calculations will be skipped
@property (nonatomic, getter=isReloadingLayout) BOOL reloadingLayout;

@end


@implementation DBPagerView

// MARK: - Setup

- (void)setupObject
{
    // Setup defaults
    [self setupDefaults];
    
    // Content view
    UICollectionViewFlowLayout *contentLayout = [UICollectionViewFlowLayout new];
    contentLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.contentView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:contentLayout];
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.backgroundColor = [UIColor clearColor];
    self.contentView.dataSource = self;
    self.contentView.delegate = self;
    self.contentView.scrollsToTop = NO;
    self.contentView.pagingEnabled = YES;
    self.contentView.showsHorizontalScrollIndicator = NO;
    self.contentView.showsVerticalScrollIndicator = NO;
    self.contentView.allowsSelection = NO;
    self.contentView.bounces = NO;
    self.contentView.contentInset = UIEdgeInsetsZero;
    [self addSubview:self.contentView];
    
    // Tab view
    UICollectionViewFlowLayout *tabLayout = [UICollectionViewFlowLayout new];
    tabLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.tabView = [[TabView alloc] initWithFrame:CGRectZero collectionViewLayout:tabLayout];
    self.tabView.contentView = self.contentView; // Weak reference
    self.tabView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tabView.backgroundColor = [UIColor clearColor];
    self.tabView.dataSource = self;
    self.tabView.delegate = self;
    self.tabView.scrollsToTop = NO;
    self.tabView.showsHorizontalScrollIndicator = NO;
    self.tabView.showsVerticalScrollIndicator = NO;
    self.tabView.allowsSelection = NO;
    self.tabView.scrollEnabled = NO;
    self.tabView.bounces = NO;
    self.tabView.contentInset = UIEdgeInsetsZero;
    // [self.tabView setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    // [self.tabView setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    [self addSubview:self.tabView];
    
    // Page indicator view
    self.pageIndicatorView = [UIView new];
    self.pageIndicatorView.userInteractionEnabled = NO;
    self.pageIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.pageIndicatorView];
    [self reloadPageIndicator];
    
    // Tap recognizer
    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognizer:)];
    self.tapRecognizer.delegate = self;
    self.tapRecognizer.cancelsTouchesInView = NO;
    [self addGestureRecognizer:self.tapRecognizer];
    
    // Collection view cells
    [self.tabView     registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"TabItemCell"];
    [self.contentView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"ContentCell"];
}

- (void)setupDefaults
{
    self.clipsToBounds = YES;
    
    _animationsEnabled = YES;
    
    _layoutDirection = DBPagerViewLayoutDirectionDefault;
    
    _tabViewPosition = DBPagerViewTabViewPositionDefault;
    _tabViewHeight   = 0.0; // Default
    _tabItemStyle    = DBPagerViewTabItemStyleDefault;
    
    _pageIndicatorPosition = DBPagerViewPageIndicatorPositionDefault;
    _pageIndicatorHeight   = DBPagerViewLayoutHeightAutomatic;
    
    _pageIndicator = [DBPagerViewLinePageIndicator indicator];
    
    _pages = [NSArray array];
}

- (void)setBounds:(CGRect)bounds
{
    if (self.superview) {
        DBPagerViewPage *presentedPage = self.presentedPage;
        
        [super setBounds:bounds];
        
        if (CGRectEqualToRect(self.currentBounds, bounds) == NO) {
            self.currentBounds = bounds;
            
            [self reloadCollectionViews];
            [self setPresentedPage:presentedPage animated:NO];
        }
    }
}


// MARK: - Public methods

@synthesize pages = _pages;
- (void)setPages:(NSArray<DBPagerViewPage *> *)pages
{
    _pages = pages;
    [self reloadAll];
}

- (void)removeAllPages
{
    self.pages = [NSArray array];
}

- (void)reloadData
{
    [self reloadAll];
}

- (DBPagerViewPage *)presentedPage
{
    return [self.pages safeObjectAtIndex:self.presentedPageIndex];
}

- (void)setPresentedPage:(DBPagerViewPage *)page
{
    [self setPresentedPage:page animated:NO];
}

- (void)setPresentedPage:(DBPagerViewPage *)presentedPage animated:(BOOL)animated
{
    if (!presentedPage) {
        // Non valido
        return;
    }
    
    NSInteger index = [self.pages indexOfObject:presentedPage];
    [self setPresentedPageIndex:index animated:animated];
}

- (NSInteger)presentedPageIndex
{
    return round(self.contentView.contentOffset.x / self.contentView.bounds.size.width);
}

- (void)setPresentedPageIndex:(NSInteger)presentedPageIndex
{
    [self setPresentedPageIndex:presentedPageIndex animated:NO];
}

- (void)setPresentedPageIndex:(NSInteger)index animated:(BOOL)animated
{
    if ([NSArray isEmpty:self.pages]) {
        // Non valido
        return;
    }
    
    NSInteger safeIndex = mantieniAllInterno(index, 0, self.pages.count - 1);
    
    if (safeIndex == self.presentedPageIndex) {
        // Useless
        return;
    }
    
    BOOL versoAvanti = (safeIndex > self.presentedPageIndex) ? YES : NO;
    
    // Temporary "Fabio Gai style" fix
    // Sometimes when the current content offset is the same it doesn't call scrollViewDidScroll, but not everytime
    CGPoint fixOffset = self.contentView.contentOffset;
    fixOffset.x += (versoAvanti) ? 1.0 : -1.0;
    [self.contentView setContentOffset:fixOffset animated:NO];
    
    CGPoint pageOffset = CGPointZero;
    pageOffset.x = self.contentView.bounds.size.width * safeIndex;
    [self.contentView setContentOffset:pageOffset animated:animated];
}

- (void)gotoPreviousPage:(BOOL)animated
{
    NSInteger index = self.presentedPageIndex;
    [self setPresentedPageIndex:index - 1 animated:animated];
}

- (void)gotoNextPage:(BOOL)animated
{
    NSInteger index = self.presentedPageIndex;
    [self setPresentedPageIndex:index + 1 animated:animated];
}

- (DBPagerViewPage *)addPageWithView:(__kindof UIView *)view withTitle:(NSString *)title
{
    DBPagerViewPage *page = [DBPagerViewPage pageWithView:view andTitle:title];
    
    if (page) {
        NSMutableArray *pages = [NSMutableArray arrayWithArray:self.pages];
        [pages addObject:page];
        self.pages = pages;
    }
    
    return page;
}

- (DBPagerViewPage *)addPageWithView:(__kindof UIView *)view withLocalizedTitle:(NSString *)localizedTitle
{
    NSString *title = DBLocalizedString(localizedTitle);
    return [self addPageWithView:view withTitle:title];
}

- (DBPagerViewPage *)addPageWithViewController:(__kindof UIViewController *)viewController
{
    DBPagerViewPage *page = [DBPagerViewPage pageWithViewController:viewController];
    
    if (page) {
        NSMutableArray *pages = [NSMutableArray arrayWithArray:self.pages];
        [pages addObject:page];
        self.pages = pages;
    }
    
    return page;
}


// MARK: - Private methods

- (void)reloadAll
{
    if (self.superview) {
        [self reloadConstraints];
        [self reloadCollectionViews];
        // [self reloadPageIndicator];
        [self reloadLayout];
        
        [self scrollViewDidScroll:self.contentView];
    }
}

- (void)reloadPageIndicator
{
    if (!self.pageIndicatorView) {
        // Useless
        return;
    }
    
    /// Da integrare in reloadConstraints perché il reloadData deve ricaricare anche il page indicator e le
    /// constraints vecchie devono essere rimosse!
    
    // Page indicator and its constraints
    DBPagerViewPageIndicator *pageIndicator = self.pageIndicator;
    pageIndicator.parentPagerView = self;
    [pageIndicator removeFromSuperview];
    [self.pageIndicatorView addSubview:pageIndicator];
    
    // ContentInset
    UIEdgeInsets inset = pageIndicator.contentInset;
    NSString *formatHorizontal = [NSString stringWithFormat:@"H:|-(%.f)-[pageIndicator]-(%.f)-|", inset.left, inset.right];
    NSString *formatVertical   = [NSString stringWithFormat:@"V:|-(%.f)-[pageIndicator]-(%.f)-|", inset.top,  inset.bottom];
    
    // Constraints
    NSDictionary *views = NSDictionaryOfVariableBindings(pageIndicator);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:formatHorizontal options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:formatVertical   options:0 metrics:nil views:views]];
    
    [self.pageIndicator reloadLayout];
}

- (void)reloadConstraints
{
    if (!self.superview) {
        // Useless
        return;
    }
    
    if (self.constraints) {
        // Rimuovo le vecchie constraints
        [self removeConstraints:self.constraints];
    }
    
    NSMutableArray *constraints = [NSMutableArray array];
    
    /// Actors
    UIView *tabView = self.tabView;
    UIView *pageIndicatorView = self.pageIndicatorView;
    UIView *contentView = self.contentView;
    NSDictionary *views = NSDictionaryOfVariableBindings(tabView, pageIndicatorView, contentView);
    
    /// Horizontal constraints
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tabView]|" options:0 metrics:nil views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[pageIndicatorView]|" options:0 metrics:nil views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[contentView]|" options:0 metrics:nil views:views]];
    
    /// Vertical constraints
    NSString *formatTabView = @"";
    
    // Tab view
    switch (self.tabViewPosition) {
        case DBPagerViewTabViewPositionBottom:
            // Bottom
            formatTabView = @"V:|[contentView][tabView(xxx)]|";
            break;
        default:
            // Top
            formatTabView = @"V:|[tabView(xxx)][contentView]|";
            break;
    }
    
    if (self.isTabViewHidden) {
        formatTabView = [formatTabView stringByReplacingOccurrencesOfString:@"(xxx)" withString:@"(0)"];
    }
    else {
        CGFloat height = [self calculatedTabViewHeight];
        NSString *stringHeight = [NSString stringWithFormat:@"(%.1f)", height];
        formatTabView = [formatTabView stringByReplacingOccurrencesOfString:@"(xxx)" withString:stringHeight];
    }
    
    // Page indicator
    NSString *formatPageIndicator = @"";
    
    DBPagerViewPageIndicatorPosition pageIndicatorPosition = self.pageIndicatorPosition;
    if (pageIndicatorPosition == DBPagerViewPageIndicatorPositionAutomatic) {
        pageIndicatorPosition = self.pageIndicator.preferredPosition;
    }
    
    switch (pageIndicatorPosition) {
        case DBPagerViewPageIndicatorPositionTop:
            formatPageIndicator = @"V:|[pageIndicatorView(xxx)]";
            break;
            
        case DBPagerViewPageIndicatorPositionInsideTabView:
            formatPageIndicator = @"V:[pageIndicatorView(xxx)]";
            
            if (self.tabViewPosition == DBPagerViewTabViewPositionTop) {
                [constraints addObject:[NSLayoutConstraint constraintWithItem:self.pageIndicatorView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.tabView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
            }
            else {
                [constraints addObject:[NSLayoutConstraint constraintWithItem:self.pageIndicatorView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.tabView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
            }
            break;
            
        case DBPagerViewPageIndicatorPositionNearTabView:
            if (self.tabViewPosition == DBPagerViewTabViewPositionTop) {
                formatPageIndicator = @"V:[tabView][pageIndicatorView(xxx)]";
            }
            else {
                formatPageIndicator = @"V:[pageIndicatorView(xxx)][tabView]";
            }
            break;
            
        case DBPagerViewPageIndicatorPositionAwayFromTabView:
            if (self.tabViewPosition == DBPagerViewTabViewPositionTop) {
                formatPageIndicator = @"V:[pageIndicatorView(xxx)]|";
            }
            else {
                formatPageIndicator = @"V:|[pageIndicatorView(xxx)]";
            }
            break;
            
        case DBPagerViewPageIndicatorPositionBottom:
            formatPageIndicator = @"V:[pageIndicatorView(xxx)]|";
            break;
            
        default:
            break;
    }
    
    if (self.isPageIndicatorHidden) {
        formatPageIndicator = [formatPageIndicator stringByReplacingOccurrencesOfString:@"(xxx)" withString:@"(0)"];
    }
    else if (self.pageIndicatorHeight == DBPagerViewLayoutHeightAutomatic) {
        formatPageIndicator = [formatPageIndicator stringByReplacingOccurrencesOfString:@"(xxx)" withString:@""];
    }
    else {
        CGFloat height = [self calculatedPageIndicatorHeight];
        NSString *stringHeight = [NSString stringWithFormat:@"(%.1f)", height];
        formatPageIndicator = [formatPageIndicator stringByReplacingOccurrencesOfString:@"(xxx)" withString:stringHeight];
    }
    
    // Applico e tengo un riferimento alle constraint appena aggiunte
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:formatTabView options:0 metrics:nil views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:formatPageIndicator options:0 metrics:nil views:views]];
    
    [self addConstraints:constraints];
    self.constraints = constraints;
}

- (void)reloadCollectionViews
{
    if (!self.superview) {
        // Useless
        return;
    }
    
    // Ricarico le CollectionView per eliminare eventuali vecchie celle ancora non deallocate
    // (ad esempio quando vengono rimosse tutte le pagine con la funzione removeAllPages)
    [self.tabView reloadData];
    [self.contentView reloadData];
    
    [self layoutIfNeeded];
    
    // Update pages
    for (DBPagerViewPage *page in self.pages) {
        page.parentPagerView = self;
        page.index = [self.pages indexOfObject:page];
        
        // Reload tab items
        page.tabItem = nil;
        [self tabItemForPage:page];
        [self tabItemSizeForPage:page];
    }
    
    [self.tabView layoutIfNeeded];
    [self.tabView reloadData];
    
    [self.contentView layoutIfNeeded];
    [self.contentView reloadData];
    
    if (self.pages.count == 0) {
        self.userInteractionEnabled = NO;
    } else {
        self.userInteractionEnabled = YES;
    }
}

- (void)reloadLayout
{
    if (!self.superview) {
        // Useless
        return;
    }
    
    self.tabView.hidden = self.isTabViewHidden;
    self.pageIndicatorView.hidden = self.isPageIndicatorHidden;
    self.contentView.hidden = self.isContentViewHidden;
    
    if (self.isTabViewHidden == NO) {
        [self.tabView reloadData];
    }
    
    if (self.isPageIndicatorHidden == NO) {
        [self.pageIndicator reloadLayout];
    }
    
    if (self.isContentViewHidden == NO) {
        [self.contentView reloadData];
    }
}

- (BOOL)tabItemShouldFillWidth
{
    if (self.isTabViewHidden) {
        return YES;
    }
    
    if (self.tabItemStyle == DBPagerViewTabItemStyleRealWidth) {
        return NO;
    }
    else if (self.tabItemStyle == DBPagerViewTabItemStyleFillWidth) {
        return YES;
    }
    
    // Automatic behavior
    CGFloat totalWidth = 0.0;
    
    for (DBPagerViewPage *page in self.pages) {
        CGSize size = [self tabItemRealSizeForPage:page];
        totalWidth += size.width;
    }
    
    if (totalWidth <= self.tabView.bounds.size.width) {
        return YES;
    }
    
    return NO;
}

- (CGFloat)calculatedTabViewHeight
{
    if (self.tabViewHeight == DBPagerViewLayoutHeightAutomatic) {
        CGFloat height = 0.0;
        
        for (DBPagerViewPage *page in self.pages) {
            CGSize size = [self tabItemRealSizeForPage:page];
            height = fmax(height, size.height);
        }
        
        if (height > 0.0) {
            return height;
        }
    }
    else if (self.tabViewHeight > 0.0) {
        return self.tabViewHeight;
    }
    
    return 50.0; // Default
}

- (CGFloat)calculatedPageIndicatorHeight
{
    if (self.pageIndicatorHeight > 0.0) {
        return self.pageIndicatorHeight;
    }
    
    return 3.0; // Default
}

// Disabilitato perché la TabView potrebbe non esserci
//- (CGSize)intrinsicContentSize
//{
//    CGFloat height = self.tabView.contentSizeReale.height + self.contentView.contentSizeReale.height;
//    return CGSizeMake(self.frame.size.width, height);
//}


// MARK: - Graphic setup

@synthesize selectedColor = _selectedColor;
- (void)setSelectedColor:(UIColor *)selectedColor
{
    _selectedColor = selectedColor;
    [self reloadLayout];
}

- (UIColor *)selectedColor
{
    if (_selectedColor) {
        return _selectedColor;
    }
    else {
        return [UIColor isColorLight:self.backgroundColor] ? [UIColor blackColor] : [UIColor whiteColor];
    }
}

@synthesize unselectedColor = _unselectedColor;
- (void)setUnselectedColor:(UIColor *)unselectedColor
{
    _unselectedColor = unselectedColor;
    [self reloadLayout];
}

- (UIColor *)unselectedColor
{
    if (_unselectedColor) {
        return _unselectedColor;
    }
    else {
        return [self.selectedColor colorWithAlphaComponent:0.5];
    }
}

@synthesize pageIndicatorColor = _pageIndicatorColor;
- (void)setPageIndicatorColor:(UIColor *)pageIndicatorColor
{
    _pageIndicatorColor = pageIndicatorColor;
    [self reloadLayout];
}

- (UIColor *)pageIndicatorColor
{
    if (_pageIndicatorColor) {
        return _pageIndicatorColor;
    }
    else {
        return self.selectedColor;
    }
}

@synthesize font = _font;
- (void)setFont:(UIFont *)font
{
    _font = font;
    [self reloadLayout];
}

- (UIFont *)font
{
    if (_font) {
        return _font;
    }
    else {
        return [UIFont boldSystemFontOfSize:15.0];
    }
}

@synthesize scrollEnabled = _scrollEnabled;
- (void)setScrollEnabled:(BOOL)scrollEnabled
{
    _scrollEnabled = scrollEnabled;
    self.contentView.scrollEnabled = scrollEnabled;
}

@synthesize bounces = _bounces;
- (void)setBounces:(BOOL)bounces
{
    _bounces = bounces;
    // self.tabView.bounces = bounces;
    self.contentView.bounces = bounces;
}

@synthesize tabViewInset = _tabViewInset;
- (void)setTabViewInset:(UIEdgeInsets)tabViewInset
{
    _tabViewInset = tabViewInset;
    self.tabView.contentInset = tabViewInset;
}

@synthesize contentViewInset = _contentViewInset;
- (void)setContentViewInset:(UIEdgeInsets)contentViewInset
{
    _contentViewInset = contentViewInset;
    
    // UICollectionViewFlowLayout *layout = self.contentView.collectionViewLayout;
    // layout.sectionInset = contentViewInset;
    
    [self reloadCollectionViews];
}

@synthesize tabViewHidden = _tabViewHidden;
- (void)setTabViewHidden:(BOOL)tabViewHidden
{
    _tabViewHidden = tabViewHidden;
    [self reloadAll];
}

- (BOOL)isTabViewHidden
{
    if ([NSArray isEmpty:self.pages]) {
        return YES;
    }
    
    return _tabViewHidden;
}

@synthesize pageIndicatorHidden = _pageIndicatorHidden;
- (void)setPageIndicatorHidden:(BOOL)pageIndicatorHidden
{
    _pageIndicatorHidden = pageIndicatorHidden;
    [self reloadAll];
}

- (BOOL)isPageIndicatorHidden
{
    if ([NSArray isEmpty:self.pages]) {
        return YES;
    }
    
    return _pageIndicatorHidden;
}

@synthesize contentViewHidden = _contentViewHidden;
- (void)setContentViewHidden:(BOOL)contentViewHidden
{
    _contentViewHidden = contentViewHidden;
    [self reloadAll];
}

- (BOOL)isContentViewHidden
{
    if ([NSArray isEmpty:self.pages]) {
        return YES;
    }
    
    return _contentViewHidden;
}

@synthesize layoutDirection = _layoutDirection;
- (void)setLayoutDirection:(DBPagerViewLayoutDirection)layoutDirection
{
    _layoutDirection = layoutDirection;
    [self reloadAll];
}

@synthesize tabViewPosition = _tabViewPosition;
- (void)setTabViewPosition:(DBPagerViewTabViewPosition)tabViewPosition
{
    _tabViewPosition = tabViewPosition;
    [self reloadConstraints];
}

@synthesize tabItemStyle = _tabItemStyle;
- (void)setTabItemStyle:(DBPagerViewTabItemStyle)tabItemStyle
{
    _tabItemStyle = tabItemStyle;
    [self reloadAll];
}

@synthesize pageIndicatorHeight = _pageIndicatorHeight;
- (void)setPageIndicatorHeight:(DBPagerViewLayoutHeight)pageIndicatorHeight
{
    _pageIndicatorHeight = pageIndicatorHeight;
    [self reloadConstraints];
}

@synthesize pageIndicatorPosition = _pageIndicatorPosition;
- (void)setPageIndicatorPosition:(DBPagerViewPageIndicatorPosition)pageIndicatorPosition
{
    _pageIndicatorPosition = pageIndicatorPosition;
    [self reloadConstraints];
}

@synthesize pageIndicator = _pageIndicator;
- (void)setPageIndicator:(__kindof DBPagerViewPageIndicator *)pageIndicator
{
    if (_pageIndicator) {
        [_pageIndicator removeFromSuperview];
    }
    
    _pageIndicator = pageIndicator;
    [self reloadPageIndicator];
}


// MARK: - CollectionView delegate

// MARK: Data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.isReloadingLayout) {
        return 0;
    }
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.pages.count;
}


// MARK: Layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:self.tabView]) {
        return [self tabView:collectionView sizeForItemAtIndexPath:indexPath];
    }
    
    if ([collectionView isEqual:self.contentView]) {
        return [self contentView:collectionView sizeForItemAtIndexPath:indexPath];
    }
    
    return CGSizeZero;
}

- (__kindof DBPagerViewTabItem *)tabItemForPage:(DBPagerViewPage *)page
{
    // This method will reuse header view if already exists
    // Otherwise will ask the delegate or create the default header
    
    // FIXME
    // Non funziona se il delegate viene impostato dopo aver settato il dataSource
    // Bisogna chiedere gli header solo quando servono realmente
    // O al massimo chiederli ogni volta
    
    // Delegate tab item
    if (!page.tabItem) {
        if ([self.delegate respondsToSelector:@selector(pagerView:tabItemForPage:)]) {
            // Ask the delegate for the header view
            page.tabItem = [self.delegate pagerView:self tabItemForPage:page];
            page.tabItem.parentPagerView = self;
        }
    }
    
    // Default tab item
    if (!page.tabItem) {
        DBPagerViewTitleTabItem *defaultTabItem = [DBPagerViewTitleTabItem item];
        defaultTabItem.parentPagerView = self;
        defaultTabItem.titleLabel.text = page.title;
        defaultTabItem.titleLabel.font = self.font;
        page.tabItem = defaultTabItem;
    }
    
    // Layout update
    [page.tabItem updateLayoutWithFocusPercentage:page.focusPercentage];
    
    if ([self.delegate respondsToSelector:@selector(pagerView:didUpdateFocusOfPage:focusPercentage:)]) {
        // Delegate update
        [self.delegate pagerView:self didUpdateFocusOfPage:page focusPercentage:page.focusPercentage];
    }
    
    return page.tabItem;
}

- (CGSize)tabItemRealSizeForPage:(DBPagerViewPage *)page
{
    if (CGSizeEqualToSize(CGSizeZero, page.tabItem.realSize) == NO) {
        // Reuse stored real size
        return page.tabItem.realSize;
    }
    
    DBPagerViewTabItem *tabItem = [self tabItemForPage:page];
    
    CGSize size = [tabItem systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    UIEdgeInsets inset = page.tabItem.contentInset;
    size.width  = size.width  + inset.left + inset.right;
    size.height = size.height + inset.top  + inset.bottom;
    
    // NB: Le volte successive viene restituito un fittingSize sbagliato,
    // questo è il motivo principale per cui viene storicizzato e riutilizzato
    tabItem.realSize = size;
    
    return size;
}

- (CGSize)tabItemSizeForPage:(DBPagerViewPage *)page
{
    if (CGSizeEqualToSize(CGSizeZero, page.tabItem.displaySize) == NO) {
        // Reuse stored display size
        return page.tabItem.displaySize;
    }
    
    CGSize size = [self tabItemRealSizeForPage:page];
    CGFloat fullWidth = self.tabView.bounds.size.width;
    fullWidth = fullWidth - (self.tabView.contentInset.left + self.tabView.contentInset.right);
    
    if ([self tabItemShouldFillWidth]) {
        size.width = fullWidth / self.pages.count;
    }
    
    if (self.pages.count > 1) {
        // Max width = 80% of fullWidth
        CGFloat maxWidth = fullWidth * 0.8;
        size.width = fmin(size.width, maxWidth);
    }
    
    // Size format
    size.height = self.tabView.bounds.size.height;
    
    page.tabItem.displaySize = size;
    return page.tabItem.displaySize;
}

- (CGSize)tabView:(UICollectionView *)collectionView sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DBPagerViewPage *page = [self.pages safeObjectAtIndex:indexPath.row];
    return [self tabItemSizeForPage:page];
}

- (CGSize)contentView:(UICollectionView *)collectionView sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = self.bounds.size;
    size.height -= self.tabView.bounds.size.height;
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}


// MARK: Cells

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:self.tabView]) {
        return [self tabView:collectionView cellForItemAtIndexPath:indexPath];
    }
    
    if ([collectionView isEqual:self.contentView]) {
        return [self contentView:collectionView cellForItemAtIndexPath:indexPath];
    }
    
    return [UICollectionViewCell new];
}

- (UICollectionViewCell *)tabView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TabItemCell" forIndexPath:indexPath];
    
    DBPagerViewPage *page = [self.pages safeObjectAtIndex:indexPath.row];
    [self populateTabViewCell:cell withPage:page];
    [cell layoutIfNeeded];
    
    return cell;
}

- (UICollectionViewCell *)contentView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ContentCell" forIndexPath:indexPath];
    
    DBPagerViewPage *page = [self.pages safeObjectAtIndex:indexPath.row];
    [self populateContentViewCell:cell withPage:page];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)notifyFocusedPage:(DBPagerViewPage *)page
{
    if ([self.delegate respondsToSelector:@selector(pagerView:didFocusPage:)]) {
        [self.delegate pagerView:self didFocusPage:page];
    }
}

- (void)notifyUnfocusedPage:(DBPagerViewPage *)page
{
    if ([self.delegate respondsToSelector:@selector(pagerView:didUnfocusPage:)]) {
        [self.delegate pagerView:self didUnfocusPage:page];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:self.tabView]) {
        [self tabView:collectionView didSelectItemAtIndexPath:indexPath];
    }
}

- (void)tabView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Delegate notifications
    if ([self.delegate respondsToSelector:@selector(pagerView:didSelectPage:)]) {
        DBPagerViewPage *page = [self.pages safeObjectAtIndex:indexPath.row];
        [self.delegate pagerView:self didSelectPage:page];
    }
    
    // Content view
    CGPoint contentOffset = self.contentView.contentOffset;
    contentOffset.x = self.contentView.bounds.size.width * indexPath.row;
    [self.contentView setContentOffset:contentOffset animated:self.animationsEnabled];
}



// MARK: - ScrollView delegate

// MARK: Layout update

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // This is where all the magic happens
    
    if ([scrollView isNotEqual:self.contentView]) {
        // Not valid
        return;
    }
    
    CGFloat currentPageOffset = safeDouble(self.contentView.contentOffset.x / self.contentView.bounds.size.width);
    __unused NSInteger currentPageIndex = parseInt(currentPageOffset + 0.5);
    currentPageIndex = mantieniAllInterno(currentPageIndex, 0, self.pages.count - 1);
    
    // DBPagerViewPage *currentPage = [self.pages safeObjectAtIndex:currentPageIndex];
    
    // Pages update
    for (DBPagerViewPage *page in self.pages) {
        CGFloat focusPercentage = currentPageOffset - page.index + 1.0;
        focusPercentage = mantieniAllInterno(focusPercentage, 0.0, 2.0);
        
        if (focusPercentage > 1.0) {
            // Reverse
            focusPercentage = 1.0 - (focusPercentage - 1.0);
        }
        
        BOOL isFocused = focusPercentage > 0.9;
        page.focusPercentage = focusPercentage;
        
        // Update only if needed
        if (page.isFocused != isFocused) {
            page.isFocused = isFocused;
            
            if (isFocused) {
                [self notifyFocusedPage:page];
            } else {
                [self notifyUnfocusedPage:page];
            }
        }
    }
    
    // Tab items update
    for (DBPagerViewPage *page in self.pages) {
        DBPagerViewTabItem *tabItem = page.tabItem;
        [tabItem updateLayoutWithFocusPercentage:page.focusPercentage];
    }
    
    // Page indicator update
    if ([self.pageIndicator respondsToSelector:@selector(updateLayoutWithCurrentPageOffset:)]) {
        [self.pageIndicator updateLayoutWithCurrentPageOffset:currentPageOffset];
    }
    
    // Delegate notifications
    if ([self.delegate respondsToSelector:@selector(pagerView:didUpdateFocusOfPage:focusPercentage:)]) {
        for (DBPagerViewPage *page in self.pages) {
            [self.delegate pagerView:self didUpdateFocusOfPage:page focusPercentage:page.focusPercentage];
        }
    }
    
    // Delegate notifications
    if ([self.delegate respondsToSelector:@selector(pagerView:didUpdateCurrentPageOffset:)]) {
        // You decide what to do when the user scrolls the pager view
        // For instance you could resign the editing status of the main view and close the keyboard
        [self.delegate pagerView:self didUpdateCurrentPageOffset:currentPageOffset];
    }
    
    // Delegate notifications
    if ([self.delegate respondsToSelector:@selector(pagerViewDidScroll:)]) {
        // You decide what to do when the user scrolls the pager view
        // For instance you could resign the editing status of the main view and close the keyboard
        [self.delegate pagerViewDidScroll:self];
    }
    
    // Layout update
    [self updateLayoutWithCurrentPageOffset:currentPageOffset];
}

- (void)updateLayoutWithCurrentPageOffset:(CGFloat)currentPageOffset
{
    UIEdgeInsets safeBounds = UIEdgeInsetsZero;
    safeBounds.left = self.bounds.size.width / 2.0;
    safeBounds.right = -safeBounds.left;
    
    for (DBPagerViewPage *page in self.pages) {
        safeBounds.right += page.tabItem.displaySize.width;
    }
    
    // Try to center the current tab item
    NSInteger currentPageIndex = floor(currentPageOffset);
    NSInteger nextPageIndex    = currentPageIndex + 1;
    // Debug: NSLog(@"^^ CurrentPageOffset: %f", currentPageOffset);
    
    // DBPagerViewPage *currentPage = [self.pages safeObjectAtIndex:currentPageIndex];
    DBPagerViewPage *nextPage    = [self.pages safeObjectAtIndex:nextPageIndex];
    
    CGFloat focusPercentage = nextPage.focusPercentage;
    
    CGPoint currentOffset = CGPointZero;
    CGPoint nextOffset = CGPointZero;
    
    for (NSInteger counter = 0; counter < self.pages.count; counter++) {
        DBPagerViewPage *page = [self.pages safeObjectAtIndex:counter];
        
        if (counter < currentPageIndex) {
            currentOffset.x += page.tabItem.displaySize.width;
        }
        else if (counter == currentPageIndex) {
            nextOffset.x = currentOffset.x + page.tabItem.displaySize.width;
            currentOffset.x += page.tabItem.displaySize.width / 2.0;
        }
        
        if (counter == nextPageIndex) {
            nextOffset.x += page.tabItem.displaySize.width / 2.0;
            break;
        }
    }
    
    CGPoint targetOffset = CGPointZero;
    targetOffset.x = currentOffset.x + ((nextOffset.x - currentOffset.x) * focusPercentage);
    
    // Bounds check
    CGPoint centerOffset = targetOffset;
    if (centerOffset.x < safeBounds.left) {
        centerOffset.x = safeBounds.left;
    }
    else if (centerOffset.x > safeBounds.right) {
        centerOffset.x = safeBounds.right;
    }
    
    // Center -> Origin
    CGPoint contentOffset = centerOffset;
    contentOffset.x -= safeBounds.left;
    
    // Debug: NSLog(@"^^ ContentOffset: %@", NSStringFromCGPoint(contentOffset));
    [self.tabView setContentOffset:contentOffset];
    
    // Temporary behavior: needs improvements to handle different indicators and situations
    if ([self.pageIndicator isKindOfClass:[DBPagerViewLinePageIndicator class]]) {
        if (self.isTabViewHidden == NO) {
            switch (self.pageIndicatorPosition) {
                case DBPagerViewPageIndicatorPositionAutomatic:
                case DBPagerViewPageIndicatorPositionInsideTabView:
                case DBPagerViewPageIndicatorPositionNearTabView:
                    self.pageIndicatorView.constraintLeading = -contentOffset.x;
                    break;
                default:
                    self.pageIndicatorView.constraintLeading = 0.0;
                    break;
            }
        }
    }
}


// MARK: - TapRecognizer delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // Il TapRecognizer funziona quando lo scroll della HeaderView è bloccato
    // e viene reindirizzato sulla ContentView, perdendo così la possibilità di tappare le celle,
    // per questo ho gestito la situazione con il TapRecognizer
    
    CGPoint point = [gestureRecognizer locationInView:self.tabView];
    
    if (CGRectContainsPoint(self.tabView.bounds, point)) {
        return YES;
    }
    
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)tapRecognizer:(UITapGestureRecognizer *)recognizer
{
    if ([recognizer isEqual:self.tapRecognizer]) {
        // Prendo la cella cliccata
        CGPoint point = [recognizer locationInView:self.tabView];
        NSIndexPath *indexPath = [self.tabView indexPathForItemAtPoint:point];
        [self tabView:self.tabView didSelectItemAtIndexPath:indexPath];
    }
}


// MARK: - Header view

- (void)populateTabViewCell:(UICollectionViewCell *)cell withPage:(DBPagerViewPage *)page
{
    // Reuse the header if exists
    UIView *view = (page.tabItem) ? page.tabItem : [self tabItemForPage:page];
    [self populateCell:cell withView:view];
    
    // ContentInset
    UIEdgeInsets inset = page.tabItem.contentInset;
    NSString *formatHorizontal = [NSString stringWithFormat:@"H:|-(%.f)-[view]-(%.f)-|", inset.left, inset.right];
    NSString *formatVertical   = [NSString stringWithFormat:@"V:|-(%.f)-[view]-(%.f)-|", inset.top,  inset.bottom];
    
    // Constraints
    NSDictionary *views = NSDictionaryOfVariableBindings(view);
    [cell addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:formatHorizontal options:0 metrics:nil views:views]];
    [cell addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:formatVertical   options:0 metrics:nil views:views]];
}

- (void)populateContentViewCell:(UICollectionViewCell *)cell withPage:(DBPagerViewPage *)page
{
    UIView *view = page.view;
    [self populateCell:cell withView:view];
    
    // ContentInset
    UIEdgeInsets inset = self.contentViewInset;
    NSString *formatHorizontal = [NSString stringWithFormat:@"H:|-(%.f)-[view]-(%.f)-|", inset.left, inset.right];
    NSString *formatVertical   = [NSString stringWithFormat:@"V:|-(%.f)-[view]-(%.f)-|", inset.top,  inset.bottom];
    
    // Constraints
    NSDictionary *views = NSDictionaryOfVariableBindings(view);
    [cell addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:formatHorizontal options:0 metrics:nil views:views]];
    [cell addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:formatVertical   options:0 metrics:nil views:views]];
}

- (void)populateCell:(UICollectionViewCell *)cell withView:(UIView *)view
{
    view.translatesAutoresizingMaskIntoConstraints = NO;
    view.clipsToBounds = YES;
    [view removeFromSuperview];
    
    [cell removeAllSubviews];
    [cell setBackgroundView:nil];
    [cell setSelectedBackgroundView:nil];
    [cell addSubview:view];
}

@end
