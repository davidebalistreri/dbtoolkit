//
//  DBPagerViewDefines.h
//  File version: 1.0.0 beta
//  Last modified: 01/24/2017
//
//  Created by Davide Balistreri on 10/12/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBPagerViewDefines.h"

const DBPagerViewLayoutHeight DBPagerViewLayoutHeightAutomatic = -666.999;
