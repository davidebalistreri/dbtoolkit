//
//  DBPagerViewPage.h
//  File version: 1.0.0 beta
//  Last modified: 10/04/2016
//
//  Created by Davide Balistreri on 08/03/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBObject.h"
#import "DBPagerViewDefines.h"


// MARK: - Class define

NS_ASSUME_NONNULL_BEGIN

@interface DBPagerViewPage : DBObject

// MARK: - Class methods

+ (nullable instancetype)pageWithViewController:(nullable __kindof UIViewController *)viewController;
+ (nullable instancetype)pageWithView:(nullable __kindof UIView *)view andTitle:(nullable NSString *)title;
+ (nullable instancetype)pageWithView:(nullable __kindof UIView *)view andLocalizedTitle:(nullable NSString *)localizedTitle;


// MARK: - Instance methods

@property (weak, nonatomic, nullable) DBPagerView *parentPagerView;

// Tab item
@property (strong, nonatomic, nullable) NSString *title;

// Use the pager view delegate protocol to provide custom tab items
@property (strong, nonatomic, readonly) __kindof DBPagerViewTabItem *tabItem;

// Content view
@property (strong, nonatomic) __kindof UIView *view;

@property (strong, nonatomic) __kindof UIViewController *viewController;

// Pager view properties
@property (nonatomic) NSInteger index;

@property (nonatomic, readonly) BOOL isFirstPage;

@property (nonatomic, readonly) BOOL isLastPage;

@property (nonatomic) BOOL isFocused;
@property (nonatomic) CGFloat focusPercentage;

@end

NS_ASSUME_NONNULL_END
