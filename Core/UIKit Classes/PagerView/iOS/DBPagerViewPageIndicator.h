//
//  DBPagerViewPageIndicator.h
//  File version: 1.0.0 beta
//  Last modified: 10/04/2016
//
//  Created by Davide Balistreri on 10/03/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBView.h"
#import "DBPagerViewDefines.h"


// MARK: - DBPagerViewPageIndicator define

@interface DBPagerViewPageIndicator : DBView

@property (weak, nonatomic) DBPagerView *parentPagerView;

/// Designated initializer
+ (instancetype)indicator;

@property (nonatomic) UIEdgeInsets contentInset;

/// Override this property if you want to change the preferred position of your custom page indicator
@property (readonly, nonatomic) DBPagerViewPageIndicatorPosition preferredPosition;

- (void)reloadLayout;

/**
 * Override this method to update the indicator layout when the user scrolls the pager view
 * and changes the focused page.
 */
- (void)updateLayoutWithCurrentPageOffset:(CGFloat)currentPageOffset;

@end


// MARK: - DBPagerViewLinePageIndicator define

@interface DBPagerViewLinePageIndicator : DBPagerViewPageIndicator

@property (strong, nonatomic) UIColor *indicatorColor;

@property (strong, nonatomic) UIView *lineView;

@end


// MARK: - DBPagerViewDotsPageIndicator define

@interface DBPagerViewDotsPageIndicator : DBPagerViewPageIndicator

@property (strong, nonatomic) UIColor *selectedColor;
@property (strong, nonatomic) UIColor *unselectedColor;

@property (strong, nonatomic) UIPageControl *pageControl;

@end
