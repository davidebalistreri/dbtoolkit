//
//  DBPagerViewTabItem.h
//  File version: 1.0.0 beta
//  Last modified: 10/04/2016
//
//  Created by Davide Balistreri on 08/24/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBView.h"
#import "DBPagerViewDefines.h"


// MARK: - DBPagerViewTabItem define

@interface DBPagerViewTabItem : DBView

@property (weak, nonatomic) DBPagerView *parentPagerView;

/// Designated initializer
+ (instancetype)item;

@property (nonatomic) UIEdgeInsets contentInset;

@property (readonly, nonatomic) CGSize realSize;
@property (readonly, nonatomic) CGSize displaySize;

/**
 * Override this method to update the item layout when the user scrolls the pager view
 * and changes the focused item.
 */
- (void)updateLayoutWithFocusPercentage:(CGFloat)focusPercentage;

@end


// MARK: - DBPagerViewTitleTabItem define

@interface DBPagerViewTitleTabItem : DBPagerViewTabItem

@property (strong, nonatomic) UILabel *titleLabel;

@end


// MARK: - DBPagerViewImageTabItem define

@interface DBPagerViewImageTabItem : DBPagerViewTabItem

@property (strong, nonatomic) UIImageView *imageView;

@end


// MARK: - DBPagerViewDetailTabItem define

@interface DBPagerViewDetailTabItem : DBPagerViewTabItem

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UIImageView *imageView;

@end
