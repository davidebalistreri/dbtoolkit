//
//  DBPagerViewTabItem.m
//  File version: 1.0.0 beta
//  Last modified: 10/04/2016
//
//  Created by Davide Balistreri on 08/24/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBPagerViewTabItem.h"

#import "DBPagerView.h"
#import "DBPagerViewPage.h"

#import "DBColorExtensions.h"


// MARK: - DBPagerViewTabItem define

@implementation DBPagerViewTabItem

+ (instancetype)item
{
    return [[self class] new];
}

- (void)setupObject
{
    [super setupObject];
    self.translatesAutoresizingMaskIntoConstraints = NO;
}

// Private method: the property is readonly
- (void)setRealSize:(CGSize)realSize
{
    _realSize = realSize;
}

// Private method: the property is readonly
- (void)setDisplaySize:(CGSize)displaySize
{
    _displaySize = displaySize;
}

- (void)updateLayoutWithFocusPercentage:(CGFloat)focusPercentage
{
    // Implementare nelle classi che ereditano da questa
}

@end


// MARK: - DBPagerViewTitleTabItem define

@implementation DBPagerViewTitleTabItem

- (void)setupObject
{
    [super setupObject];
    
    // Label
    self.titleLabel = [UILabel new];
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    // self.titleLabel.numberOfLines = 0;
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.minimumScaleFactor = 0.8;
    self.titleLabel.font = [UIFont boldSystemFontOfSize:15.0];
    [self addSubview:self.titleLabel];
    
    self.contentInset = UIEdgeInsetsMake(6.0, 14.0, 6.0, 14.0);
    
    // Constraints
    UILabel *titleLabel = self.titleLabel;
    NSDictionary *views = NSDictionaryOfVariableBindings(titleLabel);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[titleLabel]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[titleLabel]|" options:0 metrics:nil views:views]];
}

- (void)updateLayoutWithFocusPercentage:(CGFloat)focusPercentage
{
    UIColor *selectedColor   = self.parentPagerView.selectedColor;
    UIColor *unselectedColor = self.parentPagerView.unselectedColor;
    
    self.titleLabel.textColor = [UIColor blendColor:unselectedColor withColor:selectedColor alpha:focusPercentage];
}

@end


// MARK: - DBPagerViewImageTabItem define

@implementation DBPagerViewImageTabItem

- (void)setupObject
{
    [super setupObject];
    
    // Da fare
}

- (void)updateLayoutWithFocusPercentage:(CGFloat)focusPercentage
{
    // Da fare
}

@end


// MARK: - DBPagerViewDetailTabItem define

@implementation DBPagerViewDetailTabItem

- (void)setupObject
{
    [super setupObject];
    
    // Label
    self.titleLabel = [UILabel new];
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    // self.titleLabel.numberOfLines = 0;
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.minimumScaleFactor = 0.8;
    self.titleLabel.font = [UIFont boldSystemFontOfSize:15.0];
    [self addSubview:self.titleLabel];
    
    // Constraints
    UILabel *titleLabel = self.titleLabel;
    NSDictionary *views = NSDictionaryOfVariableBindings(titleLabel);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[titleLabel]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[titleLabel]|" options:0 metrics:nil views:views]];
}

- (void)updateLayoutWithFocusPercentage:(CGFloat)focusPercentage
{
    // Da fare
}

@end
