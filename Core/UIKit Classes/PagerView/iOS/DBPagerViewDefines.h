//
//  DBPagerViewDefines.h
//  File version: 1.0.0 beta
//  Last modified: 01/24/2017
//
//  Created by Davide Balistreri on 10/12/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

@class DBPagerView, DBPagerViewPage, DBPagerViewTabItem, DBPagerViewPageIndicator;


// MARK: - Protocol

@protocol DBPagerViewDelegate <NSObject>

@optional

NS_ASSUME_NONNULL_BEGIN

/**
 * Respond to this delegate method to provide different layouts for tab items.
 *
 * You can return one of the default tab items included in this bundle,
 * or subclass DBPagerViewTabItem and create yours.
 *
 * Default: DBPagerViewTitleTabItem
 */
- (__kindof DBPagerViewTabItem *)pagerView:(DBPagerView *)pagerView tabItemForPage:(DBPagerViewPage *)page;

- (void)pagerView:(DBPagerView *)pagerView didFocusPage:(DBPagerViewPage *)page;
- (void)pagerView:(DBPagerView *)pagerView didUnfocusPage:(DBPagerViewPage *)page;
- (void)pagerView:(DBPagerView *)pagerView didUpdateFocusOfPage:(DBPagerViewPage *)page focusPercentage:(CGFloat)focusPercentage;

- (void)pagerViewDidScroll:(DBPagerView *)pagerView;
- (void)pagerView:(DBPagerView *)pagerView didUpdateCurrentPageOffset:(CGFloat)currentPageOffset;

- (void)pagerView:(DBPagerView *)pagerView didSelectPage:(DBPagerViewPage *)page;

NS_ASSUME_NONNULL_END

@end


// MARK: - Type constants

/**
 * Constants used to define the direction of the layout of the pager view.
 */
typedef NS_ENUM(NSUInteger, DBPagerViewLayoutDirection) {
    DBPagerViewLayoutDirectionAutomatic,
    DBPagerViewLayoutDirectionLeftToRight,
    DBPagerViewLayoutDirectionRightToLeft,
    DBPagerViewLayoutDirectionDefault = DBPagerViewLayoutDirectionAutomatic
};

/**
 * Constants used to define where the tab view, if present, will be positioned inside the pager view.
 */
typedef NS_ENUM(NSUInteger, DBPagerViewTabViewPosition) {
    DBPagerViewTabViewPositionTop,
    DBPagerViewTabViewPositionBottom,
    DBPagerViewTabViewPositionDefault = DBPagerViewTabViewPositionTop
};


/**
 * CGFloat number used to set the layout height automatically (based on the content size)
 * or fixed to a defined value.
 *
 * You can set it to DBPagerViewLayoutHeightAutomatic or passing a CGFloat value.
 */
typedef CGFloat DBPagerViewLayoutHeight;

/**
 * Automatic behavior: the height will be based on the content size
 * (or set to a default value if the measuring fails).
 */
extern const DBPagerViewLayoutHeight DBPagerViewLayoutHeightAutomatic;


/**
 * Constants used to define the size of the tab items inside the tab view.
 */
typedef NS_ENUM(NSUInteger, DBPagerViewTabItemStyle) {
    /// Automatic behavior: tab items will try to fill the tab view,
    /// but if total width exceeds the view, they will have their original width instead.
    DBPagerViewTabItemStyleAutomatic,
    /// Tab items will have their original width (size to fit).
    DBPagerViewTabItemStyleRealWidth,
    /// Tab items will fill the header view and be equally sized.
    DBPagerViewTabItemStyleFillWidth,
    /// Default. Automatic behavior.
    DBPagerViewTabItemStyleDefault = DBPagerViewTabItemStyleAutomatic
};

/**
 * Constants used to define where the page indicator, if present, will be positioned inside the pager view.
 */
typedef NS_ENUM(NSUInteger, DBPagerViewPageIndicatorPosition) {
    /// The page indicator will be anchored to the top of the pager view.
    DBPagerViewPageIndicatorPositionTop,
    
    /// The page indicator will be positioned inside the tab view, anchored to the side facing the content view.
    DBPagerViewPageIndicatorPositionInsideTabView,
    
    /// The page indicator will be positioned close to the tab view position.
    DBPagerViewPageIndicatorPositionNearTabView,
    
    /// The page indicator will be positioned on the opposite side of the tab view position.
    DBPagerViewPageIndicatorPositionAwayFromTabView,
    
    /// The page indicator will be anchored to the bottom of the pager view.
    DBPagerViewPageIndicatorPositionBottom,
    
    /// Automatic behavior based on the chosen indicator and the pager view setup.
    /// This setting works best with the provided page indicators inside this bundle.
    /// If you are using a custom page indicator, I suggest to define its position precisely and avoid using this.
    DBPagerViewPageIndicatorPositionAutomatic,
    
    /// Default. Automatic behavior.
    DBPagerViewPageIndicatorPositionDefault = DBPagerViewPageIndicatorPositionAutomatic
};
