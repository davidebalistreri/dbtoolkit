//
//  NSNavigationController.m
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 14/02/2016
//
//  Creato da Davide Balistreri il 14/02/2016
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import "NSNavigationController.h"

@implementation NSNavigationController

- (void)setViewControllers:(NSArray *)viewControllers
{
    [self setViewControllers:viewControllers animated:NO];
}

- (void)setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated
{
    for (NSViewController *viewController in viewControllers) {
        viewController.navigationController = self;
    }
    
    [super setViewControllers:viewControllers animated:animated];
}

- (void)pushViewController:(NSViewController *)viewController animated:(BOOL)animated
{
    viewController.navigationController = self;
    [super pushViewController:viewController animated:animated];
}

@end