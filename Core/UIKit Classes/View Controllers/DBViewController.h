//
//  DBViewController.h
//  File version: 1.1.2
//  Last modified: 02/27/2016
//
//  Created by Davide Balistreri on 02/17/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

/**
 * ViewController esteso per creare uno standard di funzionamento e per semplificare alcuni scenari ricorrenti.
 *
 * Quando il ViewController verrà inizializzato, verranno eseguiti in sequenza i metodi <b>impostaSchermata</b>,
 * <b>localizzaSchermata</b>, <b>aggiornaSchermata:animazione</b> e <b>aggiornaDataSource</b>,
 * per permettere allo sviluppatore di costruire la schermata per intero.
 *
 * Espone una variabile <b>inizializzazioneCompletata</b> che indica se la schermata è stata inizializzata completamente da quando è stata creata.
 *
 * Inoltre permette di utilizzare il metodo <b>aggiornaDataSource</b> per aggiornare i dati che saranno mostrati nella schermata: ad esempio
 * per scaricare nuove informazioni da un web service.
 *
 * Se dopo l'inizializzazione verrà cambiata la localizzazione di sistema attraverso la classe <i>DBLocalization</i>,
 * verrà automaticamente eseguito il metodo <b>localizzaSchermata</b>.
 */
@interface DBViewController : UIViewController

/**
 * Boolean che indica se la schermata è stata inizializzata completamente da quando è stata creata.
 *
 * Viene impostato a <b>YES</b> quando il ViewController ha terminato l'inizializzazione, è stato
 * localizzato e aggiornato, ed è visibile sullo schermo.
 */
@property (nonatomic, readonly) BOOL inizializzazioneCompletata;

/**
 * Metodo da utilizzare per inizializzare la schermata.
 *
 * Viene richiamato automaticamente quando il ViewController ha terminato l'inizializzazione.
 *
 * Deve essere eseguito un'unica volta.
 */
- (void)inizializzaSchermata;

/**
 * Metodo da utilizzare per impostare tutti i componenti della schermata:
 * ad esempio i frame delle subviews, o le immagini e i testi che non verranno cambiati.
 *
 * Viene richiamato automaticamente quando il ViewController sta per essere mostrato.
 *
 * Deve essere eseguito un'unica volta.
 */
- (void)impostaSchermata;

/**
 * Metodo per distruggere la schermata e le eventuali risorse attive.
 *
 * Viene richiamato automaticamente quando il ViewController ha iniziato la deallocazione.
 *
 * Deve essere eseguito un'unica volta.
 */
- (void)distruggiSchermata;

/**
 * Metodo da utilizzare per la localizzazione di tutti i testi della schermata.
 *
 * Viene richiamato automaticamente dopo l'esecuzione del metodo <b>impostaSchermata</b>,
 * e verrà richiamato ogni volta che verrà cambiata la lingua attarverso la classe <i>DBLocalization</i>,
 * per facilitare l'aggiornamento di tutti i testi con la nuova lingua impostata.
 */
- (void)localizzaSchermata;

/**
 * Metodo da utilizzare quando bisogna aggiornare i dati visualizzati sulla schermata, con o senza animazione.
 *
 * Viene richiamato automaticamente quando il ViewController ha terminato l'inizializzazione,
 * dopo l'esecuzione dei metodi <b>impostaSchermata</b> e <b>localizzaSchermata</b>.
 */
- (void)aggiornaSchermata:(BOOL)animazione;

/**
 * Metodo da utilizzare quando bisogna aggiornare i dati che saranno mostrati nella schermata: ad esempio
 * per scaricare nuove informazioni da un web service.
 */
- (void)aggiornaDataSource;

@end
