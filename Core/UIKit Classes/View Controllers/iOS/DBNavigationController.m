//
//  DBNavigationController.m
//  File version: 1.1.1
//  Last modified: 09/11/2016
//
//  Created by Davide Balistreri on 03/30/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBNavigationController.h"
#import "DBLocalization.h"
#import "DBThread.h"

@interface DBNavigationController () <UINavigationControllerDelegate>

@property (nonatomic, readonly) BOOL impostazioneInizialeCompletata;

@property (copy) DBNavigationControllerCompletionBlock completionBlock;

@end


@implementation DBNavigationController

// MARK: - Override dei metodi ViewController

- (void)viewDidLoad
{
    // Il ViewController ancora non è stato inizializzato completamente
    [super viewDidLoad];
    
    // Eseguo i metodi implementati dallo sviluppatore
    [self privateInizializzaSchermata];
}

#if DBFRAMEWORK_TARGET_IOS
- (void)viewWillAppear:(BOOL)animated
{
    if (self.impostazioneInizialeCompletata == NO) {
        // L'inizializzazione del ViewController è quasi completata
        
        // Eseguo i metodi implementati dallo sviluppatore
        [self privateImpostaSchermata];
        [self privateLocalizzaSchermata];
        [self privateAggiornaSchermata:NO];
    }
    else {
        [self privateAggiornaSchermata:animated];
    }
    
    _impostazioneInizialeCompletata = YES;
    
    [super viewWillAppear:animated];
}
#else
- (void)viewWillAppear
{
    if (self.impostazioneInizialeCompletata == NO) {
        // L'inizializzazione del ViewController è quasi completata
        
        // Eseguo i metodi implementati dallo sviluppatore
        [self privateImpostaSchermata];
        [self privateLocalizzaSchermata];
        [self privateAggiornaSchermata:NO];
    }
    else {
        [self privateAggiornaSchermata:NO];
    }
    
    _impostazioneInizialeCompletata = YES;
    
    [super viewWillAppear];
}
#endif

#if DBFRAMEWORK_TARGET_IOS
- (void)viewDidAppear:(BOOL)animated
{
    // Inizializzazione del ViewController completata
    _inizializzazioneCompletata = YES;
    
    [super viewDidAppear:animated];
}
#else
- (void)viewDidAppear
{
    // Inizializzazione del ViewController completata
    _inizializzazioneCompletata = YES;
    
    [super viewDidAppear];
}
#endif

- (void)dealloc
{
    [self privateDistruggiSchermata];
}

// MARK: - Metodi privati

- (void)privateInizializzaSchermata
{
    if (self.impostazioneInizialeCompletata == NO) {
        // Gestisco i vari stati del NavigationController
        self.delegate = self;
        
        // Ascolto le notifiche per i cambi di localizzazione
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(privateLocalizzaSchermata) name:kNotificaLinguaCorrenteAggiornata object:nil];
        
        // Perform the method implemented by the developer
        [self inizializzaSchermata];
    }
}

- (void)privateImpostaSchermata
{
    if (self.impostazioneInizialeCompletata == NO) {
        // Aggiorno i frame delle subviews
        // [self.view layoutIfNeeded];
        
        // Prevengo la fuoriuscita dei contenuti dalla vista
        self.view.layer.masksToBounds = YES;
        
        // Perform the method implemented by the developer
        [self impostaSchermata];
    }
}

- (void)privateDistruggiSchermata
{
    // Rimuovo il listener per i cambi di localizzazione
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificaLinguaCorrenteAggiornata object:nil];
    
    // Perform the method implemented by the developer
    [self distruggiSchermata];
}

- (void)privateLocalizzaSchermata
{
    // Perform the method implemented by the developer
    [self localizzaSchermata];
}

- (void)privateAggiornaSchermata:(BOOL)animazione
{
    // Perform the method implemented by the developer
    [self aggiornaSchermata:animazione];
}

// MARK: - Metodi da implementare dallo sviluppatore

- (void)inizializzaSchermata
{
    // Developers may override this method and provide custom behavior
}

- (void)impostaSchermata
{
    // Developers may override this method and provide custom behavior
}

- (void)distruggiSchermata
{
    // Developers may override this method and provide custom behavior
}

- (void)localizzaSchermata
{
    // Developers may override this method and provide custom behavior
}

- (void)aggiornaSchermata:(BOOL)animazione
{
    // Developers may override this method and provide custom behavior
}

- (void)aggiornaDataSource
{
    // Developers may override this method and provide custom behavior
}


// MARK: - Gestione della navigazione estesa

/**
 * Metodo esteso per poter richiamare un blocco di codice al termine dell'operazione.
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated conCompletionBlock:(DBNavigationControllerCompletionBlock)completionBlock
{
    self.completionBlock = completionBlock;
    [super pushViewController:viewController animated:animated];
}

/**
 * Metodo esteso per poter richiamare un blocco di codice al termine dell'operazione.
 */
- (NSArray<UIViewController *> *)popToRootViewControllerAnimated:(BOOL)animated conCompletionBlock:(DBNavigationControllerCompletionBlock)completionBlock
{
    self.completionBlock = completionBlock;
    return [super popToRootViewControllerAnimated:animated];
}

/**
 * Metodo esteso per poter richiamare un blocco di codice al termine dell'operazione.
 */
- (NSArray<UIViewController *> *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated conCompletionBlock:(DBNavigationControllerCompletionBlock)completionBlock
{
    self.completionBlock = completionBlock;
    return [super popToViewController:viewController animated:animated];
}

/**
 * Metodo esteso per poter richiamare un blocco di codice al termine dell'operazione.
 */
- (UIViewController *)popViewControllerAnimated:(BOOL)animated conCompletionBlock:(DBNavigationControllerCompletionBlock)completionBlock
{
    self.completionBlock = completionBlock;
    return [super popViewControllerAnimated:animated];
}

- (NSArray<UIViewController *> *)popToNuovoRootViewController:(UIViewController *)rootViewController animated:(BOOL)animated
{
    NSMutableArray *viewControllers = [self.viewControllers mutableCopy];
    [viewControllers insertObject:rootViewController atIndex:0];
    self.viewControllers = viewControllers;
    
    return [self popToRootViewControllerAnimated:animated];
}

- (NSArray<UIViewController *> *)popToNuovoRootViewController:(UIViewController *)rootViewController animated:(BOOL)animated conCompletionBlock:(DBNavigationControllerCompletionBlock)completionBlock
{
    self.completionBlock = completionBlock;
    return [self popToNuovoRootViewController:rootViewController animated:animated];
}

// MARK: - Delegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [DBThread eseguiBlock:^{
        if (self.completionBlock) {
            // Richiamo il completion block per il termine dell'operazione
            self.completionBlock();
            self.completionBlock = nil;
        }
    }];
}

@end
