//
//  DBTutorialViewController.m
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 05/30/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBTutorialViewController.h"
#import "DBFramework.h"

@interface DBTutorialViewController ()

@property (strong, nonatomic) UIImageView *imageCerchio;
@property (strong, nonatomic) UIImageView *imageMano;

@end


@implementation DBTutorialViewController

- (void)impostaSchermata
{
    [super impostaSchermata];
    
    //    self.imageCerchio.tintColor = kColoreArancione;
    //    self.imageCerchio.image = [self.imageCerchio.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (void)impostaTitolo:(NSString *)titolo descrizione:(NSString *)descrizione
{
    self.labelTitolo.text = titolo;
    self.labelDescrizione.text = descrizione;
    
    [self.labelDescrizione adattaAltezza];
}

- (void)spostaCerchioConFrame:(CGRect)frame
{
    CGPoint centro = CGPointMake(frame.origin.x + (frame.size.width / 2.0), frame.origin.y + (frame.size.height / 2.0));
    self.imageCerchio.center = centro;
    [self ritagliaCerchio];
}

- (void)ritagliaCerchio
{
    CGFloat larghezza = self.imageCerchio.larghezza;
    CGRect frame = self.imageCerchio.frame;
    frame.size.height = frame.size.height / 2.14;
    frame.size.width  = frame.size.width  / 2.14;
    frame.origin.x += (larghezza / 2.0) - (frame.size.width / 2.0);
    frame.origin.y += (larghezza / 2.0) - (frame.size.width / 2.0);
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.view.bounds cornerRadius:0];
    UIBezierPath *cerchioPath = [UIBezierPath bezierPathWithRoundedRect:frame cornerRadius:frame.size.height / 2.0];
    [path appendPath:cerchioPath];
    [path setUsesEvenOddFillRule:YES];
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = path.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [UIColor grayColor].CGColor;
    fillLayer.opacity = 1.0;
    
    //    CAShapeLayer *cerchio = [CAShapeLayer layer];
    //    cerchio.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(10, 10, 100, 100) cornerRadius:frame.size.height / 2.0].CGPath;
    //    cerchio.fillColor = [UIColor blackColor].CGColor;
    //    cerchio.fillRule = kCAFillRuleEvenOdd;
    //    [maschera appendPath:cerchio];
    
    self.view.layer.mask = fillLayer;
}

- (void)buttonAvanti:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(clickedButtonAvantiTutorialViewController:)])
        [self.delegate clickedButtonAvantiTutorialViewController:self];
}

@end
