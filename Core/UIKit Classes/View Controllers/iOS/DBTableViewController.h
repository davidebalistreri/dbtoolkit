//
//  DBTableViewController.h
//  File version: 2.0.0
//  Last modified: 02/25/2016
//
//  Created by Davide Balistreri on 02/18/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#if DBFRAMEWORK_VERSION_EARLIER_THAN(__DBFRAMEWORK_0_7_0)
    #warning Bisogna adattare il codice DBTableViewController alla nuova versione del Framework. \
    DBTableViewController adesso estende da DBViewController. \
    Sono cambiati i metodi di Gestione delle celle. \
    Il modulo è stato tradotto in inglese.
#endif


#import "DBViewController.h"
#import "DBTableViewHandler.h"
#import "DBTableViewCell.h"
#import "DBDataSource.h"

/**
 * Questa classe rispetta e amplia i principi di funzionamento del DBViewController, permettendo di gestire facilmente una TableView.
 *
 * L'utilizzo della classe è molto semplice: basta estenderla e creare nella sottoclasse una property UITableView *tableView,
 * un IBOutlet andrà benissimo, e implementare i metodi per controllare le celle della TableView.
 */
@interface DBTableViewController : DBViewController

NS_ASSUME_NONNULL_BEGIN

// MARK: - Gestione del DataSource

/**
 * La fonte dati della TableView.
 * Attualmente è possibile impostare due tipi di dataSource: NSArray e DBDataSource.
 */
@property (nullable, strong, nonatomic) id dataSource;

/**
 * L'IndexPath dell'ultima cella selezionata.
 */
@property (nullable, weak, nonatomic, readonly) NSIndexPath *selectedIndexPath;

/**
 * Permette di gestire automaticamente l'altezza delle celle in base alla loro dimensione.
 * Default: attivato.
 */
@property (nonatomic) BOOL useSelfSizingCells;

/**
 * Permette di nascondere automaticamente il separatore dalle celle vuote.
 * Default: attivato.
 */
@property (nonatomic) BOOL hidesEmptyCellsSeparator;


// MARK: - Gestione della TableView

/**
 * Utilizzare questo metodo per forzare la TableView che sarà gestita da questa classe.
 *
 * NB: Questa classe va automaticamente alla ricerca di una property UITableView *tableView implementata nelle sottoclassi:
 * nel caso in cui essa fosse presente non sarà necessario forzare la TableView da gestire.
 */
- (void)useTableView:(UITableView *)tableView;


/// Permette di accedere all'eventuale RefreshControl della TableView.
@property (nullable, strong, nonatomic, readonly) UIRefreshControl *refreshControl;

@property (nonatomic, getter=isRefreshControlEnabled) BOOL refreshControlEnabled;

- (void)setRefreshControlEnabled:(BOOL)refreshControlEnabled target:(nullable id)target selector:(SEL)selector;


- (void)setScrollToTopEnabled:(BOOL)enabled;


// MARK: - Gestione delle celle

/**
 * Implementare questo metodo per gestire il tipo di cella che verrà istanziata in base all'oggetto o all'IndexPath.
 * <p><i>Funzionamento dettagliato nel protocollo DBTableViewHandlerDelegate.</i></p>
 */
- (nullable UITableViewCell *)tableView:(UITableView *)tableView cellForObject:(id)object atIndexPath:(NSIndexPath *)indexPath;

/**
 * Implementare questo metodo per gestire la selezione delle celle della TableView. <br>
 * <p><i>Funzionamento dettagliato nel protocollo DBTableViewHandlerDelegate.</i></p>
 */
- (void)tableView:(UITableView *)tableView didSelectCell:(UITableViewCell *)cell withObject:(id)object atIndexPath:(NSIndexPath *)indexPath;


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath;

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;

NS_ASSUME_NONNULL_END

@end


@interface DBTableViewController (DBTableViewControllerDeprecated)

/// Permette di aggiungere facilmente il RefreshControl alla TableView.
- (void)enableRefreshControl:(BOOL)enable
    DBFRAMEWORK_DEPRECATED(0.7.2, "please use -[DBTableViewController setRefreshControlEnabled:]");

/// Permette di abilitare lo ScrollToTop e di disabilitarlo alle altre ScrollView concorrenti.
- (void)enableScrollToTop:(BOOL)enable
    DBFRAMEWORK_DEPRECATED(0.7.2, "please use -[DBTableViewController setScrollToTopEnabled:]");

@end
