//
//  DBTabBarController.m
//  File version: 1.1.0
//  Last modified: 02/25/2016
//
//  Created by Davide Balistreri on 03/30/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBTabBarController.h"
#import "DBLocalization.h"

@implementation DBTabBarController

// MARK: - Override dei metodi ViewController

- (void)viewDidLoad
{
    // Il ViewController ancora non è stato inizializzato completamente
    [super viewDidLoad];
    
    // Eseguo i metodi implementati dallo sviluppatore
    [self privateInizializzaSchermata];
}

#if DBFRAMEWORK_TARGET_IOS
- (void)viewWillAppear:(BOOL)animated
{
    if (self.inizializzazioneCompletata == NO) {
        // L'inizializzazione del ViewController è quasi completata
        
        // Eseguo i metodi implementati dallo sviluppatore
        [self privateImpostaSchermata];
        [self privateLocalizzaSchermata];
        [self privateAggiornaSchermata:NO];
    }
    else {
        [self privateAggiornaSchermata:animated];
    }
    
    _inizializzazioneCompletata = YES;
    
    [super viewWillAppear:animated];
}
#else
- (void)viewWillAppear
{
    if (self.inizializzazioneCompletata == NO) {
        // L'inizializzazione del ViewController è quasi completata
        
        // Eseguo i metodi implementati dallo sviluppatore
        [self privateImpostaSchermata];
        [self privateLocalizzaSchermata];
        [self privateAggiornaSchermata:NO];
    }
    else {
        [self privateAggiornaSchermata:NO];
    }
    
    _inizializzazioneCompletata = YES;
    
    [super viewWillAppear];
}
#endif

#if DBFRAMEWORK_TARGET_IOS
- (void)viewDidAppear:(BOOL)animated
{
    // Inizializzazione del ViewController completata
    _inizializzazioneCompletata = YES;
    
    [super viewDidAppear:animated];
}
#else
- (void)viewDidAppear
{
    // Inizializzazione del ViewController completata
    _inizializzazioneCompletata = YES;
    
    [super viewDidAppear];
}
#endif

- (void)dealloc
{
    [self privateDistruggiSchermata];
}

// MARK: - Metodi privati

- (void)privateInizializzaSchermata
{
    // Imposto il delegate della TabBar
    self.delegate = self;
    
    // Ascolto le notifiche per i cambi di localizzazione
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(privateLocalizzaSchermata) name:kNotificaLinguaCorrenteAggiornata object:nil];
    
    // Perform the method implemented by the developer
    [self inizializzaSchermata];
}

- (void)privateImpostaSchermata
{
    // Aggiorno i frame delle subviews
    [self.view layoutIfNeeded];
    
    // Prevengo la fuoriuscita dei contenuti dalla vista
    self.view.layer.masksToBounds = YES;
    
    // Perform the method implemented by the developer
    [self impostaSchermata];
}

- (void)privateDistruggiSchermata
{
    // Rimuovo il listener per i cambi di localizzazione
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificaLinguaCorrenteAggiornata object:nil];
    
    // Perform the method implemented by the developer
    [self distruggiSchermata];
}

- (void)privateLocalizzaSchermata
{
    // Perform the method implemented by the developer
    [self localizzaSchermata];
}

- (void)privateAggiornaSchermata:(BOOL)animazione
{
    // Perform the method implemented by the developer
    [self aggiornaSchermata:animazione];
}

// MARK: - Metodi da implementare dallo sviluppatore

- (void)inizializzaSchermata
{
    // Developers may override this method and provide custom behavior
}

- (void)impostaSchermata
{
    // Developers may override this method and provide custom behavior
}

- (void)distruggiSchermata
{
    // Developers may override this method and provide custom behavior
}

- (void)localizzaSchermata
{
    // Developers may override this method and provide custom behavior
}

- (void)aggiornaSchermata:(BOOL)animazione
{
    // Developers may override this method and provide custom behavior
}

- (void)aggiornaDataSource
{
    // Developers may override this method and provide custom behavior
}

@end
