//
//  DBTableViewController.m
//  File version: 2.0.0
//  Last modified: 02/25/2016
//
//  Created by Davide Balistreri on 02/18/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBTableViewController.h"
#import "DBLocalization.h"
#import "DBRuntimeHandler.h"
#import "DBDeprecated.h"


@interface DBTableViewController () <DBTableViewHandlerDelegate>

/// La TableView da gestire
@property (weak, nonatomic) UITableView *tableViewPrincipale;

/// Handler della TableView
@property (strong, nonatomic) DBTableViewHandler *handler;

@end


@implementation DBTableViewController

// MARK: - Override dei metodi ViewController

- (void)viewDidLoad
{
    /**
     * Questo esegue i metodi di inizializzazione DBViewController.
     */
    [super viewDidLoad];
    
    [self privateCercaTableViewPrincipale];
}

- (void)viewWillAppear:(BOOL)animated
{
    /**
     * Questo esegue i metodi di inizializzazione DBViewController.
     */
    [super viewWillAppear:animated];
    
    if (self.inizializzazioneCompletata == NO) {
        if (!self.dataSource) {
            /**
             * Il dataSource potrebbe essere stato impostato durante l'inizializzazione, ad esempio se i dati erano già presenti in locale.
             * In caso contrario, richiamo il metodo -aggiornaDataSource implementato dallo sviluppatore.
             */
            [self privateAggiornaDataSource];
        }
    }
    
    if ([self.refreshControl isRefreshing]) {
        /**
         * Il refreshControl era presente e stava eseguendo la sua animazione.
         * Solitamente un viewWillAppear indica che la schermata è passata da invisibile a visibile,
         * e in questo caso il refreshControl potrebbe aver interrotto la sua animazione.
         */
        [self.refreshControl beginRefreshing];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableViewPrincipale deselectRowAtIndexPath:self.selectedIndexPath animated:YES];
}


// MARK: - Metodi privati

- (void)privateDistruggiSchermata
{
    // Rimuovo il controllo della TableView
    [self privateRimuoviTableView];
    
    // Rimuovo il listener per i cambi di localizzazione
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificaLinguaCorrenteAggiornata object:nil];
    
    // Perform the method implemented by the developer
    [self distruggiSchermata];
}

- (void)privateAggiornaSchermata:(BOOL)animazione
{
    /// DA RIVEDERE
    // if (self.isInViewDidLoad || self.isAspettandoDataSource)
    //     return;
    //
    // // Se questo metodo è stato richiamato entro un secondo dal viewDidLoad, tolgo il refreshControl senza eseguire l'animazione
    // NSTimeInterval actualTime = [NSDate timeIntervalSinceReferenceDate] - 1;
    // if (self.checkTimeViewDidLoad && actualTime < self.timeViewDidLoad) {
    //     self.tableView.contentOffset = CGPointZero;
    //     self.checkTimeViewDidLoad = NO;
    // }
    
    [self.tableViewPrincipale reloadData];
    
    /// DA RIVEDERE
    // L'utente ha richiesto di aggiornare la schermata, diciamo che non sta più aspettando il dataSource
    // self.isAspettandoDataSource = NO;
    
    if (self.inizializzazioneCompletata) {
        if ([self overridesSelector:@selector(aggiornaDataSource) ofClass:[DBTableViewController class]]) {
            // [self enableRefreshControl:YES];
            // [self.refreshControl beginRefreshing];
        }
    }
    
    // Perform the method implemented by the developer
    [self aggiornaSchermata:animazione];
}

- (void)privateAggiornaDataSource
{
    // Perform the method implemented by the developer
    [self aggiornaDataSource];
}

@synthesize tableViewPrincipale = _tableViewPrincipale;
- (void)setTableViewPrincipale:(UITableView *)tableViewPrincipale
{
    if (_tableViewPrincipale) {
        // C'era già una TableView impostata, gestisco il cambiamento di TableView
        [self privateRimuoviTableView];
    }
    
    _tableViewPrincipale = tableViewPrincipale;
    [self privateInizializzaTableView];
}

- (void)privateInizializzaTableView
{
    [self setScrollToTopEnabled:YES];
    
    self.handler = [DBTableViewHandler handlerWithTableView:self.tableViewPrincipale dataSource:self.dataSource delegate:self];
}

- (void)privateRimuoviTableView
{
    [self setRefreshControlEnabled:NO];
    
    [DBTableViewHandler destroyHandlerWithTableView:self.tableViewPrincipale];
}

- (void)privateCercaTableViewPrincipale
{
    if (!self.tableViewPrincipale) {
        // Vado alla ricerca della property UITableView *tableView nelle sottoclassi
        
        if ([self respondsToSelector:@selector(tableView)]) {
            // Trovata, controllo se è una UITableView
            id tableView = [self performSelector:@selector(tableView) withObject:nil];
            
            if ([tableView isKindOfClass:[UITableView class]]) {
                // Tutto ok
                self.tableViewPrincipale = tableView;
            }
        }
    }
}

@synthesize dataSource = _dataSource;
- (id)dataSource
{
    return _dataSource;
}

- (void)setDataSource:(id)dataSource
{
    _dataSource = dataSource;
    self.handler.dataSource = dataSource;
}

- (NSIndexPath *)selectedIndexPath
{
    return self.handler.selectedIndexPath;
}

- (BOOL)useSelfSizingCells
{
    return self.handler.useSelfSizingCells;
}

- (void)setUseSelfSizingCells:(BOOL)useSelfSizingCells
{
    self.handler.useSelfSizingCells = useSelfSizingCells;
}

- (BOOL)hidesEmptyCellsSeparator
{
    return self.handler.hidesEmptyCellsSeparator;
}

- (void)setHidesEmptyCellsSeparator:(BOOL)hidesEmptyCellsSeparator
{
    self.handler.hidesEmptyCellsSeparator = hidesEmptyCellsSeparator;
}


// MARK: - Public methods

- (void)useTableView:(UITableView *)tableView
{
    self.tableViewPrincipale = tableView;
}

- (BOOL)isRefreshControlEnabled
{
    return (self.refreshControl) ? YES : NO;
}

- (void)setRefreshControlEnabled:(BOOL)refreshControlEnabled
{
    id target = self;
    SEL selector = @selector(tableViewRefreshControlDidStartRefreshing);
    [self setRefreshControlEnabled:refreshControlEnabled target:target selector:selector];
}

- (void)setRefreshControlEnabled:(BOOL)refreshControlEnabled target:(id)target selector:(SEL)selector
{
    if (refreshControlEnabled && !self.refreshControl) {
        // Adding refresh control
        _refreshControl = [UIRefreshControl new];
        
        [self.tableViewPrincipale insertSubview:self.refreshControl atIndex:0];
        [self.refreshControl beginRefreshing];
        
        // Per farlo funzionare anche quando il data source è vuoto
        [self.tableViewPrincipale setAlwaysBounceVertical:YES];
    }
    else if (!refreshControlEnabled && self.refreshControl) {
        // Removing refresh control
        [self.refreshControl removeFromSuperview];
        [self.refreshControl removeTarget:nil action:nil forControlEvents:UIControlEventValueChanged];
        _refreshControl = nil;
    }
    
    if (refreshControlEnabled) {
        [self.refreshControl addTarget:target action:selector forControlEvents:UIControlEventValueChanged];
    }
}

- (void)setScrollToTopEnabled:(BOOL)enabled
{
    if (enabled) {
        // Disabilito lo ScrollToTop delle altre ScrollView
        for (UIScrollView *view in [self.tableViewPrincipale.superview subviews]) {
            if ([view isMemberOfClass:[UIScrollView class]]) {
                [view setScrollsToTop:NO];
            }
        }
    }
    
    self.tableViewPrincipale.scrollsToTop = enabled;
}


// MARK: - Delegate

/**
 * Quando il RefreshControl viene azionato.
 */
- (void)tableViewRefreshControlDidStartRefreshing
{
    [self privateAggiornaDataSource];
}


// MARK: - TableView Handler

- (UITableViewCell *)handler:(DBTableViewHandler *)handler cellForObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    if ([self respondsToSelector:@selector(tableView:cellaPerOggetto:conIndexPath:)]) {
        // Retrocompatibilità
        UITableViewCell *cell = nil;
        
        DBFRAMEWORK_DEPRECATED_CUSTOM_LOG("-[DBTableViewController tableView:cellaPerOggetto:conIndexPath:]", 0.7.0, "utilizza il metodo -[DBTableViewController tableView:cellForObject:atIndexPath:]");
        
        DBFRAMEWORK_SUPPRESS_DEPRECATED_WARNING(cell = [self tableView:handler.tableView cellaPerOggetto:object conIndexPath:indexPath]);
        
        return cell;
    }
    
    return [self tableView:handler.tableView cellForObject:object atIndexPath:indexPath];
}

- (void)handler:(DBTableViewHandler *)handler didSelectCell:(UITableViewCell *)cell withObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    if ([self respondsToSelector:@selector(tableView:cellaSelezionata:conOggetto:conIndexPath:)]) {
        // Retrocompatibilità
        DBFRAMEWORK_DEPRECATED_CUSTOM_LOG("-[DBTableViewController tableView:cellaSelezionata:conOggetto:conIndexPath:]", 0.7.0, "utilizza il metodo -[DBTableViewController tableView:didSelectCell:withObject:atIndexPath:]");
        
        DBFRAMEWORK_SUPPRESS_DEPRECATED_WARNING([self tableView:handler.tableView cellaSelezionata:(id)cell conOggetto:object conIndexPath:indexPath]);
    }
    
    [self tableView:handler.tableView didSelectCell:cell withObject:object atIndexPath:indexPath];
}

- (void)handlerDidReloadTableView:(DBTableViewHandler *)handler
{
    // Rimuovo il RefreshControl
    if (self.refreshControl && [self.refreshControl isRefreshing]) {
        [self.refreshControl endRefreshing];
    }
}


// MARK: - Metodi da implementare dallo sviluppatore

- (UITableViewCell *)tableView:(UITableView *)tableView cellForObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    // Developers may override this method and provide custom behavior
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectCell:(UITableViewCell *)cell withObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    // Developers may override this method and provide custom behavior
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Developers may override this method and provide custom behavior
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Developers may override this method and provide custom behavior
}

@end


@implementation DBTableViewController (DBTableViewControllerDeprecated)

- (void)enableRefreshControl:(BOOL)enable
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please use -[DBTableViewController setRefreshControlEnabled:]");
    [self setRefreshControlEnabled:enable];
}

- (void)enableScrollToTop:(BOOL)enable
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please use -[DBTableViewController setRefreshControlEnabled:]");
    [self setScrollToTopEnabled:enable];
}

@end
