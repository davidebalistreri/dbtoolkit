//
//  DBTutorialViewController.h
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 05/30/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBViewController.h"

@class DBTutorialViewController;

@protocol DBTutorialViewControllerDelegate <NSObject>

- (void)clickedButtonAvantiTutorialViewController:(DBTutorialViewController *)tutorialViewController;

@end


@interface DBTutorialViewController : DBViewController

@property (weak, nonatomic) id<DBTutorialViewControllerDelegate> delegate;

- (void)impostaTitolo:(NSString *)titolo descrizione:(NSString *)descrizione;
- (void)spostaCerchioConFrame:(CGRect)frame;

// MARK: - Outlets

@property (strong, nonatomic) UILabel *labelTitolo;
@property (strong, nonatomic) UILabel *labelDescrizione;

@property (strong, nonatomic) UIButton *buttonAvanti;
- (void)buttonAvanti:(id)sender;

@end
