//
//  DBPageController.h
//  File version: 1.0.1
//  Last modified: 12/02/2015
//
//  Created by Davide Balistreri on 11/30/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@protocol DBPageControllerDelegate <NSObject>

@optional

- (void)pageControllerDidShowItem:(id)item atIndex:(NSUInteger)index;
- (void)pageControllerDidSelectItem:(id)item atIndex:(NSUInteger)index;

@end


@interface DBPageController : NSObject

@property (strong, nonatomic) UIView *view;
@property (nonatomic) NSUInteger selectedIndex;

@property (weak, nonatomic) id<DBPageControllerDelegate> delegate;

@property (strong, nonatomic) NSArray *viewControllers;
- (void)aggiungiViewController:(UIViewController *)viewController;
- (void)rimuoviViewControllers;

- (void)mostraViewControllerConIndex:(NSUInteger)index animazione:(BOOL)animazione;
- (UIViewController *)viewControllerAtIndex:(NSUInteger)index;

@property (strong, nonatomic) UIColor *tintColor;

@property (nonatomic) BOOL mostraIndicatorePagina;
@property (nonatomic, getter = isSenzaAnimazione) BOOL senzaAnimazione;
@property (nonatomic, getter = isScorrimentoInvertito) BOOL scorrimentoInvertito;

// MARK: Gestione pagine

- (UIImageView *)aggiungiImageViewControllerConImmagine:(UIImage *)immagine;

@end
