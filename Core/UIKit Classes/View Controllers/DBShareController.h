//
//  DBShareController.h
//  File version: 1.1.0
//  Last modified: 02/14/2016
//
//  Created by Davide Balistreri on 04/21/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

typedef NS_ENUM(NSUInteger, DBShareControllerResult) {
    DBShareControllerResultSuccess,
    DBShareControllerResultFailure
};

typedef void (^DBShareControllerCompletionBlock)(DBShareControllerResult result);


@protocol DBShareControllerDelegate <NSObject>

/// Da documentare
- (void)shareControllerDidFinishWithResult:(DBShareControllerResult)result;

@end


@interface DBShareController : NSObject

+ (BOOL)isMailDisponibile;
+ (BOOL)isFacebookDisponibile;
+ (BOOL)isTwitterDisponibile;
+ (BOOL)isInstagramDisponibile;

// MARK: - Mail

+ (BOOL)creaMail;

+ (BOOL)creaMailConAllegati:(NSArray *)allegati;

+ (BOOL)creaMailConDestinatari:(NSArray *)destinatari conOggetto:(NSString *)oggetto conMessaggio:(NSString *)messaggio isHTML:(BOOL)isHTML;

+ (BOOL)creaMailConDestinatari:(NSArray *)destinatari conOggetto:(NSString *)oggetto conMessaggio:(NSString *)messaggio isHTML:(BOOL)isHTML conAllegati:(NSArray *)allegati;

+ (BOOL)creaMailConOggetto:(NSString *)oggetto messaggio:(NSString *)messaggio isHTML:(BOOL)isHTML allegati:(NSArray *)allegati destinatari:(NSArray *)destinatari suViewController:(UIViewController *)viewController;

+ (BOOL)creaMailConOggetto:(NSString *)oggetto messaggio:(NSString *)messaggio isHTML:(BOOL)isHTML allegati:(NSArray *)allegati destinatari:(NSArray *)destinatari suViewController:(UIViewController *)viewController conCompletionBlock:(DBShareControllerCompletionBlock)completionBlock;

// MARK: - Facebook

+ (BOOL)creaFacebookPost;

+ (BOOL)creaFacebookPostConTesto:(NSString *)testo URL:(NSURL *)URL immagine:(UIImage *)immagine;

+ (BOOL)creaFacebookPostConTesto:(NSString *)testo URL:(NSURL *)URL immagine:(UIImage *)immagine suViewController:(UIViewController *)viewController;

+ (BOOL)creaFacebookPostConTesto:(NSString *)testo URL:(NSURL *)URL immagine:(UIImage *)immagine suViewController:(UIViewController *)viewController conCompletionBlock:(DBShareControllerCompletionBlock)completionBlock;

// MARK: - Twitter

+ (BOOL)creaTwitterPost;

+ (BOOL)creaTwitterPostConTesto:(NSString *)testo URL:(NSURL *)URL immagine:(UIImage *)immagine;

+ (BOOL)creaTwitterPostConTesto:(NSString *)testo URL:(NSURL *)URL immagine:(UIImage *)immagine suViewController:(UIViewController *)viewController;

+ (BOOL)creaTwitterPostConTesto:(NSString *)testo URL:(NSURL *)URL immagine:(UIImage *)immagine suViewController:(UIViewController *)viewController conCompletionBlock:(DBShareControllerCompletionBlock)completionBlock;

@end
