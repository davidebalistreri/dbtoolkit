//
//  DBTableViewMessageCell.m
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 09/19/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBTableViewMessageCell.h"
#import "DBFramework.h"

@implementation DBTableViewMessageCell

+ (instancetype)cella
{
    return [self cellaConMessaggio:@""];
}

+ (instancetype)cellaConMessaggio:(NSString *)messaggio
{
    DBTableViewMessageCell *cella = [self new];
    cella.labelMessaggio.text = [NSString safeString:messaggio];
    return cella;
}

+ (instancetype)cellaConMessaggioLocalizzato:(NSString *)messaggio
{
    return [self cellaConMessaggio:DBLocalizedString(messaggio)];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _labelMessaggio = self.textLabel;
        [self loadView];
    }
    return self;
}

- (void)loadView
{
    self.labelMessaggio.frame = [DBGeometry frame:self.contentView.bounds conPadding:20.0];
    self.labelMessaggio.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.labelMessaggio.font = [UIFont boldSystemFontOfSize:20.0];
    self.labelMessaggio.textColor = [DBUtility tintColor];
    self.labelMessaggio.numberOfLines = 0;
    self.labelMessaggio.textAlignment = NSTextAlignmentCenter;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
