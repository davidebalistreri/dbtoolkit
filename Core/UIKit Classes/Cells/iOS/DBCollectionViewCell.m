//
//  DBCollectionViewCell.m
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 11/26/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBCollectionViewCell.h"
#import "DBLocalization.h"

@interface DBCollectionViewCell ()

@property (nonatomic, readonly) BOOL impostazioneInizialeCompletata;

@end


@implementation DBCollectionViewCell

// MARK: - Override dei metodi di sistema

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Initialization code
    [self privateInizializzaCella];
}

- (void)layoutSubviews
{
    if (self.impostazioneInizialeCompletata == NO) {
        // L'inizializzazione è quasi completata
        
        // Eseguo i metodi implementati dallo sviluppatore
        [self privateImpostaCella];
        [self privateLocalizzaCella];
        [self privateAggiornaCella:YES];
        
        _impostazioneInizialeCompletata = YES;
    }
    
    [super layoutSubviews];
}

- (void)dealloc
{
    [self privateDistruggiCella];
}

// MARK: - Metodi privati

- (void)privateInizializzaCella
{
    if (self.impostazioneInizialeCompletata == NO) {
        // Ascolto le notifiche per i cambi di localizzazione
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(privateLocalizzaCella) name:kNotificaLinguaCorrenteAggiornata object:nil];
        
        // Perform the method implemented by the developer
        [self inizializzaCella];
    }
}

- (void)privateImpostaCella
{
    if (self.impostazioneInizialeCompletata == NO) {
        // Prevengo la fuoriuscita dei contenuti dalla vista
        self.layer.masksToBounds = YES;
        
        // Perform the method implemented by the developer
        [self impostaCella];
    }
}

- (void)privateDistruggiCella
{
    // Rimuovo il listener per i cambi di localizzazione
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificaLinguaCorrenteAggiornata object:nil];
}

- (void)privateLocalizzaCella
{
    // Perform the method implemented by the developer
    [self localizzaCella];
}

- (void)privateAggiornaCella:(BOOL)animazione
{
    // Perform the method implemented by the developer
    [self aggiornaCella:animazione];
}

// MARK: - Metodi da implementare dallo sviluppatore

- (void)inizializzaCella
{
    // Developers may override this method and provide custom behavior
}

- (void)impostaCella
{
    // Developers may override this method and provide custom behavior
}

- (void)distruggiCella
{
    // Developers may override this method and provide custom behavior
}

- (void)localizzaCella
{
    // Developers may override this method and provide custom behavior
}

- (void)aggiornaCella:(BOOL)animazione
{
    // Developers may override this method and provide custom behavior
}

- (void)configuraConOggetto:(id)oggetto
{
    // Developers may override this method and provide custom behavior
}

- (void)configureWithObject:(id)object
{
    // Developers may override this method and provide custom behavior
}

@end
