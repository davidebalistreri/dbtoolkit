//
//  DBTableViewCell.h
//  File version: 1.1.0
//  Last modified: 03/08/2016
//
//  Created by Davide Balistreri on 09/07/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@interface DBTableViewCell : UITableViewCell

/**
 * L'oggetto che viene rappresentato dal contenuto della cella.
 */
@property (weak, nonatomic) id oggetto;

/**
 * L'indice della cella sulla sua TableView.
 */
@property (weak, nonatomic) NSIndexPath *indexPath;

/**
 * Viene impostato a <b>YES</b> se la cella è l'ultima mostrata sulla sua TableView.
 */
@property (nonatomic, getter = isUltimaCella) BOOL ultimaCella;


/**
 * Boolean che indica se la cella è stata inizializzata completamente da quando è stata creata.
 *
 * Viene impostato a <b>YES</b> quando la cella ha terminato l'inizializzazione, è stata
 * localizzata e aggiornata, ed è visibile sullo schermo.
 */
@property (nonatomic, readonly) BOOL inizializzazioneCompletata;


/**
 * Metodo da utilizzare per inizializzare la cella.
 *
 * Viene richiamato automaticamente quando la cella ha terminato l'inizializzazione.
 *
 * Viene eseguito un'unica volta.
 */
- (void)inizializzaCella;

/**
 * Metodo da utilizzare per impostare tutti i componenti della cella:
 * ad esempio la parte grafica che non verrà mai cambiata.
 *
 * Viene richiamato automaticamente quando la cella sta per essere mostrata.
 *
 * Viene eseguito un'unica volta.
 */
- (void)impostaCella;

/**
 * Metodo per distruggere la schermata e le eventuali risorse attive.
 *
 * Viene richiamato automaticamente quando la cella ha iniziato la deallocazione.
 *
 * Viene eseguito un'unica volta.
 */
- (void)distruggiCella;

/**
 * Metodo da utilizzare per la localizzazione di tutti i testi della cella.
 *
 * Viene richiamato automaticamente dopo l'esecuzione del metodo <b>impostaCella</b>,
 * e verrà richiamato ogni volta che verrà cambiata la lingua attarverso la classe <i>DBLocalization</i>,
 * per facilitare l'aggiornamento di tutti i testi con la nuova lingua impostata.
 */
- (void)localizzaCella;

/**
 * Metodo da utilizzare quando si vuole aggiornare i dati visualizzati sulla cella, con o senza animazione.
 *
 * Viene richiamato automaticamente quando la cella ha terminato l'inizializzazione,
 * dopo l'esecuzione dei metodi <b>impostaCella</b> e <b>localizzaCella</b>.
 */
- (void)aggiornaCella:(BOOL)animazione;

/**
 * Metodo da utilizzare per caricare i dati da visualizzare sulla cella.
 */
- (void)configuraConOggetto:(id)oggetto;

/**
 * Metodo da utilizzare per caricare i dati da visualizzare sulla cella.
 * FIXME: Devo deprecare quello in italiano!
 */
- (void)configureWithObject:(id)object;

@end
