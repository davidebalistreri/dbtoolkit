//
//  DBTableViewMessageCell.h
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 09/19/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBTableViewCell.h"

@interface DBTableViewMessageCell : DBTableViewCell

@property (weak, nonatomic, readonly) UILabel *labelMessaggio;

+ (instancetype)cella;
+ (instancetype)cellaConMessaggio:(NSString *)messaggio;
+ (instancetype)cellaConMessaggioLocalizzato:(NSString *)messaggio;

@end
