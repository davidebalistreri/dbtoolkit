//
//  DBGeometry.h
//  File version: 1.1.2
//  Last modified: 03/13/2016
//
//  Created by Davide Balistreri on 03/31/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#define kPolliciSchermo_iPhone4     3.5
#define kPolliciSchermo_iPhone5     4.0
#define kPolliciSchermo_iPhone6     4.7
#define kPolliciSchermo_iPhone6plus 5.5

#define kDimensioniSchermo_iPhone4     CGSizeMake(320.0, 480.0)
#define kDimensioniSchermo_iPhone5     CGSizeMake(320.0, 568.0)
#define kDimensioniSchermo_iPhone6     CGSizeMake(375.0, 667.0)
#define kDimensioniSchermo_iPhone6plus CGSizeMake(414.0, 736.0)


@interface DBGeometry : NSObject

/**
 * Le varie modalità di esecuzione di un thread.
 */
typedef NS_ENUM(NSUInteger, DBGeometrySistemaCoordinate) {
    /// Il sistema di coordinate nativo della piattaforma su cui si esegue l'applicazione.
    DBGeometrySistemaCoordinateNativo,
    /// Il sistema di coordinate di iOS, le cui assi X e Y hanno origine dall'angolo in alto a sinistra dell'area degli oggetti.
    DBGeometrySistemaCoordinateiOS,
    /// Il sistema di coordinate di iOS, le cui assi X e Y hanno origine dall'angolo in basso a sinistra dell'area degli oggetti.
    DBGeometrySistemaCoordinateOSX
};


/**
 * Imposta il sistema di coordinate da utilizzare quando si interagisce con il posizionamento degli oggetti attraverso i metodi di questa classe.
 *
 * I metodi che rispettano questa impostazione sono esclusivamente quelli dell'estensione View: modificando direttamente i CGRect dei frame è impossibile tener conto del sistema di coordinate, perché è necessario un riferimento alla vista e alla sua superview.
 *
 * Di default è attivo il sistema nativo della piattaforma su cui si esegue l'applicazione.
 */
+ (void)impostaSistemaCoordinate:(DBGeometrySistemaCoordinate)sistemaCoordinate;


/**
 * Converte il frame per adattarlo al sistema di coordinate utilizzato dall'applicazione, confrontando il sistema coordinate impostato dall'utente con quello nativo dell'ambiente. <br><br>
 * NB: Questa funzione va richiamata una sola volta per adattare il frame correttamente, altrimenti potrebbe produrre risultati non attesi.
 * @param frame Il frame da convertire.
 * @param superview L'area che ospiterà il frame da convertire (la superview dell'oggetto).
 */
+ (CGRect)convertiFrame:(CGRect)frame conSuperview:(UIView *)superview;

/**
 * Converte il frame per adattarlo al sistema di coordinate utilizzato dall'applicazione, confrontando il sistema coordinate impostato dall'utente con quello nativo dell'ambiente. <br><br>
 * NB: Questa funzione va richiamata una sola volta per adattare il frame correttamente, altrimenti potrebbe produrre risultati non attesi.
 * @param frame Il frame da convertire.
 * @param frameSuperview Il frame dell'area che ospiterà il frame da convertire (di solito è la superview dell'oggetto).
 */
+ (CGRect)convertiFrame:(CGRect)frame conFrameSuperview:(CGRect)frameSuperview;


+ (CGRect)frameSchermo;
+ (CGSize)dimensioniSchermo;
+ (CGFloat)altezzaSchermo;

/**
 * Controlla se le dimensioni in pollici dello schermo del dispositivo sono <b>esattamente</b> quanto il valore specificato.
 * @param dimensioneSchermo Dimensione dello schermo in pollici
 */
+ (BOOL)isScreen:(CGFloat)dimensioneSchermo;

/**
 * Controlla se le dimensioni in pollici dello schermo del dispositivo sono <b>almeno</b> quanto il valore specificato.
 * @param dimensioneSchermo Dimensione dello schermo in pollici
 */
+ (BOOL)isScreenMin:(CGFloat)dimensioneSchermo;

/**
 * Controlla se le dimensioni in pollici dello schermo del dispositivo sono <b>al massimo</b> quanto il valore specificato.
 * @param dimensioneSchermo Dimensione dello schermo in pollici
 */
+ (BOOL)isScreenMax:(CGFloat)dimensioneSchermo;

#if DBFRAMEWORK_TARGET_IOS
+ (CGRect)frameStatusBar;
+ (CGSize)dimensioniStatusBar;
+ (CGFloat)altezzaStatusBar;
#endif

+ (CGRect)frame:(CGRect)frame conOrigine:(CGPoint)origine;
+ (CGRect)frame:(CGRect)frame conDimensioni:(CGSize)dimensioni;

+ (CGRect)frame:(CGRect)frame conOrigineX:(CGFloat)origineX;
+ (CGRect)frame:(CGRect)frame conOrigineY:(CGFloat)origineY;
+ (CGRect)frame:(CGRect)frame conLarghezza:(CGFloat)larghezza;
+ (CGRect)frame:(CGRect)frame conAltezza:(CGFloat)altezza;

+ (CGRect)frame:(CGRect)frame conPuntoDestro:(CGFloat)puntoDestro;
+ (CGRect)frame:(CGRect)frame conPuntoInferiore:(CGFloat)puntoInferiore;

+ (CGRect)frame:(CGRect)frame conPadding:(CGFloat)padding;
+ (CGRect)frame:(CGRect)frame conScale:(CGFloat)scale;

+ (CGPoint)calcolaOrigineReale:(UIView *)vista conVistaParent:(UIView *)vistaParent;

CGSize CGSizeAspectFit(CGSize aspectRatio, CGSize boundingSize);
CGSize CGSizeAspectFill(CGSize aspectRatio, CGSize minimumSize);

CGFloat CGFloatGradiInRadianti(CGFloat gradi);


// MARK: Metodi deprecati

+ (BOOL)isScreenSpecific:(CGFloat)dimensioneRichiesta DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza +[DBGeometry isScreen:]");
+ (BOOL)isScreenMinimum:(CGFloat)dimensioneRichiesta  DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza +[DBGeometry isScreenMin:]");
+ (BOOL)isScreenMaximum:(CGFloat)dimensioneRichiesta  DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza +[DBGeometry isScreenMax:]");

@end


@interface UIView (DBGeometry)

@property (nonatomic) CGPoint origine;
@property (nonatomic) CGSize  dimensioni;

@property (nonatomic) CGFloat origineX;
@property (nonatomic) CGFloat origineY;
@property (nonatomic) CGFloat larghezza;
@property (nonatomic) CGFloat altezza;

#if DBFRAMEWORK_TARGET_OSX
@property (nonatomic) CGPoint center;
#endif

@property (nonatomic) CGPoint centro;
@property (nonatomic) CGFloat centroX;
@property (nonatomic) CGFloat centroY;

@property (nonatomic) CGFloat puntoDestro;
@property (nonatomic) CGFloat puntoInferiore;

- (void)posizionaOrizzontalmenteInPercentuale:(CGFloat)percentuale rispettoAllaVista:(UIView *)vista;
- (void)posizionaVerticalmenteInPercentuale:(CGFloat)percentuale rispettoAllaVista:(UIView *)vista;

- (void)posizionaOrizzontalmenteInPercentuale:(CGFloat)percentuale rispettoAlFrame:(CGRect)frame margini:(CGFloat)margini mantieniAllInterno:(BOOL)mantieniAllInterno;
- (void)posizionaVerticalmenteInPercentuale:(CGFloat)percentuale rispettoAlFrame:(CGRect)frame margini:(CGFloat)margini mantieniAllInterno:(BOOL)mantieniAllInterno;


// MARK: - Autolayout

/// Returns YES if this view has been collapsed with the -collapseLayout method.
@property (nonatomic, readonly) BOOL isLayoutCollapsed;

/**
 * This method collapses view's height and hides from the display,
 * making it easy to accomplish and, eventually, restore the previous state
 * by calling -expandLayout.
 */
- (void)collapseLayout:(BOOL)animated;

- (void)collapseLayout;

/// Calling this method to a previously collapsed view restores its original state.
- (void)expandLayout:(BOOL)animated;

- (void)expandLayout;


/// Returns YES if this view has been collapsed with the -collapseLayoutHorizontally method.
@property (nonatomic, readonly) BOOL isLayoutCollapsedHorizontally;

/**
 * This method collapses view's width and hides from the display,
 * making it easy to accomplish and, eventually, restore the previous state
 * by calling -expandLayoutHorizontally.
 */
- (void)collapseLayoutHorizontally:(BOOL)animated;

- (void)collapseLayoutHorizontally;

/// Calling this method to a previously collapsed view restores its original state.
- (void)expandLayoutHorizontally:(BOOL)animated;

- (void)expandLayoutHorizontally;


@property (nonatomic) CGFloat constraintTop;
@property (nonatomic) CGFloat constraintLeft;  // Leading
@property (nonatomic) CGFloat constraintRight; // Trailing
@property (nonatomic) CGFloat constraintLeading;  // Left
@property (nonatomic) CGFloat constraintTrailing; // Right
@property (nonatomic) CGFloat constraintBottom;

@property (nonatomic) CGFloat constraintLarghezza;
@property (nonatomic) CGFloat constraintAltezza;

@property (nonatomic) CGFloat constraintCentroX;
@property (nonatomic) CGFloat constraintCentroY;

- (NSLayoutConstraint *)constraintConAttributo:(NSLayoutAttribute)attributo;
- (void)impostaValoreConstraint:(CGFloat)valore conAttributo:(NSLayoutAttribute)attributo;

- (void)setSubviewAutolayoutEqualWidthAndHeight:(UIView *)subview
    DBFRAMEWORK_DEPRECATED(0.7.1, "please use -[UIView(DBGeometry) setAutolayoutEqualMarginsForSubview:]");

- (void)setAutolayoutSize:(CGSize)size;

- (void)setAutolayoutEqualMarginsForSubview:(UIView *)subview;
- (void)setAutolayoutEqualSizeForSubview:(UIView *)subview;
- (void)setAutolayoutEqualCenterForSubview:(UIView *)subview;

/// Copies both contentHugging and contentCompressionResistance
- (void)copyContentLayoutPrioritiesFrom:(UIView *)view;

- (void)copyContentHuggingPrioritiesFrom:(UIView *)view;
- (void)copyContentCompressionResistancePrioritiesFrom:(UIView *)view;

@end


#if DBFRAMEWORK_TARGET_IOS

@interface UILabel (DBGeometry)

/// Adatta la larghezza mantenendo l'altezza originale
- (void)sizeToFitWidth;
/// Adatta la larghezza mantenendo l'altezza originale
- (void)adattaLarghezza;

/// Adatta l'altezza mantenendo la larghezza originale
- (void)sizeToFitHeight;
/// Adatta l'altezza mantenendo la larghezza originale
- (void)adattaAltezza;

@end


@interface UITextField (DBGeometry)

- (void)sizeToFitWidth;
- (void)adattaLarghezza;

- (void)sizeToFitHeight;
- (void)adattaAltezza;

@end


@interface UITextView (DBGeometry)

- (void)sizeToFitWidth;
- (void)adattaLarghezza;

- (void)sizeToFitHeight;
- (void)adattaAltezza;

@end

#endif
