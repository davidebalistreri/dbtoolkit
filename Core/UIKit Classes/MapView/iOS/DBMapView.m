//
//  DBMapView.m
//  File version: 1.1.0
//  Last modified: 04/18/2017
//
//  Created by Davide Balistreri on 11/22/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBMapView.h"

#import "DBGeometry.h"
#import "DBUtility.h"
#import "DBAnimation.h"
#import "DBGeolocation.h"
#import "DBObjectExtensions.h"
#import "DBDevice.h"
#import "DBThread.h"

#import "QTree.h"
#import "QCluster.h"


@interface DBMapView () <MKMapViewDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

// Se l'inizializzazione della Mappa è completa, attivo le animazioni
@property (nonatomic) BOOL mapDidAppear;
@property (nonatomic) BOOL mapIsAppearing;

@property (nonatomic) BOOL userDidInteract;

@property (strong, nonatomic) QTree *clusteringManager;
@property (strong, nonatomic) NSArray *addedAnnotations;

@end

@interface DBMapViewClusterAnnotation (PrivateMethods)

- (void)setAnnotations:(NSArray<DBMapViewAnnotation *> *)annotations;

@end


@implementation DBMapView

// MARK: - Setup

@synthesize activityIndicator = _activityIndicator;
@synthesize mapView = _mapView;

- (void)setupObject
{
    // Map view
    MKMapView *mapView = [MKMapView new];
    [mapView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [mapView setTintColor:[DBUtility tintColor]];
    [mapView setDelegate:self];
    
    [self addSubview:mapView];
    [self setAutolayoutEqualMarginsForSubview:mapView];
    _mapView = mapView;
    
    // Annotations clustering
    self.clusteringManager = [QTree new];
    self.annotationsClusteringEnabled = YES;
    
    // Activity indicator
    UIActivityIndicatorView *activityIndicator = [UIActivityIndicatorView new];
    [activityIndicator setTranslatesAutoresizingMaskIntoConstraints:NO];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicator startAnimating];
    
    [self addSubview:activityIndicator];
    [self setAutolayoutEqualCenterForSubview:activityIndicator];
    _activityIndicator = activityIndicator;
    
    // Gesture recognizers
    [self setupGestureRecognizers];
}

- (void)setupGestureRecognizers
{
    UILongPressGestureRecognizer *gestureRecognizer = [UILongPressGestureRecognizer new];
    gestureRecognizer.delegate = self; // This is used to handle the gesture recognition
    gestureRecognizer.minimumPressDuration = 0.0; // Set to 0.0 to capture all kind of presses (tap and pan)
    [self addGestureRecognizer:gestureRecognizer];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.superview && !self.mapDidAppear && !self.mapIsAppearing) {
        // Map is appearing
        self.mapIsAppearing = YES;
        
        // Fade-in animation
        self.mapView.alpha = 0.0;
        
        DBAnimation *animation = [DBAnimation new];
        animation.delay = 1.0;
        animation.duration = 0.4;
        
        [animation setInitialState:^{
            self.mapDidAppear = YES;
            
            // Faster disappearing for the activity indicator only
            self.activityIndicator.alpha = 0.0;
        }];
        
        [animation setFinalState:^{
            self.mapView.alpha = 1.0;
        }];
        
        [animation setCompletionBlock:^{
            self.mapIsAppearing = NO;
            
            [self.activityIndicator removeFromSuperview];
            self.activityIndicator = nil;
        }];
        
        [animation start];
    }
}

// MARK: - Public methods

- (void)setTintColor:(UIColor *)tintColor
{
    self.mapView.tintColor = tintColor;
}

- (UIColor *)tintColor
{
    return self.mapView.tintColor;
}

- (void)setMapType:(MKMapType)mapType
{
    self.mapView.mapType = mapType;
}

- (MKMapType)mapType
{
    return self.mapView.mapType;
}

- (void)setShowsUserLocation:(BOOL)showsUserLocation
{
    self.userDidInteract = NO;
    self.mapView.showsUserLocation = showsUserLocation;
}

- (BOOL)showsUserLocation
{
    return self.mapView.showsUserLocation;
}

- (void)setShowsCompass:(BOOL)showsCompass
{
    if (@available(iOS 9.0, *)) {
        self.mapView.showsCompass = showsCompass;
    }
}

- (BOOL)showsCompass
{
    if (@available(iOS 9.0, *)) {
        return self.mapView.showsCompass;
    } else {
        // Fallback on earlier versions
        return YES;
    }
}

- (void)setUserTrackingMode:(MKUserTrackingMode)userTrackingMode animated:(BOOL)animated
{
    self.userDidInteract = NO;
    [self.mapView setUserTrackingMode:userTrackingMode animated:animated];
}

- (void)setUserTrackingMode:(MKUserTrackingMode)userTrackingMode
{
    [self setUserTrackingMode:userTrackingMode animated:NO];
}

- (MKUserTrackingMode)userTrackingMode
{
    return self.mapView.userTrackingMode;
}

@synthesize userLocation = _userLocation;
- (void)setUserLocation:(DBMapViewAnnotation *)userLocation
{
    _userLocation = userLocation;
    
    // FIXME: Should update user location pin!
}

- (DBMapViewAnnotation *)userLocation
{
    if (_userLocation) {
        return _userLocation;
    }
    else {
        // FIXME: Così il tipo della variabile è diverso!
        return (id)self.mapView.userLocation;
    }
}

- (void)setRadius:(CLLocationDistance)radius animated:(BOOL)animated
{
    CLLocationCoordinate2D centerCoordinate = self.centerCoordinate;
    
    animated = (self.mapDidAppear) ? animated : NO;
    [self setCenterCoordinate:centerCoordinate radius:radius animated:animated];
}

- (void)setRadius:(CLLocationDistance)radius
{
    [self setRadius:radius animated:NO];
}

- (CLLocationDistance)radius
{
    CLLocationCoordinate2D origin = [self.mapView convertPoint:CGPointZero toCoordinateFromView:self];
    CLLocationCoordinate2D center = [self.mapView convertPoint:self.mapView.center toCoordinateFromView:self];
    
    return [DBGeolocation distanzaDa:origin a:center];
}

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate animated:(BOOL)animated
{
    animated = (self.mapDidAppear) ? animated : NO;
    [self.mapView setCenterCoordinate:centerCoordinate animated:animated];
}

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
{
    [self setCenterCoordinate:centerCoordinate animated:NO];
}

- (CLLocationCoordinate2D)centerCoordinate
{
    return self.mapView.centerCoordinate;
}

- (void)setCenterCoordinate:(CLLocationCoordinate2D)coordinate radius:(CLLocationDistance)radius animated:(BOOL)animated
{
    CLLocationDistance safeRadius = safeDouble(radius);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, safeRadius, safeRadius);
    MKCoordinateRegion safeRegion = [self.mapView regionThatFits:region];
    
    if (doubleIsValid(safeRegion.span.latitudeDelta)) {
        animated = (self.mapDidAppear) ? animated : NO;
        [self.mapView setRegion:safeRegion animated:animated];
    }
}

@synthesize annotations = _annotations;
- (void)setAnnotations:(NSArray<__kindof DBMapViewAnnotation *> *)annotations
{
    // Saving new annotations
    _annotations = annotations;
    
    //    [self.allAnnotationsMapView removeAnnotations:self.allAnnotationsMapView.annotations];
    //    [self.allAnnotationsMapView addAnnotations:annotations];
    
    [self.clusteringManager cleanup];
    
    for (id object in annotations) {
        [self.clusteringManager insertObject:object];
    }
    
    [self updateVisibleAnnotations];
}

// Size the Map view region to fit its annotations
- (void)zoomCameraToFitAllAnnotations:(BOOL)animated
{
    NSMutableArray *annotations = [NSMutableArray arrayWithArray:self.mapView.annotations];
    
    NSInteger annotationsCount = annotations.count;
    NSInteger totalCount = annotationsCount;
    
    if (self.showsUserLocation) {
        // Adding user location
        totalCount++;
    }
    
    if (totalCount == 0) {
        // Non ci sono oggetti sulla mappa
        return;
    }
    
    // Convert NSArray of id <MKAnnotation> into an MKCoordinateRegion that can be used to set the map size
    // Can't use NSArray with MKMapPoint because MKMapPoint is not an id
    MKMapPoint points[totalCount]; // C array of MKMapPoint struct
    for (int counter = 0; counter < annotationsCount; counter++) {
        // Load points C array by converting coordinates to points
        DBMapViewAnnotation *annotation = [annotations objectAtIndex:counter];
        CLLocationCoordinate2D coordinate = annotation.coordinate;
        points[counter] = MKMapPointForCoordinate(coordinate);
    }
    
    if (self.showsUserLocation) {
        // Adding user location
        CLLocationCoordinate2D coordinate = [DBGeolocation posizioneCorrente];
        points[annotationsCount] = MKMapPointForCoordinate(coordinate);
    }
    
    // Create MKMapRect from array of MKMapPoint
    MKMapRect mapRect = [[MKPolygon polygonWithPoints:points count:totalCount] boundingMapRect];
    
    // Convert MKCoordinateRegion from MKMapRect
    MKCoordinateRegion regionFit = MKCoordinateRegionForMapRect(mapRect);
    
    CGFloat MINIMUM_ZOOM_ARC = 0.014; // Approximately 1 miles (1 degree of arc ~= 69 miles)
    CGFloat ANNOTATION_REGION_PAD_FACTOR = 1.50;
    CGFloat MAX_DEGREES_ARC = 360;
    
    // Add padding so pins aren't scrunched on the edges
    regionFit.span.latitudeDelta  *= ANNOTATION_REGION_PAD_FACTOR;
    regionFit.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR;
    
    // But padding can't be bigger than the world
    if (regionFit.span.latitudeDelta  > MAX_DEGREES_ARC) regionFit.span.latitudeDelta  = MAX_DEGREES_ARC;
    if (regionFit.span.longitudeDelta > MAX_DEGREES_ARC) regionFit.span.longitudeDelta = MAX_DEGREES_ARC;
    
    // And don't zoom in stupid-close on small samples
    if (regionFit.span.latitudeDelta  < MINIMUM_ZOOM_ARC) regionFit.span.latitudeDelta  = MINIMUM_ZOOM_ARC;
    if (regionFit.span.longitudeDelta < MINIMUM_ZOOM_ARC) regionFit.span.longitudeDelta = MINIMUM_ZOOM_ARC;
    
    // And if there is a sample of 1 we want the max zoom-in instead of max zoom-out
    if (totalCount == 1) {
        regionFit.span.latitudeDelta  = MINIMUM_ZOOM_ARC;
        regionFit.span.longitudeDelta = MINIMUM_ZOOM_ARC;
    }
    
    animated = (self.mapDidAppear) ? animated : NO;
    [self.mapView setRegion:regionFit animated:animated];
}

- (void)selectAnnotation:(nullable __kindof DBMapViewAnnotation *)annotation animated:(BOOL)animated
{
    if (annotation) {
        [self.mapView selectAnnotation:annotation animated:animated];
    }
}

- (void)deselectAnnotation:(nullable __kindof DBMapViewAnnotation *)annotation animated:(BOOL)animated
{
    if (annotation) {
        [self.mapView deselectAnnotation:annotation animated:animated];
    }
}

// MARK: - Private methods

- (void)setLayoutMargins:(UIEdgeInsets)layoutMargins
{
    [self.mapView setLayoutMargins:layoutMargins];
}

- (UIEdgeInsets)layoutMargins
{
    return self.mapView.layoutMargins;
}

// MARK: - Delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    // Pin posizione utente
    if ([annotation isEqual:mapView.userLocation]) {
        DBMapViewAnnotation *userLocation = _userLocation;
        
        if (!userLocation) {
            // Returning default user location annotation
            return nil;
        }
        
        // Returning custom user location annotation
        return userLocation;
    }
    else if ([annotation isKindOfClass:[DBMapViewAnnotation class]]) {
        DBMapViewAnnotation *annotationView = (id)annotation;
        return annotationView;
    }
    
    return nil;
}

//- (void)daSistemare
//{
//    if (YES) {
//        DBMapViewAnnotation *userLocation = self.userLocation;
//
//        mapView.userLocation.title = [NSString cleanString:userLocation.title];
//        mapView.userLocation.subtitle = [NSString cleanString:userLocation.subtitle];
//
//        if (userLocation.icon && !userLocation.annotationView) {
//            MKAnnotationView *annotationView = [self nuovoAnnotationViewConMapView:mapView conAnnotation:annotation];
//            annotationView.image = userLocation.icon;
//            userLocation.annotationView = annotationView;
//        }
//        else if (!userLocation.icon && userLocation.annotationView) {
//            userLocation.annotationView = nil;
//        }
//
//        return userLocation.annotationView;
//    }
//
//    MKAnnotationView *annotationView = [self nuovoAnnotationViewConMapView:mapView conAnnotation:annotation];
//    NSInteger pinNumero = [self.annotations indexOfObject:annotation];
//    DBMapViewPin *pin = [self.dataSource safeObjectAtIndex:pinNumero];
//    pin.annotationView = annotationView; // Reference
//
//    annotationView.tag = pinNumero;
//    annotationView.enabled = YES;
//    annotationView.canShowCallout = YES;
//    annotationView.image = pin.icon;
//    annotationView.layer.anchorPoint = pin.anchorPoint;
//
//    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//
//    if ([DBDevice isiOSMajor:8]) {
//        infoButton.larghezza += 10.0;
//        infoButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
//    }
//
//    annotationView.rightCalloutAccessoryView = infoButton;
//    return annotationView;
//}

- (__kindof DBMapViewAnnotation *)annotationWithClass:(Class)klass coordinate:(CLLocationCoordinate2D)coordinate
{
    NSString *className = NSStringFromClass(klass);
    NSString *identifier = [NSString stringWithFormat:@"%@DefaultIdentifier", className];
    
    __kindof DBMapViewAnnotation *annotation = nil;
    
    // Disabled as it leads to zombie annotations
    // annotation = (DBMapViewAnnotation *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if (!annotation) {
        annotation = [[klass alloc] initWithAnnotation:nil reuseIdentifier:identifier];
    }
    
    annotation.coordinate = coordinate;
    
    annotation.tintColor = self.tintColor;
    
    // Default right callout accessory view
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    if ([DBDevice isiOSMajor:8]) {
        infoButton.larghezza += 10.0;
        infoButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin;
    }
    
    annotation.rightCalloutAccessoryView = infoButton;
    
    if ([annotation isKindOfClass:[DBMapViewClusterAnnotation class]]) {
        // Already set up
    }
    else {
        annotation.canShowCallout = YES;
    }
    
    return annotation;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if ([view isKindOfClass:[DBMapViewAnnotation class]]) {
        DBMapViewAnnotation *annotationView = (id)view;
        
        if ([self.delegate respondsToSelector:@selector(mapView:didSelectAnnotation:)]) {
            [self.delegate mapView:self didSelectAnnotation:annotationView];
        }
    }
    
    // CODICE SMCALLOUTVIEW PER ORA NON UTILIZZATO
    //    // Tolgo la CalloutView precedente
    //    [self.calloutView dismissCalloutAnimated:YES];
    //    self.calloutView = nil;
    //
    //    self.calloutView = [SMCalloutView platformCalloutView];
    //    self.calloutView.delegate = self;
    //    self.calloutView.contentViewInset = UIEdgeInsetsZero;
    //    self.calloutView.contentView = pin.calloutView;
    //
    //    [self.calloutView presentCalloutFromRect:view.bounds inView:view constrainedToView:self.mapView animated:YES];
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    if ([view isKindOfClass:[DBMapViewAnnotation class]]) {
        DBMapViewAnnotation *annotationView = (id)view;
        
        if ([self.delegate respondsToSelector:@selector(mapView:didSelectAnnotationCallout:)]) {
            [self.delegate mapView:self didSelectAnnotationCallout:annotationView];
        }
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    // CODICE SMCALLOUTVIEW PER ORA NON UTILIZZATO
    //    // Tolgo la CalloutView
    //    [self.calloutView dismissCalloutAnimated:YES];
    //    self.calloutView = nil;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray<MKAnnotationView *> *)views
{
    // Bringing user location annotation to front
    for (MKAnnotationView *annotationView in views) {
        if ([annotationView.annotation isKindOfClass:[MKUserLocation class]]) {
            // Standard user location annotation
            [annotationView.superview bringSubviewToFront:annotationView];
        }
        else if ([annotationView isEqual:self.userLocation]) {
            // Custom user location annotation
            [annotationView.superview bringSubviewToFront:annotationView];
        }
        else {
            // Other annotations
            [annotationView.superview sendSubviewToBack:annotationView];
        }
    }
    
    if (self.mapDidAppear) {
        // Basic fade-in animation
        DBAnimation *animation = [DBAnimation new];
        animation.duration = 0.2;
        
        [animation setInitialState:^{
            for (MKAnnotationView *view in views) {
                view.alpha = 0.0;
                view.transform = CGAffineTransformMakeScale(0.9, 0.9);
            }
        }];
        
        [animation setFinalState:^{
            for (MKAnnotationView *view in views) {
                view.alpha = 1.0;
                view.transform = CGAffineTransformIdentity;
            }
        }];
        
        [animation start];
    }
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    // Notifying user interactions only
    if (self.userDidInteract) {
        if ([self.delegate respondsToSelector:@selector(mapView:regionWillChangeAnimated:)]) {
            [self.delegate mapView:self regionWillChangeAnimated:animated];
        }
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if ([self isClusteringAvailable]) {
        [self updateVisibleAnnotations];
    }
    else {
        // All annotations are already on the map
    }
    
    // Notifying user interactions only
    if (self.userDidInteract) {
        if ([self.delegate respondsToSelector:@selector(mapView:regionDidChangeAnimated:)]) {
            [self.delegate mapView:self regionDidChangeAnimated:animated];
        }
    }
}

// MARK: - Annotations clustering

- (BOOL)isClusteringAvailable
{
    if ([self.delegate respondsToSelector:@selector(mapView:clusterAnnotationForAnnotations:)]) {
        return self.isAnnotationsClusteringEnabled;
    }
    else {
        return NO;
    }
}

- (void)updateVisibleAnnotations
{
    if ([self isClusteringAvailable]) {
        // Annotations clustering algorithm
        [self updateVisibleAnnotationsWithClustering];
    }
    else {
        [self updateAnnotationsIfNeeded:self.annotations];
    }
}

- (void)updateAnnotationsIfNeeded:(NSArray *)annotations
{
    NSMutableArray *removeAnnotations = [self.addedAnnotations mutableCopy];
    [removeAnnotations removeObjectsInArray:annotations];
    
    NSMutableArray *addAnnotations = [annotations mutableCopy];
    [addAnnotations removeObjectsInArray:self.addedAnnotations];
    
    self.addedAnnotations = [NSArray arrayWithArray:annotations];
    
    [self.mapView removeAnnotations:removeAnnotations];
    [self.mapView addAnnotations:addAnnotations];
}

- (void)updateVisibleAnnotationsWithClustering
{
    MKMapView *mapView = self.mapView;
    
    // This value to controls the number of off screen annotations are displayed.
    // A bigger number means more annotations, less chance of seeing annotation views pop in but decreased performance.
    // A smaller number means fewer annotations, more chance of seeing annotation views pop in but better performance.
    static float marginFactor = 2.0;
    
    // find all the annotations in the visible area + a wide margin to avoid popping annotation views in and out while panning the map.
    MKMapRect visibleMapRect = mapView.visibleMapRect;
    MKMapRect adjustedVisibleMapRect = MKMapRectInset(visibleMapRect, -marginFactor * visibleMapRect.size.width, -marginFactor * visibleMapRect.size.height);
    
    const MKCoordinateRegion adjustedRegion = MKCoordinateRegionForMapRect(adjustedVisibleMapRect);
    
    // Calcolo la distanza minima tra un pin e l'altro
    static const CGFloat annotationSize = 80.0;
    const CLLocationCoordinate2D startCoordinate = [mapView convertPoint:CGPointZero toCoordinateFromView:self];
    const CLLocationCoordinate2D endCoordinate   = [mapView convertPoint:CGPointMake(annotationSize, annotationSize) toCoordinateFromView:self];
    CLLocationDegrees latitude  = fabs(endCoordinate.latitude  - startCoordinate.latitude);
    CLLocationDegrees longitude = fabs(endCoordinate.longitude - startCoordinate.longitude);
    CLLocationDegrees minNonClusteredSpan = fmax(latitude, longitude);
    // NSLog(@"^ MinNonClusteredSpan: %.3f", minNonClusteredSpan);
    
    // Altro modo per calcolare la distanza minima
    // const MKCoordinateRegion region = mapView.region;
    // const CLLocationDegrees minNonClusteredSpan = MIN(region.span.latitudeDelta, region.span.longitudeDelta) / 4;
    
    NSArray *objects = [self.clusteringManager getObjectsInRegion:adjustedRegion minNonClusteredSpan:minNonClusteredSpan fillClusters:YES];
    
    NSMutableArray *annotations = [NSMutableArray array];
    
    for (id object in objects) {
        if ([object isKindOfClass:[QCluster class]]) {
            QCluster *cluster = object;
            
            // Check if a cluster annotation already exists
            DBMapViewClusterAnnotation *clusterAnnotation = nil;
            
            if (self.addedAnnotations.count > 0) {
                for (DBMapViewClusterAnnotation *annotation in self.addedAnnotations) {
                    if ([annotation isKindOfClass:[DBMapViewClusterAnnotation class]]) {
                        // Check if the cluster annotation is compatible with the current annotations
                        if ([annotation.annotations isEqualToArray:(id)cluster.objects]) {
                            // It's the same
                            // Reusing clustered annotation
                            clusterAnnotation = annotation;
                            break;
                        }
                    }
                }
            }
            
            if (clusterAnnotation) {
                // Reusing clustered annotation
                // NSLog(@"^ Reusing clustered annotation");
            }
            else {
                // Creating new clustered annotation
                clusterAnnotation = [self.delegate mapView:self clusterAnnotationForAnnotations:(id)cluster.objects];
                clusterAnnotation.annotations = (id)cluster.objects;
                clusterAnnotation.coordinate = cluster.coordinate;
            }
            
            [annotations addObject:clusterAnnotation];
        }
        else {
            [annotations addObject:object];
        }
    }
    
    [self updateAnnotationsIfNeeded:annotations];
}

// MARK: - Codice AnnotationCoordinateUtility

//+ (void)mutateCoordinatesOfClashingAnnotations:(NSArray *)annotations
//{
//    NSDictionary *coordinateValuesToAnnotations = [self groupAnnotationsByLocationValue:annotations];
//
//    for (NSValue *coordinateValue in coordinateValuesToAnnotations.allKeys) {
//        NSMutableArray *outletsAtLocation = coordinateValuesToAnnotations[coordinateValue];
//
//        if (outletsAtLocation.count > 1) {
//            CLLocationCoordinate2D coordinate;
//            [coordinateValue getValue:&coordinate];
//            [self repositionAnnotations:outletsAtLocation toAvoidClashAtCoordination:coordinate];
//        }
//    }
//}
//
//+ (NSDictionary *)groupAnnotationsByLocationValue:(NSArray *)annotations
//{
//    NSMutableDictionary *result = [NSMutableDictionary dictionary];
//
//    for (id<MKAnnotation> pin in annotations) {
//
//        // Arrotondo le coordinate
//        CLLocationDegrees latitudine = [[NSString stringWithFormat:@"%.3f", pin.coordinate.latitude] doubleValue];
//        CLLocationDegrees longitudine = [[NSString stringWithFormat:@"%.3f", pin.coordinate.longitude] doubleValue];
//        pin.coordinate = CLLocationCoordinate2DMake(latitudine, longitudine);
//
//        CLLocationCoordinate2D coordinate = pin.coordinate;
//        NSValue *coordinateValue = [NSValue valueWithBytes:&coordinate objCType:@encode(CLLocationCoordinate2D)];
//
//        NSMutableArray *annotationsAtLocation = result[coordinateValue];
//        if (!annotationsAtLocation) {
//            annotationsAtLocation = [NSMutableArray array];
//            result[coordinateValue] = annotationsAtLocation;
//        }
//
//        [annotationsAtLocation addObject:pin];
//    }
//
//    return result;
//}
//
//+ (void)repositionAnnotations:(NSMutableArray *)annotations toAvoidClashAtCoordination:(CLLocationCoordinate2D)coordinate
//{
//    double distance = 8 * annotations.count;
//    double radiansBetweenAnnotations = (M_PI * 2) / annotations.count;
//
//    for (int i = 0; i < annotations.count; i++) {
//        double heading = radiansBetweenAnnotations * i;
//        CLLocationCoordinate2D newCoordinate = [self calculateCoordinateFrom:coordinate onBearing:heading atDistance:distance];
//
//        id <MKAnnotation> annotation = annotations[i];
//        annotation.coordinate = newCoordinate;
//    }
//}
//
//+ (CLLocationCoordinate2D)calculateCoordinateFrom:(CLLocationCoordinate2D)coordinate onBearing:(double)bearingInRadians atDistance:(double)distanceInMetres
//{
//    double coordinateLatitudeInRadians = coordinate.latitude * M_PI / 180;
//    double coordinateLongitudeInRadians = coordinate.longitude * M_PI / 180;
//
//    double distanceComparedToEarth = distanceInMetres / 6378100;
//
//    double resultLatitudeInRadians = asin(sin(coordinateLatitudeInRadians) * cos(distanceComparedToEarth) + cos(coordinateLatitudeInRadians) * sin(distanceComparedToEarth) * cos(bearingInRadians));
//    double resultLongitudeInRadians = coordinateLongitudeInRadians + atan2(sin(bearingInRadians) * sin(distanceComparedToEarth) * cos(coordinateLatitudeInRadians), cos(distanceComparedToEarth) - sin(coordinateLatitudeInRadians) * sin(resultLatitudeInRadians));
//
//    CLLocationCoordinate2D result;
//    result.latitude = resultLatitudeInRadians * 180 / M_PI;
//    result.longitude = resultLongitudeInRadians * 180 / M_PI;
//
//    return result;
//}

// MARK: - GestureRecognizer delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    // This never gets called because shouldReceiveTouch always returns NO
    // Theoretically multiple simultaneous gesture recognitions shouldn't occur
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (self.userDidInteract == NO) {
        // User did interact with the map
        // NSLog(@"^ User did interact");
        self.userDidInteract = YES;
    }
    
    // There's no need to handle the gesture anymore
    return NO;
}

@end
