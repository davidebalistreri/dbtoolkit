//
//  DBMapView.h
//  File version: 1.1.0
//  Last modified: 04/18/2017
//
//  Created by Davide Balistreri on 11/22/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


#import "DBView.h"

#import "DBMapViewAnnotation.h"
#import "DBMapViewClusterAnnotation.h"

#import <MapKit/MapKit.h>

@protocol DBMapViewDelegate;


@interface DBMapView : DBView

@property (weak, nonatomic, nullable) id<DBMapViewDelegate> delegate;


@property (nonatomic, readonly, nonnull) MKMapView *mapView;

@property (nonatomic) MKMapType mapType;


@property (nonatomic) BOOL showsUserLocation;

@property (nonatomic) BOOL showsCompass;

@property (nonatomic, nullable) DBMapViewAnnotation *userLocation;


@property (nonatomic) MKUserTrackingMode userTrackingMode;

- (void)setUserTrackingMode:(MKUserTrackingMode)userTrackingMode animated:(BOOL)animated;


/// La distanza dall'angolo in alto a sinistra al punto centrale della mappa, in metri
@property (nonatomic) CLLocationDistance radius;

- (void)setRadius:(CLLocationDistance)radius animated:(BOOL)animated;


/// Le coordinate del punto centrale della mappa
@property (nonatomic) CLLocationCoordinate2D centerCoordinate;

/// Modifica le coordinate del punto centrale della mappa, eventualmente effettuando lo spostamento con un'animazione
- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate animated:(BOOL)animated;

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate radius:(CLLocationDistance)radius animated:(BOOL)animated;


/// Default: enabled, but it only works if clustering delegate methods are implemented.
@property (nonatomic, getter=isAnnotationsClusteringEnabled) BOOL annotationsClusteringEnabled;

- (void)zoomCameraToFitAllAnnotations:(BOOL)animated;


@property (strong, nonatomic) NSArray<__kindof DBMapViewAnnotation *> * __nullable annotations;

- (void)selectAnnotation:(nullable __kindof DBMapViewAnnotation *)annotation animated:(BOOL)animated;
- (void)deselectAnnotation:(nullable __kindof DBMapViewAnnotation *)annotation animated:(BOOL)animated;

@end


@protocol DBMapViewDelegate <NSObject>

@optional

/// Callback quando l'utente tappa sul Pin e viene aperta la didascalia
- (void)mapView:(nonnull DBMapView *)mapView didSelectAnnotation:(nonnull __kindof DBMapViewAnnotation *)annotation;

/// Callback quando l'utente tappa sulla didascalia del Pin aperto
- (void)mapView:(nonnull DBMapView *)mapView didSelectAnnotationCallout:(nonnull __kindof DBMapViewAnnotation *)annotation;


/// Called when a user interaction is about to change the map visible region.
- (void)mapView:(nonnull DBMapView *)mapView regionWillChangeAnimated:(BOOL)animated;

/// Called when a user interaction did change the map visible region.
- (void)mapView:(nonnull DBMapView *)mapView regionDidChangeAnimated:(BOOL)animated;


// MARK: Annotation clustering

/**
 * Implement this method to enable annotations clustering.
 */
- (nonnull __kindof DBMapViewClusterAnnotation *)mapView:(nonnull DBMapView *)mapView clusterAnnotationForAnnotations:(nonnull NSArray<__kindof DBMapViewAnnotation *> *)annotations;

/**
 * - Ancora da implementare! -
 *
 * Use this optional delegate method if you want to determine which objects should be grouped into each cluster.
 *
 * Returning the annotationsToBeClustered array causes all annotations to be clustered.
 * Returning an empty array behaves as if clustering was disabled.
 */
- (nullable NSArray<__kindof DBMapViewAnnotation *> *)mapView:(nonnull DBMapView *)mapView annotationsForCluster:(nonnull NSArray<__kindof DBMapViewAnnotation *> *)annotationsToBeClustered;

@end
