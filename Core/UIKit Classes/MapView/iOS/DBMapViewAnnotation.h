//
//  DBMapViewAnnotation.h
//  File version: 1.0.0
//  Last modified: 04/11/2017
//
//  Created by Davide Balistreri on 11/22/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


#import <MapKit/MapKit.h>

@class DBMapView;


@interface DBMapViewAnnotation : MKAnnotationView

/// Use this to setup default values and optimize performances
+ (instancetype)annotationWithMapView:(DBMapView *)mapView;

+ (instancetype)annotationWithMapView:(DBMapView *)mapView coordinate:(CLLocationCoordinate2D)coordinate;


/// Variabile per associare un oggetto a piacere al Pin
@property (strong, nonatomic) id object;

/// Le coordinate del Pin sulla mappa
@property (nonatomic) CLLocationCoordinate2D coordinate;

/// Il titolo del Pin, che viene mostrato se non esiste una calloutView
@property (strong, nonatomic) NSString *title;

/// Il sottotitolo del Pin, che viene mostrato se non esiste una calloutView
@property (strong, nonatomic) NSString *subtitle;


/// Il punto di ancoraggio dell'icona sulla mappa (valori da 0.0 a 1.0, default 0.5)
@property (nonatomic) CGPoint anchorPoint;

/**
 * Indice di posizionamento del Pin sull'asse Z (valori superiori allo 0 lo mettono in funzione).
 * @note Su iOS 11 l'asse Z viene gestito dal sistema: utilizza questa proprietà per forzarne il valore.
 */
@property (nonatomic) CGFloat zPosition;

@end
