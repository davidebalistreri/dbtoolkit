//
//  DBMapViewClusterAnnotation.m
//  File version: 1.0.0
//  Last modified: 04/18/2017
//
//  Created by Davide Balistreri on 04/11/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBMapViewClusterAnnotation.h"

@implementation DBMapViewClusterAnnotation

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[DBMapViewClusterAnnotation class]]) {
        DBMapViewClusterAnnotation *clusterAnnotation = object;
        
        if ([self.annotations isEqualToArray:clusterAnnotation.annotations]) {
            return YES;
        }
    }
    
    return [super isEqual:object];
}

@synthesize annotations = _annotations;
- (void)setAnnotations:(NSArray<DBMapViewAnnotation *> *)annotations
{
    _annotations = annotations;
}

@end
