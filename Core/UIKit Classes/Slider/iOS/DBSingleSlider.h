//
//  DBSingleSlider.h
//  File version: 1.0.0
//  Last modified: 05/11/2016
//
//  Created by Davide Balistreri on 05/11/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBView.h"

@protocol DBSliderDelegate;


@interface DBSingleSlider : DBView

NS_ASSUME_NONNULL_BEGIN

// MARK: - Valori

/// NSNotFound se lo slider è a scorrimento libero.
@property (nonatomic) NSInteger sliderStep;

/// Valore da 0.0 a 1.0.
@property (nonatomic) CGFloat sliderPercentage;


// MARK: - Configurazione funzionale

/**
 * Numero di step dello slider.
 * Default: zero, lo slider sarà a scorrimento libero.
 */
@property (nonatomic) NSUInteger numberOfSteps;

@property (nonatomic, nullable) id<DBSliderDelegate> delegate;


// MARK: - Configurazione grafica

/// Stili per la presentazione grafica dello Slider.
typedef NS_ENUM(NSUInteger, DBSliderStyle) {
    /// Default. Dimensioni standard e colori in scala di grigio.
    DBSliderStyleDefault,
    /// Stile interamente configurabile dallo sviluppatore.
    DBSliderStyleNone
};

/// Lo stile di presentazione grafica dello Slider.
@property (nonatomic) DBSliderStyle sliderStyle;

/// Le dimensioni dello slider
@property (nonatomic) CGSize sliderSize;

/// L'altezza della barra
@property (nonatomic) CGFloat barHeight;


// MARK: - Configurazione grafica manuale

/// Slider
@property (strong, nonatomic, readonly) UIImageView *slider;

/// Barra di completamento
@property (strong, nonatomic, readonly) UIImageView *fillBar;

/// Barra di background
@property (strong, nonatomic, readonly) UIImageView *trackBar;

NS_ASSUME_NONNULL_END

@end


@protocol DBSliderDelegate <NSObject>

@optional

NS_ASSUME_NONNULL_BEGIN

- (BOOL)sliderShouldBeginChangingValue:(id)sender;

- (void)sliderDidBeginChangingValue:(id)sender;
- (void)sliderDidChangeValue:(id)sender;
- (void)sliderDidEndChangingValue:(id)sender;

NS_ASSUME_NONNULL_END

@end
