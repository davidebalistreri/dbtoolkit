//
//  DBDoubleSlider.m
//  File version: 1.0.0
//  Last modified: 05/11/2016
//
//  Created by Davide Balistreri on 05/11/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBDoubleSlider.h"

#import "DBViewExtensions.h"
#import "DBGeometry.h"


@interface DBSingleSlider (PrivateMethods)

- (CGFloat)trackBarWidth;
- (CGFloat)trackBarUsableWidth;

- (BOOL)isStepped;
- (CGFloat)stepWidth;
- (NSInteger)stepWithPosition:(CGFloat)position;
- (CGFloat)positionWithStep:(NSInteger)step;

@end


@interface DBDoubleSlider () <UIGestureRecognizerDelegate>

@property (nonatomic) BOOL configurazioneCompletata;

@property (strong, nonatomic) UIPanGestureRecognizer *secondSliderRecognizer;
@property (nonatomic) CGFloat secondSliderRecognizerOriginX;

@end


@implementation DBDoubleSlider

// MARK: - Inizializzazione

- (void)setupObject
{
    [super setupObject];
    
    // Init
    self.secondSlider = [UIImageView new];
    self.secondSlider.contentMode = UIViewContentModeScaleAspectFit;
    self.secondSlider.clipsToBounds = YES;
    self.secondSlider.translatesAutoresizingMaskIntoConstraints = NO;
    self.secondSlider.userInteractionEnabled = YES;
    
    // Subviews
    [self addSubview:self.secondSlider];
    
    // Constraints FillBar
    NSLayoutConstraint *layoutFillBarLeading = [self.fillBar constraintConAttributo:NSLayoutAttributeLeading];
    NSLayoutConstraint *layoutFillBarTrailing = [self.fillBar constraintConAttributo:NSLayoutAttributeTrailing];
    [NSLayoutConstraint deactivateConstraints:@[layoutFillBarLeading, layoutFillBarTrailing]];
    layoutFillBarLeading = [NSLayoutConstraint constraintWithItem:self.fillBar attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.slider attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    layoutFillBarTrailing = [NSLayoutConstraint constraintWithItem:self.secondSlider attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.fillBar attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0];
    [NSLayoutConstraint activateConstraints:@[layoutFillBarLeading, layoutFillBarTrailing]];
    
    // Constraints Slider
    NSLayoutConstraint *layoutSliderLarghezza = [NSLayoutConstraint constraintWithItem:self.secondSlider attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0];
    NSLayoutConstraint *layoutSliderAltezza = [NSLayoutConstraint constraintWithItem:self.secondSlider attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0];
    NSLayoutConstraint *layoutSliderLeading = [NSLayoutConstraint constraintWithItem:self.secondSlider attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.trackBar attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    NSLayoutConstraint *layoutSliderCentroY = [NSLayoutConstraint constraintWithItem:self.secondSlider attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.trackBar attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [NSLayoutConstraint activateConstraints:@[layoutSliderLarghezza, layoutSliderAltezza, layoutSliderLeading, layoutSliderCentroY]];
    
    // GestureRecognizer
    self.secondSliderRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(secondSliderRecognizer:)];
    self.secondSliderRecognizer.delegate = self;
    [self.secondSlider addGestureRecognizer:self.secondSliderRecognizer];
    
    // Style
    [self configureSecondSliderStyle];
    
    // Valori iniziali (da correggere perché non va se la vista cambia frame)
    self.secondSliderPercentage = 1.0;
}


// MARK: - Configurazione grafica

- (void)setSliderStyle:(DBSliderStyle)sliderStyle
{
    [super setSliderStyle:sliderStyle];
    [self configureSecondSliderStyle];
}

- (void)setSliderSize:(CGSize)sliderSize
{
    [super setSliderSize:sliderSize];
    
    self.secondSlider.constraintLarghezza = sliderSize.width;
    self.secondSlider.constraintAltezza = sliderSize.height;
    [self.secondSlider layoutIfNeeded];
    
    [self moveSecondSlider:self.secondSlider.constraintLeading];
}

- (void)configureSecondSliderStyle
{
    switch (self.sliderStyle) {
        case DBSliderStyleDefault:
            // Stile di default
            break;
        default:
            // Stile assente
            self.secondSlider.layer.cornerRadius = 0.0;
            break;
    }
    
    // SecondSlider
    self.secondSlider.backgroundColor = self.slider.backgroundColor;
    self.secondSlider.constraintLarghezza = self.slider.constraintLarghezza;
    self.secondSlider.constraintAltezza = self.slider.constraintAltezza;
    
    [self layoutIfNeeded];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    switch (self.sliderStyle) {
        case DBSliderStyleDefault:
            // Stile di default
            [self.secondSlider aggiungiAngoliArrotondatiCerchio];
            break;
        default:
            // Stile assente
            break;
    }
    
    if (self.configurazioneCompletata == NO) {
        self.configurazioneCompletata = YES;
        
        // Valore di default
        [self moveSecondSlider:MAXFLOAT];
    }
}


// MARK: - Configurazione grafica manuale

@synthesize secondSlider = _secondSlider;
- (void)setSecondSlider:(UIImageView *)secondSlider
{
    _secondSlider = secondSlider;
}

- (CGFloat)trackBarUsableWidth
{
    CGFloat width = [self trackBarWidth];
    CGFloat larghezzaSlider = self.secondSlider.constraintLarghezza;
    return width - larghezzaSlider;
}


// MARK: - Metodi pubblici

- (NSInteger)secondSliderStep
{
    return [self stepWithPosition:self.secondSlider.constraintLeading];
}

- (void)setSecondSliderStep:(NSInteger)secondSliderStep
{
    [self moveSecondSlider:[self positionWithStep:secondSliderStep]];
}

- (CGFloat)secondSliderPercentage
{
    CGFloat width = [self trackBarUsableWidth];
    CGFloat leading = self.secondSlider.constraintLeading;
    return (leading / width);
}

- (void)setSecondSliderPercentage:(CGFloat)secondSliderPercentage
{
    CGFloat percentage = mantieniAllInterno(secondSliderPercentage, 0.0, 1.0);
    CGFloat width = [self trackBarUsableWidth];
    CGFloat position = width * percentage;
    [self moveSecondSlider:position];
}


// MARK: - Metodi privati

- (void)moveSlider:(CGFloat)pixel
{
    // Override del metodo del singolo slider perché altrimenti la posizione non è valida con le nuove constraints del doppio slider
    
    // CGFloat larghezzaSlider = self.slider.constraintLarghezza;
    CGFloat larghezzaTotale = [self trackBarUsableWidth];
    CGFloat larghezzaStep = [self stepWidth];
    
    if ([self isStepped]) {
        NSInteger step = [self stepWithPosition:pixel];
        pixel = larghezzaStep * step;
    }
    
    pixel = mantieniAllInterno(pixel, 0.0, larghezzaTotale);
    
    // Controllo se collide con il secondo Slider (di destra)
    if ((pixel + larghezzaStep) > self.secondSlider.constraintLeading) {
        pixel = self.secondSlider.constraintLeading - larghezzaStep;
    }
    
    if ([self isStepped]) {
        if (self.slider.constraintLeading != pixel) {
            // Animazione
            [UIView animateWithDuration:0.12 animations:^{
                self.slider.constraintLeading = pixel;
                [self layoutIfNeeded];
            }];
        }
    }
    else {
        self.slider.constraintLeading = pixel;
        [self layoutIfNeeded];
    }
    
    if ([self.delegate respondsToSelector:@selector(sliderDidChangeValue:)]) {
        [self.delegate sliderDidChangeValue:self];
    }
}

- (void)moveSecondSlider:(CGFloat)pixel
{
    __unused CGFloat larghezzaSlider = self.secondSlider.constraintLarghezza;
    CGFloat larghezzaTotale = [self trackBarUsableWidth];
    CGFloat larghezzaStep = [self stepWidth];
    
    if ([self isStepped]) {
        NSInteger step = [self stepWithPosition:pixel];
        pixel = larghezzaStep * step;
    }
    
    pixel = mantieniAllInterno(pixel, 0.0, larghezzaTotale);
    
    // Controllo se collide con il primo Slider (di sinistra)
    if ((pixel - larghezzaStep) < self.slider.constraintLeading) {
        pixel = self.slider.constraintLeading + larghezzaStep;
    }
    
    if ([self isStepped]) {
        if (self.secondSlider.constraintLeading != pixel) {
            // Animazione
            [UIView animateWithDuration:0.12 animations:^{
                self.secondSlider.constraintLeading = pixel;
                [self layoutIfNeeded];
            }];
        }
    }
    else {
        self.secondSlider.constraintLeading = pixel;
        [self layoutIfNeeded];
    }
    
    if ([self.delegate respondsToSelector:@selector(sliderDidChangeValue:)]) {
        [self.delegate sliderDidChangeValue:self];
    }
}


// MARK: - Gesture recognizer

- (void)secondSliderRecognizer:(UIPanGestureRecognizer *)recognizer
{
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan: {
            // Salvo il punto di origine iniziale
            self.secondSliderRecognizerOriginX = self.secondSlider.constraintLeading;
            [self bringSubviewToFront:self.secondSlider];
            
            if ([self.delegate respondsToSelector:@selector(sliderDidBeginChangingValue:)]) {
                [self.delegate sliderDidBeginChangingValue:self];
            }
            
            break;
        }
        case UIGestureRecognizerStateChanged: {
            CGPoint translation = [recognizer translationInView:self];
            CGFloat originX = self.secondSliderRecognizerOriginX + translation.x;
            [self moveSecondSlider:originX];
            break;
        }
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStateCancelled: {
            if ([self.delegate respondsToSelector:@selector(sliderDidEndChangingValue:)]) {
                [self.delegate sliderDidEndChangingValue:self];
            }
            
            break;
        }
        default:
            break;
    }
}

@end
