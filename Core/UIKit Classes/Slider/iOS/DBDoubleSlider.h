//
//  DBDoubleSlider.h
//  File version: 1.0.0
//  Last modified: 05/11/2016
//
//  Created by Davide Balistreri on 05/11/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBSingleSlider.h"

@interface DBDoubleSlider : DBSingleSlider

NS_ASSUME_NONNULL_BEGIN

// MARK: - Valori

/// NSNotFound se lo slider è a scorrimento libero.
@property (nonatomic) NSInteger secondSliderStep;

/// Valore da 0.0 a 1.0.
@property (nonatomic) CGFloat secondSliderPercentage;


// MARK: - Configurazione grafica

/// Secondo slider (destra)
@property (strong, nonatomic, readonly) UIImageView *secondSlider;

NS_ASSUME_NONNULL_END

@end
