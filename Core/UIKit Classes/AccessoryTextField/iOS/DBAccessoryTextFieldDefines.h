//
//  DBAccessoryViewDefines.h
//  File version: 1.0.0 beta
//  Last modified: 01/05/2017
//
//  Created by Davide Balistreri on 12/05/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


@class DBAccessoryTextField;


// MARK: - Type constants

/**
 * Constants used to define the layout position of the AccessoryView.
 */
typedef NS_ENUM(NSUInteger, DBAccessoryViewPosition) {
    DBAccessoryViewPositionAutomatic,
    DBAccessoryViewPositionTop,
    DBAccessoryViewPositionBottom,
    DBAccessoryViewPositionDefault = DBAccessoryViewPositionAutomatic
};


// MARK: - Defines

/**
 * The AccessoryView will be sized to match the size of the TextField: their widths will be the same
 * and the AccessoryView bottom margin will respect the top margin of the TextField.
 * The AccessoryView contentInset will be considered too.
 */
extern const CGSize DBAccessoryViewAutomaticSize;

/**
 * The AccessoryView will fill the available space below the TextField and above the Keyboard.
 * The contentInset will be considered too.
 */
extern const CGSize DBAccessoryViewExpandSize;
