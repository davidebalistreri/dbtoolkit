//
//  DBGeometry.m
//  File version: 1.1.2
//  Last modified: 03/13/2016
//
//  Created by Davide Balistreri on 03/31/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBGeometry.h"
#import "DBFramework.h"

@interface DBGeometry ()

@property (nonatomic) DBGeometrySistemaCoordinate sistemaCoordinate;

@end


@implementation DBGeometry

+ (instancetype)sharedInstance
{
    @synchronized(self) {
        DBGeometry *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedGeometry"];
        
        if (!sharedInstance) {
            sharedInstance = [DBGeometry new];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedGeometry"];
        }
        
        return sharedInstance;
    }
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.sistemaCoordinate = DBGeometrySistemaCoordinateNativo;
    }
    return self;
}

/**
 * Imposta il sistema di coordinate da utilizzare quando si interagisce con il posizionamento degli oggetti attraverso i metodi di questa classe.
 *
 * I metodi che rispettano questa impostazione sono esclusivamente quelli dell'estensione View: modificando direttamente i CGRect dei frame è impossibile tener conto del sistema di coordinate, perché è necessario un riferimento alla vista e alla sua superview.
 *
 * Di default è attivo il sistema nativo della piattaforma su cui si esegue l'applicazione.
 */
+ (void)impostaSistemaCoordinate:(DBGeometrySistemaCoordinate)sistemaCoordinate
{
    DBGeometry *sharedInstance = [DBGeometry sharedInstance];
    sharedInstance.sistemaCoordinate = sistemaCoordinate;
}

/**
 * Converte il frame per adattarlo al sistema di coordinate utilizzato dall'applicazione, confrontando il sistema coordinate impostato dall'utente con quello nativo dell'ambiente. <br><br>
 * NB: Questa funzione va richiamata una sola volta per adattare il frame correttamente, altrimenti potrebbe produrre risultati non attesi.
 * @param frame Il frame da convertire.
 * @param superview L'area che ospiterà il frame da convertire (la superview dell'oggetto).
 */
+ (CGRect)convertiFrame:(CGRect)frame conSuperview:(UIView *)superview
{
    return [self convertiFrame:frame conFrameSuperview:superview.frame];
}

/**
 * Converte il frame per adattarlo al sistema di coordinate utilizzato dall'applicazione, confrontando il sistema coordinate impostato dall'utente con quello nativo dell'ambiente. <br><br>
 * NB: Questa funzione va richiamata una sola volta per adattare il frame correttamente, altrimenti potrebbe produrre risultati non attesi.
 * @param frame Il frame da convertire.
 * @param frameSuperview Il frame dell'area che ospiterà il frame da convertire (la superview dell'oggetto).
 */
+ (CGRect)convertiFrame:(CGRect)frame conFrameSuperview:(CGRect)frameSuperview
{
    if ([DBGeometry conversioneSistemaCoordinate]) {
        // Conversione sistema di coordinate
        frame.origin.y = frameSuperview.size.height - frame.size.height - frame.origin.y;
    }
    
    return frame;
}

/**
 * Restituisce YES se bisogna effettuare una conversione, confrontando il sistema coordinate impostato dall'utente con quello nativo dell'ambiente.
 */
+ (BOOL)conversioneSistemaCoordinate
{
    DBGeometry *sharedInstance = [DBGeometry sharedInstance];
#if DBFRAMEWORK_TARGET_IOS
    if (sharedInstance.sistemaCoordinate == DBGeometrySistemaCoordinateOSX) {
        return YES;
    }
#else
    if (sharedInstance.sistemaCoordinate == DBGeometrySistemaCoordinateiOS) {
        return YES;
    }
#endif
    return NO;
}

#if DBFRAMEWORK_TARGET_IOS
+ (CGRect)frameSchermo
{
    CGRect frame = [UIScreen mainScreen].bounds;
    
    if ([DBDevice isiOSMin:8.0] == NO) {
        // Prima di iOS 8 le dimensioni delle viste non tenevano conto dell'orientamento del dispositivo
        CGSize size = [UIScreen mainScreen].bounds.size;
        
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        BOOL landscape = (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight);
        
        if (landscape) {
            frame.size.width  = size.height;
            frame.size.height = size.width;
        }
    }
    
    return frame;
}
#else
+ (CGRect)frameSchermo
{
    return [NSScreen mainScreen].frame;
}
#endif

+ (CGSize)dimensioniSchermo
{
    return [self frameSchermo].size;
}

+ (CGFloat)altezzaSchermo
{
    CGSize dimensioniSchermo = [self dimensioniSchermo];
    return fmax(dimensioniSchermo.height, dimensioniSchermo.width);
}

/**
 * Controlla se le dimensioni in pollici dello schermo del dispositivo sono <b>esattamente</b> quanto il valore specificato.
 * @param dimensioneSchermo Dimensione dello schermo in pollici
 */
+ (BOOL)isScreen:(CGFloat)dimensioneSchermo
{
    if (dimensioneSchermo == kPolliciSchermo_iPhone4) {
        return ([self altezzaSchermo] == kDimensioniSchermo_iPhone4.height) ? YES : NO;
    }
    else if (dimensioneSchermo == kPolliciSchermo_iPhone5) {
        return ([self altezzaSchermo] == kDimensioniSchermo_iPhone5.height) ? YES : NO;
    }
    else if (dimensioneSchermo == kPolliciSchermo_iPhone6) {
        return ([self altezzaSchermo] == kDimensioniSchermo_iPhone6.height) ? YES : NO;
    }
    else if (dimensioneSchermo == kPolliciSchermo_iPhone6plus) {
        return ([self altezzaSchermo] == kDimensioniSchermo_iPhone6plus.height) ? YES : NO;
    }
    
    return NO;
}

/**
 * Controlla se le dimensioni in pollici dello schermo del dispositivo sono <b>almeno</b> quanto il valore specificato.
 * @param dimensioneSchermo Dimensione dello schermo in pollici
 */
+ (BOOL)isScreenMin:(CGFloat)dimensioneSchermo
{
    if (dimensioneSchermo >= kPolliciSchermo_iPhone6plus) {
        return ([self altezzaSchermo] >= kDimensioniSchermo_iPhone6plus.height) ? YES : NO;
    }
    else if (dimensioneSchermo >= kPolliciSchermo_iPhone6) {
        return ([self altezzaSchermo] >= kDimensioniSchermo_iPhone6.height) ? YES : NO;
    }
    else if (dimensioneSchermo >= kPolliciSchermo_iPhone5) {
        return ([self altezzaSchermo] >= kDimensioniSchermo_iPhone5.height) ? YES : NO;
    }
    else if (dimensioneSchermo >= kPolliciSchermo_iPhone4) {
        return ([self altezzaSchermo] >= kDimensioniSchermo_iPhone4.height) ? YES : NO;
    }
    
    return NO;
}

/**
 * Controlla se le dimensioni in pollici dello schermo del dispositivo sono <b>al massimo</b> quanto il valore specificato.
 * @param dimensioneSchermo Dimensione dello schermo in pollici
 */
+ (BOOL)isScreenMax:(CGFloat)dimensioneSchermo
{
    if (dimensioneSchermo <= kPolliciSchermo_iPhone4) {
        return ([self altezzaSchermo] <= kDimensioniSchermo_iPhone4.height) ? YES : NO;
    }
    else if (dimensioneSchermo <= kPolliciSchermo_iPhone5) {
        return ([self altezzaSchermo] <= kDimensioniSchermo_iPhone5.height) ? YES : NO;
    }
    else if (dimensioneSchermo <= kPolliciSchermo_iPhone6) {
        return ([self altezzaSchermo] <= kDimensioniSchermo_iPhone6.height) ? YES : NO;
    }
    else if (dimensioneSchermo <= kPolliciSchermo_iPhone6plus) {
        return ([self altezzaSchermo] <= kDimensioniSchermo_iPhone6plus.height) ? YES : NO;
    }
    
    return NO;
}

#if DBFRAMEWORK_TARGET_IOS
+ (CGRect)frameStatusBar
{
    return [[UIApplication sharedApplication] statusBarFrame];
}

+ (CGSize)dimensioniStatusBar
{
    return [self frameStatusBar].size;
}

+ (CGFloat)altezzaStatusBar
{
    return [self dimensioniStatusBar].height;
}
#endif

+ (CGRect)frame:(CGRect)frame conOrigine:(CGPoint)origine
{
    CGRect nuovoFrame = frame;
    nuovoFrame.origin = origine;
    return nuovoFrame;
}

+ (CGRect)frame:(CGRect)frame conDimensioni:(CGSize)dimensioni
{
    CGRect nuovoFrame = frame;
    nuovoFrame.size = dimensioni;
    return nuovoFrame;
}

+ (CGRect)frame:(CGRect)frame conOrigineX:(CGFloat)origineX
{
    CGRect nuovoFrame = frame;
    nuovoFrame.origin.x = origineX;
    return nuovoFrame;
}

+ (CGRect)frame:(CGRect)frame conOrigineY:(CGFloat)origineY
{
    CGRect nuovoFrame = frame;
    nuovoFrame.origin.y = origineY;
    return nuovoFrame;
}

+ (CGRect)frame:(CGRect)frame conLarghezza:(CGFloat)larghezza
{
    CGRect nuovoFrame = frame;
    nuovoFrame.size.width = larghezza;
    return nuovoFrame;
}

+ (CGRect)frame:(CGRect)frame conAltezza:(CGFloat)altezza
{
    CGRect nuovoFrame = frame;
    nuovoFrame.size.height = altezza;
    return nuovoFrame;
}

+ (CGRect)frame:(CGRect)frame conPuntoDestro:(CGFloat)puntoDestro
{
    CGRect nuovoFrame = frame;
    nuovoFrame.origin.x = puntoDestro - frame.size.width;
    return nuovoFrame;
}

+ (CGRect)frame:(CGRect)frame conPuntoInferiore:(CGFloat)puntoInferiore
{
    CGRect nuovoFrame = frame;
    nuovoFrame.origin.y = puntoInferiore - frame.size.height;
    return nuovoFrame;
}

+ (CGRect)frame:(CGRect)frame conPadding:(CGFloat)padding
{
    CGRect nuovoFrame = frame;
    nuovoFrame.origin.x += padding;
    nuovoFrame.origin.y += padding;
    nuovoFrame.size.width  -= (padding * 2.0);
    nuovoFrame.size.height -= (padding * 2.0);
    return nuovoFrame;
}

+ (CGRect)frame:(CGRect)frame conScale:(CGFloat)scale
{
    CGRect nuovoFrame = frame;
    nuovoFrame.origin.x *= scale;
    nuovoFrame.origin.y *= scale;
    nuovoFrame.size.width  *= scale;
    nuovoFrame.size.height *= scale;
    return nuovoFrame;
}

+ (CGPoint)calcolaOrigineReale:(UIView *)vista conVistaParent:(UIView *)vistaParent
{
    // TODO: Controllare che vista sia discenda da vistaParent, altrimenti non è valido!
    
    CGPoint origine = vista.frame.origin;
    
    UIView *superview = vista.superview;
    
    while (superview && (superview != vistaParent)) {
        origine.x += superview.frame.origin.x;
        origine.y += superview.frame.origin.y;
        superview = superview.superview;
    }
    
    return origine;
}

CGSize CGSizeAspectFit(CGSize aspectRatio, CGSize boundingSize)
{
    float mW = boundingSize.width / aspectRatio.width;
    float mH = boundingSize.height / aspectRatio.height;
    if( mH < mW )
        boundingSize.width = boundingSize.height / aspectRatio.height * aspectRatio.width;
    else if( mW < mH )
        boundingSize.height = boundingSize.width / aspectRatio.width * aspectRatio.height;
    return boundingSize;
}

CGSize CGSizeAspectFill(CGSize aspectRatio, CGSize minimumSize)
{
    float mW = minimumSize.width / aspectRatio.width;
    float mH = minimumSize.height / aspectRatio.height;
    if( mH > mW )
        minimumSize.width = minimumSize.height / aspectRatio.height * aspectRatio.width;
    else if( mW > mH )
        minimumSize.height = minimumSize.width / aspectRatio.width * aspectRatio.height;
    return minimumSize;
}

CGFloat CGFloatGradiInRadianti(CGFloat gradi)
{
    return ((gradi) / 180.0 * M_PI);
}

// MARK: Metodi deprecati

+ (BOOL)isScreenSpecific:(CGFloat)dimensioneRichiesta
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza +[DBGeometry isScreen:]");
    return [self isScreen:dimensioneRichiesta];
}

+ (BOOL)isScreenMinimum:(CGFloat)dimensioneRichiesta
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza +[DBGeometry isScreenMin:]");
    return [self isScreenMin:dimensioneRichiesta];
}

+ (BOOL)isScreenMaximum:(CGFloat)dimensioneRichiesta
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza +[DBGeometry isScreenMax:]");
    return [self isScreenMax:dimensioneRichiesta];
}

@end


@implementation UIView (DBGeometry)

- (CGPoint)origine
{
    return self.frame.origin;
}

- (CGSize)dimensioni
{
    return self.frame.size;
}

- (CGFloat)origineX
{
    return self.frame.origin.x;
}

- (CGFloat)origineY
{
    CGRect frame = [DBGeometry convertiFrame:self.frame conFrameSuperview:self.superview.frame];
    return frame.origin.y;
}

- (CGFloat)larghezza
{
    return self.frame.size.width;
}

- (CGFloat)altezza
{
    return self.frame.size.height;
}

#if DBFRAMEWORK_TARGET_OSX
- (CGPoint)center
{
    return CGPointMake(NSMidX(self.frame), NSMidY(self.frame));
}

- (void)setCenter:(CGPoint)nuovoCentro
{
    CGPoint centroAttuale = self.center;
    self.origineX += centroAttuale.x - nuovoCentro.x;
    self.origineY += centroAttuale.y - nuovoCentro.y;
}
#endif

- (CGPoint)centro
{
    return self.center;
}

- (CGFloat)centroX
{
    return self.center.x;
}

- (CGFloat)centroY
{
    return self.center.y;
}

- (CGFloat)puntoDestro
{
    return self.frame.origin.x + self.frame.size.width;
}

- (CGFloat)puntoInferiore
{
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setOrigine:(CGPoint)origine
{
    self.frame = [DBGeometry frame:self.frame conOrigine:origine];
}

- (void)setDimensioni:(CGSize)dimensioni
{
    self.frame = [DBGeometry frame:self.frame conDimensioni:dimensioni];
}

- (void)setOrigineX:(CGFloat)origineX
{
    self.frame = [DBGeometry frame:self.frame conOrigineX:origineX];
}

- (void)setOrigineY:(CGFloat)origineY
{
    CGRect frame = [DBGeometry frame:self.frame conOrigineY:origineY];
    self.frame = [DBGeometry convertiFrame:frame conFrameSuperview:self.superview.frame];
}

- (void)setLarghezza:(CGFloat)larghezza
{
    self.frame = [DBGeometry frame:self.frame conLarghezza:larghezza];
}

- (void)setAltezza:(CGFloat)altezza
{
    CGRect frame = [DBGeometry frame:self.frame conAltezza:altezza];
    
    if ([DBGeometry conversioneSistemaCoordinate]) {
#if DBFRAMEWORK_TARGET_IOS
        // Da OSX a iOS
        frame.origin.y += altezza - self.frame.size.height;
#else
        // Da iOS a OSX
        frame.origin.y -= altezza - self.frame.size.height;
#endif
    }
    
    self.frame = frame;
}

- (void)setCentro:(CGPoint)centro
{
    self.center = centro;
}

- (void)setCentroX:(CGFloat)centroX
{
    self.center = CGPointMake(centroX, self.center.y);
}

- (void)setCentroY:(CGFloat)centroY
{
    self.center = CGPointMake(self.center.x, centroY);
}

- (void)setPuntoDestro:(CGFloat)puntoDestro
{
    self.frame = [DBGeometry frame:self.frame conPuntoDestro:puntoDestro];
}

- (void)setPuntoInferiore:(CGFloat)puntoInferiore
{
    self.frame = [DBGeometry frame:self.frame conPuntoInferiore:puntoInferiore];
}

- (void)posizionaOrizzontalmenteInPercentuale:(CGFloat)percentuale rispettoAllaVista:(UIView *)vista
{
    [self posizionaOrizzontalmenteInPercentuale:percentuale rispettoAlFrame:vista.frame margini:0 mantieniAllInterno:YES];
}

- (void)posizionaVerticalmenteInPercentuale:(CGFloat)percentuale rispettoAllaVista:(UIView *)vista
{
    [self posizionaVerticalmenteInPercentuale:percentuale rispettoAlFrame:vista.frame margini:0 mantieniAllInterno:YES];
}

- (void)posizionaOrizzontalmenteInPercentuale:(CGFloat)percentuale rispettoAlFrame:(CGRect)frame margini:(CGFloat)margini mantieniAllInterno:(BOOL)mantieniAllInterno
{
    // Check validita
    if (isnan(percentuale) || isinf(percentuale)) percentuale = 0.0;
    if (isnan(margini) || isinf(margini)) margini = 0.0;
    if (CGRectIsEmpty(frame)) return;
    
    // Aggiungo i margini
    frame.origin.x += margini;
    frame.size.width -= margini * 2.0;
    
    // Calcolo la futura origine, in percentuale all'interno del frame specificato
    CGFloat nuovaOrigine = frame.size.width * percentuale;
    nuovaOrigine += frame.origin.x;
    
    CGRect newFrame = self.frame;
    newFrame.origin.x = nuovaOrigine;
    
    // Faccio combaciare il punto centrale con il valore di percentuale specificato
    newFrame.origin.x -= newFrame.size.width / 2.0;
    
    if (mantieniAllInterno) {
        // Check sinistra
        if (newFrame.origin.x < frame.origin.x) {
            newFrame.origin.x = frame.origin.x;
        }
        
        // Check destra
        if (newFrame.origin.x + newFrame.size.width > frame.origin.x + frame.size.width) {
            newFrame.origin.x = frame.origin.x + frame.size.width - newFrame.size.width;
        }
    }
    
    self.frame = newFrame;
}

- (void)posizionaVerticalmenteInPercentuale:(CGFloat)percentuale rispettoAlFrame:(CGRect)frame margini:(CGFloat)margini mantieniAllInterno:(BOOL)mantieniAllInterno
{
    // Check validita
    if (isnan(percentuale) || isinf(percentuale)) percentuale = 0.0;
    if (isnan(margini) || isinf(margini)) margini = 0.0;
    if (CGRectIsEmpty(frame)) return;
    
    // Aggiungo i margini
    frame.origin.x += margini;
    frame.size.width -= margini * 2.0;
    
    // Calcolo la futura origine, in percentuale all'interno del frame specificato
    CGFloat nuovaOrigine = frame.size.height * percentuale;
    nuovaOrigine += frame.origin.y;
    
    // Faccio combaciare il punto centrale con il valore di percentuale specificato
    CGRect newFrame = self.frame;
    newFrame.origin.y = nuovaOrigine;
    
    // Faccio combaciare il punto centrale con il valore di percentuale specificato
    newFrame.origin.y -= newFrame.size.height / 2.0;
    
    if (mantieniAllInterno) {
        // Check sopra
        if (newFrame.origin.y < frame.origin.y) {
            newFrame.origin.y = frame.origin.y;
        }
        
        // Check sotto
        if (newFrame.origin.y + newFrame.size.height > frame.origin.y + frame.size.height) {
            newFrame.origin.y = frame.origin.y + frame.size.height - newFrame.size.height;
        }
    }
    
    self.frame = newFrame;
}


// MARK: - Autolayout

// This is a tag used to find the LayoutCollapsed constraint in the future
NSString *kLayoutCollapsedIdentifier = @"DBViewLayoutCollapsedConstraintIdentifier";

- (NSLayoutConstraint *)layoutCollapsedConstraint
{
    for (NSLayoutConstraint *constraint in self.constraints) {
        if ([constraint.identifier isEqualToString:kLayoutCollapsedIdentifier]) {
            return constraint;
        }
    }
    
    return nil;
}

/// Returns YES if this view has been collapsed with the -collapseLayout method.
- (BOOL)isLayoutCollapsed
{
    return ([self layoutCollapsedConstraint]) ? YES : NO;
}

/**
 * This method collapses view's height and hides from the display,
 * making it easy to accomplish and, eventually, restore the previous state
 * by calling -expandLayout.
 */
- (void)collapseLayout:(BOOL)animated
{
    if (self.isLayoutCollapsed == NO) {
        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0];
        constraint.identifier = kLayoutCollapsedIdentifier;
        [self addConstraint:constraint];
        
        [self setNeedsUpdateConstraints];
        [self setNeedsLayout];
        
        if (animated) {
            [self setAlpha:0.0];
        } else {
            [self setHidden:YES];
        }
    }
}

- (void)collapseLayout
{
    [self collapseLayout:NO];
}

/// Calling this method to a previously collapsed view restores its original state.
- (void)expandLayout:(BOOL)animated
{
    if (self.isLayoutCollapsed) {
        NSLayoutConstraint *constraint = [self layoutCollapsedConstraint];
        [self removeConstraint:constraint];
        
        [self setNeedsUpdateConstraints];
        [self setNeedsLayout];
        
        if (animated) {
            [self setAlpha:1.0];
        }
        
        [self setHidden:NO];
    }
}

- (void)expandLayout
{
    [self expandLayout:NO];
}

// This is a tag used to find the LayoutCollapsedHorizontally constraint in the future
NSString *kLayoutCollapsedHorizontallyIdentifier = @"DBViewLayoutCollapsedHorizontallyConstraintIdentifier";

- (NSLayoutConstraint *)layoutCollapsedHorizontallyConstraint
{
    for (NSLayoutConstraint *constraint in self.constraints) {
        if ([constraint.identifier isEqualToString:kLayoutCollapsedHorizontallyIdentifier]) {
            return constraint;
        }
    }
    
    return nil;
}

/// Returns YES if this view has been collapsed with the -collapseLayoutHorizontally method.
- (BOOL)isLayoutCollapsedHorizontally
{
    return ([self layoutCollapsedHorizontallyConstraint]) ? YES : NO;
}

/**
 * This method collapses view's width and hides from the display,
 * making it easy to accomplish and, eventually, restore the previous state
 * by calling -expandLayoutHorizontally.
 */
- (void)collapseLayoutHorizontally:(BOOL)animated
{
    if (self.isLayoutCollapsedHorizontally == NO) {
        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0];
        constraint.identifier = kLayoutCollapsedHorizontallyIdentifier;
        [self addConstraint:constraint];
        
        [self setNeedsUpdateConstraints];
        [self setNeedsLayout];
        
        if (animated) {
            [self setAlpha:0.0];
        } else {
            [self setHidden:YES];
        }
    }
}

- (void)collapseLayoutHorizontally
{
    [self collapseLayoutHorizontally:NO];
}

/// Calling this method to a previously collapsed view restores its original state.
- (void)expandLayoutHorizontally:(BOOL)animated
{
    if (self.isLayoutCollapsedHorizontally) {
        NSLayoutConstraint *constraint = [self layoutCollapsedHorizontallyConstraint];
        [self removeConstraint:constraint];
        
        [self setNeedsUpdateConstraints];
        [self setNeedsLayout];
        
        if (animated) {
            [self setAlpha:1.0];
        }
        
        [self setHidden:NO];
    }
}

- (void)expandLayoutHorizontally
{
    [self expandLayoutHorizontally:NO];
}


- (NSLayoutConstraint *)constraintConAttributo:(NSLayoutAttribute)attributo
{
    NSArray *constraints = [NSArray arrayWithArray:self.constraints];
    constraints = [constraints arrayByAddingObjectsFromArray:self.superview.constraints];
    
    NSMutableArray *trovate = [NSMutableArray array];
    
    // Determino se la constraint è agganciata al primo elemento o al secondo
    BOOL isPrimoElemento = (attributo != NSLayoutAttributeTrailing && attributo != NSLayoutAttributeBottom);
    
    // Constraint da evitare
    NSLayoutConstraint *layoutCollapsedConstraint = [self layoutCollapsedConstraint];
    
    for (NSLayoutConstraint *constraint in constraints) {
        
        // Mi assicuro che sia una constraint valida
        if ([constraint isMemberOfClass:[NSLayoutConstraint class]] == NO) continue;
        if ([DBDevice isiOSMin:8.0] && constraint.isActive == NO) continue;
        if ([constraint isEqual:layoutCollapsedConstraint]) continue;
        
        if (isPrimoElemento) {
            if (constraint.firstAttribute != attributo) continue;
        }
        else {
            if (constraint.secondAttribute != attributo) continue;
        }
        
        if (constraint.firstItem != self && constraint.secondItem != self) continue;
        
        if (!trovate) {
            // Trovata la prima constraint
            [trovate addObject:constraint];
        }
        else {
            // Trovata un'altra constraint, ordino per priorità
            NSLayoutConstraint *primaConstraint = trovate.firstObject;
            if (constraint.priority > primaConstraint.priority) {
                // Constraint più forte
                [trovate insertObject:constraint atIndex:0];
            }
            else {
                // Constraint meno forte
                [trovate addObject:constraint];
            }
        }
    }
    
    // Constraint con priorità più alta
    return trovate.firstObject;
}

- (void)impostaValoreConstraint:(CGFloat)valore conAttributo:(NSLayoutAttribute)attributo
{
    NSLayoutConstraint *constraint = [self constraintConAttributo:attributo];
    
    if (constraint) {
        constraint.constant = safeDouble(valore);
        
        // Non richiamando questi metodi alcuni progetti potrebbero non aggiornare il layout automaticamente
        [self setNeedsUpdateConstraints];
        [self updateConstraintsIfNeeded];
        [self setNeedsLayout];
        [self layoutIfNeeded];
    }
}

- (void)setSubviewAutolayoutEqualWidthAndHeight:(UIView *)subview
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.1, "please use -[UIView(DBGeometry) setAutolayoutEqualMarginsForSubview:]");
    [self setAutolayoutEqualMarginsForSubview:subview];
}

- (void)setAutolayoutSize:(CGSize)size
{
    NSMutableArray *constraints = [NSMutableArray array];
    
    // Constraints
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:size.width];
    [width setPriority:900];
    [constraints addObject:width];
    
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:size.height];
    [height setPriority:900];
    [constraints addObject:height];
    
    [self addConstraints:constraints];
    [self layoutIfNeeded];
}

- (void)setAutolayoutEqualMarginsForSubview:(UIView *)subview
{
    if (!subview || !subview.superview) {
        // Not valid
        return;
    }
    
    // Constraints
    NSDictionary *views = NSDictionaryOfVariableBindings(subview);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0@900-[subview]-0@900-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0@900-[subview]-0@900-|" options:0 metrics:nil views:views]];
    
    [self layoutIfNeeded];
}

- (void)setAutolayoutEqualSizeForSubview:(UIView *)subview
{
    if (!subview || !subview.superview) {
        // Not valid
        return;
    }
    
    NSMutableArray *constraints = [NSMutableArray array];
    
    // Constraints
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:subview attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0];
    [width setPriority:900];
    [constraints addObject:width];
    
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:subview attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0];
    [height setPriority:900];
    [constraints addObject:height];
    
    [self addConstraints:constraints];
    [self layoutIfNeeded];
}

- (void)setAutolayoutEqualCenterForSubview:(UIView *)subview
{
    if (!subview || !subview.superview) {
        // Not valid
        return;
    }
    
    NSMutableArray *constraints = [NSMutableArray array];
    
    // Constraints
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:subview attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    [centerX setPriority:900];
    [constraints addObject:centerX];
    
    NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:subview attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [centerY setPriority:900];
    [constraints addObject:centerY];
    
    [self addConstraints:constraints];
    [self layoutIfNeeded];
}

- (void)copyContentLayoutPrioritiesFrom:(UIView *)view
{
    [self copyContentHuggingPrioritiesFrom:view];
    [self copyContentCompressionResistancePrioritiesFrom:view];
}

- (void)copyContentHuggingPrioritiesFrom:(UIView *)view
{
    UILayoutPriority horizontal = [view contentHuggingPriorityForAxis:UILayoutConstraintAxisHorizontal];
    UILayoutPriority vertical   = [view contentHuggingPriorityForAxis:UILayoutConstraintAxisVertical];
    [self setContentHuggingPriority:horizontal forAxis:UILayoutConstraintAxisHorizontal];
    [self setContentHuggingPriority:vertical   forAxis:UILayoutConstraintAxisVertical];
}

- (void)copyContentCompressionResistancePrioritiesFrom:(UIView *)view
{
    UILayoutPriority horizontal = [view contentCompressionResistancePriorityForAxis:UILayoutConstraintAxisHorizontal];
    UILayoutPriority vertical   = [view contentCompressionResistancePriorityForAxis:UILayoutConstraintAxisVertical];
    [self setContentCompressionResistancePriority:horizontal forAxis:UILayoutConstraintAxisHorizontal];
    [self setContentCompressionResistancePriority:vertical   forAxis:UILayoutConstraintAxisVertical];
}

- (NSLayoutConstraint *)aggiungiConstraintConValore:(CGFloat)valore attributo:(NSLayoutAttribute)attributo secondaView:(UIView *)secondaView
{
    // In sviluppo...
    
    NSLayoutConstraint *check = [self constraintConAttributo:attributo];
    
    if (check) {
        // Già esiste
        [self impostaValoreConstraint:valore conAttributo:attributo];
        return check;
    }
    
    // La creo
    NSLayoutAttribute secondoAttributo = [self secondLayoutAttributeWithFirst:attributo];
    secondaView = (secondoAttributo == NSLayoutAttributeNotAnAttribute) ? nil : secondaView;
    
    return [NSLayoutConstraint constraintWithItem:self attribute:attributo relatedBy:NSLayoutRelationEqual toItem:secondaView attribute:secondoAttributo multiplier:1.0 constant:valore];
}

- (NSLayoutAttribute)secondLayoutAttributeWithFirst:(NSLayoutAttribute)first
{
    switch (first) {
        case NSLayoutAttributeWidth:
        case NSLayoutAttributeHeight:
            return NSLayoutAttributeNotAnAttribute;
            
        case NSLayoutAttributeLeft:
            return NSLayoutAttributeRight;
        case NSLayoutAttributeRight:
            return NSLayoutAttributeLeft;
        case NSLayoutAttributeTop:
            return NSLayoutAttributeBottom;
        case NSLayoutAttributeBottom:
            return NSLayoutAttributeTop;
        case NSLayoutAttributeLeading:
            return NSLayoutAttributeTrailing;
        case NSLayoutAttributeTrailing:
            return NSLayoutAttributeLeading;
            
        default:
        case NSLayoutAttributeCenterX:
        case NSLayoutAttributeCenterY:
        case NSLayoutAttributeBaseline:
            return first;
    }
}

- (CGFloat)constraintTop
{
    return [self constraintConAttributo:NSLayoutAttributeTop].constant;
}

- (void)setConstraintTop:(CGFloat)constraintTop
{
    [self impostaValoreConstraint:constraintTop conAttributo:NSLayoutAttributeTop];
}

- (CGFloat)constraintBottom
{
    return [self constraintConAttributo:NSLayoutAttributeBottom].constant;
}

- (void)setConstraintBottom:(CGFloat)constraintBottom
{
    [self impostaValoreConstraint:constraintBottom conAttributo:NSLayoutAttributeBottom];
}

- (CGFloat)constraintLeft
{
    return self.constraintLeading;
}

- (void)setConstraintLeft:(CGFloat)constraintLeft
{
    self.constraintLeading = constraintLeft;
}

- (CGFloat)constraintRight
{
    return self.constraintTrailing;
}

- (void)setConstraintRight:(CGFloat)constraintRight
{
    self.constraintTrailing = constraintRight;
}

- (CGFloat)constraintLeading
{
    return [self constraintConAttributo:NSLayoutAttributeLeading].constant;
}

- (void)setConstraintLeading:(CGFloat)constraintLeading
{
    [self impostaValoreConstraint:constraintLeading conAttributo:NSLayoutAttributeLeading];
}

- (CGFloat)constraintTrailing
{
    return [self constraintConAttributo:NSLayoutAttributeTrailing].constant;
}

- (void)setConstraintTrailing:(CGFloat)constraintTrailing
{
    [self impostaValoreConstraint:constraintTrailing conAttributo:NSLayoutAttributeTrailing];
}

- (CGFloat)constraintLarghezza
{
    return [self constraintConAttributo:NSLayoutAttributeWidth].constant;
}

- (void)setConstraintLarghezza:(CGFloat)constraintLarghezza
{
    [self impostaValoreConstraint:constraintLarghezza conAttributo:NSLayoutAttributeWidth];
}

- (CGFloat)constraintAltezza
{
    return [self constraintConAttributo:NSLayoutAttributeHeight].constant;
}

- (void)setConstraintAltezza:(CGFloat)constraintAltezza
{
    [self impostaValoreConstraint:constraintAltezza conAttributo:NSLayoutAttributeHeight];
}

- (CGFloat)constraintCentroX
{
    return [self constraintConAttributo:NSLayoutAttributeCenterX].constant;
}

- (void)setConstraintCentroX:(CGFloat)constraintCentroX
{
    [self impostaValoreConstraint:constraintCentroX conAttributo:NSLayoutAttributeCenterX];
}

- (CGFloat)constraintCentroY
{
    return [self constraintConAttributo:NSLayoutAttributeCenterY].constant;
}

- (void)setConstraintCentroY:(CGFloat)constraintCentroY
{
    [self impostaValoreConstraint:constraintCentroY conAttributo:NSLayoutAttributeCenterY];
}

@end


#if DBFRAMEWORK_TARGET_IOS

@implementation UILabel (DBGeometry)

- (void)sizeToFitWidth
{
    CGSize dimensioniMassime = CGSizeMake(MAXFLOAT, self.frame.size.height);
    CGSize dimensioni = [self sizeThatFits:dimensioniMassime];
    
    CGRect frame = self.frame;
    frame.size.width = dimensioni.width;
    self.frame = frame;
}

/// Adatta la larghezza mantenendo l'altezza originale
- (void)adattaLarghezza
{
    [self sizeToFitWidth];
}

- (void)sizeToFitHeight
{
    CGSize dimensioniMassime = CGSizeMake(self.frame.size.width, MAXFLOAT);
    CGSize dimensioni = [self sizeThatFits:dimensioniMassime];
    
    CGRect frame = self.frame;
    frame.size.height = dimensioni.height;
    self.frame = frame;
}

/// Adatta l'altezza mantenendo la larghezza originale
- (void)adattaAltezza
{
    [self sizeToFitHeight];
}

@end


@implementation UITextField (DBGeometry)

- (void)sizeToFitWidth
{
    CGRect frame = self.frame;
    
    [self sizeToFit];
    
    frame.size.width = self.frame.size.width;
    self.frame = frame;
}

- (void)adattaLarghezza
{
    [self sizeToFitWidth];
}

- (void)sizeToFitHeight
{
    CGRect frame = self.frame;
    
    [self sizeToFit];
    
    frame.size.height = self.frame.size.height;
    self.frame = frame;
}

- (void)adattaAltezza
{
    [self sizeToFitHeight];
}

@end


@implementation UITextView (DBGeometry)

- (void)sizeToFitWidth
{
    CGRect frame = self.frame;
    
    [self sizeToFit];
    
    frame.size.width = self.frame.size.width;
    self.frame = frame;
}

- (void)adattaLarghezza
{
    [self sizeToFitWidth];
}

- (void)sizeToFitHeight
{
    CGRect frame = self.frame;
    
    [self sizeToFit];
    
    frame.size.height = self.frame.size.height;
    self.frame = frame;
}

- (void)adattaAltezza
{
    [self sizeToFitHeight];
}

@end

#endif
