//
//  DBZoomableImageView.h
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 09/20/2018
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@protocol DBZoomableImageViewDelegate;


@interface DBZoomableImageView : UIControl

@property (weak, nonatomic) id<DBZoomableImageViewDelegate> _Nullable delegate;

@property (strong, nonatomic) UIImage * _Nullable image;

@property (nonatomic) CGFloat minimumZoomScale;
@property (nonatomic) CGFloat maximumZoomScale;

@end


@protocol DBZoomableImageViewDelegate <NSObject>

@optional

- (void)zoomableImageViewTouchUpInside:(nonnull DBZoomableImageView *)sender
    NS_SWIFT_NAME(zoomableImageViewTouchUpInside(_:));

@end
