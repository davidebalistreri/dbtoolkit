//
//  DBImageView.m
//  File version: 1.0.0
//  Last modified: 03/02/2017
//
//  Created by Davide Balistreri on 03/02/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBImageView.h"

#import "DBNetworking.h"
#import "DBFileManager.h"
#import "DBGeometry.h"
#import "DBStringExtensions.h"
#import "DBThread.h"


@interface DBImageView ()

@property (weak, nonatomic) UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSString *imageTag;

@end


@implementation DBImageView

// MARK: - Setup

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

@synthesize setupCompleted = _setupCompleted;
- (void)privateSetupObject
{
    if (self.setupCompleted == NO) {
        // Prevent further initialization requests
        _setupCompleted = YES;
        
        // Perform the method implemented by the developer
        [self setupObject];
    }
}

- (void)setupObject
{
    // Developers may override this method and provide custom behavior
}


// MARK: - Private methods

- (void)setupActivityIndicator
{
    if (!self.activityIndicator) {
        UIActivityIndicatorView *activityIndicator = [UIActivityIndicatorView new];
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        
        // Layout
        activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:activityIndicator];
        [self setAutolayoutEqualCenterForSubview:activityIndicator];
        
        self.activityIndicator = activityIndicator;
    }
    
    [self.activityIndicator startAnimating];
    [self.activityIndicator layoutIfNeeded];
}

- (void)removeActivityIndicator
{
    if (self.activityIndicator) {
        [self.activityIndicator removeFromSuperview];
    }
}


// MARK: - Methods override

- (void)setImageWithoutAnimation:(UIImage *)image
{
    [super setImage:image];
}

- (void)setImage:(UIImage *)image
{
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    
    if (!self.superview) {
        // No animation when the image view is offscreen
        animationsEnabled = NO;
    }
    
    if (self.image) {
        // No animation when the image is changing
        animationsEnabled = NO;
    }
    
    if (animationsEnabled) {
        [UIView transitionWithView:self duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [super setImage:image];
        } completion:nil];
    }
    else {
        [super setImage:image];
    }
}

- (void)setImageWithURL:(NSURL *)url
{
    NSString *urlString = url.absoluteString;
    [self setImageWithURLString:urlString placeholderImage:nil completionBlock:nil];
}

- (void)setImageWithURLString:(NSString *)urlString
{
    [self setImageWithURLString:urlString placeholderImage:nil completionBlock:nil];
}

- (void)setImageWithURLString:(NSString *)urlString placeholderImage:(UIImage *)placeholderImage
{
    [self setImageWithURLString:urlString placeholderImage:placeholderImage completionBlock:nil];
}

- (void)setImageWithURLString:(NSString *)urlString placeholderImage:(UIImage *)placeholderImage completionBlock:(void (^)(UIImage *image))completionBlock
{
    NSString *imageTag = [NSString stringaMD5:urlString];
    self.imageTag = imageTag;
    
    BOOL exists = [DBFileManager imageWithURLStringExists:urlString];
    
    if (exists) {
        // Image already downloaded
        UIImage *image = [DBFileManager imageWithURLString:urlString];
        [self setImageWithoutAnimation:image];
        
        if (completionBlock) {
            completionBlock(image);
        }
    }
    else {
        // We need to download the image
        [self setupActivityIndicator];
        [self setImageWithoutAnimation:nil];
        
        __weak DBImageView *weakSelf = self;
        
        [DBNetworking GETImmagineConURLString:urlString completionBlock:^(DBNetworkingResponse * __nullable response) {
            
            if (imageTag != weakSelf.imageTag) {
                // In the meantime, another image was requested for this ImageView
                return;
            }
            
            if (response.success && [response.responseObject isKindOfClass:[UIImage class]]) {
                UIImage *image = response.responseObject;
                [weakSelf setImage:image];
                
                if (completionBlock) {
                    completionBlock(image);
                }
            }
            else {
                // Download failed: the placeholder image will be shown
                [weakSelf setImageWithoutAnimation:placeholderImage];
                
                if (completionBlock) {
                    completionBlock(nil);
                }
            }
            
            [weakSelf removeActivityIndicator];
        }];
    }
}

@end
