//
//  DBParallaxView.m
//  File version: 1.2.0
//  Last modified: 09/16/2016
//
//  Created by Davide Balistreri on 07/31/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBParallaxView.h"
#import "DBGeometry.h"
#import "DBViewExtensions.h"
#import "DBObjectExtensions.h"

@interface DBParallaxView ()

@property (strong, nonatomic) UIImageView *imageView;

// MARK: - Configurazione grafica (ancora in sviluppo)

/**
 * Le modalità di esecuzione del parallasse.
 */
typedef NS_ENUM(NSUInteger, DBParallaxViewModalitaParallasse) {
    /// Effetto parallasse solo se il contenuto esce fuori dallo schermo
    DBParallaxViewParallasseSoloFuori,
    /// Leggero effetto di parallasse quando il contenuto si sposta sullo schermo
    DBParallaxViewParallasseNormale,
    /// Accentuato effetto di parallasse quando il contenuto si sposta sullo schermo
    DBParallaxViewParallasseAccentuato,
    /// Configurazione di default
    DBParallaxViewParallasseAutomatico = DBParallaxViewParallasseSoloFuori
};

/**
 * La modalità di esecuzione del parallasse.
 */
@property (nonatomic) DBParallaxViewModalitaParallasse modalita;

@end


@implementation DBParallaxView

- (void)setupObject
{
    if (!self.imageView) {
        self.imageView = [UIImageView new];
        self.imageView.clipsToBounds = YES;
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        // self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        [self insertSubview:self.imageView atIndex:0];
        
        //        UIView *imageView = self.imageView;
        //        NSDictionary *views = NSDictionaryOfVariableBindings(imageView);
        //        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imageView]|" options:0 metrics:nil views:views]];
        //        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imageView]|" options:0 metrics:nil views:views]];
    }
    
    // Defaults
    self.parallaxEnabled = YES;
}

- (void)dealloc
{
    if (self.imageView) {
        [self.imageView removeFromSuperview];
        self.imageView = nil;
    }
    
    if (self.contentView) {
        [self.contentView removeFromSuperview];
        self.contentView = nil;
    }
}

- (void)didMoveToSuperview
{
    [super didMoveToSuperview];
    
    if (self.superview) {
        [self layoutIfNeeded];
        
        // Check contentView dinamica (inserita come subview già dallo Storyboard)
        if (!self.contentView) {
            for (UIView *subview in self.subviews) {
                if ([subview isNotEqual:self.imageView]) {
                    [subview removeFromSuperview];
                    // [subview removeConstraints:subview.constraints];
                    [self setContentView:subview];
                }
            }
        }
        
        [self impostaContenuto];
        self.superview.clipsToBounds = NO;
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.setupCompleted == NO) {
        if ([self.superview isKindOfClass:[UIScrollView class]]) {
            [self scrollViewDidScroll:(id)self.superview];
        }
    }
}

// MARK: - Configurazione

- (void)impostaContenuto
{
    self.imageView.frame   = self.bounds;
    self.contentView.frame = self.bounds;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    
    self.imageView.image = image;
    [self impostaContenuto];
}

- (void)setContentView:(UIView *)contentView
{
    if (_contentView) {
        // Rimuovo la vecchia contentView
        [_contentView removeFromSuperview];
    }
    
    _contentView = contentView;
    
    self.contentView.clipsToBounds = YES;
    self.contentView.translatesAutoresizingMaskIntoConstraints = YES;
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = DBViewAdattaDimensioni;
    [self addSubview:self.contentView];
    [self impostaContenuto];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self calcolaParallasseX:scrollView];
    [self calcolaParallasseY:scrollView];
    [self layoutIfNeeded];
}

- (void)calcolaParallasseX:(UIScrollView *)scrollView
{
    if (!scrollView || !self.isParallaxEnabled) {
        return;
    }
    
    // Ancora non va
    
    //    CGFloat contentOffsetX = scrollView.contentOffset.x;
    //    CGFloat puntoSinistro  = self.origineX - contentOffsetX;
    //    CGFloat puntoDestro    = self.puntoDestro - (contentOffsetX + scrollView.larghezza);
    //
    //    self.clipsToBounds = YES;
    //
    //    if (contentOffsetX > self.puntoDestro) {
    //        // E' fuori dallo schermo (sinistra)
    //    }
    //    else if (puntoSinistro < 0) {
    //        // L'immagine non è più visibile per intero perché sta scomparendo da sinistra
    //        self.imageView.origineX = -puntoSinistro / 2.0;
    //    }
    //    else if (puntoDestro > self.larghezza) {
    //        // E' fuori dallo schermo (destra)
    //    }
    //    else if (puntoDestro > 0) {
    //        // L'immagine non è più visibile per intero perché sta scomparendo da destra
    //        self.imageView.origineX = -puntoDestro / 2.0;
    //    }
    //    else {
    //        if (self.origineX <= 0.0) {
    //            // Aggancio l'immagine al bordo superiore
    //            self.imageView.origineX = contentOffsetX;
    //            self.imageView.larghezza = self.frame.size.width + fabs(self.imageView.origineX);
    //            self.clipsToBounds = NO;
    //        }
    //        else {
    //            // Rappresentazione 1:1
    //            self.imageView.origineX = 0.0;
    //            self.imageView.larghezza = self.frame.size.width;
    //        }
    //    }
    //    
    //    self.contentView.frame = self.imageView.frame;
}

- (void)calcolaParallasseY:(UIScrollView *)scrollView
{
    if (!scrollView || !self.isParallaxEnabled) {
        return;
    }
    
    CGRect frame = self.frame;
    
    CGFloat contentOffsetY = scrollView.contentOffset.y + scrollView.contentInset.top;
    CGFloat puntoSuperiore = self.origineY - contentOffsetY;
    CGFloat puntoInferiore = self.puntoInferiore - (contentOffsetY + scrollView.altezza);
    // NSLog(@"ContentOffsetY: %.2f | PuntoSuperiore: %.2f | PuntoInferiore: %.2f", contentOffsetY, puntoSuperiore, puntoInferiore);
    
    self.clipsToBounds = YES;
    
    if (contentOffsetY > self.puntoInferiore) {
        // NSLog(@"E' fuori dallo schermo (sopra)");
    }
    else if (puntoSuperiore < 0) {
        // NSLog(@"Il contenuto non è più visibile per intero perché sta scomparendo dall'alto");
        frame.origin.y = -puntoSuperiore / 2.0;
    }
    else if (self.origineY <= 0.0) {
        // NSLog(@"Aggancio il contenuto al bordo superiore");
        frame.origin.y = contentOffsetY;
        frame.size.height = self.frame.size.height + fabs(frame.origin.y);
        self.clipsToBounds = NO;
    }
    else if (puntoInferiore > self.altezza) {
        // NSLog(@"E' fuori dallo schermo (sotto)");
    }
    else if (puntoInferiore > 0) {
        // NSLog(@"Il contenuto non è più visibile per intero perché sta scomparendo dal basso");
        frame.origin.y = -puntoInferiore / 2.0;
    }
    else {
        // NSLog(@"Rappresentazione 1:1");
        frame.origin.y = 0.0;
        // OLD: frame.size.height = self.frame.size.height;
    }
    
    // Rounded values: a bit less smooth but more performance friendly
    frame = CGRectIntegral(frame);
    
    // NSLog(@"OrigineY: %.2f ContentOffset: %.2f", self.imageView.origineY, contentOffsetY);
    // NSLog(@"Frame: %@", NSStringFromCGRect(frame));
    
    self.imageView.frame = frame;
    self.contentView.frame = frame;
}

@end
