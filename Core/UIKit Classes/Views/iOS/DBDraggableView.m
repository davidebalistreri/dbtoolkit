//
//  DBDraggableView.m
//  File version: 1.0.1
//  Last modified: 03/21/2016
//
//  Created by Davide Balistreri on 07/07/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBDraggableView.h"

@interface DBDraggableView () <UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIPanGestureRecognizer *panRecognizer;
@property (nonatomic) CGPoint centroIniziale;

@end


@implementation DBDraggableView

// MARK: - Inizializzazione

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self inizializza];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self inizializza];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self inizializza];
    }
    return self;
}

- (void)inizializza
{
    self.userInteractionEnabled = YES;
    self.mode = DBDraggableViewModeLibero;
    
    // Pan Gesture Recognizer
    self.panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panRecognizer:)];
    self.panRecognizer.delegate = self;
    [self addGestureRecognizer:self.panRecognizer];
}

- (void)dealloc
{
    [self removeGestureRecognizer:self.panRecognizer];
    self.panRecognizer = nil;
}

@synthesize mode = _mode;
- (void)setMode:(DBDraggableViewMode)mode
{
    _mode = mode;
    
    // Controllo se la nuova modalità è coerente con il frame attuale della DraggableView
    self.centroIniziale = self.center;
    [self aggiornaPosizioneConSpostamento:CGPointZero];
}

- (BOOL)isEnabled
{
    return [self.panRecognizer isEnabled];
}

- (void)setEnabled:(BOOL)enabled
{
    self.panRecognizer.enabled = enabled;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // Controllo se il nuovo frame è coerente con i limiti di configurazione della DraggableView
    self.centroIniziale = self.center;
    [self aggiornaPosizioneConSpostamento:CGPointZero];
}

// MARK: - Gesture Recognizer

- (void)panRecognizer:(UIPanGestureRecognizer *)recognizer
{
    if (self.mode == DBDraggableViewModeNessuno) {
        // Spostamento bloccato
        return;
    }
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // Memorizzo la posizione di partenza
        self.centroIniziale = self.center;
        
        if (self.animazioneTouchDown) {
            // Animazione
            [UIView animateWithDuration:0.1 animations:^{
                self.transform = CGAffineTransformMakeScale(0.95, 0.95);
            }];
        }
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateEnded) {
        if (self.animazioneTouchDown) {
            // Animazione
            [UIView animateWithDuration:0.1 animations:^{
                self.transform = CGAffineTransformIdentity;
            }];
        }
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // Spostamento della DraggableView
        CGPoint spostamento = [recognizer translationInView:recognizer.view];
        
        // Aggiorno la posizione
        [self aggiornaPosizioneConSpostamento:spostamento];
    }
}

- (void)aggiornaPosizioneConSpostamento:(CGPoint)spostamento
{
    CGPoint centro = [self calcolaCentroConSpostamento:spostamento];
    
    BOOL didMove = (CGPointEqualToPoint(centro, self.center)) ? NO : YES;
    
    self.center = centro;
    
    if (didMove && [self.delegate respondsToSelector:@selector(draggableViewDidMove:)]) {
        // Notifico il delegate che il frame della DraggableView è cambiato
        [self.delegate draggableViewDidMove:self];
    }
}

- (CGPoint)calcolaCentroConSpostamento:(CGPoint)spostamento
{
    CGPoint centro = CGPointMake(self.centroIniziale.x + spostamento.x, self.centroIniziale.y + spostamento.y);
    
    if (self.mode == DBDraggableViewModeLibero) {
        // Qualsiasi spostamento va bene
        return centro;
    }
    else if (self.mode == DBDraggableViewModeNessuno) {
        // Nessuno spostamento va bene
        return self.centroIniziale;
    }
    
    // Controllo se con il nuovo centro la View finisce fuori dalla Superview
    CGSize dimensioniSuperview = self.superview.bounds.size;
    CGSize dimensioniView = CGSizeZero;
    
    if (self.mode == DBDraggableViewModeBordiAllInterno)
        dimensioniView = self.bounds.size;
    
    // Controllo punto sinistro
    if (centro.x - (dimensioniView.width / 2.0) < 0.0)
        centro.x = (dimensioniView.width / 2.0);
    
    // Controllo punto superiore
    if (centro.y - (dimensioniView.height / 2.0) < 0.0)
        centro.y = (dimensioniView.height / 2.0);
    
    // Controllo punto destro
    if (centro.x + (dimensioniView.width / 2.0) > dimensioniSuperview.width)
        centro.x = dimensioniSuperview.width - (dimensioniView.width / 2.0);
    
    // Controllo punto inferiore
    if (centro.y + (dimensioniView.height / 2.0) > dimensioniSuperview.height)
        centro.y = dimensioniSuperview.height - (dimensioniView.height / 2.0);
    
    // Restituisco il nuovo centro calcolato (che resta all'interno dei limiti stabiliti)
    return centro;
}


//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    UITouch *touch = [touches anyObject];
//    CGPoint location = [touch locationInView:self.superview];
//
//    // Per spostare la vista in base al punto di partenza
//    CGFloat correzioneX = self.center.x - location.x;
//    CGFloat correzioneY = self.center.y - location.y;
//    self.correzioneDalCentro = CGPointMake(correzioneX, correzioneY);
//}
//
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    if (self.mode == DBDraggableViewModeNessuno) {
//        // Spostamento bloccato
//        return;
//    }
//
//
//    UITouch *touch = [touches anyObject];
//    CGPoint center = [touch locationInView:self.superview];
//
//    // Sposto la vista in base al punto cliccato in partenza
//    center.x += self.correzioneDalCentro.x;
//    center.y += self.correzioneDalCentro.y;
//
//    if (self.mode != DBDraggableViewModeLibero) {
//        // Controllo se la vista va a finire fuori dallo schermo
//        CGSize dimensioni = CGSizeZero;
//
//        if (self.mode == DBDraggableViewModeBordiAllInterno)
//            dimensioni = self.bounds.size;
//
//        // Larghezza
//        if (center.x + (dimensioni.width / 2.0) > self.superview.bounds.size.width)
//            center.x = self.superview.bounds.size.width - (dimensioni.width / 2.0);
//
//        // Altezza
//        if (center.y + (dimensioni.height / 2.0) > self.superview.bounds.size.height)
//            center.y = self.superview.bounds.size.height - (dimensioni.height / 2.0);
//
//        // Origine X
//        if (center.x - (dimensioni.width / 2.0) < 0.0)
//            center.x = (dimensioni.width / 2.0);
//
//        // Origine Y
//        if (center.y - (dimensioni.height / 2.0) < 0.0)
//            center.y = (dimensioni.height / 2.0);
//    }
//
//    [UIView beginAnimations:nil context:nil];
//    self.center = center;
//    [UIView commitAnimations];
//}
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    if (self.mode == DBDraggableViewModeNessuno) {
//        // Spostamento bloccato
//        return;
//    }
//
//    if (self.animazioneTouchDown) {
//        [UIView animateWithDuration:0.1 animations:^{
//            self.transform = CGAffineTransformIdentity;
//        }];
//    }
//}

@end
