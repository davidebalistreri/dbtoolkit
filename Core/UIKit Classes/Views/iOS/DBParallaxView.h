//
//  DBParallaxView.h
//  File version: 1.2.0
//  Last modified: 09/16/2016
//
//  Created by Davide Balistreri on 07/31/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBView.h"

/**
 * ParallaxView permette di applicare effetti di parallasse su un'immagine (image) o su una vista (contentView).
 *
 * <b>Come funziona:</b>
 * <p>Una volta istanziata la vista, bisogna fissare la sua altezza e aggiungerla in una ScrollView sulla schermata.</p>
 * <p>Si imposta il contenuto attraverso una delle due proprietà <b>image</b> o <b>contentView</b>. </p>
 * <p>Per far funzionare l'effetto di spostamento è necessario notificare gli spostamenti della ScrollView, richiamando il metodo <b>scrollViewDidScroll</b>. </p>
 */
@interface DBParallaxView : DBView

/// L'immagine da aggiungere alla ParallaxView, su cui verrà eseguito l'effetto di parallasse.
@property (strong, nonatomic) IBInspectable UIImage *image;

/// La vista da aggiungere alla ParallaxView, su cui verrà eseguito l'effetto di parallasse.
@property (strong, nonatomic) UIView *contentView;

/// Default: enabled.
@property (nonatomic, getter=isParallaxEnabled) BOOL parallaxEnabled;

/**
 * Callback da richiamare quando la ScrollView viene spostata, per ricalcolare il parallasse.
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
    NS_SWIFT_NAME(scrollViewDidScroll(_:));

@end
