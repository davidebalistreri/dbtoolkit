//
//  DBImageView.h
//  File version: 1.0.0
//  Last modified: 03/02/2017
//
//  Created by Davide Balistreri on 03/02/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

/**
 * UIImageView object extended for an easier setup process and later use.
 * The setup sequence is the same as other objects, views and controls inside this framework.
 *
 * This custom ImageView has the following features:
 * - animates the image change with a cross dissolve animation
 * - shows a placeholder image when the download fails
 * - shows a spinning loader when setImageWithURL is used
 * - handles automatically multiple setImageWithURL requests, applying only the last one received
 */


@interface DBImageView : UIImageView

/// Boolean che indica se l'oggetto è stato inizializzato completamente da quando è stato creato.
@property (nonatomic, readonly) BOOL setupCompleted;

/**
 * Metodo da utilizzare per inizializzare l'oggetto.
 * <p>Viene richiamato automaticamente appena l'oggetto viene istanziato.</p>
 * <p>Deve essere eseguito un'unica volta.</p>
 */
- (void)setupObject;


- (void)setImageWithoutAnimation:(UIImage *)image;


- (void)setImageWithURL:(NSURL *)url;

- (void)setImageWithURLString:(NSString *)urlString;

- (void)setImageWithURLString:(NSString *)urlString placeholderImage:(UIImage *)placeholderImage;

- (void)setImageWithURLString:(NSString *)urlString placeholderImage:(UIImage *)placeholderImage completionBlock:(void (^)(UIImage *image))completionBlock;

@end
