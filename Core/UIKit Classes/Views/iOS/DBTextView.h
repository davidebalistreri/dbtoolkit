//
//  DBTextView.h
//  File version: 1.0.3
//  Last modified: 04/27/2017
//
//  Created by Davide Balistreri on 03/15/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

/**
 * UITextView object extended for an easier setup process and later use.
 * The setup sequence is the same as other objects, views and controls inside this framework.
 */

@interface DBTextView : UITextView

/// Boolean che indica se l'oggetto è stato inizializzato completamente da quando è stata creato.
@property (nonatomic, readonly) BOOL setupCompleted;

/**
 * Metodo da utilizzare per inizializzare l'oggetto.
 * <p>Viene richiamato automaticamente appena l'oggetto viene istanziato.</p>
 * <p>Deve essere eseguito un'unica volta.</p>
 */
- (void)setupObject;

@property (strong, nonatomic) NSString *placeholder;
@property (strong, nonatomic) UIFont   *placeholderFont;
@property (strong, nonatomic) UIColor  *placeholderColor;

@end
