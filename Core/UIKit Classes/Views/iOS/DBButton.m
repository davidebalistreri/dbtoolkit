//
//  DBButton.m
//  File version: 1.1.0
//  Last modified: 03/13/2016
//
//  Created by Davide Balistreri on 02/23/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBButton.h"
#import "DBGeometry.h"
#import "DBThread.h"

const CGFloat kDurataAnimazione = 0.12;

@interface DBButton ()

@property (nonatomic, getter = isSelezionato) BOOL selezionato;
@property (nonatomic) CGFloat zoomTouchDown;

@end


@implementation DBButton

// MARK: - Inizializzazione

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

@synthesize setupCompleted = _setupCompleted;
- (void)privateSetupObject
{
    if (self.setupCompleted == NO) {
        // Prevent further initialization requests
        _setupCompleted = YES;
        
        self.mode = DBButtonModeNessunaAnimazione;
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        // Perform the method implemented by the developer
        [self setupObject];
    }
}

- (void)setupObject
{
    // Developers may override this method and provide custom behavior
}

- (UIButtonType)buttonType
{
    return UIButtonTypeCustom;
}

- (void)setImage:(UIImage *)image forState:(UIControlState)state
{
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [super setImage:image forState:state];
}

@synthesize selezionato = _selezionato;
- (void)setSelezionato:(BOOL)selezionato
{
    if (selezionato == _selezionato) {
        // Lo stato non è cambiato dall'ultima volta
        return;
    }
    
    // Aggiorno lo stato e la grafica
    _selezionato = selezionato;
    
    [self aggiornaLayout];
}

- (UIImage *)immagineNormale
{
    return [self imageForState:UIControlStateNormal];
}

- (UIImage *)immagineSelezionato
{
    return [self imageForState:UIControlStateHighlighted];
}

- (void)setImmagineNormale:(UIImage *)immagineNormale
{
    [self setImage:immagineNormale forState:UIControlStateNormal];
}

- (void)setImmagineSelezionato:(UIImage *)immagineSelezionato
{
    [self setImage:immagineSelezionato forState:UIControlStateHighlighted];
}

- (UIColor *)coloreNormale
{
    return [self titleColorForState:UIControlStateNormal];
}

- (UIColor *)coloreSelezionato
{
    return [self titleColorForState:UIControlStateHighlighted];
}

- (void)setColoreNormale:(UIColor *)coloreNormale
{
    self.selezionatoAggiornaStile = YES;
    [self setTitleColor:coloreNormale forState:UIControlStateNormal];
}

- (void)setColoreSelezionato:(UIColor *)coloreSelezionato
{
    self.selezionatoAggiornaStile = YES;
    [self setTitleColor:coloreSelezionato forState:UIControlStateHighlighted];
}

- (void)aggiornaLayout
{
    if (self.selezionatoAggiornaStile) {
        [UIView performWithoutAnimation:^{
            if (self.isSelezionato == NO) {
                self.imageView.image = self.immagineNormale;
                self.titleLabel.textColor = self.coloreNormale;
            }
            else {
                self.imageView.image = self.immagineSelezionato;
                self.titleLabel.textColor = self.coloreSelezionato;
            }
        }];
    }
    
    if (self.mode == DBButtonModeNessunaAnimazione) {
        // Effetto disattivato
        return;
    }
    
    if (self.zoomTouchDown == 0.0) {
        // Calcolo lo zoom in base alla dimensione del frame
        
        CGFloat zoomTouchDown    = 0.94; // Minimo
        
        //        CGFloat riduzioneMassima = 10.0; // Pixel
        //        CGFloat dimensione = fmin(self.larghezza, self.altezza);
        //        CGFloat dimensioneTouchDown = dimensione * zoomTouchDown;
        //        CGFloat riduzione = dimensione - dimensioneTouchDown;
        //
        //        while (riduzione > riduzioneMassima) {
        //            zoomTouchDown += 0.005;
        //            dimensioneTouchDown = dimensione * zoomTouchDown;
        //            riduzione = dimensione - dimensioneTouchDown;
        //        }
        
        self.zoomTouchDown = zoomTouchDown;
        
        // NSLog(@"Zoom consigliato: %f", self.zoomTouchDown);
        // NSLog(@"Riduzione: %f", riduzione);
    }
    
    [self eseguiAnimazione:^{
        if (self.isSelezionato) {
            self.transform = CGAffineTransformMakeScale(self.zoomTouchDown, self.zoomTouchDown);
        }
        else {
            self.transform = CGAffineTransformIdentity;
        }
    }];
    
    // Effetto 3D, ancora da testare ***
    // CALayer *layer = self.layer;
    // CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    // rotationAndPerspectiveTransform.m34 = 1.0 / -500;
    // rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, 45.0f * M_PI / 180.0f, 0.0f, 1.0f, 0.0f);
    // layer.transform = rotationAndPerspectiveTransform;
}

- (void)eseguiAnimazione:(void(^)(void))block
{
    if ((NO)) {
        // Animazione normale
        [UIView animateWithDuration:kDurataAnimazione animations:^{
            block();
        }];
    }
    else {
        // Animazione fluida
        [UIView animateWithDuration:kDurataAnimazione delay:0 usingSpringWithDamping:1 initialSpringVelocity:1 options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState animations:^{
            block();
        } completion:nil];
    }
}

// MARK: - Control

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (self.enabled) {
        // [super touchesBegan:touches withEvent:event];
        self.highlighted = YES;
        [self sendActionsForControlEvents:UIControlEventTouchDown];
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (self.enabled) {
        self.highlighted = NO;
        
        UIControlEvents controlEvent = UIControlEventTouchUpOutside;
        
        CGPoint point = [[touches anyObject] locationInView:self];
        if (CGRectContainsPoint(self.bounds, point)) {
            controlEvent = UIControlEventTouchUpInside;
        }
        
        // [DBThread eseguiBlockConRitardo:(kDurataAnimazione / 2.0) block:^{
        [self sendActionsForControlEvents:controlEvent];
        // }];
    }
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (self.enabled) {
        self.highlighted = NO;
        [super touchesCancelled:touches withEvent:event];
    }
}

- (void)setHighlighted:(BOOL)highlighted
{
    if (self.enabled) {
        self.selezionato = highlighted;
    }
    
}

- (void)setSelected:(BOOL)selected
{
    
}

@end
