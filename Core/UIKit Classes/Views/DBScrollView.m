//
//  DBScrollView.m
//  File version: 1.0.0
//  Last modified: 06/25/2017
//
//  Created by Davide Balistreri on 06/25/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBScrollView.h"

@implementation DBScrollView

// MARK: - Setup

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

@synthesize setupCompleted = _setupCompleted;
- (void)privateSetupObject
{
    if (self.setupCompleted == NO) {
        // Prevent further initialization requests
        _setupCompleted = YES;
        
        // Perform the method implemented by the developer
        [self setupObject];
    }
}

- (void)setupObject
{
    // Developers may override this method and provide custom behavior
}

@end
