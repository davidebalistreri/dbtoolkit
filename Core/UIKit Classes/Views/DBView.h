//
//  DBView.h
//  File version: 1.0.1
//  Last modified: 11/26/2016
//
//  Created by Davide Balistreri on 05/11/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

/**
 * UIView object extended for an easier setup process and later use.
 * The setup sequence is the same as other objects, views and controls inside this framework.
 */

@interface DBView : UIView

/// Boolean che indica se l'oggetto è stato inizializzato completamente da quando è stato creato.
@property (nonatomic, readonly) BOOL setupCompleted;

@property (nonatomic, readonly) BOOL inizializzazioneCompletata
    DBFRAMEWORK_DEPRECATED(0.7.1, "please use -[DBView setupCompleted]");

@property (nonatomic, readonly) BOOL viewDidAppear;

/**
 * Metodo da utilizzare per inizializzare l'oggetto.
 * <p>Viene richiamato automaticamente appena l'oggetto viene istanziato.</p>
 * <p>Deve essere eseguito un'unica volta.</p>
 */
- (void)setupObject;

@end
