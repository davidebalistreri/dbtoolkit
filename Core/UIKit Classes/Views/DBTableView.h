//
//  DBTableView.h
//  File version: 1.0.0
//  Last modified: 08/03/2016
//
//  Created by Davide Balistreri on 08/03/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

/**
 * UITableView object extended for an easier setup process and later use.
 * The setup sequence is the same as other objects, views and controls inside this framework.
 */

@interface DBTableView : UITableView

/// Boolean che indica se l'oggetto è stato inizializzato completamente da quando è stata creato.
@property (nonatomic, readonly) BOOL setupCompleted;

/**
 * Metodo da utilizzare per inizializzare l'oggetto.
 * <p>Viene richiamato automaticamente appena l'oggetto viene istanziato.</p>
 * <p>Deve essere eseguito un'unica volta.</p>
 */
- (void)setupObject;

@end
