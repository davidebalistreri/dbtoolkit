//
//  DBAutoLayoutScrollView.m
//  File version: 1.1.1
//  Last modified: 01/05/2017
//
//  Created by Davide Balistreri on 08/03/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBAutoLayoutScrollView.h"
#import "DBUtility.h"

// MARK: - DBAutoLayoutScrollViewFixer subclass

/**
 * A simple subclassed View created to simplify identification and debugging purposes.
 */
@interface DBAutoLayoutScrollViewFixer : UIView

@end

@implementation DBAutoLayoutScrollViewFixer

@end


// MARK: - DBAutoLayoutScrollView class

@implementation DBAutoLayoutScrollView

// MARK: - Setup

- (void)setupObject
{
    [super setupObject];
    
    // Scroll view autolayout fix
    [self setupAutolayoutViewFix];
}

- (void)prepareForInterfaceBuilder
{
    [super prepareForInterfaceBuilder];
    
    // Scroll view autolayout fix
    [self setupAutolayoutViewFix];
    
    // Empty background replacement
    if (!self.backgroundColor) {
        self.backgroundColor = [UIColor colorWithRed:0.72 green:0.80 blue:0.89 alpha:0.5];
    }
}

- (void)setupAutolayoutViewFix
{
    // To fix the scroll view autolayout problem we need to insert a subview with some constraints
    DBAutoLayoutScrollViewFixer *view = [DBAutoLayoutScrollViewFixer new];
    [view setHidden:YES];
    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self insertSubview:view atIndex:0];
    
    // Constraints setup
    NSDictionary *views = NSDictionaryOfVariableBindings(view);
    
    // All priorities are set to 800 so that they are higher than default views priorities (750) but they are not required (1000)
    // If they were required, the content size would have been equal to this constraints and it wouldn't have been customizable
    UILayoutPriority priority = ([DBUtility isDevelopmentModeEnabled]) ? 900 : 800;
    NSDictionary *metrics = @{@"priority": [NSNumber numberWithFloat:priority]};
    
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0];
    width.priority = priority;
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]-0@priority-|" options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]-0@500-|" options:0 metrics:metrics views:views]];
    
    [self addConstraint:width];
}

@end
