//
//  DBAutoLayoutView.m
//  File version: 1.0.0
//  Last modified: 02/14/2016
//
//  Created by Davide Balistreri on 02/14/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBAutoLayoutView.h"
#import "DBGeometry.h"

@implementation DBAutoLayoutView

#if DBFRAMEWORK_TARGET_IOS
- (CGSize)intrinsicContentSize
{
    /**
     * Per massimizzare l'effetto di questa classe,
     * imposto il ContentHugging a 1000 su entrambe le direzioni
     * e il ContentCompression a 750 orizzontalmente e 250 verticalmente
     */
    
    [self setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [self setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    
    [self setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    [self setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
    
    [self setNeedsLayout];
    return [DBGeometry dimensioniSchermo];
}
#else
- (NSSize)intrinsicContentSize
{
    /**
     * Per massimizzare l'effetto di questa classe,
     * imposto il ContentHugging a 1000 su entrambe le direzioni
     * e il ContentCompression a 750 orizzontalmente e 250 verticalmente
     */
    
    [self setContentHuggingPriority:NSLayoutPriorityRequired forOrientation:NSLayoutConstraintOrientationHorizontal];
    [self setContentHuggingPriority:NSLayoutPriorityRequired forOrientation:NSLayoutConstraintOrientationVertical];
    
    [self setContentCompressionResistancePriority:NSLayoutPriorityDefaultHigh forOrientation:NSLayoutConstraintOrientationHorizontal];
    [self setContentCompressionResistancePriority:NSLayoutPriorityDefaultLow forOrientation:NSLayoutConstraintOrientationVertical];
    
    [self setNeedsLayout];
    return [DBGeometry dimensioniSchermo];
}
#endif

@end
