//
//  DBAutoLayoutScrollView.h
//  File version: 1.1.1
//  Last modified: 01/05/2017
//
//  Created by Davide Balistreri on 08/03/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBScrollView.h"

IB_DESIGNABLE

/**
 * DBScrollView object further extended to resolve autolayout issues in interface builder.
 *
 * This class aims to resolve one of the biggest problems with ScrollView's autolayout mechanism:
 * you will be able to add subviews in interface builder as if you were placing them inside a
 * normal view, because DBAutoLayoutScrollView's contentSize will be aware of its frame
 * and will not complain about content ambiguity.
 *
 * @note
 * If you are using this class through CocoaPods, please make sure you added/uncommented this line
 * of the Podfile, otherwise it will not work properly:
 * use_frameworks!
 */

@interface DBAutoLayoutScrollView : DBScrollView

@end
