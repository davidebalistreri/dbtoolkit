//
//  DBAutoLayoutTableView.m
//  File version: 1.0.0
//  Last modified: 06/24/2017
//
//  Created by Davide Balistreri on 06/24/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBAutoLayoutTableView.h"

#import "DBViewExtensions.h"


@interface DBAutoLayoutTableView ()

@property (strong, nonatomic) NSLayoutConstraint *layoutAutomaticHeight;

@end


@implementation DBAutoLayoutTableView

//- (void)setupObject
//{
//    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:300.0];
//    height.priority = UILayoutPriorityDefaultLow;
//
//    self.layoutAutomaticHeight = height;
//}

- (void)prepareForInterfaceBuilder
{
    [super prepareForInterfaceBuilder];
    
    if (self.scrollEnabled == NO) {
        NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:300.0];
        height.priority = UILayoutPriorityDefaultHigh;
        
        [self addConstraint:height];
    }
}

- (CGSize)intrinsicContentSize
{
    CGSize contentSize = [super intrinsicContentSize];
    
    if (self.scrollEnabled == NO) {
        CGFloat height = self.contentSizeReale.height;
        contentSize.height = unsignedDouble(floor(height));
    }
    
    return contentSize;
}

- (void)reloadData
{
    [super reloadData];
    
    if (self.scrollEnabled == NO) {
        [self invalidateIntrinsicContentSize];
        [self layoutIfNeeded];
    }
}

@end
