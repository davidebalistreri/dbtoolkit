//
//  DBAutoLayoutTableView.h
//  File version: 1.0.0
//  Last modified: 06/24/2017
//
//  Created by Davide Balistreri on 06/24/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBTableView.h"

IB_DESIGNABLE

/**
 * DBTableView object further extended to support automatic height based on the content size.
 *
 * If you want to enable the automatic height you need to clear all height constraints and
 * disable the scroll. It will automatically calculate the content size, even with automatic
 * cell height, and set the proper height to the TableView.
 *
 * @note
 * If you are using this class through CocoaPods, please make sure you added/uncommented this line
 * of the Podfile, otherwise it will not work properly:
 * use_frameworks!
 */

@interface DBAutoLayoutTableView : DBTableView

@end
