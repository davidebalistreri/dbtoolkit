//
//  NSImageView+AspectFill.h
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 14/02/2016
//
//  Creato da Davide Balistreri il 14/02/2016
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import <Cocoa/Cocoa.h>

@interface NSImageViewAspectFill : NSImageView

@end
