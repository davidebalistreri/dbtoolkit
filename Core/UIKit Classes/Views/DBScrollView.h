//
//  DBScrollView.h
//  File version: 1.0.0
//  Last modified: 06/25/2017
//
//  Created by Davide Balistreri on 06/25/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#if DBFRAMEWORK_VERSION_EARLIER_THAN(__DBFRAMEWORK_0_7_2)
    #error I progetti che utilizzavano DBScrollView per le funzioni di layout, devono aggiornarsi ad DBAutoLayoutScrollView.
#endif


/**
 * UIScrollView object extended for an easier setup process and later use.
 * The setup sequence is the same as other objects, views and controls inside this framework.
 */

@interface DBScrollView : UIScrollView

/// Boolean che indica se l'oggetto è stato inizializzato completamente da quando è stata creato.
@property (nonatomic, readonly) BOOL setupCompleted;

/**
 * Metodo da utilizzare per inizializzare l'oggetto.
 * <p>Viene richiamato automaticamente appena l'oggetto viene istanziato.</p>
 * <p>Deve essere eseguito un'unica volta.</p>
 */
- (void)setupObject;

@end
