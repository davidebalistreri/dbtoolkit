//
//  DBWebView.h
//  File version: 1.0.2
//  Last modified: 04/17/2020
//
//  Created by Davide Balistreri on 11/21/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


#import "DBView.h"
#import "DBWebViewPage.h"

@protocol DBWebViewDelegate;


@interface DBWebView : DBView

NS_ASSUME_NONNULL_BEGIN

@property (weak, nonatomic, nullable) id<DBWebViewDelegate> delegate;

/// Default: YES if WebKit is available
@property (nonatomic) BOOL useWebKitWebView;

/// Returns a WKWebView instance if supported
@property (nonatomic, readonly) id webView;

@property (nonatomic, readonly) UIScrollView *scrollView;
@property (nonatomic, getter=isScrollEnabled) BOOL scrollEnabled;

@property (strong, nonatomic, readonly) DBWebViewPage *currentPage;


- (void)loadHTMLString:(nullable NSString *)HTMLString;
- (void)loadHTMLString:(nullable NSString *)HTMLString baseURL:(nullable NSURL *)baseURL;

- (void)loadURLString:(nullable NSString *)URLString;
- (void)loadURL:(nullable NSURL *)URL;
- (void)loadURLRequest:(nullable NSURLRequest *)URLRequest;

- (void)reload;
- (void)stopLoading;

@property (nonatomic, readonly) BOOL canGoBack;
@property (nonatomic, readonly) BOOL canGoForward;

- (void)goBack;
- (void)goForward;

NS_ASSUME_NONNULL_END

@end


@interface DBWebView (Extended)

- (void)currentPageSize:(void(^ _Nullable)(CGSize currentPageSize))completionBlock;

@end


typedef NS_ENUM(NSInteger, DBWebViewNavigationType) {
    DBWebViewNavigationTypeLinkActivated,
    DBWebViewNavigationTypeFormSubmitted,
    DBWebViewNavigationTypeBackForward,
    DBWebViewNavigationTypeReload,
    DBWebViewNavigationTypeFormResubmitted,
    DBWebViewNavigationTypeOther = -1,
};


@protocol DBWebViewDelegate <NSObject>

NS_ASSUME_NONNULL_BEGIN

@optional

- (BOOL)webView:(DBWebView *)webView shouldLoadRequest:(NSURLRequest *)request withNavigationType:(DBWebViewNavigationType)navigationType;

- (void)webViewDidStartLoad:(DBWebView *)webView;
- (void)webView:(DBWebView *)webView didFailLoadWithError:(NSError *)error;
- (void)webViewDidFinishLoad:(DBWebView *)webView;

NS_ASSUME_NONNULL_END

@end
