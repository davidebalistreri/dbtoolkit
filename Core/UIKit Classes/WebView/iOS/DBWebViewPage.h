//
//  DBWebViewPage.h
//  File version: 1.0.0
//  Last modified: 11/21/2016
//
//  Created by Davide Balistreri on 11/21/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBObject.h"

@interface DBWebViewPage : DBObject

@property (nullable, nonatomic, readonly, copy) NSString *title;

@property (nullable, nonatomic, readonly, copy) NSURL *URL;

@end
