//
//  DBAssetLibrary.h
//  File version: 1.0.1
//  Last modified: 09/11/2016
//
//  Created by Davide Balistreri on 05/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#import <Photos/Photos.h>


/**
 * Questa classe necessita dell'inserimento di una chiave nell'Info.plist,
 * dove lo sviluppatore spiega in che modo viene utilizzata la fotocamera dall'app.
 *
 * Senza l'inserimento di questa chiave, il framework potrebbe non funzionare e/o lanciare un'eccezione.
 *
 * NSPhotoLibraryUsageDescription
 */

@interface DBAssetLibrary : NSObject

/// Generic CompletionBlock.
typedef void (^DBAssetLibraryBlock)(void);

typedef void (^DBAssetLibraryImageBlock)(UIImage * __nullable image, BOOL isThumbnail);

typedef void (^DBAssetLibraryVideoBlock)(AVAsset * __nullable video);


+ (BOOL)isAuthorizationGranted;

+ (void)requestAuthorizationWithSuccessBlock:(nullable DBAssetLibraryBlock)successBlock;

+ (nonnull NSArray<PHAsset *> *)assetsWithType:(PHAssetMediaType)type;

/**
 * Recupera l'Image dell'Asset specificato.
 *
 * Utilizzare questo metodo per recuperare l'immagine nella risoluzione specificata.
 */
+ (nullable UIImage *)imageWithAsset:(nullable PHAsset *)asset size:(CGSize)size;

/**
 * Recupera l'Image dell'Asset specificato, in modalità asincrona.
 *
 * Utilizzare questo metodo per velocizzare la rappresentazione dell'immagine: in base alla dimensione richiesta,
 * verrà restituita inizialmente un'immagine di anteprima, poi l'immagine con la risoluzione specificata.
 */
+ (void)asyncImageWithAsset:(nullable PHAsset *)asset size:(CGSize)size completionBlock:(nullable DBAssetLibraryImageBlock)completionBlock;

+ (void)asyncVideoWithAsset:(nullable PHAsset *)asset completionBlock:(nullable DBAssetLibraryVideoBlock)completionBlock;

@end
