//
//  DBAnimation.m
//  File version: 1.0.1
//  Last modified: 09/11/2016
//
//  Created by Davide Balistreri on 06/24/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBAnimation.h"
#import "DBThread.h"
#import "DBMath.h"

@interface DBAnimation ()

@property (nonatomic, getter=areAnimationsEnabled) BOOL animationsEnabled;

@property NSUInteger numberOfPerformedRepetitions;
@property UIViewAnimationOptions options;

@property (nonatomic, getter=isBouncingBack) BOOL bouncingBack;
@property (readonly) NSTimeInterval realDuration;
@property (readonly) NSTimeInterval realBouncePoint;

@end


@implementation DBAnimation

//+ (NSMutableArray *)sharedInstances
//{
//    @synchronized(self) {
//        NSMutableArray *sharedInstances = [DBCentralManager getRisorsa:@"DBSharedAnimations"];
//
//        if (!sharedInstances) {
//            sharedInstances = [NSMutableArray array];
//            [DBCentralManager setRisorsa:sharedInstances conIdentifier:@"DBSharedAnimations"];
//        }
//
//        return sharedInstances;
//    }
//}

- (void)setupObject
{
    self.duration = 0.2;
    
    self.animationsEnabled = [UIView areAnimationsEnabled];
    
    self.numberOfRepetitions = 1;
    self.bouncePoint = 0.5;
}

@synthesize bouncePoint = _bouncePoint;
- (void)setBouncePoint:(CGFloat)bouncePoint
{
    _bouncePoint = mantieniAllInterno(bouncePoint, 0.0, 1.0);
}

/// Durata che tiene conto della direzione attuale dell'animazione
- (NSTimeInterval)realDuration
{
    if (self.areAnimationsEnabled == NO) {
        return 0.0;
    }
    
    if (self.bouncesBack) {
        return self.duration * self.realBouncePoint;
    }
    
    return self.duration;
}

/// BouncePoint che tiene conto della direzione attuale dell'animazione
- (NSTimeInterval)realBouncePoint
{
    return (self.isBouncingBack) ? (1.0 - self.bouncePoint) : self.bouncePoint;
}

/// Opzioni che tengono conto della direzione attuale dell'animazione
- (UIViewAnimationOptions)realOptions
{
    NSInteger defaultOptions = UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionLayoutSubviews;
    
    if (self.bouncesBack) {
        // return (self.isBouncingBack == NO) ? UIViewAnimationOptionCurveEaseIn : UIViewAnimationOptionCurveEaseOut;
    }
    
    if (self.userInteractionEnabled) {
        return defaultOptions|UIViewAnimationOptionAllowUserInteraction;
    }
    
    return defaultOptions;
}

- (void)start
{
    if (self.isRunning) {
        // Non eseguirò di nuovo la stessa richiesta
        return;
    }
    
    _running = YES;
    
    [DBThread eseguiBlockConRitardo:self.delay block:^{
        // Avvio l'animazione
        [self prepareAnimation];
    }];
}

- (void)forceStart
{
    if (self.isRunning) {
        // Non eseguirò di nuovo la stessa richiesta
        return;
    }
    
    _running = YES;
    
    // Avvio l'animazione
    [self prepareAnimation];
}

@synthesize stopped = _stopped;
- (void)stop
{
    if (self.isRunning == NO) {
        // Richiesta non valida
        return;
    }
    
    if (self.isStopped) {
        // Non eseguirò di nuovo la stessa richiesta
        return;
    }
    
    _stopped = YES;
}

- (void)forceStop
{
    if (self.isRunning == NO) {
        // Richiesta non valida
        return;
    }
    
    if (self.isStopped) {
        // Non eseguirò di nuovo la stessa richiesta
        return;
    }
    
    _stopped = YES;
    [self invalidateAllAnimations];
    [self performFinalState:NO];
}

@synthesize cancelled = _cancelled;
- (void)cancel
{
    if (self.isRunning == NO) {
        // Richiesta non valida
        return;
    }
    
    if (self.isCancelled) {
        // Non eseguirò di nuovo la stessa richiesta
        return;
    }
    
    _cancelled = YES;
    [self invalidateAllAnimations];
    [self performFinalState:NO];
}

- (void)prepareAnimation
{
    // Azzero il conteggio delle ripetizioni eseguite
    self.numberOfPerformedRepetitions = 0;
    
    [self performAnimation];
}

- (void)performAnimation
{
    // Aggiorno il conteggio delle ripetizioni eseguite
    self.numberOfPerformedRepetitions++;
    
    // Andata...
    self.bouncingBack = NO;
    
    [self performInitialState:NO];
    
    [self animateWithCompletionBlock:^{
        if (self.bouncesBack == NO) {
            [self animationEnded];
        }
        else {
            // Ritorno...
            self.bouncingBack = YES;
            
            [self animateWithCompletionBlock:^{
                [self animationEnded];
            }];
        }
    }];
}

- (void)animationEnded
{
    if ((self.numberOfPerformedRepetitions >= self.numberOfRepetitions) && self.repeatsAutomatically == NO) {
        // Animazione terminata
        _finished = YES;
    }
    
    if (self.isCancelled) {
        // Animazione annullata
        _finished = YES;
    }
    else if (self.isStopped || self.isFinished) {
        // Animazione terminata
        [self performCompletionBlock];
    }
    else {
        // Animazione in corso
        [self performAnimation];
    }
}

- (void)animateWithCompletionBlock:(DBAnimationBlock)completionBlock
{
    if (self.useSpringWithDampingEffect) {
        if (self.bouncesBack && self.isBouncingBack == NO) {
            [self animateWithSpringEffect:completionBlock];
        }
        else {
            [self animateWithSpringDampingEffect:completionBlock];
        }
    }
    else if (self.useSpringEffect) {
        [self animateWithSpringEffect:completionBlock];
    }
    else {
        [self animateWithSmoothCurve:completionBlock];
    }
}

- (void)animateWithSmoothCurve:(DBAnimationBlock)completionBlock
{
    [UIView animateWithDuration:self.realDuration delay:0.0 options:self.realOptions animations:^{
        [self performFinalState:self.isBouncingBack];
    } completion:^(BOOL finished) {
        completionBlock();
    }];
}

- (void)animateWithSpringEffect:(DBAnimationBlock)completionBlock
{
    [UIView animateWithDuration:self.realDuration delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.0 options:self.realOptions animations:^{
        [self performFinalState:self.isBouncingBack];
    } completion:^(BOOL finished) {
        completionBlock();
    }];
}

- (void)animateWithSpringDampingEffect:(DBAnimationBlock)completionBlock
{
    [UIView animateWithDuration:self.realDuration delay:0.0 usingSpringWithDamping:0.6 initialSpringVelocity:0.4 options:self.realOptions animations:^{
        [self performFinalState:self.isBouncingBack];
    } completion:^(BOOL finished) {
        completionBlock();
    }];
}

- (void)invalidateAllAnimations
{
    BOOL areEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [UIView setAnimationsEnabled:areEnabled];
}

- (void)performInitialState:(BOOL)isBouncingBack
{
    BOOL reverse = (isBouncingBack) ? !self.reverse : self.reverse;
    
    if (reverse == NO && self.initialState) {
        self.initialState();
    }
    else if (reverse && self.finalState) {
        self.finalState();
    }
}

- (void)performFinalState:(BOOL)isBouncingBack
{
    BOOL reverse = (isBouncingBack) ? !self.reverse : self.reverse;
    
    if (reverse == NO && self.finalState) {
        self.finalState();
    }
    else if (reverse && self.initialState) {
        self.initialState();
    }
}

- (void)performCompletionBlock
{
    if (self.completionBlock) {
        self.completionBlock();
    }
}

@end


@implementation DBAnimation (DBAnimationHelpers)

+ (DBAnimation *)performWithDuration:(NSTimeInterval)duration finalState:(DBAnimationBlock)finalState
{
    return [self performAfterDelay:0.0 withDuration:duration useSpringEffect:NO useSpringWithDampingEffect:NO finalState:finalState completionBlock:nil];
}

+ (DBAnimation *)performWithDuration:(NSTimeInterval)duration finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock
{
    return [self performAfterDelay:0.0 withDuration:duration useSpringEffect:NO useSpringWithDampingEffect:NO finalState:finalState completionBlock:completionBlock];
}

+ (DBAnimation *)performWithDuration:(NSTimeInterval)duration useSpringEffect:(BOOL)useSpringEffect finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock
{
    return [self performAfterDelay:0.0 withDuration:duration useSpringEffect:useSpringEffect useSpringWithDampingEffect:NO finalState:finalState completionBlock:completionBlock];
}

+ (DBAnimation *)performWithDuration:(NSTimeInterval)duration useSpringWithDampingEffect:(BOOL)useSpringWithDampingEffect finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock
{
    return [self performAfterDelay:0.0 withDuration:duration useSpringEffect:NO useSpringWithDampingEffect:useSpringWithDampingEffect finalState:finalState completionBlock:completionBlock];
}

// MARK: Delay

+ (DBAnimation *)performAfterDelay:(NSTimeInterval)delay withDuration:(NSTimeInterval)duration finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock
{
    return [self performAfterDelay:delay withDuration:duration useSpringEffect:NO useSpringWithDampingEffect:NO finalState:finalState completionBlock:completionBlock];
}

+ (DBAnimation *)performAfterDelay:(NSTimeInterval)delay withDuration:(NSTimeInterval)duration useSpringEffect:(BOOL)useSpringEffect finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock
{
    return [self performAfterDelay:delay withDuration:duration useSpringEffect:useSpringEffect useSpringWithDampingEffect:NO finalState:finalState completionBlock:completionBlock];
}

+ (DBAnimation *)performAfterDelay:(NSTimeInterval)delay withDuration:(NSTimeInterval)duration useSpringWithDampingEffect:(BOOL)useSpringWithDampingEffect finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock
{
    return [self performAfterDelay:delay withDuration:duration useSpringEffect:NO useSpringWithDampingEffect:useSpringWithDampingEffect finalState:finalState completionBlock:completionBlock];
}

// MARK: Master

+ (DBAnimation *)performAfterDelay:(NSTimeInterval)delay withDuration:(NSTimeInterval)duration useSpringEffect:(BOOL)useSpringEffect useSpringWithDampingEffect:(BOOL)useSpringWithDampingEffect finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock
{
    DBAnimation *animation = [DBAnimation new];
    animation.delay = delay;
    animation.duration = duration;
    animation.finalState = finalState;
    animation.useSpringEffect = useSpringEffect;
    animation.useSpringWithDampingEffect = useSpringWithDampingEffect;
    animation.completionBlock = completionBlock;
    
    [animation start];
    return animation;
}

@end
