//
//  DBAssetLibrary.m
//  File version: 1.0.1
//  Last modified: 09/11/2016
//
//  Created by Davide Balistreri on 05/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBAssetLibrary.h"
#import "DBThread.h"
#import "DBDictionaryExtensions.h"
#import "DBDevice.h"

@interface DBAssetLibrary ()

@property (strong, nonatomic) PHCachingImageManager *cacheManager;

@end


@implementation DBAssetLibrary

+ (DBAssetLibrary *)sharedInstance
{
    @synchronized(self) {
        DBAssetLibrary *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedAssetLibrary"];
        
        if (!sharedInstance) {
            sharedInstance = [DBAssetLibrary new];
            sharedInstance.cacheManager = [PHCachingImageManager new];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedAssetLibrary"];
        }
        
        return sharedInstance;
    }
}

+ (BOOL)isAuthorizationGranted
{
    return ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized) ? YES : NO;
}

+ (void)requestAuthorizationWithSuccessBlock:(DBAssetLibraryBlock)successBlock
{
    // Synchronous check
    if ([self isAuthorizationGranted] && successBlock) {
        successBlock();
        return;
    }
    
    // Asynchronous request
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        // Asynchronous response
        [DBThread eseguiBlock:^{
            // We must call this in the main thread
            if ([self isAuthorizationGranted] && successBlock) {
                successBlock();
            }
        }];
    }];
}

+ (NSArray<PHAsset *> *)assetsWithType:(PHAssetMediaType)type
{
    NSMutableArray *assets = [NSMutableArray array];
    
    PHFetchOptions *fetchOptions = [PHFetchOptions new];
    
    if (@available(iOS 9.0, *)) {
        fetchOptions.includeAssetSourceTypes = PHAssetSourceTypeUserLibrary;
    }
    
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    
    PHFetchResult *assetCollection = [PHAsset fetchAssetsWithOptions:fetchOptions];
    
    for (PHAsset *asset in assetCollection) {
        if (asset.mediaType == type) {
            [assets addObject:asset];
        }
    }
    
    return [NSArray arrayWithArray:assets];
}

+ (nullable UIImage *)imageWithAsset:(PHAsset *)asset size:(CGSize)size
{
    DBAssetLibrary *sharedInstance = [DBAssetLibrary sharedInstance];
    
    PHImageRequestOptions *options = [PHImageRequestOptions new];
    options.synchronous = YES;
    options.networkAccessAllowed = YES;
    
    __block UIImage *image = nil;
    
    [sharedInstance.cacheManager requestImageForAsset:asset targetSize:size contentMode:PHImageContentModeDefault options:options resultHandler:^(UIImage *result, NSDictionary *info) {
        image = result;
    }];
    
    return image;
}

+ (void)asyncImageWithAsset:(PHAsset *)asset size:(CGSize)size completionBlock:(DBAssetLibraryImageBlock)completionBlock
{
    if (!asset || !completionBlock) {
        // Invalid request
        return;
    }
    
    DBAssetLibrary *sharedInstance = [DBAssetLibrary sharedInstance];
    
    PHImageRequestOptions *options = [PHImageRequestOptions new];
    options.networkAccessAllowed = YES;
    
    [sharedInstance.cacheManager requestImageForAsset:asset targetSize:size contentMode:PHImageContentModeDefault options:options resultHandler:^(UIImage *result, NSDictionary *info) {
        
        UIImage *image = result;
        BOOL isThumbnail = [info safeBoolForKey:PHImageResultIsDegradedKey];
        
        [DBThread eseguiBlock:^{
            if (completionBlock) {
                completionBlock(image, isThumbnail);
            }
        }];
    }];
}

+ (void)asyncVideoWithAsset:(PHAsset *)asset completionBlock:(DBAssetLibraryVideoBlock)completionBlock
{
    if (!asset || !completionBlock) {
        // Invalid request
        return;
    }
    
    DBAssetLibrary *sharedInstance = [DBAssetLibrary sharedInstance];
    
    PHVideoRequestOptions *options = [PHVideoRequestOptions new];
    options.networkAccessAllowed = YES;
    
    [sharedInstance.cacheManager requestAVAssetForVideo:asset options:options resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
        
        [DBThread eseguiBlock:^{
            if (completionBlock) {
                completionBlock(asset);
            }
        }];
    }];
}

@end
