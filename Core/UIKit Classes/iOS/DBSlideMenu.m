//
//  DBSlideMenu.m
//  File version: 1.2.2
//  Last modified: 08/05/2016
//
//  Created by Davide Balistreri on 08/19/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBSlideMenu.h"
#import "DBColorExtensions.h"
#import "DBViewExtensions.h"
#import "DBObjectExtensions.h"
#import "DBGeometry.h"
#import "DBUtility.h"
#import "DBDevice.h"
#import "DBApplication.h"
#import "DBNavigationStoryController.h"
#import "DBThread.h"
#import "DBAnimation.h"

CGFloat kDBRaggioOmbra = 8.0;
CGFloat kDBOpacitaOmbra = 0.3;
CGFloat kDBOscuramentoMenu = 0.6;
CGFloat kSlideMenuMinimumOffset = 0.1;
CGFloat kSlideMenuMaximumOffset = 0.8;
CGFloat kDBDurataAnimazioneApertura = 0.50;
CGFloat kDBDurataAnimazioneChiusura = 0.35;


@interface DBSlideMenu () <UIGestureRecognizerDelegate>

// Views
@property (strong, nonatomic, readonly) UIView *viewLeft;
@property (strong, nonatomic, readonly) UIView *viewMain;
@property (strong, nonatomic, readonly) UIView *viewRight;
@property (strong, nonatomic, readonly) UIView *viewSuper;
@property (strong, nonatomic, readonly) UIView *viewStatusBar;

/// Overlay della StatusBar
@property (strong, nonatomic) UIView *statusBarOverlay;
@property (strong, nonatomic) UIWindow *statusBarWindow;
/// Stile della StatusBar dell'applicazione
@property (strong, nonatomic) NSNumber *statusBarStyleBackup;

@property (strong, nonatomic) UITapGestureRecognizer *closeTapRecognizer;
@property (strong, nonatomic) UIPanGestureRecognizer *panRecognizer;

@property (nonatomic) CGFloat origineMenuAperto;

/// Se YES, la gesture in corso sta aprendo il menu, altrimenti lo sta chiudendo
@property (nonatomic) BOOL isCurrentGestureOpeningMenu;
@property (nonatomic) CGFloat lastOffset;

/// Lo spostamento minimo per iniziare ad aprire il menu.
@property (nonatomic) CGFloat minimumOffset;
@property (nonatomic) BOOL minimumOffsetPassed;

@property (nonatomic) BOOL isOpeningLeftMenu;
@property (nonatomic) BOOL isOpeningRightMenu;

/// L'eventuale animazione di apertura/chiusura in corso.
@property (strong, nonatomic) DBAnimation *animation;

/// L'apertura massima del menu.
@property (nonatomic) CGFloat maximumOffset;

@end


@implementation DBSlideMenu

+ (instancetype)sharedInstance
{
    @synchronized(self) {
        DBSlideMenu *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedSlideMenu"];
        
        if (!sharedInstance) {
            sharedInstance = [DBSlideMenu new];
            [sharedInstance inizializza];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedSlideMenu"];
        }
        
        return sharedInstance;
    }
}

- (void)inizializza
{
    if ([DBDevice isiPhoneApp] && [DBDevice isiPad]) {
        // Sulle app iPhone in esecuzione su iPad la UIStatusBar
        // non deve essere spostata perché è al di fuori del frame dell'app
    }
    else {
        
        //self.statusBarWindow = [[UIApplication sharedApplication] valueForKey:@"statusBarWindow"];

        /*
        Fix per iOS 13:
        Assertion failure in -[UIApplication _createStatusBarWithRequestedStyle:orientation:hidden:],  /BuildRoot/Library/Caches/com.apple.xbs/Sources/UIKitCore/UIKit-3899.22.15/UIApplication.m:5311
        Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'App called -statusBar or -statusBarWindow on UIApplication: this code must be changed as there's no longer a status bar or status bar window. Use the statusBarManager object on the window scene instead.'
         */
        self.statusBarWindow = nil;
    }
    
    // Gesture Recognizers
    self.panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panRecognizer:)];
    self.panRecognizer.delegate = self;
    // self.panRecognizer.cancelsTouchesInView = NO;
    // self.panRecognizer.delaysTouchesBegan = YES;
    
    self.closeTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognizer:)];
    self.closeTapRecognizer.delegate = self;
    // self.tapRecognizer.cancelsTouchesInView = NO;
    // self.tapRecognizer.delaysTouchesBegan = YES;
    
    // Listener per i cambiamenti della StatusBar
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificaStatusBarWillChangeFrame:) name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificaStatusBarDidChangeFrame:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
    
    // Default
    self.leftMenuOpeningMode = DBSlideMenuOpeningModeDefault;
    self.leftMenuStatusBarMode = DBSlideMenuStatusBarModeDefault;
    
    self.rightMenuOpeningMode = DBSlideMenuOpeningModeDefault;
    self.rightMenuStatusBarMode = DBSlideMenuStatusBarModeDefault;
    
    self.enabled = YES;
}

- (void)notificaStatusBarWillChangeFrame:(NSNotification *)notification
{
    [self forceCloseMenu:YES completionBlock:nil];
    
    // Animazione per il cambio di frame (la durata dell'animazione di sistema è di circa 0.35 secondi)
    [DBAnimation performWithDuration:0.35 finalState:^{
        self.statusBarOverlay.altezza = [DBGeometry altezzaStatusBar];
    }];
}

- (void)notificaStatusBarDidChangeFrame:(NSNotification *)notification
{
    // Confermo il frame finale della StatusBar
    self.statusBarOverlay.altezza = [DBGeometry altezzaStatusBar];
}

- (void)enableShadows:(UIView *)view
{
    view.clipsToBounds = NO;
    
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowRadius = kDBRaggioOmbra;
    view.layer.shadowOpacity = kDBOpacitaOmbra;
    view.layer.shadowPath = [UIBezierPath bezierPathWithRect:view.bounds].CGPath;
}

- (void)disableShadows:(UIView *)view
{
    view.layer.shadowOpacity = 0.0;
    view.layer.shadowRadius = 0.0;
}

@synthesize tintColor = _tintColor;
- (void)setTintColor:(UIColor *)tintColor
{
    _tintColor = tintColor;
    self.statusBarOverlay.backgroundColor = self.tintColor;
}

- (UIColor *)tintColor
{
    if (_tintColor) {
        return _tintColor;
    }
    else if ([DBUtility tintColor]) {
        return [DBUtility tintColor];
    }
    else {
        return [UIColor blackColor];
    }
}

@synthesize mainController = _mainController;
- (void)setMainController:(UIViewController *)mainController
{
    [self forceCloseMenu:NO completionBlock:nil];
    
    if (_mainController) {
        // Rimuovo i riferimenti al vecchio controller
        [self.panRecognizer.view removeGestureRecognizer:self.panRecognizer];
        [self disableShadows:self.viewMain];
    }
    
    _mainController = mainController;
    
    if (mainController) {
        // Configurazione
        CGFloat larghezza = [DBApplication mainWindow].larghezza;
        // self.minimumOffset = parseInt(larghezza * kSlideMenuMinimumOffset);
        self.maximumOffset = parseInt(larghezza * kSlideMenuMaximumOffset);
        
        // Reimposto gli stati iniziali dei menu e i Gesture Recognizer
        [self setLeftMenuController:self.leftMenuController];
        [self setRightMenuController:self.rightMenuController];
        
        [self setupGestureRecognizers];
    }
}

- (UIView *)viewMain
{
    return self.mainController.view;
}

- (UIView *)viewSuper
{
    UIView *view = self.viewMain;
    UIView *superview = view.superview;
    UIWindow *window = view.window;
    UIWindow *mainWindow = [DBApplication mainWindow];
    
    if (superview) {
        return superview;
    }
    else if (window) {
        return window;
    }
    else if (mainWindow) {
        return mainWindow;
    }
    else {
        return view;
    }
}

@synthesize leftMenuController = _leftMenuController;
- (void)setLeftMenuController:(UIViewController *)leftMenuController
{
    [self forceCloseMenu:NO completionBlock:nil];
    
    _leftMenuController = leftMenuController;
    
    [self setupMenu:self.viewLeft];
}

- (UIView *)viewLeft
{
    return self.leftMenuController.view;
}

@synthesize rightMenuController = _rightMenuController;
- (void)setRightMenuController:(UIViewController *)rightMenuController
{
    [self forceCloseMenu:NO completionBlock:nil];
    
    _rightMenuController = rightMenuController;
    
    [self setupMenu:self.viewRight];
}

- (UIView *)viewRight
{
    return self.rightMenuController.view;
}

- (void)setupMenu:(UIView *)view
{
    view.frame = self.viewMain.bounds;
    view.larghezza = self.maximumOffset;
    view.autoresizingMask = DBViewAdattaDimensioni;
    view.alpha = 1.0 - kDBOscuramentoMenu;
}

@synthesize enabled = _enabled;
- (void)setEnabled:(BOOL)enabled
{
    _enabled = enabled;
    
    if (!enabled) {
        [self forceCloseMenu:NO completionBlock:nil];
    }
}

@synthesize leftMenuOpeningMode = _leftMenuOpeningMode;
- (void)setLeftMenuOpeningMode:(DBSlideMenuOpeningMode)leftMenuOpeningMode
{
    [self forceCloseMenu:NO completionBlock:nil];
    _leftMenuOpeningMode = leftMenuOpeningMode;
}

@synthesize leftMenuStatusBarMode = _leftMenuStatusBarMode;
- (void)setLeftMenuStatusBarMode:(DBSlideMenuStatusBarMode)leftMenuStatusBarMode
{
    [self forceCloseMenu:NO completionBlock:nil];
    _leftMenuStatusBarMode = leftMenuStatusBarMode;
}

@synthesize rightMenuOpeningMode = _rightMenuOpeningMode;
- (void)setRightMenuOpeningMode:(DBSlideMenuOpeningMode)rightMenuOpeningMode
{
    [self forceCloseMenu:NO completionBlock:nil];
    _rightMenuOpeningMode = rightMenuOpeningMode;
}

@synthesize rightMenuStatusBarMode = _rightMenuStatusBarMode;
- (void)setRightMenuStatusBarMode:(DBSlideMenuStatusBarMode)rightMenuStatusBarMode
{
    [self forceCloseMenu:NO completionBlock:nil];
    _rightMenuStatusBarMode = rightMenuStatusBarMode;
}

/**
 * Imposta il PanGestureRecognizer in base allo stato del menu:
 * - se chiuso va aggiunto al viewMain
 * - se aperto va aggiunto al viewSuper
 */
- (void)setupGestureRecognizers
{
    // Rimuovo il pan recognizer dalla view precedente
    [self.panRecognizer.view removeGestureRecognizer:self.panRecognizer];
    
    if (self.isMenuOpen) {
        // Aggiungo il tap recognizer alla viewSuper così posso bloccare i click sulla viewMain
        self.viewMain.userInteractionEnabled = NO;
        [self.viewSuper addGestureRecognizer:self.closeTapRecognizer];
        
        // Aggiungo il pan recognizer alla viewSuper così posso gestire il pan anche fuori dalla viewMain
        [self.viewSuper addGestureRecognizer:self.panRecognizer];
    }
    else {
        // Rimuovo il tap recognizere riabilito i click sulla viewMain
        self.viewMain.userInteractionEnabled = YES;
        [self.closeTapRecognizer.view removeGestureRecognizer:self.closeTapRecognizer];
        
        // Aggiungo il pan recognizer solo alla viewMain per evitare di raggiungere view sbagliate
        [self.viewMain addGestureRecognizer:self.panRecognizer];
    }
}

- (BOOL)isConfigurazioneValida
{
    if (!self.mainController) {
        return NO;
    }
    else if (!self.leftMenuController && !self.rightMenuController) {
        return NO;
    }
    
    return self.enabled;
}

/// Indica se l'utente sta eseguendo la gesture.
- (BOOL)isPanGestureOngoing
{
    switch (self.panRecognizer.state) {
        case UIGestureRecognizerStateBegan:
        case UIGestureRecognizerStateChanged:
        case UIGestureRecognizerStateRecognized:
            return YES;
            
        default:
            break;
    }
    
    return NO;
}

@synthesize animation = _animation;
- (void)setAnimation:(DBAnimation *)animation
{
    _animation = animation;
    
    if (animation) {
        // Nuova animazione
        [self.panRecognizer invalidateGesture];
        [self.closeTapRecognizer invalidateGesture];
    }
}

+ (void)setEnabled:(BOOL)enabled
{
    DBSlideMenu *sharedInstance = [self sharedInstance];
    sharedInstance.enabled = enabled;
}

+ (BOOL)isLeftMenuOpen
{
    DBSlideMenu *sharedInstance = [self sharedInstance];
    return [sharedInstance isLeftMenuOpen];
}

+ (BOOL)isRightMenuOpen
{
    DBSlideMenu *sharedInstance = [self sharedInstance];
    return [sharedInstance isRightMenuOpen];
}

+ (void)openLeftMenu:(BOOL)animated
{
    [self openLeftMenu:animated completionBlock:nil];
}

+ (void)openLeftMenu:(BOOL)animated completionBlock:(DBSlideMenuBlock)completionBlock
{
    DBSlideMenu *sharedInstance = [self sharedInstance];
    [sharedInstance openLeftMenu:animated completionBlock:completionBlock];
}

+ (void)openRightMenu:(BOOL)animated
{
    [self openRightMenu:animated completionBlock:nil];
}

+ (void)openRightMenu:(BOOL)animated completionBlock:(DBSlideMenuBlock)completionBlock
{
    DBSlideMenu *sharedInstance = [self sharedInstance];
    [sharedInstance openRightMenu:animated completionBlock:completionBlock];
}

+ (void)closeMenu:(BOOL)animated
{
    [self closeMenu:animated completionBlock:nil];
}

+ (void)closeMenu:(BOOL)animated completionBlock:(DBSlideMenuBlock)completionBlock
{
    DBSlideMenu *sharedInstance = [self sharedInstance];
    [sharedInstance closeMenu:animated completionBlock:completionBlock];
}

- (BOOL)isMenuOpen
{
    return self.isLeftMenuOpen || self.isRightMenuOpen;
}

- (BOOL)isLeftMenuOpen
{
    return (self.viewLeft.superview) ? YES : NO;
}

- (BOOL)isRightMenuOpen
{
    return (self.viewRight.superview) ? YES : NO;
}

@synthesize isOpeningLeftMenu = _isOpeningLeftMenu;
- (BOOL)isOpeningLeftMenu
{
    return self.isLeftMenuOpen || _isOpeningLeftMenu;
}

@synthesize isOpeningRightMenu = _isOpeningRightMenu;
- (BOOL)isOpeningRightMenu
{
    return self.isRightMenuOpen || _isOpeningRightMenu;
}

- (void)menuWillAppear:(UIView *)view
{
    BOOL isLeftMenu = [view isEqual:self.viewLeft];
    
    // Configurations
    DBSlideMenuOpeningMode openingMode = (isLeftMenu) ? self.leftMenuOpeningMode : self.rightMenuOpeningMode;
    DBSlideMenuStatusBarMode statusBarMode = (isLeftMenu) ? self.leftMenuStatusBarMode : self.rightMenuStatusBarMode;
    
    // Menu View
    if (!view.superview) {
        if (openingMode == DBSlideMenuOpeningModeAboveMainController) {
            [self.viewSuper addSubview:view];
            [self enableShadows:view];
        }
        else {
            [self.viewSuper insertSubview:view atIndex:0];
            [self enableShadows:self.viewMain];
        }
        
        [self setupMenu:view];
        
        if (isLeftMenu) {
            [self moveLeftMenu:0.0];
        }
        else {
            [self moveRightMenu:0.0];
        }
        
        [self.viewMain endEditing:YES];
    }
    
    // StatusBar Overlay
    if (statusBarMode == DBSlideMenuStatusBarModeOverlay && !self.statusBarOverlay && self.statusBarWindow) {
        self.statusBarOverlay = [UIView new];
        self.statusBarOverlay.frame = self.statusBarWindow.bounds;
        self.statusBarOverlay.altezza = [DBGeometry altezzaStatusBar];
        self.statusBarOverlay.backgroundColor = self.tintColor;
        self.statusBarOverlay.alpha = 0.0;
        [self.statusBarWindow insertSubview:self.statusBarOverlay atIndex:0];
    }
}

- (void)menuDidDisappear:(UIView *)view
{
    // Configurations
    DBSlideMenuOpeningMode openingMode = ([view isEqual:self.viewLeft]) ? self.leftMenuOpeningMode : self.rightMenuOpeningMode;
    DBSlideMenuStatusBarMode statusBarMode = ([view isEqual:self.viewLeft]) ? self.leftMenuStatusBarMode : self.rightMenuStatusBarMode;
    
    // Menu View
    if (view.superview) {
        if (openingMode == DBSlideMenuOpeningModeAboveMainController) {
            [self disableShadows:view];
        }
        else {
            [self disableShadows:self.viewMain];
        }
        
        [view removeFromSuperview];
        view.alpha = 0.0;
        
        self.viewMain.alpha = 1.0;
    }
    
    // StatusBar Overlay
    if (statusBarMode == DBSlideMenuStatusBarModeOverlay && self.statusBarOverlay) {
        [self.statusBarOverlay removeFromSuperview];
        self.statusBarOverlay = nil;
    }
}

/**
 * Questo metodo apre il menu assicurandosi prima che una serie di condizioni siano valide.
 */
- (void)openLeftMenu:(BOOL)animated completionBlock:(DBSlideMenuBlock)completionBlock
{
    // Check se la configurazione del menu non è valida
    if ([self isConfigurazioneValida] == NO) return;
    
    // Check se l'utente ha terminato la gesture
    // if ([self isPanGestureOngoing] == NO) return;
    
    // Check se c'è già un'animazione in corso (per evitare chiamate consecutive)
    if (self.animation && self.animation.duration > 0.0) return;
    
    // Tutto ok, posso procedere con l'operazione richiesta
    [self forceOpenLeftMenu:animated completionBlock:completionBlock];
}

- (void)openRightMenu:(BOOL)animated completionBlock:(DBSlideMenuBlock)completionBlock
{
    // Check se la configurazione del menu non è valida
    if ([self isConfigurazioneValida] == NO) return;
    
    // Check se l'utente ha terminato la gesture
    // if ([self isPanGestureOngoing] == NO) return;
    
    // Check se c'è già un'animazione in corso (per evitare chiamate consecutive)
    if (self.animation && self.animation.duration > 0.0) return;
    
    // Tutto ok, posso procedere con l'operazione richiesta
    [self forceOpenRightMenu:animated completionBlock:completionBlock];
}

- (void)forceOpenLeftMenu:(BOOL)animated completionBlock:(DBSlideMenuBlock)completionBlock
{
    if (self.animation) {
        // Invalido l'animazione precedente
        [self.animation cancel];
    }
    
    if (self.leftMenuStatusBarMode == DBSlideMenuStatusBarModeOverlay && !self.statusBarStyleBackup) {
        // Aggiorno la StatusBar per l'apertura del Menu
        UIStatusBarStyle statusBarStyle = [[UIApplication sharedApplication] statusBarStyle];
        self.statusBarStyleBackup = [NSNumber numberWithInteger:statusBarStyle];
        
        if ([UIColor isColorLight:self.tintColor]) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        }
        else {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }
    }
    
    [self menuWillAppear:self.viewLeft];
    
    // Leggero ritardo altrimenti non viene aggiornata l'opacità iniziale
    [DBThread eseguiBlock:^{
        NSTimeInterval durata = (animated) ? kDBDurataAnimazioneApertura : 0;
        
        __block DBAnimation *currentAnimation = [DBAnimation performWithDuration:durata useSpringEffect:YES finalState:^{
            [self moveLeftMenu:self.maximumOffset];
            
        } completionBlock:^{
            // Controllo se nel frattempo l'animazione è stata invalidata
            if ([self.animation isNotEqual:currentAnimation]) {
                // L'animazione è stata invalidata, interrompo l'esecuzione del completion block
                return;
            }
            
            // Animazione terminata
            self.animation = nil;
            
            [self setupGestureRecognizers];
            
            if (completionBlock) {
                completionBlock();
            }
        }];
        
        self.animation = currentAnimation;
    }];
}

- (void)forceOpenRightMenu:(BOOL)animated completionBlock:(DBSlideMenuBlock)completionBlock
{
    if (self.animation) {
        // Invalido l'animazione precedente
        [self.animation cancel];
    }
    
    if (self.rightMenuStatusBarMode == DBSlideMenuStatusBarModeOverlay && !self.statusBarStyleBackup) {
        // Aggiorno la StatusBar per l'apertura del Menu
        UIStatusBarStyle statusBarStyle = [[UIApplication sharedApplication] statusBarStyle];
        self.statusBarStyleBackup = [NSNumber numberWithInteger:statusBarStyle];
        
        if ([UIColor isColorLight:self.tintColor]) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        }
        else {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }
    }
    
    [self menuWillAppear:self.viewRight];
    
    // Leggero ritardo altrimenti non viene aggiornata l'opacità iniziale
    [DBThread eseguiBlock:^{
        NSTimeInterval durata = (animated) ? kDBDurataAnimazioneApertura : 0;
        
        __block DBAnimation *currentAnimation = [DBAnimation performWithDuration:durata useSpringEffect:YES finalState:^{
            [self moveRightMenu:self.maximumOffset];
            
        } completionBlock:^{
            // Controllo se nel frattempo l'animazione è stata invalidata
            if ([self.animation isNotEqual:currentAnimation]) {
                // L'animazione è stata invalidata, interrompo l'esecuzione del completion block
                return;
            }
            
            // Animazione terminata
            self.animation = nil;
            
            [self setupGestureRecognizers];
            
            if (completionBlock) {
                completionBlock();
            }
        }];
        
        self.animation = currentAnimation;
    }];
}

/**
 * Questo metodo chiude il menu assicurandosi prima che una serie di condizioni siano valide.
 */
- (void)closeMenu:(BOOL)animated completionBlock:(DBSlideMenuBlock)completionBlock
{
    // Check se la configurazione del menu non è valida
    if ([self isConfigurazioneValida] == NO) return;
    
    // Check se c'è già un'animazione in corso (per evitare chiamate consecutive)
    if (self.animation && self.animation.duration > 0.0) return;
    
    // Tutto ok, posso procedere con l'operazione richiesta
    [self forceCloseMenu:animated completionBlock:completionBlock];
}

- (void)forceCloseMenu:(BOOL)animated completionBlock:(DBSlideMenuBlock)completionBlock
{
    if (self.animation) {
        // Invalido l'animazione precedente
        [self.animation cancel];
    }
    
    // Invalido i Gesture Recognizer
    [self.panRecognizer invalidateGesture];
    [self.closeTapRecognizer invalidateGesture];
    
    // Aggiorno la StatusBar per la chiusura del Menu, se necessario
    if (self.statusBarStyleBackup) {
        UIStatusBarStyle statusBarStyle = self.statusBarStyleBackup.integerValue;
        [[UIApplication sharedApplication] setStatusBarStyle:statusBarStyle animated:YES];
        self.statusBarStyleBackup = nil;
    }
    
    NSTimeInterval durata = (animated) ? kDBDurataAnimazioneChiusura : 0;
    
    __block DBAnimation *currentAnimation = [DBAnimation performWithDuration:durata finalState:^{
        if (self.isOpeningLeftMenu) {
            [self moveLeftMenu:0.0];
        }
        else if (self.isOpeningRightMenu) {
            [self moveRightMenu:0.0];
        }
        
    } completionBlock:^{
        // Controllo se nel frattempo l'animazione è stata invalidata
        if ([self.animation isNotEqual:currentAnimation]) {
            // L'animazione è stata invalidata, interrompo l'esecuzione del completion block
            return;
        }
        
        // Animazione terminata
        self.animation = nil;
        
        [self menuDidDisappear:self.viewLeft];
        [self menuDidDisappear:self.viewRight];
        
        [self setupGestureRecognizers];
        
        if (completionBlock) {
            completionBlock();
        }
    }];
    
    self.animation = currentAnimation;
}

- (void)moveLeftMenu:(CGFloat)offset
{
    CGFloat safeOffset = mantieniAllInterno(offset, 0.0, self.maximumOffset);
    CGFloat percentage = percentualeTraNumeri(safeOffset, 0.0, self.maximumOffset, YES);
    
    CGFloat alpha = (1.0 - kDBOscuramentoMenu) + (percentage * kDBOscuramentoMenu);
    
    switch (self.leftMenuOpeningMode) {
        case DBSlideMenuOpeningModeAlongOpeningSide:
            self.viewMain.origineX = safeOffset;
            self.viewLeft.origineX = 0.0;
            self.viewLeft.alpha = alpha;
            break;
        case DBSlideMenuOpeningModeAlongMainController:
            self.viewMain.origineX = safeOffset;
            self.viewLeft.origineX = safeOffset - self.maximumOffset;
            self.viewLeft.alpha = alpha;
            break;
        case DBSlideMenuOpeningModeParallax:
            self.viewMain.origineX = safeOffset;
            self.viewLeft.origineX = (safeOffset - self.maximumOffset) / 2.0;
            self.viewLeft.alpha = alpha;
            break;
        case DBSlideMenuOpeningModeAboveMainController:
            self.viewMain.origineX = 0.0;
            self.viewMain.alpha = 1.0 - (kDBOscuramentoMenu * percentage);
            self.viewLeft.origineX = safeOffset - self.maximumOffset;
            self.viewLeft.alpha = 1.0;
            self.viewLeft.layer.shadowOpacity = kDBOpacitaOmbra * percentage;
            break;
    }
    
    switch (self.leftMenuStatusBarMode) {
        case DBSlideMenuStatusBarModeDefault:
            break;
        case DBSlideMenuStatusBarModeOverlay:
            self.statusBarOverlay.alpha = percentage;
            break;
        case DBSlideMenuStatusBarModeAlongOpeningSide:
            if (self.leftMenuOpeningMode != DBSlideMenuOpeningModeAboveMainController) {
                self.statusBarWindow.origineX = safeOffset;
            }
            
            break;
    }
}

- (void)moveRightMenu:(CGFloat)offset
{
    CGFloat safeOffset = mantieniAllInterno(offset, 0.0, self.maximumOffset);
    CGFloat percentage = percentualeTraNumeri(safeOffset, 0.0, self.maximumOffset, YES);
    
    CGFloat alpha = (1.0 - kDBOscuramentoMenu) + (percentage * kDBOscuramentoMenu);
    
    switch (self.rightMenuOpeningMode) {
        case DBSlideMenuOpeningModeAlongOpeningSide:
            self.viewMain.origineX = -safeOffset;
            self.viewRight.origineX = self.viewMain.larghezza - self.maximumOffset;
            self.viewRight.alpha = alpha;
            break;
        case DBSlideMenuOpeningModeAlongMainController:
            self.viewMain.origineX = -safeOffset;
            self.viewRight.origineX = self.viewMain.larghezza - safeOffset;
            self.viewRight.alpha = alpha;
            break;
        case DBSlideMenuOpeningModeParallax:
            self.viewMain.origineX = -safeOffset;
            self.viewRight.origineX = self.viewMain.larghezza - ((safeOffset + self.maximumOffset) / 2.0);
            self.viewRight.alpha = alpha;
            break;
        case DBSlideMenuOpeningModeAboveMainController:
            self.viewMain.origineX = 0.0;
            self.viewMain.alpha = 1.0 - (kDBOscuramentoMenu * percentage);
            self.viewRight.origineX = self.viewMain.larghezza - safeOffset;
            self.viewRight.alpha = 1.0;
            self.viewRight.layer.shadowOpacity = kDBOpacitaOmbra * percentage;
            break;
    }
    
    switch (self.rightMenuStatusBarMode) {
        case DBSlideMenuStatusBarModeDefault:
            break;
        case DBSlideMenuStatusBarModeOverlay:
            self.statusBarOverlay.alpha = percentage;
            break;
        case DBSlideMenuStatusBarModeAlongOpeningSide:
            if (self.rightMenuOpeningMode != DBSlideMenuOpeningModeAboveMainController) {
                self.statusBarWindow.origineX = -safeOffset;
            }
            
            break;
    }
}

/**
 * Restituisce il view controller presentato dal view controller specificato,
 * potrebbe essere uno dei suoi child controllers o lui stesso.
 */
- (__kindof UIViewController *)presentedViewController:(__kindof UIViewController *)viewController
{
    // DBNavigationStoryController
    if ([viewController isKindOfClass:[DBNavigationStoryController class]]) {
        // Prendo il NavigationController attualmente mostrato dal DBNavigationStoryController
        DBNavigationStoryController *navigationStoryController = viewController;
        DBNavigationStory *navigationStory = navigationStoryController.presentedNavigationStory;
        UIViewController *childController = navigationStory.navigationController;
        
        if (childController) {
            return [self presentedViewController:childController];
        }
    }
    
    // UITabBarController
    if ([viewController isKindOfClass:[UITabBarController class]]) {
        // Prendo il ViewController attualmente mostrato dalla TabBarController
        UITabBarController *tabBarController = viewController;
        UIViewController *childController = tabBarController.selectedViewController;
        
        if (childController) {
            return [self presentedViewController:childController];
        }
    }
    
    // UINavigationController
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        // Prendo il ViewController attualmente mostrato dal NavigationController
        UINavigationController *navigationController = viewController;
        UIViewController *childController = navigationController.viewControllers.lastObject;
        
        if (childController) {
            return [self presentedViewController:childController];
        }
    }
    
    // Presented view controller
    if (viewController.presentedViewController) {
        UIViewController *childController = viewController.presentedViewController;
        return [self presentedViewController:childController];
    }
    
    // NSLog(@"^ presentedViewController: %@", NSStringFromClass([viewController class]));
    return viewController;
}

/**
 * Alcuni view controller devono essere filtrati.
 */
- (BOOL)isPresentedViewControllerValid:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:NSClassFromString(@"UIAlertController")]) {
        return NO;
    }
    
    return YES;
}

/**
 * Questa funzione chiede al presented view controller se è possibile aprire il menu di sinistra (left).
 */
- (BOOL)shouldOpenLeftMenu
{
    BOOL shouldOpen = (self.requireProtocolImplementation) ? NO : YES;
    
    id viewController = [self presentedViewController:self.mainController];
    
    if ([self isPresentedViewControllerValid:viewController] == NO) {
        return NO;
    }
    
    if (!self.leftMenuController) {
        return NO;
    }
    
    if ([viewController respondsToSelector:@selector(shouldOpenLeftMenu)]) {
        shouldOpen = [viewController shouldOpenLeftMenu];
    }
    
    return shouldOpen;
}

/**
 * Questa funzione chiede al presented view controller se è possibile aprire il menu di destra (right).
 */
- (BOOL)shouldOpenRightMenu
{
    BOOL shouldOpen = (self.requireProtocolImplementation) ? NO : YES;
    
    id viewController = [self presentedViewController:self.mainController];
    
    if ([self isPresentedViewControllerValid:viewController] == NO) {
        return NO;
    }
    
    if (!self.rightMenuController) {
        return NO;
    }
    
    if ([viewController respondsToSelector:@selector(shouldOpenRightMenu)]) {
        shouldOpen = [viewController shouldOpenRightMenu];
    }
    
    return shouldOpen;
}

/**
 * Questa funzione chiede al presented view controller se è possibile aprire uno dei due menu (left e right).
 */
- (BOOL)shouldOpenMenus
{
    return [self shouldOpenLeftMenu] || [self shouldOpenRightMenu];
}

// MARK: - Gesture Recognizers

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    // NSLog(@"^ gestureRecognizer shouldReceiveTouch");
    
    if ([self isConfigurazioneValida] == NO) {
        // Slide Menu isn't enabled or properly set up
        return NO;
    }
    
    if (self.animation) {
        // There's an animation in progress
        return NO;
    }
    
    if ([gestureRecognizer isEqual:self.closeTapRecognizer]) {
        UIView *view = touch.view;
        
        if (self.viewLeft && [view isDescendantOfView:self.viewLeft]) {
            return NO;
        }
        else if (self.viewRight && [view isDescendantOfView:self.viewRight]) {
            return NO;
        }
    }
    
    // Check menu abilitato
    if ([self shouldOpenMenus] == NO) {
        return NO;
    }
    
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    // NSLog(@"^ gestureRecognizer shouldRecognizeSimultaneouslyWith: %@", NSStringFromClass([otherGestureRecognizer class]));
    
    NSArray *gestureRecognizers = @[self.closeTapRecognizer, self.panRecognizer];
    if ([gestureRecognizers containsObject:otherGestureRecognizer]) {
        // Tap and Pan recognizers of Slide Menu can be triggered at the same time
        return YES;
    }
    
    // Recognize only the other
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    // NSLog(@"^ gestureRecognizer shouldRequireFailureOf: %@", NSStringFromClass([otherGestureRecognizer class]));
    
    if ([otherGestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]]) {
        // Recognize only the other
        return YES;
    }
    
    if ([otherGestureRecognizer.view isDescendantOfViewWithClass:[UIScrollView class]]) {
        // Recognize both
        return NO;
    }
    
    // Recognize only the other
    return YES;
}

//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
//{
//    return YES;
//}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // NSLog(@"^ gestureRecognizer shouldBegin");
    
    // Controllo il pan gesture recognizer
    if ([gestureRecognizer isEqual:self.panRecognizer]) {
        // Permetto solo gli spostamenti orizzontali
        UIPanGestureRecognizer *panRecognizer = (id)gestureRecognizer;
        CGPoint velocity = [panRecognizer velocityInView:panRecognizer.view];
        
        // NO se lo spostamento è verticale
        BOOL shouldBegin = fabs(velocity.x) > fabs(velocity.y);
        
        if (shouldBegin) {
            // Lo spostamento è orizzontale
            return YES;
        }
        else {
            return NO;
        }
    }
    
    // Tutti gli altri gesture recognizer
    return YES;
}

- (void)tapRecognizer:(UITapGestureRecognizer *)recognizer
{
    [self closeMenu:YES completionBlock:nil];
}

- (void)panRecognizer:(UIPanGestureRecognizer *)recognizer
{
    // NSLog(@"^ panRecognizer selector");
    
    // Distanza dal punto iniziale
    CGPoint translation = [recognizer translationInView:recognizer.view];
    CGFloat offset = translation.x;
    
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            [self panRecognizerBegan];
            break;
        case UIGestureRecognizerStateChanged:
            [self panRecognizerChangedWithOffset:offset];
            break;
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded:
            [self panRecognizerEnded];
            break;
        default:
            break;
    }
}

- (void)panRecognizerBegan
{
    // Initialization
    self.minimumOffsetPassed = NO;
    self.isCurrentGestureOpeningMenu = NO;
    
    if (self.isLeftMenuOpen && self.leftMenuOpeningMode == DBSlideMenuOpeningModeAboveMainController) {
        self.origineMenuAperto = self.viewLeft.larghezza;
    }
    else if (self.isRightMenuOpen && self.rightMenuOpeningMode == DBSlideMenuOpeningModeAboveMainController) {
        self.origineMenuAperto = -self.viewRight.larghezza;
    }
    else {
        self.origineMenuAperto = self.viewMain.origineX;
    }
}

- (void)panRecognizerChangedWithOffset:(CGFloat)offset
{
    // Check spostamento minimo
    if (self.minimumOffsetPassed == NO) {
        if (fabs(offset) <= self.minimumOffset) {
            // Spostamento minimo ancora non superato
            return;
        }
        
        // Spostamento minimo superato
        // NSLog(@"^ offset di spostamento superato: %.f", offset);
        
        // Controllo che nessuno dei menu sia già aperto
        if (!self.isLeftMenuOpen && !self.isRightMenuOpen) {
            
            // Determino il senso di apertura del menu
            if (offset > 0.0) {
                // Si vuole aprire il menu di sinistra, controllo che sia possibile
                if (self.viewLeft && [self shouldOpenLeftMenu]) {
                    // Permetto al gesture recognizer di aprire il menu
                    self.isOpeningLeftMenu = YES;
                    self.isOpeningRightMenu = NO;
                    [self menuWillAppear:self.viewLeft];
                }
                else {
                    // Ignoro lo spostamento
                    // NSLog(@"^ ignoro lo spostamento verso destra perché il menu di sinistra non si può aprire");
                    return;
                }
            }
            else if (offset < 0.0) {
                // Si vuole aprire il menu di destra, controllo che sia possibile
                if (self.viewRight && [self shouldOpenRightMenu]) {
                    // Permetto al gesture recognizer di aprire il menu
                    self.isOpeningLeftMenu = NO;
                    self.isOpeningRightMenu = YES;
                    [self menuWillAppear:self.viewRight];
                }
                else {
                    // Ignoro lo spostamento
                    // NSLog(@"^ ignoro lo spostamento verso sinistra perché il menu di destra non si può aprire");
                    return;
                }
            }
            else {
                // Ignoro lo spostamento
                return;
            }
        }
        
        self.minimumOffsetPassed = YES;
        self.lastOffset = MAXFLOAT;
    }
    
    offset += self.origineMenuAperto;
    
    // TODO FIXME Scatto all'inizio dello spostamento
    // if (self.isOpeningLeftMenu  && self.isLeftMenuOpen)  offset -= self.minimumOffset;
    //
    // if (self.isOpeningRightMenu) {
    //     if (self.isRightMenuOpen) {
    //         // FIXME
    //         offset -= self.minimumOffset;
    //     } else {
    //         offset += self.minimumOffset;
    //     }
    // }
    
    if (self.lastOffset == MAXFLOAT) {
        // Non faccio nulla
        self.lastOffset = offset;
    }
    else if (self.lastOffset > (offset + self.minimumOffset)) {
        self.isCurrentGestureOpeningMenu = !self.isOpeningLeftMenu;
        self.lastOffset = offset;
    }
    else if (self.lastOffset < (offset - self.minimumOffset)) {
        self.isCurrentGestureOpeningMenu = !self.isOpeningRightMenu;
        self.lastOffset = offset;
    }
    
    if (self.isOpeningLeftMenu) {
        [self moveLeftMenu:offset];
    }
    else if (self.isOpeningRightMenu) {
        [self moveRightMenu:-offset];
    }
}

- (void)panRecognizerEnded
{
    // Finisco di aprire / chiudere il menu
    if (self.isCurrentGestureOpeningMenu) {
        if (self.isOpeningLeftMenu) {
            [self openLeftMenu:YES completionBlock:nil];
        }
        else if (self.isOpeningRightMenu) {
            [self openRightMenu:YES completionBlock:nil];
        }
    }
    else {
        [self closeMenu:YES completionBlock:nil];
    }
}

@end
