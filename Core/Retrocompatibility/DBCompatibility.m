//
//  DBCompatibility.m
//  File version: 1.1.2
//  Last modified: 03/10/2016
//
//  Created by Davide Balistreri on 01/11/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBCompatibility.h"
#import "DBFramework.h"
#import "DBRuntimeHandler.h"

#import <QuartzCore/QuartzCore.h>

#if DBFRAMEWORK_TARGET_OSX

NSData *UIImagePNGRepresentation(NSImage *image)
{
    return NSImagePNGRepresentation(image);
}


// MARK: - Storyboard

@implementation NSStoryboard (DBCompatibility)

- (id)instantiateViewControllerWithIdentifier:(NSString *)identifier
{
    return [self instantiateControllerWithIdentifier:identifier];
}

@end


// MARK: - Window

@implementation NSWindow (DBCompatibility)

- (NSWindowController *)rootWindowController
{
    return [self oggettoAssociatoConSelector:@selector(rootWindowController)];
}

- (void)setRootWindowController:(NSWindowController *)rootWindowController
{
    [self associaOggettoStrong:rootWindowController conSelector:@selector(rootWindowController)];
}

- (NSViewController *)rootViewController
{
    return self.contentViewController;
}

- (void)setRootViewController:(NSViewController *)rootViewController
{
    self.contentViewController = rootViewController;
}

- (NSColor *)backgroundColor
{
    return nil;
}

- (void)setBackgroundColor:(NSColor *)backgroundColor
{
    
}

- (BOOL)clipsToBounds
{
    return NO;
}

- (void)setClipsToBounds:(BOOL)clipsToBounds
{
    
}

- (CGFloat)alpha
{
    return (self.isVisible) ? 1.0 : 0.0;
}

- (void)setAlpha:(CGFloat)alpha
{
    self.isVisible = (alpha == 0.0) ? NO : YES;
}

- (BOOL)opaque
{
    return YES;
}

- (void)setOpaque:(BOOL)opaque
{
    
}

- (BOOL)hidden
{
    return self.isVisible;
}

- (void)setHidden:(BOOL)hidden
{
    self.isVisible = !hidden;
}

- (NSRect)bounds
{
    return CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

- (NSInteger)windowLevel
{
    return self.level;
}

- (void)setWindowLevel:(NSInteger)windowLevel
{
    self.level = windowLevel;
}

- (void)addSubview:(NSView *)view
{
    [self.contentView addSubview:view];
}

@end


// MARK: - ViewController

@implementation NSViewController (DBCompatibility)

- (NSNavigationController *)navigationController
{
    return [self oggettoAssociatoConSelector:@selector(navigationController)];
}

- (void)setNavigationController:(NSNavigationController *)navigationController
{
    [self associaOggettoWeak:navigationController conSelector:@selector(navigationController)];
}

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^ __nullable)(void))completion
{
    [self dismissViewController:self];
    if (completion) completion();
}

@end


// MARK: - View

@implementation NSView (DBCompatibility)

- (BOOL)isUserInteractionEnabled
{
    return NO;
}

- (void)setUserInteractionEnabled:(BOOL)userInteractionEnabled
{
    
}

- (void)insertSubview:(NSView *)view atIndex:(NSInteger)index
{
    if (!view || index >= self.subviews.count) {
        // Parametri non validi
        return;
    }
    
    NSMutableArray *subviews = [NSMutableArray arrayWithArray:self.subviews];
    [subviews insertObject:view atIndex:index];
    self.subviews = subviews;
}

- (void)exchangeSubviewAtIndex:(NSInteger)index1 withSubviewAtIndex:(NSInteger)index2
{
    if ((index1 >= self.subviews.count) || (index2 >= self.subviews.count)) {
        // Parametri non validi
        return;
    }
    
    NSMutableArray *subviews = [NSMutableArray arrayWithArray:self.subviews];
    [subviews exchangeObjectAtIndex:index1 withObjectAtIndex:index2];
    self.subviews = subviews;
}

- (void)insertSubview:(NSView *)subview belowSubview:(NSView *)siblingSubview
{
    if (!subview || !siblingSubview) {
        // Parametri non validi
        return;
    }
    
    NSUInteger index = [self.subviews indexOfObject:siblingSubview];
    [self insertSubview:subview atIndex:index];
}

- (void)insertSubview:(NSView *)subview aboveSubview:(NSView *)siblingSubview
{
    if (!subview || !siblingSubview) {
        // Parametri non validi
        return;
    }
    
    NSUInteger index = [self.subviews indexOfObject:siblingSubview];
    [self insertSubview:subview atIndex:index+1];
}

- (void)bringSubviewToFront:(NSView *)view
{
    if (!view) {
        // Parametri non validi
        return;
    }
    
    NSMutableArray *subviews = [NSMutableArray arrayWithArray:self.subviews];
    [subviews removeObject:view];
    [subviews addObject:view];
    self.subviews = subviews;
}

- (void)sendSubviewToBack:(NSView *)view
{
    if (!view) {
        // Parametri non validi
        return;
    }
    
    NSMutableArray *subviews = [NSMutableArray arrayWithArray:self.subviews];
    [subviews removeObject:view];
    [subviews insertObject:view atIndex:0];
    self.subviews = subviews;
}

- (BOOL)endEditing:(BOOL)force
{
    return YES;
}

- (void)setNeedsUpdateConstraints
{
    [self setNeedsUpdateConstraints:YES];
}

- (void)updateConstraintsIfNeeded
{
    [self updateConstraintsForSubtreeIfNeeded];
}

- (void)setNeedsLayout
{
    [self setNeedsLayout:YES];
}

- (void)layoutIfNeeded
{
    [self layoutSubtreeIfNeeded];
}

#pragma View animations

static NSMutableArray *_animationGroups;
// static BOOL _animationsEnabled = YES;

+ (void)animateWithDuration:(NSTimeInterval)duration animations:(void (^)(void))animations
{
    [self animateWithDuration:duration delay:0 options:0 animations:animations completion:nil];
}

+ (void)animateWithDuration:(NSTimeInterval)duration animations:(void (^)(void))animations completion:(void (^)(BOOL))completion
{
    [self animateWithDuration:duration delay:0 options:0 animations:animations completion:completion];
}

+ (void)animateWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay options:(UIViewAnimationOptions)options animations:(void (^)(void))animations completion:(void (^)(BOOL))completion
{
    // Fin quando le animazioni non funzionano, è meglio eseguire tutto fin da subito
    duration = 0.0;
    
    if (duration == 0.0) {
        if (animations) animations();
        if (completion) completion(YES);
        return;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [NSAnimationContext runAnimationGroup:^(NSAnimationContext *context) {
            [context setDuration:duration];
            
            switch (options) {
                case UIViewAnimationOptionCurveEaseInOut:
                    [context setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                    break;
                case UIViewAnimationOptionCurveEaseIn:
                    [context setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
                    break;
                case UIViewAnimationOptionCurveEaseOut:
                    [context setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
                    break;
                case UIViewAnimationOptionCurveLinear:
                    [context setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
                    break;
                default:
                    [context setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
                    break;
            }
            
            if (animations) animations();
            
        } completionHandler:^{
            if (completion) completion(YES);
        }];
    });
}

//- (NSView *)hitTest:(NSPoint)point
//{
//    if (self.hidden || !self.userInteractionEnabled || /* self.alpha < 0.01 || */ ![self pointInside:point]) {
//        return nil;
//    }
//
//    return [super hitTest:point];
//}
//
//- (BOOL)pointInside:(CGPoint)point
//{
//    return CGRectContainsPoint(self.bounds, point);
//}

@end


// MARK: - Application

@implementation NSApplication (DBCompatibility)

- (BOOL)isNetworkActivityIndicatorVisible
{
    return NO;
}

- (void)setNetworkActivityIndicatorVisible:(BOOL)networkActivityIndicatorVisible
{
    
}

- (BOOL)statusBarHidden
{
    return NO;
}

- (void)setStatusBarHidden:(BOOL)statusBarHidden
{
    
}

- (NSWindow *)window
{
    return self.mainWindow;
}

- (BOOL)openURL:(NSURL *)url
{
    return [[NSWorkspace sharedWorkspace] openURL:url];
}

@end


// MARK: - Screen

@implementation NSScreen (DBCompatibility)

- (CGFloat)scale
{
    return self.backingScaleFactor;
}

@end


// MARK: - WebView

@implementation WebView (DBCompatibility)

// - (NSScrollView *)scrollView
// {
//     return nil;
// }

- (void)loadRequest:(NSURLRequest *)request
{
    [self.mainFrame loadRequest:request];
}

- (void)loadHTMLString:(NSString *)string baseURL:(NSURL *)URL
{
    [self.mainFrame loadHTMLString:string baseURL:URL];
}

@end


// MARK: - NSIndexPath

@implementation NSIndexPath (DBCompatibility)

#if __MAC_OS_X_VERSION_MIN_REQUIRED < __MAC_10_11
- (NSInteger)section
{
    return [self indexAtPosition:0];
}

- (NSInteger)item
{
    return [self indexAtPosition:1];
}

+ (NSIndexPath *)indexPathForItem:(NSInteger)item inSection:(NSInteger)section
{
    NSUInteger indexPath[2] = {section, item};
    return [self indexPathWithIndexes:indexPath length:2];
}
#endif

+ (NSIndexPath *)indexPathForRow:(NSInteger)row inSection:(NSInteger)section
{
    return [self indexPathForItem:row inSection:section];
}

- (NSInteger)row
{
    return self.item;
}

@end


// MARK: - NSControl

@implementation NSControl (DBCompatibility)

- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents
{
    [self setTarget:target];
    [self setAction:action];
}

- (void)removeTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents
{
    [self setTarget:nil];
    [self setAction:nil];
}

@end


// MARK: - Nib

@implementation NSNib (DBCompatibility)

+ (NSNib *)nibWithNibName:(NSString *)name bundle:(NSBundle *)bundleOrNil
{
    return [[NSNib alloc] initWithNibNamed:name bundle:bundleOrNil];
}

@end


// MARK: - ActivityIndicator

@implementation NSProgressIndicator (DBCompatibility)

- (BOOL)hidesWhenStopped
{
    return !self.displayedWhenStopped;
}

- (void)setHidesWhenStopped:(BOOL)hidesWhenStopped
{
    self.displayedWhenStopped = !hidesWhenStopped;
}

- (void)startAnimating
{
    [self startAnimation:nil];
}

- (void)stopAnimating
{
    [self stopAnimation:nil];
}

@end


// MARK: - SegmentedControl

@implementation NSSegmentedControl (DBCompatibility)

- (NSInteger)selectedSegmentIndex
{
    return self.selectedSegment;
}

- (void)setSelectedSegmentIndex:(NSInteger)selectedSegmentIndex
{
    self.selectedSegment = selectedSegmentIndex;
}

- (void)setTitle:(NSString *)title forSegmentAtIndex:(NSUInteger)segment
{
    [self setLabel:title forSegment:segment];
}

- (NSString *)titleForSegmentAtIndex:(NSUInteger)segment
{
    return [self labelForSegment:segment];
}

@end


// MARK: - AlertView

@implementation NSAlert (DBCompatibility)

- (NSString *)title
{
    return self.messageText;
}

- (void)setTitle:(NSString *)title
{
    self.messageText = title;
}

- (NSString *)message
{
    return self.informativeText;
}

- (void)setMessage:(NSString *)message
{
    self.informativeText = message;
}

- (NSInteger)cancelButtonIndex
{
    // Da fare
    return -1;
}

- (void)setCancelButtonIndex:(NSInteger)cancelButtonIndex
{
    // Da fare
}

- (void)show
{
    [self beginSheetModalForWindow:[DBApplication mainWindow] completionHandler:nil];
}

- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated
{
    // Da fare
    [[DBApplication mainWindow] endSheet:self.window returnCode:NSModalResponseCancel];
}

@end

#endif
