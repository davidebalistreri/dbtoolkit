//
//  DBDeprecated.m
//  File version: 1.0.0
//  Last modified: 09/10/2016
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBDeprecated.h"

// MARK: - DBToolkit

@implementation DBToolkit

+ (id)appDelegate
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBApplication delegate]");
    return [DBApplication delegate];
}

+ (NSString *)nomeApplicazione
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBApplication nomeApplicazione]");
    return [DBApplication nomeApplicazione];
}

+ (BOOL)randomBool
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBUtility randomBool]");
    return [DBUtility randomBool];
}

+ (int)randomIntDa:(NSInteger)da a:(NSInteger)a
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBUtility randomIntDa:a:]");
    return [DBUtility randomIntDa:da a:a];
}

+ (NSString *)randomStringConNumeroCaratteri:(NSInteger)numeroCaratteri
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBUtility randomStringConNumeroCaratteri:]");
    return [DBUtility randomStringConNumeroCaratteri:numeroCaratteri];
}

+ (UIStoryboard *)storyboard
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBApplication storyboard]");
    return [DBApplication storyboard];
}

+ (UIViewController *)rootViewController
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBApplication rootViewController]");
    return [DBApplication rootViewController];
}

+ (UIViewController *)presentedViewController
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBApplication presentedViewController]");
    return [DBApplication presentedViewController];
}

+ (UIWindow *)window
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBApplication mainWindow]");
    return [DBApplication mainWindow];
}

+ (UIWindow *)topWindow
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBApplication topmostWindow]");
    return [DBApplication topmostWindow];
}

#if DBFRAMEWORK_TARGET_IOS
+ (UIWindow *)windowStatusBar
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBApplication statusBarWindow]");
    return [DBApplication statusBarWindow];
}
#endif

+ (UIColor *)tintColor
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBUtility tintColor]");
    return [DBUtility tintColor];
}

+ (void)impostaTintColor:(UIColor *)tintColor
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza +[DBUtility impostaTintColor:]");
    [DBUtility impostaTintColor:tintColor];
}

#if DBFRAMEWORK_TARGET_IOS

+ (BOOL)isiPhone
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    return ([[[UIDevice currentDevice] model] hasPrefix:@"iPhone"]) ? YES : NO;
}

+ (BOOL)isiPad
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    return ([[[UIDevice currentDevice] model] hasPrefix:@"iPad"]) ? YES : NO;
}

+ (BOOL)isiPhoneApp
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) ? YES : NO;
}

+ (BOOL)isiPadApp
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? YES : NO;
}

+ (BOOL)isPortrait
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return UIInterfaceOrientationIsPortrait(orientation);
}

+ (BOOL)isPortraitUp
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationPortrait);
}

+ (BOOL)isPortraitUpsideDown
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationPortraitUpsideDown);
}

+ (BOOL)isLandscape
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return UIInterfaceOrientationIsLandscape(orientation);
}

+ (BOOL)isLandscapeLeft
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationLandscapeLeft);
}

+ (BOOL)isLandscapeRight
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationLandscapeRight);
}

+ (void)impostaOrientamento:(UIInterfaceOrientation)orientamento
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    
    if (orientamento) {
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:orientamento] forKey:@"orientation"];
    }
}

+ (BOOL)isApplicazioneAttiva
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    return (state == UIApplicationStateActive) ? YES : NO;
}

+ (BOOL)isApplicazioneInBackground
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza i metodi DBDevice");
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    return (state == UIApplicationStateBackground) ? YES : NO;
}

+ (BOOL)isiOSMinimum:(double)versioneRichiesta
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza +[DBToolkit isiOSMin:]");
    double versioneAttuale = [[[UIDevice currentDevice] systemVersion] doubleValue];
    return (versioneAttuale >= versioneRichiesta) ? YES : NO;
}

+ (BOOL)isiOSMaximum:(double)versioneRichiesta
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza +[DBDevice isiOSMax:]");
    double versioneAttuale = [[[UIDevice currentDevice] systemVersion] doubleValue];
    return (versioneAttuale <= versioneRichiesta) ? YES : NO;
}

+ (BOOL)isiOSMajorMinimum:(int)versioneRichiesta
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza +[DBToolkit isiOSMin:]");
    int versioneAttuale = [[[UIDevice currentDevice] systemVersion] intValue];
    return (versioneAttuale >= versioneRichiesta) ? YES : NO;
}

+ (BOOL)isiOSMajorMaximum:(int)versioneRichiesta
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza +[DBDevice isiOSMax:]");
    int versioneAttuale = [[[UIDevice currentDevice] systemVersion] intValue];
    return (versioneAttuale <= versioneRichiesta) ? YES : NO;
}

+ (BOOL)isiOSSpecific:(double)versioneRichiesta
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza +[DBDevice isiOS:]");
    double versioneAttuale = [[[UIDevice currentDevice] systemVersion] doubleValue];
    return (versioneAttuale == versioneRichiesta) ? YES : NO;
}

+ (BOOL)isiOSMajor:(int)versioneRichiesta
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza +[DBDevice isiOSMajor:]");
    int versioneAttuale = [[[UIDevice currentDevice] systemVersion] doubleValue];
    return (versioneAttuale == versioneRichiesta) ? YES : NO;
}

+ (CGRect)frameSchermo
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.0, "utilizza i metodi DBGeometry");
    return [DBGeometry frameSchermo];
}

+ (CGSize)dimensioniSchermo
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.0, "utilizza i metodi DBGeometry");
    return [DBGeometry dimensioniSchermo];
}

+ (CGFloat)altezzaSchermo
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.0, "utilizza i metodi DBGeometry");
    return [DBGeometry altezzaSchermo];
}

+ (BOOL)isScreenSpecific:(CGFloat)dimensioneRichiesta
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.0, "utilizza i metodi DBGeometry");
    return [DBGeometry isScreen:dimensioneRichiesta];
}

+ (BOOL)isScreenMinimum:(CGFloat)dimensioneRichiesta
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.0, "utilizza i metodi DBGeometry");
    return [DBGeometry isScreenMin:dimensioneRichiesta];
}

+ (BOOL)isScreenMaximum:(CGFloat)dimensioneRichiesta
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.0, "utilizza i metodi DBGeometry");
    return [DBGeometry isScreenMax:dimensioneRichiesta];
}

+ (CGRect)frameStatusBar
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.0, "utilizza i metodi DBGeometry");
    return [DBGeometry frameStatusBar];
}

+ (CGSize)dimensioniStatusBar
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.0, "utilizza i metodi DBGeometry");
    return [DBGeometry dimensioniStatusBar];
}

+ (CGFloat)altezzaStatusBar
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.0, "utilizza i metodi DBGeometry");
    return [DBGeometry altezzaStatusBar];
}

#endif

@end


// MARK: - DBApplication

@implementation DBApplication (DBDeprecated)

+ (id)topViewController
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.1, "please use +[DBApplication topmostViewController]");
    return [self topmostViewController];
}

+ (UIWindow *)topWindow
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.1, "please use +[DBApplication topmostWindow]");
    return [self topmostWindow];
}

+ (NSString *)versioneApplicazione
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.1, "please use +[DBApplication version]");
    return [self version];
}

@end


// MARK: - DBViewController

@implementation DBViewController (DBDeprecated)

- (BOOL)didPerformFirstLoad
{
    DBFRAMEWORK_DEPRECATED_LOG(0.5.1, "utilizza la proprietà -inizializzazioneCompletata");
    return self.inizializzazioneCompletata;
}

@end


// MARK: - DBTableViewController

@implementation DBTableViewController (DBDeprecated)

- (NSIndexPath *)indexPathCellaSelezionata
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza la proprietà -selectedIndexPath");
    return self.selectedIndexPath;
}

- (void)impostaTableView:(UITableView *)tableView
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza il metodo -[DBTableViewController useTableView:]");
    [self useTableView:tableView];
}

- (void)mostraRefreshControl:(BOOL)mostra
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.0, "utilizza il metodo -[DBTableViewController enableRefreshControl:]");
    [self enableRefreshControl:mostra];
}

- (void)abilitaRefreshControl:(BOOL)abilita
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza il metodo -[DBTableViewController enableRefreshControl:]");
    [self enableRefreshControl:abilita];
}

- (void)abilitaScrollToTop:(BOOL)abilita
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza il metodo -[DBTableViewController enableScrollToTop:]");
    [self enableScrollToTop:abilita];
}

@end


// MARK: - DBStringExtensions

@implementation NSString (DBDeprecated)

- (BOOL)isEmpty
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.4, "utilizza il metodo di classe per ricevere risultati piu' coerenti");
    return [NSString isEmpty:self];
}

@end


// MARK: - DBDictionaryExtensions

@implementation NSDictionary (DBDeprecated)

- (BOOL)isEmpty
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.4, "utilizza il metodo di classe per ricevere risultati piu' coerenti");
    return [NSDictionary isEmpty:self];
}

@end


// MARK: - DBKeyboardHandler

#if DBFRAMEWORK_TARGET_IOS

@implementation DBKeyboardHandler (DBDeprecated)

// MARK: Metodi di classe

+ (instancetype)controllaVista:(UIView *)vista conTextFields:(NSArray *)textFields conDelegate:(id<DBKeyboardHandlerDelegate>)delegate
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBKeyboardHandler handlerWithView:inputFields:delegate:]");
    return [self handlerWithView:vista inputFields:textFields delegate:delegate];
}

+ (instancetype)controllaVista:(UIView *)vista conTextFields:(NSArray *)textFields
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBKeyboardHandler handlerWithView:inputFields:delegate:]");
    return [self handlerWithView:vista inputFields:textFields delegate:nil];
}

+ (instancetype)handlerConView:(UIView *)view inputFields:(NSArray *)inputFields delegate:(id<DBKeyboardHandlerDelegate>)delegate
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBKeyboardHandler handlerWithView:inputFields:delegate:]");
    return [self handlerWithView:view inputFields:inputFields delegate:delegate];
}

+ (void)rimuoviControlloVista:(UIView *)vista
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBKeyboardHandler destroyHandlerWithView:]");
    [self destroyHandlerWithView:vista];
}

+ (void)distruggiHandlerConView:(UIView *)view
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBKeyboardHandler destroyHandlerWithView:]");
    [self destroyHandlerWithView:view];
}

/// Disabilita e rimuove l'Handler dalla memoria
+ (void)distruggiHandler:(DBKeyboardHandler *)handler
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBKeyboardHandler destroyHandler]");
    [handler destroyHandler];
}

+ (instancetype)handlerConVista:(UIView *)vista
{
    return nil;
}

+ (instancetype)getHandlerVista:(UIView *)vista
{
    return nil;
}

// MARK: Metodi d'istanza

- (instancetype)initWithView:(id)view andTextFields:(NSArray *)textFields andDelegate:(id<DBKeyboardHandlerDelegate>)delegate
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBKeyboardHandler handlerWithView:inputFields:delegate:]");
    return [DBKeyboardHandler handlerWithView:view inputFields:textFields delegate:delegate];
}

/// Inizializza l'Handler e abilita il controllo delle Input Fields
- (instancetype)initWithView:(id)view andTextFields:(NSArray *)textFields
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBKeyboardHandler handlerWithView:inputFields:delegate:]");
    return [DBKeyboardHandler handlerWithView:view inputFields:textFields delegate:nil];
}

/// Disabilita e rimuove l'Handler dalla memoria
- (void)distruggiHandler
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBKeyboardHandler destroyHandler]");
    [self destroyHandler];
}

/// Imposta le InputFields controllate dall'Handler. Se una InputField è già controllata dall'Handler, la sua configurazione viene mantenuta (ad esempio il limite di caratteri o il delegate).
- (void)impostaInputFields:(NSArray *)inputFields
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBKeyboardHandler setInputFields:]");
    self.inputFields = inputFields;
}

/// Abilita il controllo delle UITextFields
- (void)abilitaControlloTextFieldsConSchermata:(id)view
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBKeyboardHandler enableHandler:]");
    [self enableHandler:YES];
}

/// Disabilita il controllo delle UITextFields
- (void)disabilitaControlloTextFields
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBKeyboardHandler enableHandler:]");
    [self enableHandler:NO];
}

/// Da impostare a YES per fixare il frame errato se è presente una TabBar
- (void)impostaTabBar:(BOOL)tabBar
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "non è piu' possibile correggere il frame della TabBar, bisogna correggere il frame manualmente");
}

- (void)impostaLimiteCaratteri:(NSInteger)limite perTextField:(id)textField
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBKeyboardHandler inputField:setMaximumNumberOfCharacters:]");
    [self inputField:textField setMaximumNumberOfCharacters:limite];
}

- (void)impostaLimiteCaratteri:(NSInteger)limite perInputField:(id)inputField
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBKeyboardHandler inputField:setMaximumNumberOfCharacters:]");
    [self inputField:inputField setMaximumNumberOfCharacters:limite];
}

- (void)impostaCaratteriPermessi:(NSString *)caratteriPermessi perTextField:(id)textField
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBKeyboardHandler inputField:setAllowedCharacters:]");
    [self inputField:textField setAllowedCharacters:caratteriPermessi];
}

- (void)impostaCaratteriProibiti:(NSString *)caratteriProibiti perTextField:(id)textField
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBKeyboardHandler inputField:setForbiddenCharacters:]");
    [self inputField:textField setForbiddenCharacters:caratteriProibiti];
}

- (void)impostaDelegate:(id<DBKeyboardHandlerDelegate>)delegate perInputFields:(NSArray *)inputFields
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza il singolo -[DBKeyboardHandler impostaDelegate:perInputField:]");
    
    for (id inputField in inputFields) {
        [self inputField:inputField setDelegate:delegate];
    }
}

@end


// MARK: - DBPickerHandler

@implementation DBPickerHandler (DBDeprecated)

// MARK: Metodi di classe

/// Crea un nuovo Handler che si occuperà di gestire le TextFields specificate (o utilizza l'Handler con il delegate specificato)
+ (instancetype)controllaTextFields:(NSArray *)textFields conDataSources:(NSArray *)dataSources conDelegate:(id<DBPickerHandlerDelegate>)delegate
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBPickerHandler handlerWithDelegate:inputFields:]");
    
    DBPickerHandler *handler = [DBPickerHandler handlerWithDelegate:delegate inputFields:textFields];
    
    for (NSUInteger counter = 0; counter < dataSources.count; counter++) {
        UITextField *inputField = [textFields safeObjectAtIndex:counter];
        id dataSource = [dataSources safeObjectAtIndex:counter];
        
        [handler inputField:inputField setDataSource:dataSource];
    }
    
    return handler;
}

/// Elimina l'Handler che si occupava delle TextFields associate al delegate
+ (BOOL)rimuoviControlloDelegate:(id<DBPickerHandlerDelegate>)delegate
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBPickerHandler destroyHandlerWithDelegate:]");
    [DBPickerHandler destroyHandlerWithDelegate:delegate];
    return YES;
}

// MARK: Metodi d'istanza

/// Inizializza l'Handler e abilita il controllo delle Input Fields
- (instancetype)initWithDelegate:(id<DBPickerHandlerDelegate>)delegate inputFields:(NSArray *)inputFields dataSources:(NSArray *)dataSources
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBPickerHandler handlerWithDelegate:inputFields:]");
    return [DBPickerHandler handlerWithDelegate:delegate inputFields:inputFields];
}

/// Disabilita il controllo delle Input Fields
- (void)disabilitaControlloInputFields
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBPickerHandler enableHandler:]");
    [self enableHandler:NO];
}

- (void)impostaData:(NSDate *)data perDatePickerInputField:(id<UITextInput>)inputField
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBPickerHandler inputField:setDate:]");
    [self inputField:(id)inputField setDate:data];
}

- (void)impostaDataInizio:(NSDate *)dataInizio dataFine:(NSDate *)dataFine perDatePickerInputField:(id<UITextInput>)inputField
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBPickerHandler inputField:setMinimumDate:maximumDate:]");
    [self inputField:(id)inputField setMinimumDate:dataInizio maximumDate:dataFine];
}

- (void)impostaMinutiIntervallo:(NSInteger)minutiIntervallo perDatePickerInputField:(id<UITextInput>)inputField
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBPickerHandler inputField:setMinuteInterval:]");
    [self inputField:(id)inputField setMinuteInterval:minutiIntervallo];
}

/// Imposta o sostituisce il dataSource per l'Input Field richiesta
- (void)impostaDataSource:(NSArray *)dataSource perInputField:(id<UITextInput>)inputField
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBPickerHandler inputField:setDataSource:]");
    [self inputField:(id)inputField setDataSource:dataSource];
}

/// Imposta o sostituisce il dataSource per l'Input Field inserita alla posizione richiesta dell'array di InputFields
- (void)impostaDataSource:(NSArray *)dataSource perInputFieldAtIndex:(NSInteger)index
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBPickerHandler inputField:setDataSource:]");
    UITextField *inputField = [self.inputFields safeObjectAtIndex:index];
    [self inputField:inputField setDataSource:dataSource];
}

- (void)selezionaIndex:(NSInteger)index inComponent:(NSInteger)component perInputField:(id<UITextInput>)inputField
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBPickerHandler inputField:selectObjectAtIndex:]");
    [self inputField:(id)inputField selectObjectAtIndex:index];
}

- (void)selezionaItem:(id)item perInputField:(id<UITextInput>)inputField
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBPickerHandler inputField:selectObject:]");
    [self inputField:(id)inputField selectObject:item];
}

- (id)getItemPerInputField:(id<UITextInput>)inputField
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBPickerHandler inputFieldSelectedObject:]");
    return [self inputFieldSelectedObject:(id)inputField];
}

- (NSInteger)getIndexPerInputField:(id<UITextInput>)inputField
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza -[DBPickerHandler inputFieldSelectedObjectIndex:]");
    return [self inputFieldSelectedObjectIndex:(id)inputField];
}

@end

#endif


// MARK: - DBNetworking

@implementation DBNetworking (DBDeprecated)

+ (void)impostaDebug:(BOOL)debug
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBNetworking setDebugMode:]");
    [self setDebugMode:debug];
}

+ (void)abilitaModalitaDebug:(BOOL)abilita
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBNetworking setDebugMode:]");
    [self setDebugMode:abilita];
}

+ (void)impostaHeaderGlobali:(nullable NSDictionary *)headers
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBNetworking setGlobalHeader:]");
    [self setGlobalHeader:headers];
}

+ (void)impostaHeaderGlobale:(nullable NSDictionary *)header
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBNetworking setGlobalHeader:]");
    [self setGlobalHeader:header];
}

/// Restituisce gli Header che vengono attualmente aggiunti ad ogni chiamata effettuata.
+ (nullable NSDictionary *)headerGlobali
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBNetworking globalHeader]");
    return [self globalHeader];
}

/// Restituisce gli Header che vengono attualmente aggiunti ad ogni chiamata effettuata.
+ (nullable NSDictionary *)headerGlobale
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBNetworking globalHeader]");
    return [self globalHeader];
}

/// Indica se il dispositivo è connesso ad internet.
+ (BOOL)checkConnessioneInternet
{
    DBFRAMEWORK_DEPRECATED_LOG(0.5.1, "utilizza +[DBNetworking isNetworkAvailable]");
    return [self isNetworkAvailable];
}

/// Indica se il dispositivo è connesso ad internet.
+ (BOOL)connessioneInternetDisponibile
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBNetworking isNetworkAvailable]");
    return [self isNetworkAvailable];
}

/// Indica se il dispositivo è connesso ad internet.
+ (BOOL)isConnessioneInternetDisponibile
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza +[DBNetworking isNetworkAvailable]");
    return [self isNetworkAvailable];
}

+ (void)JSONGETConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlockDeprecato)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");
    
    [self JSONGETConURLString:URLString parametri:parametri completionBlock:^(DBNetworkingResponse * __nullable response) {
        
        if (completionBlock) {
            completionBlock(response.error, response.responseObject);
        }
    }];
}

+ (void)JSONPOSTConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlockDeprecato)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");
    
    [self JSONPOSTConURLString:URLString parametri:parametri completionBlock:^(DBNetworkingResponse * __nullable response) {
        
        if (completionBlock) {
            completionBlock(response.error, response.responseObject);
        }
    }];
}

+ (void)JSONDELETEConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlockDeprecato)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");
    
    [self JSONDELETEConURLString:URLString parametri:parametri completionBlock:^(DBNetworkingResponse * __nullable response) {
        
        if (completionBlock) {
            completionBlock(response.error, response.responseObject);
        }
    }];
}

+ (void)JSONPUTConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlockDeprecato)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");
    
    [self JSONPUTConURLString:URLString parametri:parametri completionBlock:^(DBNetworkingResponse * __nullable response) {
        
        if (completionBlock) {
            completionBlock(response.error, response.responseObject);
        }
    }];
}

+ (void)HTMLGETConURLString:(NSString *)URLString conParametri:(NSDictionary *)parametri conCompletionBlock:(DBNetworkingCompletionBlockDeprecato)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");
    
    [self HTMLGETConURLString:URLString parametri:parametri completionBlock:^(DBNetworkingResponse * __nullable response) {
        
        if (completionBlock) {
            completionBlock(response.error, response.responseObject);
        }
    }];
}

+ (void)GETImmagineConURLString:(NSString *)URLString conCompletionBlock:(DBNetworkingCompletionBlockDeprecato)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");
    
    [self GETImmagineConURLString:URLString completionBlock:^(DBNetworkingResponse * __nullable response) {
        
        if (completionBlock) {
            completionBlock(response.error, response.responseObject);
        }
    }];
}

+ (void)GETImmagineConURLString:(NSString *)URLString sovrascrivi:(BOOL)sovrascrivi conCompletionBlock:(DBNetworkingCompletionBlockDeprecato)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");
    
    [self GETImmagineConURLString:URLString sovrascrivi:sovrascrivi completionBlock:^(DBNetworkingResponse * __nullable response) {
        
        if (completionBlock) {
            completionBlock(response.error, response.responseObject);
        }
    }];
}

+ (void)GETFileConURLString:(NSString *)URLString conDownloadPath:(NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi conSuccessBlock:(DBNetworkingSuccessBlockDeprecato)successBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");
    
    [self GETFileConURLString:URLString downloadPath:downloadPath sovrascrivi:sovrascrivi completionBlock:^(DBNetworkingResponse * __nullable response) {
        
        if (successBlock) {
            successBlock(response.error);
        }
    }];
}

+ (void)GETFileConURLString:(NSString *)URLString conDownloadPath:(NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi conProgressHUD:(BOOL)progressHUDAbilitato messaggioProgressHUD:(NSString *)messaggioProgressHUD conSuccessBlock:(DBNetworkingSuccessBlockDeprecato)successBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");
    
    [self GETFileConURLString:URLString downloadPath:downloadPath sovrascrivi:sovrascrivi progressHUD:progressHUDAbilitato messaggioProgressHUD:messaggioProgressHUD completionBlock:^(DBNetworkingResponse * __nullable response) {
        
        if (successBlock) {
            successBlock(response.error);
        }
    }];
}

@end


// MARK: - View

#if DBFRAMEWORK_TARGET_IOS

@implementation UIView (DBDeprecated)

- (void)aggiungiEffettoSfocato
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza il metodo -[UIView setVisualEffect:]");
    [self setVisualEffect:DBViewVisualEffectDarkBlur];
}

- (void)rimuoviEffettoSfocato
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "utilizza il metodo -[UIView setVisualEffect:DBViewVisualEffectNone]");
    [self setVisualEffect:DBViewVisualEffectNone];
}

@end


// MARK: - ImageView

@implementation UIImageView (DBDeprecated)

- (void)caricaImmagineConURLString:(NSString *)URLString conActivityIndicator:(UIActivityIndicatorView *)activityIndicator
{
    [self setImageWithURLString:URLString activityIndicator:activityIndicator completionBlock:nil];
}

- (void)caricaImmagineConURLString:(NSString *)URLString conActivityIndicator:(UIActivityIndicatorView *)activityIndicator conCompletionBlock:(void (^)(BOOL finished))completionBlock
{
    [self setImageWithURLString:URLString activityIndicator:activityIndicator completionBlock:^(DBNetworkingResponse * __nullable response) {
        if (completionBlock) {
            completionBlock(response.success);
        }
    }];
}

- (void)setImageWithURLString:(NSString *)URLString activityIndicator:(UIActivityIndicatorView *)activityIndicator completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please extend your UIImageView object from the new DBImageView class, and use the new method -[DBImageView setImageWithURLString:placeholderImage:completionBlock:]");
    
    self.image = nil;
    
    NSInteger tag = self.tag;
    
    // Avvio lo spinner di caricamento
    [activityIndicator startAnimating];
    
    [DBNetworking GETImmagineConURLString:URLString completionBlock:^(DBNetworkingResponse * __nullable response) {
        
        // Fermo lo spinner di caricamento
        [activityIndicator stopAnimating];
        
        if (response.success && [response.responseObject isKindOfClass:[UIImage class]]) {
            if (self.tag == tag) {
                UIImage *immagine = response.responseObject;
                self.image = immagine;
            }
        }
        
        if (completionBlock) {
            completionBlock(response);
        }
    }];
}

- (void)setImageWithURLString:(NSString *)URLString withDefaultImage:(UIImage *)defaultImage
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please extend your UIImageView object from the new DBImageView class, and use the new method -[DBImageView setImageWithURLString:placeholderImage:completionBlock:]");
    
    self.image = defaultImage;
    
    NSInteger tag = self.tag;
    
    [DBNetworking GETImmagineConURLString:URLString completionBlock:^(DBNetworkingResponse * __nullable response) {
        
        if (self.tag == tag) {
            if (response.success && [response.responseObject isKindOfClass:[UIImage class]]) {
                UIImage *immagine = response.responseObject;
                self.image = immagine;
            }
        }
    }];
}

@end

#endif


// MARK: - Reachability

@implementation Reachability

/// Indica se il dispositivo è connesso ad internet.
+ (BOOL)checkConnessioneInternet
{
    DBFRAMEWORK_DEPRECATED_LOG(0.5.1, "utilizza +[DBNetworking isNetworkAvailable]");
    return [DBNetworking isNetworkAvailable];
}

@end


// MARK: - DBProgressHUD

#import "DBProgressHUD.h"

@implementation MBProgressHUD (DBDeprecated)

+ (void)creaHUDSullaVista:(UIView *)vista
{
    DBFRAMEWORK_DEPRECATED_LOG(0.2.0, "utilizza il metodo +[DBProgressHUD progressHUDConMessaggio:conVista:]");
    [DBProgressHUD progressHUDConMessaggio:nil conVista:vista];
}

+ (void)creaHUDConMessaggio:(NSString *)messaggio sullaVista:(UIView *)vista
{
    DBFRAMEWORK_DEPRECATED_LOG(0.2.0, "utilizza il metodo +[DBProgressHUD progressHUDConMessaggio:conVista:]");
    [DBProgressHUD progressHUDConMessaggio:messaggio conVista:vista];
}

+ (void)creaAlertHUDConMessaggio:(NSString *)messaggio
{
    DBFRAMEWORK_DEPRECATED_LOG(0.2.0, "utilizza il metodo +[DBProgressHUD toastConMessaggio:]");
    [DBProgressHUD toastConMessaggio:messaggio];
}

+ (void)creaAlertHUDConMessaggio:(NSString *)messaggio sullaVista:(UIView *)vista
{
    DBFRAMEWORK_DEPRECATED_LOG(0.2.0, "utilizza il metodo +[DBProgressHUD toastConMessaggio:conVista:conDurata:ritardaSuccessivi:]");
    [DBProgressHUD toastConMessaggio:messaggio conVista:vista conDurata:2.0 ritardaSuccessivi:YES];
}

+ (void)rimuoviHUD
{
    DBFRAMEWORK_DEPRECATED_LOG(0.2.0, "utilizza il metodo +[DBProgressHUD rimuoviProgressHUDs]");
    [DBProgressHUD rimuoviProgressHUDs];
}

@end


// MARK: - Storyboard

@implementation UIStoryboard (DBDeprecated)

- (id)istanziaViewController:(Class)classeViewController
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.0, "please use -[UIStoryboard instantiateViewControllerWithClass:]");
    return [self instantiateViewControllerWithClass:classeViewController];
}

@end
