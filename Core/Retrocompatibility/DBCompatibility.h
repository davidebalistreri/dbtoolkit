//
//  DBCompatibility.h
//  File version: 1.1.2
//  Last modified: 03/10/2016
//
//  Created by Davide Balistreri on 01/11/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#if DBFRAMEWORK_TARGET_OSX

#import <WebKit/WebKit.h>

#import "NSImageView+AFNetworking.h"
#import "NSBezierPath+iOS.h"
#import "NSImage+iOS.h"
#import "NSString+iOS.h"
#import "NSValue+iOS.h"

#import "NSNavigationController.h"


NS_ASSUME_NONNULL_BEGIN

@compatibility_alias UIButton     NSButton;
@compatibility_alias UIImage      NSImage;
@compatibility_alias UIImageView  NSImageView;
@compatibility_alias UITextField  NSTextField;
@compatibility_alias UITextView   NSTextView;
@compatibility_alias UIPickerView NSComboBox;
@compatibility_alias UIColor      NSColor;
@compatibility_alias UIBezierPath NSBezierPath;
@compatibility_alias UIEvent      NSEvent;
@compatibility_alias UIFont       NSFont;

@compatibility_alias UIScrollView NSScrollView;
@compatibility_alias UITableView  NSTableView;
@compatibility_alias UICollectionView NSCollectionView;
@compatibility_alias UITableViewCell NSTableCellView;


NSData *UIImagePNGRepresentation(NSImage *image);


// MARK: - Storyboard

@compatibility_alias UIStoryboard NSStoryboard;

@interface NSStoryboard (DBCompatibility)

- (id)instantiateViewControllerWithIdentifier:(NSString *)identifier;

@end


// MARK: - Window

@compatibility_alias UIWindow NSWindow;

@interface NSWindow (DBCompatibility)

@property (strong, nonatomic) NSWindowController *rootWindowController;
@property (strong, nonatomic) NSViewController *rootViewController;

@property (strong, nonatomic) NSColor *backgroundColor;
@property (nonatomic) BOOL clipsToBounds;
@property (nonatomic) CGFloat alpha;
@property (nonatomic) BOOL opaque;
@property (nonatomic) BOOL hidden;
@property (nonatomic, readonly) NSRect bounds;
@property NSInteger windowLevel;

- (void)addSubview:(NSView *)view;

@end


// MARK: - ViewController

@compatibility_alias UIViewController NSViewController;

@interface NSViewController (DBCompatibility)

@property (weak, nonatomic) NSNavigationController *navigationController;

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^ __nullable)(void))completion;

@end


// MARK: - View

@compatibility_alias UIView NSView;

@interface NSView (DBCompatibility)

typedef NS_OPTIONS(NSUInteger, UIViewAnimationOptions) {
    UIViewAnimationOptionCurveEaseInOut            = 0 << 16, // default
    UIViewAnimationOptionCurveEaseIn               = 1 << 16,
    UIViewAnimationOptionCurveEaseOut              = 2 << 16,
    UIViewAnimationOptionCurveLinear               = 3 << 16
};

@property (nonatomic, getter=isUserInteractionEnabled) BOOL userInteractionEnabled;

//#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IOS_8_0
//@property (nullable, nonatomic) UIView *maskView;
//#endif

- (void)insertSubview:(NSView *)subview atIndex:(NSInteger)index;
- (void)exchangeSubviewAtIndex:(NSInteger)index1 withSubviewAtIndex:(NSInteger)index2;

- (void)insertSubview:(UIView *)view belowSubview:(UIView *)siblingSubview;
- (void)insertSubview:(UIView *)view aboveSubview:(UIView *)siblingSubview;

- (void)bringSubviewToFront:(NSView *)view;
- (void)sendSubviewToBack:(NSView *)view;

// use to make the view or any subview that is the first responder resign (optionally force)
- (BOOL)endEditing:(BOOL)force;

- (void)setNeedsUpdateConstraints;
- (void)updateConstraintsIfNeeded;
- (void)setNeedsLayout;
- (void)layoutIfNeeded;

+ (void)animateWithDuration:(NSTimeInterval)duration animations:(void (^)(void))animations;
+ (void)animateWithDuration:(NSTimeInterval)duration animations:(void (^)(void))animations completion:(void (^ __nullable)(BOOL finished))completion;
+ (void)animateWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay options:(UIViewAnimationOptions)options animations:(void (^)(void))animations completion:(void (^ __nullable)(BOOL finished))completion;

@end


// MARK: - Application

@compatibility_alias UIApplication NSApplication;

@interface NSApplication (DBCompatibility)

// Non fa nulla
@property (nonatomic, getter=isNetworkActivityIndicatorVisible) BOOL networkActivityIndicatorVisible;
@property (nonatomic) BOOL statusBarHidden;

@property (nullable, readonly, assign) NSWindow *window;

- (BOOL)openURL:(NSURL *)url;

@end


// MARK: - Screen

@compatibility_alias UIScreen NSScreen;

@interface NSScreen (DBCompatibility)

@property (readonly) CGFloat scale;

@end


// MARK: - Label

@compatibility_alias UILabel NSTextField;


// MARK: - NSIndexPath

@interface NSIndexPath (DBCompatibility)

@property (nonatomic, readonly) NSInteger row;
+ (NSIndexPath *)indexPathForRow:(NSInteger)row inSection:(NSInteger)section;

#if __MAC_OS_X_VERSION_MIN_REQUIRED < __MAC_10_11
+ (NSIndexPath *)indexPathForItem:(NSInteger)item inSection:(NSInteger)section;
@property (readonly) NSInteger item;
@property (readonly) NSInteger section;
#endif

@end


// MARK: - NSControl

@interface NSControl (DBCompatibility)

typedef NS_OPTIONS(NSUInteger, UIControlEvents) {
    UIControlEventTouchDown                                         = 1 <<  0,      // on all touch downs
    UIControlEventTouchDownRepeat                                   = 1 <<  1,      // on multiple touchdowns (tap count > 1)
    UIControlEventTouchDragInside                                   = 1 <<  2,
    UIControlEventTouchDragOutside                                  = 1 <<  3,
    UIControlEventTouchDragEnter                                    = 1 <<  4,
    UIControlEventTouchDragExit                                     = 1 <<  5,
    UIControlEventTouchUpInside                                     = 1 <<  6,
    UIControlEventTouchUpOutside                                    = 1 <<  7,
    UIControlEventTouchCancel                                       = 1 <<  8,
    
    UIControlEventValueChanged                                      = 1 << 12,     // sliders, etc.
    UIControlEventPrimaryActionTriggered NS_ENUM_AVAILABLE_IOS(9_0) = 1 << 13,     // semantic action: for buttons, etc.
    
    UIControlEventEditingDidBegin                                   = 1 << 16,     // UITextField
    UIControlEventEditingChanged                                    = 1 << 17,
    UIControlEventEditingDidEnd                                     = 1 << 18,
    UIControlEventEditingDidEndOnExit                               = 1 << 19,     // 'return key' ending editing
    
    UIControlEventAllTouchEvents                                    = 0x00000FFF,  // for touch events
    UIControlEventAllEditingEvents                                  = 0x000F0000,  // for UITextField
    UIControlEventApplicationReserved                               = 0x0F000000,  // range available for application use
    UIControlEventSystemReserved                                    = 0xF0000000,  // range reserved for internal framework use
    UIControlEventAllEvents                                         = 0xFFFFFFFF
};

// add target/action for particular event. you can call this multiple times and you can specify multiple target/actions for a particular event.
// passing in nil as the target goes up the responder chain. The action may optionally include the sender and the event in that order
// the action cannot be NULL. Note that the target is not retained.
- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents;

// remove the target/action for a set of events. pass in NULL for the action to remove all actions for that target
- (void)removeTarget:(id)target action:(nullable SEL)action forControlEvents:(UIControlEvents)controlEvents;

@end


// MARK: - Nib

@compatibility_alias UINib NSNib;

@interface NSNib (DBCompatibility)

+ (NSNib *)nibWithNibName:(NSString *)name bundle:(nullable NSBundle *)bundleOrNil;

@end


// MARK: - PageController

@compatibility_alias UIPageViewController NSPageController;

@interface NSPageController (DBCompatibility)



@end


// MARK: - ActivityIndicator

@compatibility_alias UIActivityIndicatorView NSProgressIndicator;

@interface NSProgressIndicator (DBCompatibility)

@property (assign) BOOL hidesWhenStopped;

- (void)startAnimating;
- (void)stopAnimating;

@end


// MARK: - SegmentedControl

@compatibility_alias UISegmentedControl NSSegmentedControl;

@interface NSSegmentedControl (DBCompatibility)

@property (assign, nonatomic) NSInteger selectedSegmentIndex;

- (void)setTitle:(nullable NSString *)title forSegmentAtIndex:(NSUInteger)segment;
- (nullable NSString *)titleForSegmentAtIndex:(NSUInteger)segment;

@end


// MARK: - AlertView

@compatibility_alias UIAlertView  NSAlert;

@interface NSAlert (DBCompatibility)

@property (nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *message;
@property (nonatomic) NSInteger cancelButtonIndex;

- (void)show;
- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END

#endif
