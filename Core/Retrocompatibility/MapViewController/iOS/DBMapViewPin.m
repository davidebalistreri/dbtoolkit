//
//  DBMapViewPin.m
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 28/05/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBMapViewPin.h"

@implementation DBMapViewPin

- (void)setupObject
{
    self.anchorPoint = CGPointMake(0.5, 0.5);
}

@end
