//
//  DBDeprecated.h
//  File version: 1.0.0
//  Last modified: 09/10/2016
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

// Retrocompatibilità vecchi progetti
#ifdef DBFRAMEWORK_VERSIONE_IN_USO
    #error Cerca nel tuo progetto la costante "DBFRAMEWORK_VERSIONE_IN_USO" e modifica il suo nome in "DBFRAMEWORK_CURRENT_VERSION"
#endif


#import "DBFramework.h"

/**
 * Classe creata per raggruppare i metodi deprecati e pulire le altre classi.
 */

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wnullability-completeness"

// MARK: - DBToolkit

@interface DBToolkit : NSObject

+ (id)appDelegate
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBApplication delegate]");

+ (NSString *)nomeApplicazione
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBApplication nomeApplicazione]");

+ (BOOL)randomBool
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBUtility randomBool]");

+ (int)randomIntDa:(NSInteger)da a:(NSInteger)a
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBUtility randomIntDa:a:]");

+ (NSString *)randomStringConNumeroCaratteri:(NSInteger)numeroCaratteri
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBUtility randomStringConNumeroCaratteri:]");

+ (UIStoryboard *)storyboard
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBApplication storyboard]");

+ (UIViewController *)rootViewController
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBApplication rootViewController]");

+ (UIViewController *)presentedViewController
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBApplication presentedViewController]");

+ (UIWindow *)window
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBApplication mainWindow]");

+ (UIWindow *)topWindow
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBApplication topmostWindow]");

#if DBFRAMEWORK_TARGET_IOS
+ (UIWindow *)windowStatusBar
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBApplication statusBarWindow]");
#endif

+ (UIColor *)tintColor
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBUtility tintColor]");

+ (void)impostaTintColor:(UIColor *)tintColor
    DBFRAMEWORK_DEPRECATED(0.6.3, "utilizza +[DBUtility impostaTintColor:]");

#if DBFRAMEWORK_TARGET_IOS

+ (BOOL)isiPhone    DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");
+ (BOOL)isiPad      DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");
+ (BOOL)isiPhoneApp DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");
+ (BOOL)isiPadApp   DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");

+ (BOOL)isPortrait           DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");
+ (BOOL)isPortraitUp         DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");
+ (BOOL)isPortraitUpsideDown DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");
+ (BOOL)isLandscape          DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");
+ (BOOL)isLandscapeLeft      DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");
+ (BOOL)isLandscapeRight     DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");

+ (void)impostaOrientamento:(UIInterfaceOrientation)orientamento
    DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");

+ (BOOL)isApplicazioneAttiva       DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");
+ (BOOL)isApplicazioneInBackground DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza i metodi DBDevice");

+ (BOOL)isiOSMinimum:(double)versioneRichiesta   DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza +[DBDevice isiOSMin:]");
+ (BOOL)isiOSMaximum:(double)versioneRichiesta   DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza +[DBDevice isiOSMax:]");
+ (BOOL)isiOSMajorMinimum:(int)versioneRichiesta DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza +[DBDevice isiOSMin:]");
+ (BOOL)isiOSMajorMaximum:(int)versioneRichiesta DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza +[DBDevice isiOSMax:]");
+ (BOOL)isiOSSpecific:(double)versioneRichiesta  DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza +[DBDevice isiOS:]");
+ (BOOL)isiOSMajor:(int)versioneRichiesta        DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza +[DBDevice isiOSMajor:]");

+ (CGRect)frameSchermo      DBFRAMEWORK_DEPRECATED(0.3.0, "utilizza i metodi DBGeometry");
+ (CGSize)dimensioniSchermo DBFRAMEWORK_DEPRECATED(0.3.0, "utilizza i metodi DBGeometry");
+ (CGFloat)altezzaSchermo   DBFRAMEWORK_DEPRECATED(0.3.0, "utilizza i metodi DBGeometry");

+ (BOOL)isScreenSpecific:(CGFloat)dimensioneRichiesta DBFRAMEWORK_DEPRECATED(0.3.0, "utilizza i metodi DBGeometry");
+ (BOOL)isScreenMinimum:(CGFloat)dimensioneRichiesta  DBFRAMEWORK_DEPRECATED(0.3.0, "utilizza i metodi DBGeometry");
+ (BOOL)isScreenMaximum:(CGFloat)dimensioneRichiesta  DBFRAMEWORK_DEPRECATED(0.3.0, "utilizza i metodi DBGeometry");

+ (CGRect)frameStatusBar      DBFRAMEWORK_DEPRECATED(0.3.0, "utilizza i metodi DBGeometry");
+ (CGSize)dimensioniStatusBar DBFRAMEWORK_DEPRECATED(0.3.0, "utilizza i metodi DBGeometry");
+ (CGFloat)altezzaStatusBar   DBFRAMEWORK_DEPRECATED(0.3.0, "utilizza i metodi DBGeometry");

#endif

@end


// MARK: - DBApplication

#import "DBApplication.h"

@interface DBApplication (DBDeprecated)

+ (id)topViewController
    DBFRAMEWORK_DEPRECATED(0.7.1, "please use +[DBApplication topmostViewController]");

+ (UIWindow *)topWindow
    DBFRAMEWORK_DEPRECATED(0.7.1, "please use +[DBApplication topmostWindow]");

+ (NSString *)versioneApplicazione
    DBFRAMEWORK_DEPRECATED(0.7.1, "please use +[DBApplication version]");

@end


// MARK: - DBViewController

#import "DBViewController.h"

@interface DBViewController (DBDeprecated)

/// Boolean che indica se la schermata è stata inizializzata completamente da quando è stata creata.
@property (nonatomic, readonly) BOOL didPerformFirstLoad
    DBFRAMEWORK_DEPRECATED(0.5.1, "utilizza la proprietà -inizializzazioneCompletata");

@end


// MARK: - DBTableViewController

#import "DBTableViewController.h"

@interface DBTableViewController (DBDeprecated)

@property (weak, nonatomic, readonly) NSIndexPath *indexPathCellaSelezionata
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza la proprietà -selectedIndexPath");

- (void)impostaTableView:(UITableView *)tableView
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza il metodo -[DBTableViewController useTableView:]");

- (void)mostraRefreshControl:(BOOL)mostra
    DBFRAMEWORK_DEPRECATED(0.6.0, "utilizza il metodo -[DBTableViewController enableRefreshControl:]");

- (void)abilitaRefreshControl:(BOOL)abilita
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza il metodo -[DBTableViewController enableRefreshControl:]");

- (void)abilitaScrollToTop:(BOOL)abilita
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza il metodo -[DBTableViewController enableScrollToTop:]");

- (DBTableViewCell *)tableView:(UITableView *)tableView cellaPerOggetto:(id)oggetto conIndexPath:(NSIndexPath *)indexPath
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza il metodo -[DBTableViewController tableView:cellForObject:atIndexPath:]");

- (void)tableView:(UITableView *)tableView cellaSelezionata:(DBTableViewCell *)cella conOggetto:(id)oggetto conIndexPath:(NSIndexPath *)indexPath
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza il metodo -[DBTableViewController tableView:didSelectCell:withObject:atIndexPath:]");

@end


// MARK: - DBTableViewHandler

#import "DBTableViewHandler.h"

@protocol DBTableViewHandlerDelegateDeprecated <DBTableViewHandlerDelegate>

- (DBTableViewCell *)handler:(DBTableViewHandler *)handler cellaPerOggetto:(id)oggetto conIndexPath:(NSIndexPath *)indexPath
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza il metodo -[DBTableViewHandlerDelegate handler:cellForObject:atIndexPath:]");

- (void)handler:(DBTableViewHandler *)handler cellaSelezionata:(DBTableViewCell *)cella conOggetto:(id)oggetto conIndexPath:(NSIndexPath *)indexPath
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza il metodo -[DBTableViewHandlerDelegate handler:didSelectCell:withObject:atIndexPath:]");

- (void)handlerTableViewAggiornata:(DBTableViewHandler *)handler
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza il metodo -[DBTableViewHandlerDelegate handlerDidReloadTableView:]");

@end


// MARK: - DBStringExtensions

#import "DBStringExtensions.h"

@interface NSString (DBDeprecated)

- (BOOL)isEmpty
    DBFRAMEWORK_DEPRECATED(0.6.4, "utilizza il metodo di classe per ricevere risultati piu' coerenti");

@end


// MARK: - DBDictionaryExtensions

#import "DBDictionaryExtensions.h"

@interface NSDictionary (DBDeprecated)

- (BOOL)isEmpty
    DBFRAMEWORK_DEPRECATED(0.6.4, "utilizza il metodo di classe per ricevere risultati piu' coerenti");

@end


#if DBFRAMEWORK_TARGET_IOS

// MARK: - DBKeyboardHandler

#import "DBKeyboardHandler.h"

@interface DBKeyboardHandler (DBDeprecated)

// MARK: Metodi di classe

/// Crea un nuovo Handler che si occuperà di gestire le InputFields specificate, assegnandogli un delegate
+ (instancetype)controllaVista:(UIView *)vista conTextFields:(NSArray *)textFields conDelegate:(id<DBKeyboardHandlerDelegate>)delegate
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBKeyboardHandler handlerWithView:inputFields:delegate:]");

/// Crea un nuovo Handler che si occuperà di gestire le InputFields specificate
+ (instancetype)controllaVista:(UIView *)vista conTextFields:(NSArray *)textFields
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBKeyboardHandler handlerWithView:inputFields:delegate:]");

/// Crea un nuovo Handler che si occuperà di gestire le InputFields specificate
+ (instancetype)handlerConView:(UIView *)view inputFields:(NSArray *)inputFields delegate:(id<DBKeyboardHandlerDelegate>)delegate
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBKeyboardHandler handlerWithView:inputFields:delegate:]");

/// Disabilita e rimuove gli Handler che controllano la vista e le relative InputFields
+ (void)rimuoviControlloVista:(UIView *)vista
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBKeyboardHandler destroyHandlerWithView:]");

/// Disabilita e rimuove gli Handler che controllano la vista e le relative InputFields
+ (void)distruggiHandlerConView:(UIView *)view
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBKeyboardHandler destroyHandlerWithView:]");

/// Disabilita e rimuove l'Handler dalla memoria
+ (void)distruggiHandler:(DBKeyboardHandler *)handler
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBKeyboardHandler destroyHandler]");

/// Restituisce il primo Handler che si sta attualmente occupando della vista specificata
+ (instancetype)handlerConVista:(UIView *)vista
    DBFRAMEWORK_FORBIDDEN(0.7.0, "non è piu' possibile recuperare l'Handler, bisogna mantenere il riferimento manualmente");

/// Restituisce il primo Handler che si sta attualmente occupando della vista specificata
+ (instancetype)getHandlerVista:(UIView *)vista
    DBFRAMEWORK_FORBIDDEN(0.7.0, "non è piu' possibile recuperare l'Handler, bisogna mantenere il riferimento manualmente");

// MARK: Metodi d'istanza

- (instancetype)initWithView:(id)view andTextFields:(NSArray *)textFields andDelegate:(id<DBKeyboardHandlerDelegate>)delegate
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBKeyboardHandler handlerWithView:inputFields:delegate:]");

/// Inizializza l'Handler e abilita il controllo delle Input Fields
- (instancetype)initWithView:(id)view andTextFields:(NSArray *)textFields
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBKeyboardHandler handlerWithView:inputFields:delegate:]");

/// Disabilita e rimuove l'Handler dalla memoria
- (void)distruggiHandler
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBKeyboardHandler destroyHandler]");

/// Imposta le InputFields controllate dall'Handler. Se una InputField è già controllata dall'Handler, la sua configurazione viene mantenuta (ad esempio il limite di caratteri o il delegate).
- (void)impostaInputFields:(NSArray *)inputFields
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBKeyboardHandler setInputFields:]");

/// Abilita il controllo delle UITextFields
- (void)abilitaControlloTextFieldsConSchermata:(id)view
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBKeyboardHandler enableHandler:]");

/// Disabilita il controllo delle UITextFields
- (void)disabilitaControlloTextFields
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBKeyboardHandler enableHandler:]");

/// Da impostare a YES per fixare il frame errato se è presente una TabBar
- (void)impostaTabBar:(BOOL)tabBar
    DBFRAMEWORK_DEPRECATED(0.7.0, "non è piu' possibile correggere il frame della TabBar, bisogna gestire il frame manualmente");

- (void)impostaLimiteCaratteri:(NSInteger)limite perTextField:(id)textField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBKeyboardHandler inputField:setMaximumNumberOfCharacters:]");

- (void)impostaLimiteCaratteri:(NSInteger)limite perInputField:(id)inputField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBKeyboardHandler inputField:setMaximumNumberOfCharacters:]");

- (void)impostaCaratteriPermessi:(NSString *)caratteriPermessi perTextField:(id)textField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBKeyboardHandler inputField:setAllowedCharacters:]");

- (void)impostaCaratteriProibiti:(NSString *)caratteriProibiti perTextField:(id)textField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBKeyboardHandler inputField:setForbiddenCharacters:]");

- (void)impostaDelegate:(id<DBKeyboardHandlerDelegate>)delegate perInputFields:(NSArray *)inputFields
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza il singolo -[DBKeyboardHandler inputField:setDelegate:]");

@end


// MARK: - DBPickerHandler

#import "DBPickerHandler.h"

@interface DBPickerHandler (DBDeprecated)

// MARK: Metodi di classe

/// Crea un nuovo Handler che si occuperà di gestire le TextFields specificate (o utilizza l'Handler con il delegate specificato)
+ (instancetype)controllaTextFields:(NSArray *)textFields conDataSources:(NSArray *)dataSources conDelegate:(id<DBPickerHandlerDelegate>)delegate
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBPickerHandler handlerWithDelegate:inputFields:]");

/// Elimina l'Handler che si occupava delle TextFields associate al delegate
+ (BOOL)rimuoviControlloDelegate:(id<DBPickerHandlerDelegate>)delegate
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBPickerHandler destroyHandlerWithDelegate:]");

/// Restituisce l'Handler associato al delegate specificato
+ (instancetype)getHandlerConDelegate:(id<DBPickerHandlerDelegate>)delegate
    DBFRAMEWORK_FORBIDDEN(0.7.0, "non è piu' possibile recuperare l'Handler, bisogna mantenere il riferimento manualmente");

// MARK: Metodi d'istanza

/// Inizializza l'Handler e abilita il controllo delle InputField
- (instancetype)initWithDelegate:(id<DBPickerHandlerDelegate>)delegate inputFields:(NSArray *)inputFields dataSources:(NSArray *)dataSources
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBPickerHandler handlerWithDelegate:inputFields:]");

/// Disabilita il controllo delle InputField
- (void)disabilitaControlloInputFields
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBPickerHandler enableHandler:]");

- (void)impostaData:(NSDate *)data perDatePickerInputField:(id<UITextInput>)inputField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBPickerHandler inputField:setDate:]");

- (void)impostaDataInizio:(NSDate *)dataInizio dataFine:(NSDate *)dataFine perDatePickerInputField:(id<UITextInput>)inputField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBPickerHandler inputField:setMinimumDate:maximumDate:]");

- (void)impostaMinutiIntervallo:(NSInteger)minutiIntervallo perDatePickerInputField:(id<UITextInput>)inputField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBPickerHandler inputField:setMinuteInterval:]");

/// Imposta o sostituisce il dataSource per l'InputField richiesta
- (void)impostaDataSource:(NSArray *)dataSource perInputField:(id<UITextInput>)inputField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBPickerHandler inputField:setDataSource:]");

/// Imposta o sostituisce il dataSource per l'InputField inserita alla posizione richiesta dell'array di InputFields
- (void)impostaDataSource:(NSArray *)dataSource perInputFieldAtIndex:(NSInteger)index
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBPickerHandler inputField:setDataSource:]");

- (void)selezionaIndex:(NSInteger)index inComponent:(NSInteger)component perInputField:(id<UITextInput>)inputField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBPickerHandler inputField:selectObjectAtIndex:]");

- (void)selezionaItem:(id)item perInputField:(id<UITextInput>)inputField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBPickerHandler inputField:selectObject:]");

- (id)getItemPerInputField:(id<UITextInput>)inputField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBPickerHandler inputFieldSelectedObject:]");

- (NSInteger)getIndexPerInputField:(id<UITextInput>)inputField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza -[DBPickerHandler inputFieldSelectedObjectIndex:]");

@end

#endif


// MARK: - DBNetworking

#import "DBNetworking.h"

@interface DBNetworking (DBDeprecated)

/// Attiva il debug delle chiamate nella console.
+ (void)impostaDebug:(BOOL)debug
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBNetworking setDebugMode:]");

/// Attiva il debug delle chiamate nella console.
+ (void)abilitaModalitaDebug:(BOOL)abilita
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBNetworking setDebugMode:]");

/// Se attivo, ogni nuova chiamata con lo stesso URL annullerà le chiamate precedenti. Altrimenti, ogni nuova chiamata rimarrà in attesa della risposta della prima.
+ (void)impostaCancellaRichiestePrecedenti:(BOOL)cancellaRichiestePrecedenti
    DBFRAMEWORK_FORBIDDEN(0.7.0, "non è piu' possibile utilizzare questa funzionalità");

+ (void)impostaHeaderGlobali:(nullable NSDictionary *)headers
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBNetworking setGlobalHeader:]");

+ (void)impostaHeaderGlobale:(nullable NSDictionary *)header
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBNetworking setGlobalHeader:]");

/// Restituisce gli Header che vengono attualmente aggiunti ad ogni chiamata effettuata.
+ (nullable NSDictionary *)headerGlobali
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBNetworking globalHeader]");

/// Restituisce gli Header che vengono attualmente aggiunti ad ogni chiamata effettuata.
+ (nullable NSDictionary *)headerGlobale
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBNetworking globalHeader]");

/// Indica se il dispositivo è connesso ad internet.
+ (BOOL)checkConnessioneInternet
    DBFRAMEWORK_DEPRECATED(0.5.1, "utilizza +[DBNetworking isNetworkAvailable]");

/// Indica se il dispositivo è connesso ad internet.
+ (BOOL)connessioneInternetDisponibile
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBNetworking isNetworkAvailable]");

/// Indica se il dispositivo è connesso ad internet.
+ (BOOL)isConnessioneInternetDisponibile
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza +[DBNetworking isNetworkAvailable]");


/// CompletionBlock per le richieste web.
typedef void (^DBNetworkingCompletionBlockDeprecato)(NSError * __nullable error, id __nullable responseObject);

/// CompletionBlock per le richieste web.
typedef void (^DBNetworkingSuccessBlockDeprecato)(NSError * __nullable error);


+ (void)JSONGETConURLString:(nullable NSString *)URLString conParametri:(nullable NSDictionary *)parametri conCompletionBlock:(nullable DBNetworkingCompletionBlockDeprecato)completionBlock
    DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");

+ (void)JSONPOSTConURLString:(nullable NSString *)URLString conParametri:(nullable NSDictionary *)parametri conCompletionBlock:(nullable DBNetworkingCompletionBlockDeprecato)completionBlock
    DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");

+ (void)JSONDELETEConURLString:(nullable NSString *)URLString conParametri:(nullable NSDictionary *)parametri conCompletionBlock:(nullable DBNetworkingCompletionBlockDeprecato)completionBlock
    DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");

+ (void)JSONPUTConURLString:(nullable NSString *)URLString conParametri:(nullable NSDictionary *)parametri conCompletionBlock:(nullable DBNetworkingCompletionBlockDeprecato)completionBlock
    DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");


+ (void)HTMLGETConURLString:(nullable NSString *)URLString conParametri:(nullable NSDictionary *)parametri conCompletionBlock:(nullable DBNetworkingCompletionBlockDeprecato)completionBlock
DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");


+ (void)GETImmagineConURLString:(nullable NSString *)URLString conCompletionBlock:(nullable DBNetworkingCompletionBlockDeprecato)completionBlock
    DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");

+ (void)GETImmagineConURLString:(nullable NSString *)URLString sovrascrivi:(BOOL)sovrascrivi conCompletionBlock:(nullable DBNetworkingCompletionBlockDeprecato)completionBlock
    DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");


+ (void)GETFileConURLString:(nullable NSString *)URLString conDownloadPath:(nullable NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi conSuccessBlock:(nullable DBNetworkingSuccessBlockDeprecato)successBlock
    DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");

+ (void)GETFileConURLString:(nullable NSString *)URLString conDownloadPath:(nullable NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi conProgressHUD:(BOOL)progressHUDAbilitato messaggioProgressHUD:(nullable NSString *)messaggioProgressHUD conSuccessBlock:(nullable DBNetworkingSuccessBlockDeprecato)successBlock
    DBFRAMEWORK_DEPRECATED(0.4.0, "utilizza il nuovo metodo con il nuovo CompletionBlock");

@end


// MARK: - View

#if DBFRAMEWORK_TARGET_IOS

@interface UIView (DBDeprecated)

- (void)aggiungiEffettoSfocato
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza il metodo -[UIView setVisualEffect:]");

- (void)rimuoviEffettoSfocato
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza il metodo -[UIView setVisualEffect:DBViewVisualEffectNone]");

@end


// MARK: - ImageView

@interface UIImageView (DBDeprecated)

- (void)caricaImmagineConURLString:(NSString *)URLString conActivityIndicator:(UIActivityIndicatorView *)activityIndicator
    DBFRAMEWORK_DEPRECATED(0.7.0, "please extend your UIImageView object from the new DBImageView class, and use the new method -[DBImageView setImageWithURLString:placeholderImage:completionBlock:]");

- (void)caricaImmagineConURLString:(nonnull NSString *)URLString conActivityIndicator:(nullable UIActivityIndicatorView *)activityIndicator conCompletionBlock:(nullable void (^)(BOOL finished))completionBlock
    DBFRAMEWORK_DEPRECATED(0.7.0, "please extend your UIImageView object from the new DBImageView class, and use the new method -[DBImageView setImageWithURLString:placeholderImage:completionBlock:]");

/**
 * Removed method: it is very generic and may conflict with any other UIImageView implementation. It is now included in custom DBImageView,
 * please use that class if you need this feature.
 */
// - (void)setImageWithURLString:(nonnull NSString *)URLString;

- (void)setImageWithURLString:(nonnull NSString *)URLString activityIndicator:(nullable UIActivityIndicatorView *)activityIndicator completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    DBFRAMEWORK_DEPRECATED(0.7.2, "please extend your UIImageView object from the new DBImageView class, and use the new method -[DBImageView setImageWithURLString:placeholderImage:completionBlock:]");

- (void)setImageWithURLString:(nullable NSString *)URLString withDefaultImage:(nullable UIImage *)defaultImage
    DBFRAMEWORK_DEPRECATED(0.7.2, "please extend your UIImageView object from the new DBImageView class, and use the new method -[DBImageView setImageWithURLString:placeholderImage:completionBlock:]");

@end

#endif


// MARK: - Reachability

@interface Reachability : NSObject

/// Indica se il dispositivo è connesso ad internet.
+ (BOOL)checkConnessioneInternet
    DBFRAMEWORK_DEPRECATED(0.5.1, "utilizza +[DBNetworking isNetworkAvailable]");

@end


// MARK: - DBProgressHUD

#import "DBProgressHUD.h"

@interface MBProgressHUD (DBDeprecated)

+ (void)creaHUDSullaVista:(UIView *)vista
    DBFRAMEWORK_DEPRECATED(0.2.0, "utilizza il metodo +[DBProgressHUD progressHUDConMessaggio:conVista:]");

+ (void)creaHUDConMessaggio:(NSString *)messaggio sullaVista:(UIView *)vista
    DBFRAMEWORK_DEPRECATED(0.2.0, "utilizza il metodo +[DBProgressHUD progressHUDConMessaggio:conVista:]");

+ (void)creaAlertHUDConMessaggio:(NSString *)messaggio
    DBFRAMEWORK_DEPRECATED(0.2.0, "utilizza il metodo +[DBProgressHUD toastConMessaggio:]");

+ (void)creaAlertHUDConMessaggio:(NSString *)messaggio sullaVista:(UIView *)vista
    DBFRAMEWORK_DEPRECATED(0.2.0, "utilizza il metodo +[DBProgressHUD toastConMessaggio:conVista:conDurata:ritardaSuccessivi:]");

+ (void)rimuoviHUD
    DBFRAMEWORK_DEPRECATED(0.2.0, "utilizza il metodo +[DBProgressHUD rimuoviProgressHUDs]");

@end


// MARK: - Storyboard

@interface UIStoryboard (DBDeprecated)

- (id)istanziaViewController:(Class)classeViewController
    DBFRAMEWORK_DEPRECATED(0.7.0, "please use -[UIStoryboard instantiateViewControllerWithClass:]");

@end


// MARK: - MapViewController

#import "DBMapViewController.h"

#pragma clang diagnostic pop
