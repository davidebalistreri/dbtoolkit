//
//  DBNotificationCenter.h
//  File version: 1.0.0 beta
//  Last modified: 10/09/2016
//
//  Created by Davide Balistreri on 10/09/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


#import "DBFoundation.h"

@interface DBNotificationCenter : NSObject

typedef void (^DBNotificationCenterBlock)(NSNotification * __nonnull notification);

+ (void)postNotification:(nonnull NSNotification *)notification;

/**
 * Registers an observer on the target, to receive notifications for a given name and
 * execute a block when a notification triggers.
 *
 * When the target gets deallocated, the observer is automatically unregistered:
 * you don't need to call +[DBNotificationCenter removeAllObserversOnTarget:] unless
 * you want to unregister the observers earlier.
 *
 * Be careful when using this block based method as it could led to strong retain-cycles.
 * Always use weak references inside the block. Pay even more attention if your block calls other blocks.
 */
+ (void)addObserverOnTarget:(nonnull id)target forName:(nonnull NSString *)name withNotificationBlock:(nullable DBNotificationCenterBlock)notificationBlock;

/**
 * Registers an observer on the target, to receive notifications for a given name and
 * execute a block when a notification triggers.
 *
 * When the target gets deallocated, the observer is automatically unregistered:
 * you don't need to call +[DBNotificationCenter removeAllObserversOnTarget:] unless
 * you want to unregister the observers earlier.
 *
 * This method is generally safer as it will perform the selector on a weak referenced target.
 */
+ (void)addObserverOnTarget:(nonnull id)target forName:(nonnull NSString *)name withSelector:(nullable SEL)selector object:(nullable id)object;

+ (void)removeObserversOnTarget:(nonnull id)target forName:(nonnull NSString *)name;

+ (void)removeAllObserversOnTarget:(nonnull id)target;

@end


@interface DBNotificationCenter (Extended)

+ (void)postNotificationWithName:(nonnull NSString *)name;

+ (void)postNotificationWithName:(nonnull NSString *)name object:(nullable id)object;

+ (void)postNotificationWithName:(nonnull NSString *)name object:(nullable id)object userInfo:(nullable NSDictionary *)userInfo;

@end
