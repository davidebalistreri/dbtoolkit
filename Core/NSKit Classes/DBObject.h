//
//  DBObject.h
//  File version: 1.0.2
//  Last modified: 11/26/2016
//
//  Created by Davide Balistreri on 03/27/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"


/**
 * NSObject extended for an easier setup process and later use.
 * The setup sequence is the same as other objects, views and controls inside this framework.
 */

NS_ASSUME_NONNULL_BEGIN

@interface DBObject : NSObject <NSCopying, NSCoding>

/// Boolean che indica se l'oggetto è stato inizializzato completamente da quando è stato creato.
@property (nonatomic, readonly) BOOL setupCompleted;

@property (nonatomic, readonly) BOOL inizializzazioneCompletata
    DBFRAMEWORK_DEPRECATED(0.7.1, "please use -[DBObject setupCompleted]");

/**
 * Metodo da utilizzare per inizializzare l'oggetto.
 * <p>Viene richiamato automaticamente appena l'oggetto viene istanziato.</p>
 * <p>Deve essere eseguito un'unica volta.</p>
 */
- (void)setupObject;

- (void)inizializza
    DBFRAMEWORK_DEPRECATED(0.7.1, "please use -[DBObject setupObject]");


// MARK: - Methods

- (NSData *)encodedObject;
+ (nullable instancetype)decodedObject:(nullable NSData *)data;
- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;
- (id)copyWithZone:(nullable NSZone *)zone;

@end

NS_ASSUME_NONNULL_END
