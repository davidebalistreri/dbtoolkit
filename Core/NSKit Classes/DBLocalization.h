//
//  DBLocalization.h
//  File version: 1.0.3
//  Last modified: 05/09/2018
//
//  Created by Davide Balistreri on 03/30/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

NS_ASSUME_NONNULL_BEGIN

/// Per le notifiche
extern NSString * const kNotificaLinguaCorrenteAggiornata;


@interface DBLocalization : NSObject

/// Localizza la stringa. Restituisce una stringa vuota se la stringa passata non è valida.
NSString * DBLocalizedString(NSString * _Nullable stringa);

/// Localizza la stringa in UPPERCASE. Restituisce una stringa vuota se la stringa passata non è valida.
NSString * DBLocalizedUppercaseString(NSString * _Nullable stringa);

/**
 * Imposta la lingua corrente dell'App, utilizzata dal metodo stringaLocalizzata.
 *
 * Se questa lingua non è impostata, viene utilizzata la localizzazione corrente del sistema.
 *
 * @param identifierLingua L'identifier della lingua da impostare come corrente.
 */
+ (void)impostaLinguaCorrente:(NSString *)identifierLingua;

/**
 * Imposta la lingua di default dell'App, utilizzata dal metodo stringaLocalizzata quando non esiste una localizzazione per la lingua corrente.
 *
 * @param identifierLingua L'identifier della lingua da impostare come default.
 */
+ (void)impostaLinguaDefault:(NSString *)identifierLingua;


/**
 * Restituisce l'identifier della lingua impostata manualmente.
 * Se non è stata impostata manualmente la lingua corrente, viene restituita la lingua di default.
 * Se non è stata impostata manualmente la lingua di default, viene restituita la lingua dell'app.
 */
+ (NSString *)identifierLinguaCorrente;

/**
 * Restituisce l'identifier della lingua impostata manualmente come default.
 */
+ (NSString *)identifierLinguaDefault;

/**
 * Restituisce l'identifier della lingua utilizzata dall'app.
 * NB: Potrebbe essere diversa da quella del sistema operativo.
 */
+ (NSString *)identifierLinguaApp;

/**
 * Restituisce l'identifier della lingua utilizzata dal sistema operativo.
 * NB: Potrebbe non essere inclusa nelle lingue disponibili e supportate dall'app.
 */
+ (NSString *)identifierLinguaSistema;


#if DBFRAMEWORK_TARGET_IOS
/**
 * Esegue il download di un file Localizable.strings, lo struttura dentro il suo bundle
 * nella cartella Documenti e lo carica nell'app tra le lingue disponibili.
 * @param URL L'URL del file Localizable.strings da scaricare.
 * @param identifierLingua L'identifier della lingua da scaricare.
 */
+ (void)downloadLinguaConURL:(NSURL *)URL conIdentifierLingua:(NSString *)identifierLingua;

/**
 * Esegue il download di un file Localizable.strings, lo struttura dentro il suo bundle
 * nella cartella Documenti e lo carica nell'app tra le lingue disponibili.
 * @param URL L'URL del file Localizable.strings da scaricare.
 * @param identifierLingua L'identifier della lingua da scaricare.
 * @param sovrascrivi Indica se bisogna sovrascrivere il file precedentemente scaricato.
 */
+ (void)downloadLinguaConURL:(NSURL *)URL conIdentifierLingua:(NSString *)identifierLingua sovrascrivi:(BOOL)sovrascrivi;
#endif

@end


@interface NSString (DBLocalization)

/**
 * Utilizza l'istanza della stringa come chiave per cercare la sua localizzazione con la lingua corrente.
 *
 * Se non esiste alcuna localizzazione con la lingua corrente, viene usata la lingua di default.
 *
 * Se non esiste alcuna localizzazione neanche con la lingua di default, viene restituita l'istanza originale.
 *
 * Se nessuna delle due lingue (corrente e default) è impostata, il sistema cercherà automaticamente la versione inglese.
 */
- (NSString *)stringaLocalizzata;

/**
 * Restituisce la stringa localizzata del metodo stringaLocalizzata in UPPERCASE.
 */
- (NSString *)stringaLocalizzataUppercase;

/**
 * Metodo di classe che localizza la stringa, e restituisce una stringa vuota se la stringa passata non è valida.
 */
+ (NSString *)stringaLocalizzata:(nullable NSString *)stringa;

/**
 * Metodo di classe che localizza la stringa in UPPERCASE, e restituisce una stringa vuota se la stringa passata non è valida.
 */
+ (NSString *)stringaLocalizzataUppercase:(nullable NSString *)stringa;

@end

NS_ASSUME_NONNULL_END
