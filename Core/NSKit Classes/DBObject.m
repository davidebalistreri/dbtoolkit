//
//  DBObject.m
//  File version: 1.0.2
//  Last modified: 11/26/2016
//
//  Created by Davide Balistreri on 03/27/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBObject.h"

#import "DBRuntimeHandler.h"
#import <objc/runtime.h>

@implementation DBObject

// MARK: - Setup

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

@synthesize setupCompleted = _setupCompleted;
- (void)privateSetupObject
{
    if (self.setupCompleted == NO) {
        // Prevent further initialization requests
        _setupCompleted = YES;
        
        // Perform the method implemented by the developer
        [self setupObject];
        
        if ([self overridesSelector:@selector(inizializza) ofClass:[DBObject class]]) {
            [self performSelector:@selector(inizializza)];
        }
    }
}

- (BOOL)inizializzazioneCompletata
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.1, "please use -[DBObject setupCompleted]");
    return self.setupCompleted;
}

- (void)setupObject
{
    // Developers may override this method and provide custom behavior
}

- (void)inizializza
{
    DBFRAMEWORK_DEPRECATED_CUSTOM_LOG("-[DBObject inizializza]", 0.7.1, "please use -[DBObject setupObject]");
}


// MARK: - Methods

- (NSData *)encodedObject
{
    return [NSKeyedArchiver archivedDataWithRootObject:self];
}

+ (instancetype)decodedObject:(NSData *)data
{
    if (!data) {
        return nil;
    }
    else {
        return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (self) {
        // Ancora da implementare...
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    // Ancora da implementare...
}

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] allocWithZone:zone] init];
    
    if (copy) {
        // Ancora da implementare...
    }
    
    return copy;
}


// MARK: - Copy/Encode/Decode automatico

- (NSDictionary *)listaProprieta
{
    return [DBObject listaProprietaDellaClasse:[self class]];
}

+ (NSDictionary *)listaProprietaDellaClasse:(Class)classe
{
    // La lista delle proprietà della classe specificata
    NSMutableDictionary *proprieta = [NSMutableDictionary dictionary];
    
    // Prendo le proprietà della classe specificata
    Class classeAttuale = classe;
    
    while (classeAttuale != NULL && classeAttuale != [NSObject class]) {
        unsigned int count;
        objc_property_t *propertyList = class_copyPropertyList(classe, &count);
        
        for (int counter = 0; counter < count; counter++) {
            objc_property_t property = propertyList[counter];
            const char *propertyName = property_getName(property);
            
            if (propertyName) {
                const char *propertyType = getPropertyType(property);
                NSString *nome = [NSString stringWithUTF8String:propertyName];
                NSString *tipo = [NSString stringWithUTF8String:propertyType];
                [proprieta setObject:tipo forKey:nome];
            }
        }
        
        free(propertyList);
        
        // Procedo prendendo anche le proprietà della superclasse
        classeAttuale = [classeAttuale superclass];
    }
    
    return proprieta;
}

static const char *getPropertyType(objc_property_t property)
{
    const char *attributes = property_getAttributes(property);
    
    // DEBUG
    // printf("attributes=%s\n", attributes);
    
    char buffer[1 + strlen(attributes)];
    strcpy(buffer, attributes);
    char *state = buffer, *attribute;
    
    while ((attribute = strsep(&state, ",")) != NULL) {
        if (attribute[0] == 'T' && attribute[1] != '@') {
            // it's a C primitive type:
            /*
             if you want a list of what will be returned for these primitives, search online for
             "objective-c" "Property Attribute Description Examples"
             apple docs list plenty of examples of what you get for int "i", long "l", unsigned "I", struct, etc.
             */
            return (const char *)[[NSData dataWithBytes:(attribute + 1) length:strlen(attribute) - 1] bytes];
        }
        else if (attribute[0] == 'T' && attribute[1] == '@' && strlen(attribute) == 2) {
            // it's an ObjC id type:
            return "id";
        }
        else if (attribute[0] == 'T' && attribute[1] == '@') {
            // it's another ObjC object type:
            return (const char *)[[NSData dataWithBytes:(attribute + 3) length:strlen(attribute) - 4] bytes];
        }
    }
    
    return "";
}

@end
