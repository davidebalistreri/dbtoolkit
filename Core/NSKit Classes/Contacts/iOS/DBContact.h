//
//  DBContact.h
//  File version: 1.0.0 beta
//  Last modified: 05/31/2017
//
//  Created by Davide Balistreri on 05/31/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


#import "DBObject.h"

@interface DBContact : DBObject

@property (strong, nonatomic) id rawData;


@property (copy) NSString *firstName;

@property (copy) NSString *lastName;

@property (copy) NSString *organizationName;

@property (copy) NSString *phoneNumber;

@property (copy) UIImage *image;

@end
