//
//  DBContacts.h
//  File version: 1.0.0 beta
//  Last modified: 05/31/2017
//
//  Created by Davide Balistreri on 05/31/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


#import "DBFoundation.h"

#import "DBContactsDefines.h"
#import "DBContact.h"


/**
 * In order to use this module and its related features, the app's Info.plist file must contain an
 * NSContactsUsageDescription key with a string value explaining to the user how the app uses this privacy-sensitive data.
 */

@interface DBContacts : NSObject

+ (DBContactsAuthorizationStatus)authorizationStatus;

/// Returns YES if the user has already chosen whether to authorize the access (or not).
+ (BOOL)isAuthorizationAsked;

/// Returns YES if the user has authorized the app.
+ (BOOL)isAuthorizationGranted;

/// Returns NO if the minimum requirements for contacts access are not met.
+ (BOOL)canAccessContacts;


/**
 * Asks authorization for contacts access to the user, if needed.
 *
 * iOS will prompt to the user only if the NSContactsUsageDescription key is present in the Info.plist file.
 *
 * Its safe to call this method everytime you need to use the contacts,
 * as it executes the completion block even if the authorization was already granted (or not).
 */
+ (void)requestAuthorizationWithCompletionBlock:(nullable DBContactsAuthorizationBlock)completionBlock;


/// Returns YES if authorization for contacts access is granted, and a similar contact is found in Contacts.
+ (BOOL)contactExists:(nullable DBContact *)contact;


+ (void)addContact:(nullable DBContact *)contact completionBlock:(nullable DBContactsCompletionBlock)completionBlock;

@end
