//
//  DBContacts.m
//  File version: 1.0.0 beta
//  Last modified: 05/31/2017
//
//  Created by Davide Balistreri on 05/31/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


#import "DBContacts.h"

#import "DBThread.h"

#import <AddressBook/AddressBook.h>


@implementation DBContacts

+ (DBContactsAuthorizationStatus)authorizationStatus
{
    ABAuthorizationStatus authorizationStatus = ABAddressBookGetAuthorizationStatus();
    
    switch (authorizationStatus) {
        case kABAuthorizationStatusAuthorized:
            return DBContactsAuthorizationStatusAuthorized;
        case kABAuthorizationStatusDenied:
            return DBContactsAuthorizationStatusDenied;
        case kABAuthorizationStatusRestricted:
            return DBContactsAuthorizationStatusRestricted;
        case kABAuthorizationStatusNotDetermined:
        default:
            return DBContactsAuthorizationStatusNotDetermined;
    }
}

+ (BOOL)isAuthorizationAsked
{
    if ([DBContacts authorizationStatus] == DBContactsAuthorizationStatusNotDetermined) {
        return NO;
    }
    
    return YES;
}

+ (BOOL)isAuthorizationGranted
{
    switch ([DBContacts authorizationStatus]) {
        case DBContactsAuthorizationStatusNotDetermined:
        case DBContactsAuthorizationStatusRestricted:
        case DBContactsAuthorizationStatusDenied:
            return NO;
            
        default:
            return YES;
    }
}

+ (BOOL)canAccessContacts
{
    if ([DBContacts isAuthorizationGranted] == NO) {
        return NO;
    }
    
    return YES;
}

+ (void)requestAuthorizationWithCompletionBlock:(DBContactsAuthorizationBlock)completionBlock
{
    ABAddressBookRef addressBook = ABAddressBookCreate();
    
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
        DBContactsAuthorizationStatus authorizationStatus = [DBContacts authorizationStatus];
        
        // ABAddressBook completion block could get called on a background thread
        [DBThread eseguiBlock:^{
            if (completionBlock) {
                completionBlock(authorizationStatus);
            }
        }];
    });
}

+ (BOOL)contactExists:(DBContact *)contact
{
    if ([DBContacts isAuthorizationGranted] == NO) {
        // Authorization needed
        return NO;
    }
    
    if (!contact) {
        // Not valid
        return NO;
    }
    
    ABRecordRef thisRecord = (__bridge ABRecordRef)contact.rawData;
    CFStringRef thisNameRef = ABRecordCopyCompositeName(thisRecord);
    
    if (!thisNameRef) {
        // Not valid
        return NO;
    }
    
    // Check in the address book
    ABAddressBookRef addressBookRef = ABAddressBookCreate();
    CFArrayRef contactsRef = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
    CFRelease(addressBookRef);
    
    NSArray *contacts = CFBridgingRelease(contactsRef);
    
    for (id aContact in contacts) {
        ABRecordRef aRecord = (__bridge ABRecordRef)aContact;
        CFStringRef aNameRef = ABRecordCopyCompositeName(aRecord);
        
        if (aNameRef) {
            if (CFStringCompare(thisNameRef, aNameRef, 0) == kCFCompareEqualTo) {
                // The contact already exists
                CFRelease(thisNameRef);
                CFRelease(aNameRef);
                return YES;
            }
            
            CFRelease(aNameRef);
        }
    }
    
    CFRelease(thisNameRef);
    return NO;
}

+ (void)addContact:(DBContact *)contact completionBlock:(DBContactsCompletionBlock)completionBlock
{
    if ([DBContacts isAuthorizationGranted] == NO) {
        // Authorization needed
        if (completionBlock) {
            completionBlock(NO);
        }
        
        return;
    }
    
    if (!contact) {
        // Not valid
        if (completionBlock) {
            completionBlock(NO);
        }
        
        return;
    }
    
    // Adding the contact (even if it already exists)
    ABRecordRef record = (__bridge ABRecordRef)contact.rawData;
    
    ABAddressBookRef addressBookRef = ABAddressBookCreate();
    
    bool addResult = ABAddressBookAddRecord(addressBookRef, record, nil);
    bool saveResult = ABAddressBookSave(addressBookRef, nil);
    CFRelease(addressBookRef);
    
    if (completionBlock) {
        if (addResult && saveResult) {
            completionBlock(YES);
        } else {
            completionBlock(NO);
        }
    }
}

@end
