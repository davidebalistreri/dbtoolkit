//
//  DBContactsDefines.h
//  File version: 1.0.0 beta
//  Last modified: 05/31/2017
//
//  Created by Davide Balistreri on 05/31/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


typedef NS_ENUM(NSInteger, DBContactsAuthorizationStatus) {
    // User has not yet made a choice with regards to this application
    DBContactsAuthorizationStatusNotDetermined = 0,
    
    // This application is not authorized to use location services. Due
    // to active restrictions on location services, the user cannot change
    // this status, and may not have personally denied authorization.
    DBContactsAuthorizationStatusRestricted,
    
    // User has explicitly denied authorization for this application.
    DBContactsAuthorizationStatusDenied,
    
    // User has authorized this application to use location services.
    DBContactsAuthorizationStatusAuthorized
};


typedef void (^DBContactsAuthorizationBlock)(DBContactsAuthorizationStatus authorizationStatus);

typedef void (^DBContactsCompletionBlock)(BOOL success);
