//
//  DBSocialKit.h
//  File version: 1.0.0
//  Last modified: 04/21/2016
//
//  Created by Davide Balistreri on 18/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"
#import "DBSocialKitResponse.h"

@interface DBSocialKit : NSObject

// MARK: - Configurazione da AppDelegate

/**
 * Interroga tutti i SocialKit integrati nell'App.
 */
+ (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

/**
 * Interroga tutti i SocialKit integrati nell'App.
 */
+ (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

/**
 * Interroga tutti i SocialKit integrati nell'App.
 */
+ (void)applicationDidBecomeActive:(UIApplication *)application;

// MARK: - Metodi

typedef void (^DBSocialCompletionBlock)(DBSocialKitResponse *response);

@end


#import "DBFacebookKit.h"
#import "DBGoogleKit.h"
