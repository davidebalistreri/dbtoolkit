//
//  DBSocialKitResponse.h
//  File version: 1.0.1
//  Last modified: 09/19/2016
//
//  Created by Davide Balistreri on 04/24/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBObject.h"

/**
 * La risposta dei social network alle richieste DBSocialKit, restituita nei completionBlock.
 */
@interface DBSocialKitResponse : DBObject

/// The user authorization token returned by the social framework.
@property (strong, nonatomic, nullable) NSString *token;

/// Optional user secret token returned by the social framework.
@property (strong, nonatomic, nullable) NSString *secretToken;


/// Boolean che indica se la richiesta è andata a buon fine.
@property (nonatomic) BOOL success;

/// Boolean che indica se la richiesta è stata cancellata prematuramente.
@property (nonatomic) BOOL isCancelled;

/// L'eventuale errore restituito dal server o dal framework.
@property (strong, nonatomic, nullable) NSError *error;


/// The original response object returned by the social framework.
@property (strong, nonatomic, nullable) id rawResponse;

@end
