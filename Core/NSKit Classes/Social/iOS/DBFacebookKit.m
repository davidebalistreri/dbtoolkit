//
//  DBFacebookKit.m
//  File version: 1.1.0
//  Last modified: 04/07/2017
//
//  Created by Davide Balistreri on 18/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFacebookKit.h"
#import "DBApplication.h"
#import "DBThread.h"

#ifdef DBFRAMEWORK_SUBSPEC_FACEBOOKKIT

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#endif


@implementation DBFacebookKit

// MARK: - Configurazione da AppDelegate

+ (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#ifdef DBFRAMEWORK_SUBSPEC_FACEBOOKKIT
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
#endif
    return YES;
}

+ (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
#ifdef DBFRAMEWORK_SUBSPEC_FACEBOOKKIT
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
#endif
    return NO;
}

+ (void)applicationDidBecomeActive:(UIApplication *)application
{
#ifdef DBFRAMEWORK_SUBSPEC_FACEBOOKKIT
    [FBSDKAppEvents activateApp];
#endif
}

// MARK: - Methods

+ (void)loginWithCompletionBlock:(DBSocialCompletionBlock)completionBlock
{
    // Default permissions
    NSArray *permissions = @[@"public_profile", @"email"];
    [self loginWithReadPermissions:permissions completionBlock:completionBlock];
}

+ (void)loginWithReadPermissions:(NSArray<NSString *> *)readPermissions completionBlock:(DBSocialCompletionBlock)completionBlock
{
#ifdef DBFRAMEWORK_SUBSPEC_FACEBOOKKIT
    // Controllo se i permessi già sono stati ottenuti
    BOOL hasPermissions = [self loggedUserHasPermissions:readPermissions];
    
    if (hasPermissions) {
        // Non bisogna chiedere i permessi
        DBSocialKitResponse *response = [self currentLoginSessionSuccessfulResponse];
        
        if (completionBlock) {
            completionBlock(response);
        }
    }
    else {
        // Richiedo i nuovi permessi
        UIViewController *presentedViewController = [DBApplication presentedViewController];
        
        // Aggiorno la grafica automaticamente
        UIApplication *application = [UIApplication sharedApplication];
        UIStatusBarStyle statusBarStyle = application.statusBarStyle;
        [application setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        
        FBSDKLoginManager *manager = [FBSDKLoginManager new];
        [manager logInWithReadPermissions:readPermissions fromViewController:presentedViewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            // Aggiorno la grafica automaticamente
            [application setStatusBarStyle:statusBarStyle animated:YES];
            
            // Compongo l'oggetto da restituire in risposta
            DBSocialKitResponse *response = [DBSocialKitResponse new];
            response.error = error;
            response.success = (error || result.isCancelled) ? NO : YES;
            response.isCancelled = result.isCancelled;
            response.token = result.token.tokenString;
            
            // Aspetto che si chiude il ViewController di Facebook...
            [DBThread eseguiBlockConRitardo:0.5 block:^{
                if (completionBlock) {
                    completionBlock(response);
                }
            }];
        }];
    }
#endif
}

+ (void)loginWithPublishPermissions:(NSArray<NSString *> *)publishPermissions completionBlock:(DBSocialCompletionBlock)completionBlock
{
#ifdef DBFRAMEWORK_SUBSPEC_FACEBOOKKIT
    // Controllo se i permessi già sono stati ottenuti
    BOOL hasPermissions = [self loggedUserHasPermissions:publishPermissions];
    
    if (hasPermissions) {
        // Non bisogna chiedere i permessi
        DBSocialKitResponse *response = [self currentLoginSessionSuccessfulResponse];
        
        if (completionBlock) {
            completionBlock(response);
        }
    }
    else {
        // Richiedo i nuovi permessi
        UIViewController *presentedViewController = [DBApplication presentedViewController];
        
        // Aggiorno la grafica automaticamente
        UIApplication *application = [UIApplication sharedApplication];
        UIStatusBarStyle statusBarStyle = application.statusBarStyle;
        [application setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        
        FBSDKLoginManager *manager = [FBSDKLoginManager new];
        [manager logInWithPublishPermissions:publishPermissions fromViewController:presentedViewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            // Aggiorno la grafica automaticamente
            [application setStatusBarStyle:statusBarStyle animated:YES];
            
            // Compongo l'oggetto da restituire in risposta
            DBSocialKitResponse *response = [DBSocialKitResponse new];
            response.error = error;
            response.success = (error || result.isCancelled) ? NO : YES;
            response.isCancelled = result.isCancelled;
            response.token = result.token.tokenString;
            
            // Aspetto che si chiude il ViewController di Facebook...
            [DBThread eseguiBlockConRitardo:0.5 block:^{
                if (completionBlock) {
                    completionBlock(response);
                }
            }];
        }];
    }
#endif
}

#ifdef DBFRAMEWORK_SUBSPEC_FACEBOOKKIT
+ (DBSocialKitResponse *)currentLoginSessionSuccessfulResponse
{
    DBSocialKitResponse *response = [DBSocialKitResponse new];
    response.error = nil;
    response.success = YES;
    response.isCancelled = NO;
    
    FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
    response.token = accessToken.tokenString;
    
    return response;
}
#endif

+ (void)logout
{
#ifdef DBFRAMEWORK_SUBSPEC_FACEBOOKKIT
    FBSDKLoginManager *manager = [FBSDKLoginManager new];
    [manager logOut];
#endif
}

+ (nullable NSString *)loggedUserToken
{
#ifdef DBFRAMEWORK_SUBSPEC_FACEBOOKKIT
    FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
    return accessToken.tokenString;
#else
    return nil;
#endif
}

+ (BOOL)loggedUserHasPermissions:(NSArray<NSString *> *)permissions
{
#ifdef DBFRAMEWORK_SUBSPEC_FACEBOOKKIT
    // Controllo se i permessi già sono stati ottenuti
    FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
    
    if (accessToken) {
        NSArray *grantedPermissions = [accessToken.permissions allObjects];
        
        if (grantedPermissions) {
            for (NSString *requestingPermission in permissions) {
                if ([grantedPermissions containsObject:requestingPermission] == NO) {
                    return NO;
                }
            }
            
            return YES;
        }
    }
#endif
    
    return NO;
}

@end


@implementation DBFacebookKit (Deprecated)

+ (void)loginConCompletionBlock:(DBSocialCompletionBlock)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please use +[DBFacebookKit loginWithCompletionBlock:]");
    
    [self loginWithCompletionBlock:completionBlock];
}

+ (void)loginConPermessi:(NSArray<NSString *> *)permessi completionBlock:(DBSocialCompletionBlock)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please use +[DBFacebookKit loginWithCompletionBlock:]");
    [self loginWithReadPermissions:permessi completionBlock:completionBlock];
}

@end
