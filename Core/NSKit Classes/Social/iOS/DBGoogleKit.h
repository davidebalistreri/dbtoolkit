//
//  DBGoogleKit.h
//  File version: 1.1.0
//  Last modified: 04/07/2017
//
//  Created by Davide Balistreri on 18/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBSocialKit.h"

/**
 * Modulo creato per semplificare l'interazione con l'SDK di Google.
 *
 * Please reference and follow the official guide to properly integrate this Google module:
 * https://developers.google.com/identity/sign-in/ios/start-integrating
 */
@interface DBGoogleKit : DBSocialKit

// MARK: - Setup

+ (void)configureWithClientID:(nullable NSString *)clientID;


// MARK: - Methods

+ (void)loginWithCompletionBlock:(nullable DBSocialCompletionBlock)completionBlock;

/// Use this to logout any possible logged user (i.e. when you want to log in with a brand new user)
+ (void)logout;

@end


@interface DBGoogleKit (Deprecated)

+ (void)loginConCompletionBlock:(nullable DBSocialCompletionBlock)completionBlock
    DBFRAMEWORK_DEPRECATED(0.7.2, "please use +[DBGoogleKit loginWithCompletionBlock:]");

@end
