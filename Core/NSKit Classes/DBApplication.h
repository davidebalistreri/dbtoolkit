//
//  DBApplication.h
//  File version: 1.0.0
//  Last modified: 03/27/2016
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@interface DBApplication : NSObject

/**
 * Restituisce l'istanza dell'AppDelegate dell'applicazione.
 */
+ (id<UIApplicationDelegate>)delegate;

/**
 * Restituisce il nome dell'applicazione in uso.
 */
+ (NSString *)nomeApplicazione;

/**
 * Stringa della versione dell'applicazione in uso.
 */
+ (NSString *)version;

/**
 * Stringa della versione di build dell'applicazione in uso.
 */
+ (NSString *)buildVersion;

+ (BOOL)isVersionEarlierThan:(NSString *)compareVersion;
+ (BOOL)isVersionLaterThan:(NSString *)compareVersion;

+ (NSComparisonResult)compareVersion:(NSString *)firstVersion with:(NSString *)secondVersion;

/**
 * Restituisce lo Storyboard principale.
 */
+ (UIStoryboard *)storyboard;

/**
 * Il ViewController principale.
 */
+ (__kindof UIViewController *)rootViewController;

/**
 * Il ViewController visibile più in alto.
 */
+ (__kindof UIViewController *)topmostViewController;

/**
 * Il ViewController attualmente visualizzato sullo schermo.
 */
+ (__kindof UIViewController *)presentedViewController;

/**
 * La Window principale.
 */
+ (__kindof UIWindow *)mainWindow;

/**
 * La Window con livello più alto.
 */
+ (__kindof UIWindow *)topmostWindow;

/**
 * La Window della StatusBar (se presente).
 */
+ (__kindof UIWindow *)statusBarWindow;

+ (void)openApplicationSettings NS_AVAILABLE_IOS(8_0);

+ (void)openURL:(NSURL *)URL;

+ (void)openURLString:(NSString *)URLString;

@end
