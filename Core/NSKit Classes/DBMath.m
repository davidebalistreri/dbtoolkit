//
//  DBMath.m
//  File version: 1.0.0
//  Last modified: 03/27/2016
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBMath.h"

// MARK: - Funzioni matematiche

int parseInt(double valore)
{
    return (int)valore;
}

float parseFloat(double valore)
{
    return (float)valore;
}

double parseDouble(double valore)
{
    return (double)valore;
}

BOOL doubleIsValid(double number)
{
    return isnormal(number);
}

double unsignedDouble(double number)
{
    if (number < 0.0) return 0.0;
    return number;
}

double safeDouble(double numero)
{
    if (isnan(numero)) return 0.0;
    if (isinf(numero)) return MAXFLOAT;
    return (double)numero;
}

double mantieniAllInterno(double numero, double min, double max)
{
    double safeMin = min;
    double safeMax = fmax(max, min); // max can't be < min
    
    if (numero < safeMin) return safeDouble(safeMin);
    if (numero > safeMax) return safeDouble(safeMax);
    
    return safeDouble(numero);
}

double percentuale(double numero, double altroNumero)
{
    double risultato = (numero * 100.0 / altroNumero);
    return safeDouble(risultato);
}

double percentualeChange(double numero, double altroNumero)
{
    double risultato = (((numero - altroNumero) / altroNumero) * 100.0);
    return safeDouble(risultato);
}

double percentualeTraNumeri(double numero, double min, double max, BOOL mantieniAllInterno)
{
    double risultato = (numero - min) / (max - min);
    
    if (mantieniAllInterno && risultato < 0.0) return 0.0;
    if (mantieniAllInterno && risultato > 1.0) return 1.0;
    
    return safeDouble(risultato);
}

double floorConDecimali(double valore, int decimali)
{
    double moltiplicatore = pow(10.0, decimali);
    
    double risultato = valore * moltiplicatore;
    risultato = floor(risultato) / moltiplicatore;
    
    return safeDouble(risultato);
}


// MARK: - Oggetti tridimensionali

DBMath3DVector DBMath3DVectorMake(CGFloat x, CGFloat y, CGFloat z)
{
    DBMath3DVector vector;
    vector.x = x;
    vector.y = y;
    vector.z = z;
    return vector;
}

const DBMath3DVector DBMath3DVectorZero = {0.0, 0.0, 0.0};


// MARK: - NSNumber

@implementation NSNumber (DBToolkit)

+ (NSNumber *)safeNumber:(NSNumber *)numero
{
    return [NSNumber numberWithDouble:safeDouble(numero.doubleValue)];
}

+ (NSNumber *)mantieniNumero:(NSNumber *)numero traMin:(NSNumber *)min max:(NSNumber *)max
{
    NSNumber *safeNumero = [self safeNumber:numero];
    if (safeNumero.doubleValue > max.doubleValue) return [max copy];
    if (safeNumero.doubleValue < min.doubleValue) return [min copy];
    return safeNumero;
}

+ (NSNumber *)mediaNumeri:(NSArray *)numeri
{
    double risultato = 0.0;
    double counter = 0.0;
    
    for (id oggetto in numeri) {
        if ([oggetto isKindOfClass:[NSNumber class]]) {
            double valore = [oggetto doubleValue];
            risultato += valore;
            counter += 1.0;
        }
    }
    
    risultato /= counter;
    return [NSNumber numberWithDouble:safeDouble(risultato)];
}

+ (NSNumber *)percentualeNumero:(NSNumber *)numero altroNumero:(NSNumber *)altroNumero
{
    double risultato = percentuale(numero.doubleValue, altroNumero.doubleValue);
    return [NSNumber numberWithDouble:risultato];
}

+ (NSNumber *)percentualeChangeNumero:(NSNumber *)numero altroNumero:(NSNumber *)altroNumero
{
    double risultato = percentualeChange(numero.doubleValue, altroNumero.doubleValue);
    return [NSNumber numberWithDouble:risultato];
}

+ (NSNumber *)percentualeNumero:(NSNumber *)numero traMin:(NSNumber *)min max:(NSNumber *)max
{
    return [self percentualeNumero:numero traMin:min max:max mantieniAllInterno:YES];
}

+ (NSNumber *)percentualeNumero:(NSNumber *)numero traMin:(NSNumber *)min max:(NSNumber *)max mantieniAllInterno:(BOOL)mantieniAllInterno
{
    double risultato = percentualeTraNumeri(numero.doubleValue, min.doubleValue, max.doubleValue, mantieniAllInterno);
    return [NSNumber numberWithDouble:risultato];
}

+ (double)safeDouble:(double)numero
{
    return safeDouble(numero);
}

@end
