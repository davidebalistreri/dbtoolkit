//
//  DBApplication.m
//  File version: 1.0.0
//  Last modified: 03/27/2016
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBApplication.h"
#import "DBToolkit.h"
#import "DBDevice.h"

@implementation DBApplication

+ (id<UIApplicationDelegate>)delegate
{
    return [UIApplication sharedApplication].delegate;
}

/**
 * Restituisce il nome dell'applicazione in uso.
 */
+ (NSString *)nomeApplicazione
{
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *nome1 = [bundle objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    NSString *nome2 = [bundle objectForInfoDictionaryKey:@"CFBundleName"];
    return (nome1) ? nome1 : nome2;
}

/**
 * Stringa della versione dell'applicazione in uso.
 */
+ (NSString *)version
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

/**
 * Stringa della versione di build dell'applicazione in uso.
 */
+ (NSString *)buildVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
}

+ (BOOL)isVersionEarlierThan:(NSString *)compareVersion
{
    NSString *currentVersion  = [self version];
    NSComparisonResult result = [self compareVersion:currentVersion with:compareVersion];
    
    if (result == NSOrderedAscending) {
        return YES;
    }
    
    return NO;
}

+ (BOOL)isVersionLaterThan:(NSString *)compareVersion
{
    NSString *currentVersion  = [self version];
    NSComparisonResult result = [self compareVersion:currentVersion with:compareVersion];
    
    if (result == NSOrderedDescending) {
        return YES;
    }
    
    return NO;
}

+ (NSComparisonResult)compareVersion:(NSString *)firstVersion with:(NSString *)secondVersion
{
    NSArray *firstComponents  = [firstVersion componentsSeparatedByString:@"."];
    NSArray *secondComponents = [secondVersion componentsSeparatedByString:@"."];
    NSInteger numberOfComponents = MAX(firstComponents.count, secondComponents.count);
    
    for (NSInteger index = 0; index < numberOfComponents; index++) {
        NSInteger firstSegment  = (index < firstComponents.count)  ? [[firstComponents objectAtIndex:index]  integerValue] : 0;
        NSInteger secondSegment = (index < secondComponents.count) ? [[secondComponents objectAtIndex:index] integerValue] : 0;
        
        if (firstSegment < secondSegment) {
            return NSOrderedAscending;
        }
        
        if (firstSegment > secondSegment) {
            return NSOrderedDescending;
        }
    }
    
    return NSOrderedSame;
}

+ (UIStoryboard *)storyboard
{
    return [[self rootViewController] storyboard];
}

+ (__kindof UIViewController *)rootViewController
{
    UIWindow *window = [self mainWindow];
    return window.rootViewController;
}

+ (__kindof UIViewController *)topmostViewController
{
    UIWindow *window = [self topmostWindow];
    if (window.rootViewController) {
        return window.rootViewController;
    }
    
    return [self rootViewController];
}

+ (__kindof UIViewController *)presentedViewController
{
#if DBFRAMEWORK_TARGET_IOS
    UIViewController *presentedViewController = [self topmostViewController];
    
    // Cycle through all possible presentedViewControllers
    while (presentedViewController.presentedViewController) {
        presentedViewController = presentedViewController.presentedViewController;
    }
    
    // UINavigationController
    if ([presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (id)presentedViewController;
        return [navigationController.viewControllers lastObject];
    }
    
    // UITabBarController
    if ([presentedViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarController = (id)presentedViewController;
        return tabBarController.selectedViewController;
    }
    
    return presentedViewController;
#else
    // Da fare
    return nil;
#endif
}

+ (__kindof UIWindow *)mainWindow
{
#if DBFRAMEWORK_TARGET_IOS
    return [[self delegate] window];
#else
    return [NSApplication sharedApplication].windows.firstObject;
#endif
}

+ (__kindof UIWindow *)topmostWindow
{
    // Prendo tutte le Windows dell'applicazione e le ordino per livello di visualizzazione
    NSArray *windows = [[UIApplication sharedApplication] windows];
    NSArray *sortedWindows = [windows sortedArrayUsingComparator:^NSComparisonResult(UIWindow *window1, UIWindow *window2) {
        return window1.windowLevel - window2.windowLevel;
    }];
    
    UIWindow *mainWindow = [self mainWindow];
    
    // Controllo ogni Window se è visibile sullo schermo
    for (UIWindow *window in [sortedWindows reverseObjectEnumerator]) {
        if ([window isMemberOfClass:[UIWindow class]] && window.alpha > 0.0 && !window.hidden) {
            if (window.windowLevel == mainWindow.windowLevel) {
                // Do la preferenza alla finestra principale
                return mainWindow;
            }
            else {
                return window;
            }
        }
    }
    
    // Nessuna finestra visibile trovata
    return mainWindow;
}

+ (UIWindow *)statusBarWindow
{
#if DBFRAMEWORK_TARGET_IOS
    return [[UIApplication sharedApplication] valueForKey:@"statusBarWindow"];
#else
    return nil;
#endif
}

+ (void)openApplicationSettings
{
#if DBFRAMEWORK_TARGET_IOS
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
#endif
}

+ (void)openURL:(NSURL *)URL
{
    UIApplication *application = [UIApplication sharedApplication];
    
    if (@available(iOS 10.0, *)) {
        NSDictionary *options = [NSDictionary dictionary];
        [application openURL:URL options:options completionHandler:nil];
    }
    else {
        // Fallback on earlier versions
        [application openURL:URL];
    }
}

+ (void)openURLString:(NSString *)URLString
{
    NSURL *URL = [NSURL httpURLWithString:URLString];
    [self openURL:URL];
}

@end
