//
//  DBBluetooth.h
//  File version: 1.0.0
//  Last modified: 09/12/2016
//
//  Created by Davide Balistreri on 04/23/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBObject.h"

#import "DBBluetoothPeripheral.h"

// Beacons
#import "DBBluetoothEddystone.h"
#import "DBBluetoothiBeacon.h"

#import <CoreBluetooth/CoreBluetooth.h>

/// Notifica inviata quando cambia lo stato del bluetooth.
extern NSString * __nonnull DBBluetoothDidChangeState;


/// Notifica che viene inviata ogniqualvolta una periferica generica viene trovata.
extern NSString * __nonnull DBBluetoothDidFindPeripheralNotification;

/// Notifica che viene inviata ogniqualvolta un Beacon Google Eddystone viene trovato.
extern NSString * __nonnull DBBluetoothDidFindEddystoneNotification;

/// Notifica che viene inviata ogniqualvolta un Beacon Apple iBeacon viene trovato.
extern NSString * __nonnull DBBluetoothDidFindiBeaconNotification;


extern NSString * __nonnull DBBluetoothDidConnectPeripheralNotification;



@interface DBBluetooth : DBObject

// MARK: - Setup

+ (void)setDebugMode:(BOOL)debugMode;

/**
 * Restituisce un Boolean che indica se il Bluetooth del dispositivo è attivo.
 */
+ (BOOL)isBluetoothPoweredOn;


// MARK: - Search

/**
 * Avvia la scansione di tutte le periferiche circostanti.
 *
 * Appena una periferica viene riconosciuta, viene inviata una notifica \c DBBluetoothDidFindPeripheralNotification,
 * contenente la periferica \c DBBluetoothPeripheral come oggetto della notifica.
 */
+ (void)scanForPeripheralsNearby;

/**
 * Avvia la scansione delle periferiche circostanti, che possono essere filtrate in base ai servizi o al nome specificati.
 *
 * Appena una periferica viene riconosciuta, viene inviata una notifica \c DBBluetoothDidFindPeripheralNotification,
 * contenente la periferica \c DBBluetoothPeripheral come oggetto della notifica.
 *
 * @param similarName  L'eventuale stringa che deve essere presente nel nome della periferica.
 * @param services     L'eventuale lista dei servizi che la periferica deve rispettare.
 */
+ (void)scanForPeripheralsWithSimilarName:(nullable NSString *)similarName withServices:(nullable NSArray<CBUUID *> *)services;

/**
 * Avvia la scansione delle periferiche circostanti, che possono essere filtrate in base ai servizi o al nome specificati.
 *
 * Appena una periferica viene riconosciuta, viene inviata una notifica \c DBBluetoothDidFindPeripheralNotification,
 * contenente la periferica \c DBBluetoothPeripheral come oggetto della notifica.
 *
 * @param similarNames L'eventuale lista di stringhe che devono essere presenti nel nome della periferica,
 *                     se una di queste stringhe è presente nel nome la periferica viene accettata.
 * @param services     L'eventuale lista dei servizi che la periferica deve rispettare.
 */
+ (void)scanForPeripheralsWithSimilarNames:(nullable NSArray<NSString *> *)similarNames withServices:(nullable NSArray<CBUUID *> *)services;

/**
 * Interrompe la scansione delle periferiche circostanti.
 */
+ (void)stopScanningForPeripherals;

/**
 * Lista di tutte le periferiche trovate durante l'ultima scansione.
 */
+ (nullable NSArray<DBBluetoothPeripheral *> *)scannedPeripherals;


// MARK: - Connection

/**
 *
 */
+ (void)connectToPeripheral:(nullable DBBluetoothPeripheral *)peripheral;

/**
 *
 */
+ (void)disconnectFromPeripheral:(nullable DBBluetoothPeripheral *)peripheral;


// MARK: - Writing

+ (BOOL)writeData:(nullable NSData *)value toPeripheral:(nullable DBBluetoothPeripheral *)peripheral;


@end


@interface DBBluetooth (BeaconExtension)

/**
 * Avvia la scansione di tutti i Beacon Google Eddystone circostanti.
 *
 * Appena un Beacon viene riconosciuto, viene inviata una notifica \c DBBluetoothDidFindEddystoneNotification,
 * contenente il Beacon \c DBBluetoothEddystone come oggetto della notifica.
 */
+ (void)scanForEddystonesNearby;

/**
 * Avvia la scansione di tutti i Beacon Apple iBeacon circostanti.
 *
 * Appena un Beacon viene riconosciuto, viene inviata una notifica \c DBBluetoothDidFindiBeaconNotification,
 * contenente il Beacon \c DBBluetoothiBeacon come oggetto della notifica.
 */
+ (void)scanForiBeaconsNearby;

@end
