//
//  DBBluetoothEddystone.m
//  File version: 1.0.0
//  Last modified: 05/10/2016
//
//  Created by Davide Balistreri on 05/10/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBBluetoothEddystone.h"

@implementation DBBluetoothEddystone

+ (instancetype)createWithPeripheral:(DBBluetoothPeripheral *)peripheral
{
    if ([self isPeripheralValid:peripheral] == NO) {
        return nil;
    }
    
    DBBluetoothEddystone *beacon = [DBBluetoothEddystone createWithPeripheral:peripheral.peripheral
                                                            advertisementData:peripheral.advertisementData
                                                                         RSSI:peripheral.RSSI];
    
    
    return beacon;
}

+ (BOOL)isPeripheralValid:(DBBluetoothPeripheral *)peripheral
{
    if ([peripheral isKindOfClass:[DBBluetoothPeripheral class]] == NO) {
        return NO;
    }
    
    // TODO
    
    return YES;
}

@synthesize URL = _URL;
- (void)setURL:(NSURL *)URL
{
    _URL = URL;
}

@synthesize UID = _UID;
- (void)setUID:(NSString *)UID
{
    _UID = UID;
}

- (NSString *)UIDNamespace
{
    return self.UID;
}

- (NSString *)UIDInstance
{
    return self.UID;
}

@end
