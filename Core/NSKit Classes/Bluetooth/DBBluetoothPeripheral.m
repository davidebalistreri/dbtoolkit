//
//  DBBluetoothPeripheral.m
//  File version: 1.0.0
//  Last modified: 05/09/2016
//
//  Created by Davide Balistreri on 04/23/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBBluetoothPeripheral.h"
#import "DBBluetooth.h"

@interface DBBluetoothPeripheral ()

@property (nonatomic) NSInteger numberOfDiscoveredCharacteristics;

@end


@implementation DBBluetoothPeripheral

// MARK: - Setup

- (void)setupObject
{
    // Standard Nordic UART Service UUIDs: you can change them if your device uses different ones
    self.serviceUUID          = [CBUUID UUIDWithString:@"6E400001-B5A3-F393-E0A9-E50E24DCCA9E"];
    self.TXCharacteristicUUID = [CBUUID UUIDWithString:@"6E400002-B5A3-F393-E0A9-E50E24DCCA9E"];
    self.RXCharacteristicUUID = [CBUUID UUIDWithString:@"6E400003-B5A3-F393-E0A9-E50E24DCCA9E"];
}

+ (instancetype)createWithPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI
{
    DBBluetoothPeripheral *bluetoothPeripheral = [DBBluetoothPeripheral new];
    bluetoothPeripheral.peripheral = peripheral;
    bluetoothPeripheral.advertisementData = advertisementData;
    bluetoothPeripheral.RSSI = RSSI;
    return bluetoothPeripheral;
}

@synthesize peripheral = _peripheral;
- (void)setPeripheral:(CBPeripheral *)peripheral
{
    _peripheral = peripheral;
}

@synthesize advertisementData = _advertisementData;
- (void)setAdvertisementData:(NSDictionary<NSString *,id> *)advertisementData
{
    _advertisementData = advertisementData;
}

@synthesize RSSI = _RSSI;
- (void)setRSSI:(NSNumber *)RSSI
{
    _RSSI = RSSI;
}


// MARK: - Public methods

- (void)writeData:(NSData *)data
{
    [DBBluetooth writeData:data toPeripheral:self];
}

- (void)setNotifyValue:(BOOL)notify
{
    [self.peripheral setNotifyValue:notify forCharacteristic:self.RXCharacteristic];
}

- (NSString *)name
{
    return self.peripheral.name;
}

- (CBPeripheralState)state
{
    return self.peripheral.state;
}

- (NSString *)description
{
    return self.peripheral.description;
}

- (NSString *)debugDescription
{
    return self.peripheral.debugDescription;
}

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[DBBluetoothPeripheral class]]) {
        DBBluetoothPeripheral *otherPeripheral = object;
        return [otherPeripheral.peripheral isEqual:self.peripheral];
    }
    
    return [super isEqual:object];
}

//- (CBUUID *)serviceUUID
//{
//    NSArray<CBUUID *> *UUIDs = self.advertisementData[CBAdvertisementDataServiceUUIDsKey];
//    CBUUID *firstUUID = UUIDs.firstObject;
//    return firstUUID;
//}


// MARK: - UART

/// UART service
- (CBService *)service
{
    NSArray<CBService *> *services = self.peripheral.services;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"UUID = %@", self.serviceUUID];
    NSArray<CBService *> *filteredServices = [services filteredArrayUsingPredicate:predicate];
    return filteredServices.firstObject;
}

/// Trasmitting characteristic
- (CBCharacteristic *)TXCharacteristic
{
    CBService *service = [self service];
    NSArray<CBCharacteristic *> *characteristics = service.characteristics;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"UUID = %@", self.TXCharacteristicUUID];
    NSArray<CBCharacteristic *> *filteredCharacteristics = [characteristics filteredArrayUsingPredicate:predicate];
    return filteredCharacteristics.firstObject;
}

/// Receiving characteristic
- (CBCharacteristic *)RXCharacteristic
{
    CBService *service = [self service];
    NSArray<CBCharacteristic *> *characteristics = service.characteristics;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"UUID = %@", self.RXCharacteristicUUID];
    NSArray<CBCharacteristic *> *filteredCharacteristics = [characteristics filteredArrayUsingPredicate:predicate];
    return filteredCharacteristics.firstObject;
}

@end
