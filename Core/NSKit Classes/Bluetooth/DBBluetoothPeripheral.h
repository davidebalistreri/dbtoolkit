//
//  DBBluetoothPeripheral.h
//  File version: 1.0.0
//  Last modified: 05/09/2016
//
//  Created by Davide Balistreri on 04/23/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBObject.h"

#import <CoreBluetooth/CoreBluetooth.h>

@protocol DBBluetoothPeripheralDelegate;


@interface DBBluetoothPeripheral : DBObject

@property (weak, nonatomic) id<DBBluetoothPeripheralDelegate> delegate;


@property (readonly) NSString *name;

@property (readonly) CBPeripheralState state;


// MARK: - Init

+ (instancetype)createWithPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI;

@property (strong, nonatomic, readonly) CBPeripheral *peripheral;
@property (strong, nonatomic, readonly) NSDictionary<NSString *,id> *advertisementData;
@property (strong, nonatomic, readonly) NSNumber *RSSI;


// MARK: - Methods

- (void)setNotifyValue:(BOOL)notify;

- (void)writeData:(NSData *)value;


// MARK: - UART

/// UART service
@property (strong, nonatomic) CBUUID *serviceUUID;
@property (readonly) CBService *service;

/// Trasmitting characteristic
@property (strong, nonatomic) CBUUID *TXCharacteristicUUID;
@property (readonly) CBCharacteristic *TXCharacteristic;

/// Receiving characteristic
@property (strong, nonatomic) CBUUID *RXCharacteristicUUID;
@property (readonly) CBCharacteristic *RXCharacteristic;

@end


@protocol DBBluetoothPeripheralDelegate <NSObject>

@optional

- (void)bluetoothPeripheral:(DBBluetoothPeripheral *)bluetoothPeripheral didReceiveValue:(id)value;

- (void)bluetoothPeripheral:(DBBluetoothPeripheral *)bluetoothPeripheral characteristic:(CBCharacteristic *)characteristic didReceiveValue:(id)value;

@end
