//
//  DBBluetoothiBeacon.m
//  File version: 1.0.0
//  Last modified: 05/10/2016
//
//  Created by Davide Balistreri on 05/10/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBBluetoothiBeacon.h"

@implementation DBBluetoothiBeacon

+ (instancetype)createWithPeripheral:(DBBluetoothPeripheral *)peripheral
{
    if ([self isPeripheralValid:peripheral] == NO) {
        return nil;
    }
    
    DBBluetoothiBeacon *beacon = [DBBluetoothiBeacon createWithPeripheral:peripheral.peripheral
                                                        advertisementData:peripheral.advertisementData
                                                                     RSSI:peripheral.RSSI];
    
    
    return beacon;
}

+ (BOOL)isPeripheralValid:(DBBluetoothPeripheral *)peripheral
{
    if ([peripheral isKindOfClass:[DBBluetoothPeripheral class]] == NO) {
        return NO;
    }
    
    // TODO
    
    return YES;
}

@synthesize UUID = _UUID;
- (void)setUUID:(NSString *)UUID
{
    _UUID = UUID;
}

@synthesize major = _major;
- (void)setMajor:(NSString *)major
{
    _major = major;
}

@synthesize minor = _minor;
- (void)setMinor:(NSString *)minor
{
    _minor = minor;
}

// MARK: - Ricerca



//// MARK: - LocationManager Delegate
//
//- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
//{
//    [manager requestStateForRegion:(CLBeaconRegion *)region];
//}
//
//- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
//{
//    [manager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
//}
//
//- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
//{
//    [manager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
//}
//
//- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
//{
//    [manager stopRangingBeaconsInRegion:(CLBeaconRegion *)region];
//}
//
//- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray<CLBeacon *> *)beacons inRegion:(CLBeaconRegion *)region
//{
//    CLBeacon *beacon = beacons.firstObject;
//    NSLog(@"%@", beacon);
//    
//    if (!beacon) {
//        // Condizioni non valide
//        return;
//    }
//    
//    [self beaconTrovato:beacon.proximityUUID.UUIDString];
//}

@end
