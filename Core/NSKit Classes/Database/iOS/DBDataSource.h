//
//  DBDataSource.h
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 04/20/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBObject.h"

/**
 * Questa classe permette di gestire facilmente un insieme di dati e suddividerli per sezioni.
 */
@interface DBDataSource : DBObject

@property (nonatomic) BOOL ordinaSezioniPerNome;
@property (strong, nonatomic) NSString *ordinaOggettiPerProprieta;

- (void)aggiungiSezioneConNome:(NSString *)nome;

- (void)aggiungiOggetto:(id)oggetto nellaSezioneConNome:(NSString *)nome;
- (void)aggiungiOggetto:(id)oggetto nellaSezioneConIndice:(NSUInteger)indice;
- (void)aggiungiOggetti:(NSArray *)oggetti nellaSezioneConNome:(NSString *)nome;
- (void)aggiungiOggetti:(NSArray *)oggetti nellaSezioneConIndice:(NSUInteger)indice;

- (NSString *)nomeSezioneConIndice:(NSUInteger)indice;
- (NSArray *)oggettiNellaSezioneConNome:(NSString *)nome;
- (NSArray *)oggettiNellaSezioneConIndice:(NSUInteger)indice;
- (id)oggettoConIndice:(NSUInteger)indiceOggetto nellaSezioneConNome:(NSString *)nome;
- (id)oggettoConIndice:(NSUInteger)indiceOggetto nellaSezioneConIndice:(NSUInteger)indice;
- (id)oggettoConIndexPath:(NSIndexPath *)indexPath;

- (void)aggiungiOggetto:(id)oggetto;
- (id)oggettoConIndice:(NSUInteger)indice;

- (NSIndexPath *)indexPathOggetto:(id)oggetto;

- (NSArray *)nomiSezioni;
- (NSArray *)oggetti;

- (NSInteger)numeroSezioni;
- (NSInteger)numeroOggetti;
- (NSInteger)numeroOggettiNellaSezioneConNome:(NSString *)nome;
- (NSInteger)numeroOggettiNellaSezioneConIndice:(NSUInteger)indice;

@end
