//
//  DBManagedObject.m
//  File version: 1.0.1
//  Last modified: 12/15/2015
//
//  Created by Davide Balistreri on 04/21/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBManagedObject.h"

@implementation DBManagedObject

- (NSString *)nomeEntita
{
    return NSStringFromClass([self class]);
}

+ (NSString *)nomeEntita
{
    return NSStringFromClass([self class]);
}

@end
