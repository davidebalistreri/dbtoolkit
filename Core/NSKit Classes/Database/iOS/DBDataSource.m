//
//  DBDataSource.m
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 04/20/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBDataSource.h"
#import "DBFramework.h"

NSString *const kDBNomeSezioneDefault = @"kDBNomeSezioneDefault";

@interface DBSezione : DBObject

@property (strong, nonatomic) NSString *nome;
@property (strong, nonatomic) NSMutableArray *dataSource;

@end

@implementation DBSezione

@end


@interface DBDataSource ()

@property (strong, nonatomic) NSMutableArray *sezioni;


@end


@implementation DBDataSource

@synthesize ordinaSezioniPerNome = _ordinaSezioniPerNome;
- (void)setOrdinaSezioniPerNome:(BOOL)ordinaSezioniPerNome
{
    _ordinaSezioniPerNome = ordinaSezioniPerNome;
    [self riordinaSezioni];
}

@synthesize ordinaOggettiPerProprieta = _ordinaOggettiPerProprieta;
- (void)setOrdinaOggettiPerProprieta:(NSString *)ordinaOggettiPerProprieta
{
    _ordinaOggettiPerProprieta = ordinaOggettiPerProprieta;
    [self riordinaOggetti];
}

- (void)riordinaSezioni
{
    if (self.ordinaSezioniPerNome) {
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"nome" ascending:YES];
        NSArray *sezioniOrdinate = [self.sezioni sortedArrayUsingDescriptors:@[descriptor]];
        self.sezioni = [sezioniOrdinate mutableCopy];
    }
}

- (void)riordinaOggetti
{
    if (![NSString isEmpty:self.ordinaOggettiPerProprieta]) {
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:self.ordinaOggettiPerProprieta ascending:YES];
        BOOL error = NO;
        
        for (DBSezione *sezione in self.sezioni) {
            @try {
                NSArray *dataSourceOrdinato = [sezione.dataSource sortedArrayUsingDescriptors:@[descriptor]];
                sezione.dataSource = [dataSourceOrdinato mutableCopy];
            } @catch (NSException *exception) {
                error = YES;
            }
        }
        
        if (error) {
            DBExtendedLog(@"Si è verificato un problema durante l'ordinamento con la proprietà specificata.");
        }
    }
}

- (void)aggiungiSezioneConNome:(NSString *)nome
{
    if (!self.sezioni) {
        self.sezioni = [NSMutableArray array];
    }
    
    DBSezione *sezione = [DBSezione new];
    sezione.nome = nome;
    sezione.dataSource = [NSMutableArray array];
    
    [self.sezioni addObject:sezione];
    [self riordinaSezioni];
}

- (DBSezione *)sezioneConNome:(NSString *)nome
{
    for (DBSezione *sezione in self.sezioni) {
        if ([sezione.nome isEqualToString:nome]) {
            return sezione;
        }
    }
    
    return nil;
}

- (DBSezione *)sezioneConIndice:(NSUInteger)indice
{
    return [self.sezioni safeObjectAtIndex:indice];
}

- (NSString *)nomeSezioneConIndice:(NSUInteger)indice
{
    DBSezione *sezione = [self.sezioni safeObjectAtIndex:indice];
    
    if (sezione && [sezione.nome isEqualToString:kDBNomeSezioneDefault] == NO) {
        return sezione.nome;
    }
    
    return nil;
}

- (NSArray *)nomiSezioni
{
    NSMutableArray *nomiSezioni = [NSMutableArray array];
    
    for (DBSezione *sezione in self.sezioni) {
        if (!sezione.nome || [sezione.nome isEqualToString:kDBNomeSezioneDefault]) {
            [nomiSezioni addObject:@""];
        }
        else {
            [nomiSezioni addObject:[sezione.nome copy]];
        }
    }
    
    return nomiSezioni;
}

- (NSInteger)numeroSezioni
{
    return [self nomiSezioni].count;
}

- (void)aggiungiOggetto:(id)oggetto nellaSezioneConNome:(NSString *)nome
{
    DBSezione *sezione = [self sezioneConNome:nome];
    
    if (!sezione) {
        [self aggiungiSezioneConNome:nome];
        sezione = [self sezioneConNome:nome];
    }
    
    [sezione.dataSource addObject:oggetto];
    [self riordinaOggetti];
}

- (void)aggiungiOggetto:(id)oggetto nellaSezioneConIndice:(NSUInteger)indice
{
    DBSezione *sezione = [self sezioneConIndice:indice];
    
    if (!sezione) {
        [self aggiungiSezioneConNome:nil];
        sezione = [self sezioneConIndice:indice];
    }
    
    [sezione.dataSource addObject:oggetto];
    [self riordinaOggetti];
}

- (void)aggiungiOggetti:(NSArray *)oggetti nellaSezioneConNome:(NSString *)nome
{
    DBSezione *sezione = [self sezioneConNome:nome];
    
    if (!sezione) {
        [self aggiungiSezioneConNome:nome];
        sezione = [self sezioneConNome:nome];
    }
    
    [sezione.dataSource addObjectsFromArray:oggetti];
    [self riordinaOggetti];
}

- (void)aggiungiOggetti:(NSArray *)oggetti nellaSezioneConIndice:(NSUInteger)indice
{
    DBSezione *sezione = [self sezioneConIndice:indice];
    
    if (!sezione) {
        [self aggiungiSezioneConNome:nil];
        sezione = [self sezioneConIndice:indice];
    }
    
    [sezione.dataSource addObjectsFromArray:oggetti];
    [self riordinaOggetti];
}

- (id)oggettoConIndice:(NSUInteger)indiceOggetto nellaSezioneConNome:(NSString *)nome
{
    DBSezione *sezione = [self sezioneConNome:nome];
    return (!sezione) ? nil : [sezione.dataSource safeObjectAtIndex:indiceOggetto];
}

- (id)oggettoConIndice:(NSUInteger)indiceOggetto nellaSezioneConIndice:(NSUInteger)indice
{
    DBSezione *sezione = [self.sezioni safeObjectAtIndex:indice];
    return (!sezione) ? nil : [sezione.dataSource safeObjectAtIndex:indiceOggetto];
}

- (id)oggettoConIndexPath:(NSIndexPath *)indexPath
{
    DBSezione *sezione = [self sezioneConIndice:indexPath.section];
    return (!sezione) ? nil : [sezione.dataSource safeObjectAtIndex:indexPath.row];
}

- (NSArray *)oggettiNellaSezioneConNome:(NSString *)nome
{
    DBSezione *sezione = [self sezioneConNome:nome];
    return (!sezione) ? [NSArray array] : sezione.dataSource;
}

- (NSArray *)oggettiNellaSezioneConIndice:(NSUInteger)indice
{
    DBSezione *sezione = [self.sezioni safeObjectAtIndex:indice];
    return (!sezione) ? [NSArray array] : sezione.dataSource;
}

- (void)aggiungiOggetto:(id)oggetto
{
    // Check
    if (!oggetto) {
        return;
    }
    
    if ([NSArray isEmpty:self.sezioni]) {
        [self aggiungiSezioneConNome:kDBNomeSezioneDefault];
    }
    
    DBSezione *sezione = [self sezioneConNome:kDBNomeSezioneDefault];
    [sezione.dataSource addObject:oggetto];
    [self riordinaOggetti];
}

- (id)oggettoConIndice:(NSUInteger)indice
{
    DBSezione *sezione = [self sezioneConNome:kDBNomeSezioneDefault];
    return (!sezione) ? nil : [sezione.dataSource safeObjectAtIndex:indice];
}

- (NSIndexPath *)indexPathOggetto:(id)oggetto
{
    NSUInteger indiceSezione = -1;
    NSUInteger indiceOggetto = -1;
    
    for (DBSezione *sezione in self.sezioni) {
        indiceSezione++;
        indiceOggetto = -1;
        
        for (id ricerca in sezione.dataSource) {
            indiceOggetto++;
            
            if ([ricerca isEqual:oggetto])
                return [NSIndexPath indexPathForRow:indiceOggetto inSection:indiceSezione];
        }
    }
    
    return nil;
}

- (NSArray *)oggetti
{
    NSMutableArray *oggetti = [NSMutableArray array];
    
    for (DBSezione *sezione in self.sezioni) {
        for (id oggetto in sezione.dataSource) {
            [oggetti addObject:oggetto];
        }
    }
    
    return oggetti;
}

- (NSInteger)numeroOggetti
{
    return [self oggetti].count;
}

- (NSInteger)numeroOggettiNellaSezioneConNome:(NSString *)nome
{
    DBSezione *sezione = [self sezioneConNome:nome];
    return (!sezione) ? 0 : sezione.dataSource.count;
}

- (NSInteger)numeroOggettiNellaSezioneConIndice:(NSUInteger)indice
{
    DBSezione *sezione = [self sezioneConIndice:indice];
    return (!sezione) ? 0 : sezione.dataSource.count;
}

@end
