//
//  DBUtility.h
//  File version: 1.0.0
//  Last modified: 03/27/2016
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@interface DBUtility : NSObject

+ (BOOL)randomBool;

+ (int)randomIntDa:(NSInteger)da a:(NSInteger)a;

+ (NSString *)randomStringConNumeroCaratteri:(NSInteger)numeroCaratteri;

+ (NSString *)randomStringLeggibileConNumeroCaratteri:(NSInteger)numeroCaratteri;

/**
 * Restituisce il tintColor impostato per il Framework.
 */
+ (UIColor *)tintColor;

/**
 * Se impostato, alcune classi del Framework utilizzeranno questo colore per alcuni elementi.
 * @param tintColor Il colore da impostare.
 */
+ (void)impostaTintColor:(UIColor *)tintColor;

/**
 * Precarica i campi di testo ed elimina il fastidioso lag al primo avvio delle applicazioni su iOS 8.
 */
+ (void)correzioneLagInputFields;

/**
 * Consente al framework di utilizzare le funzioni ancora in fase di testing.
 *
 * Questa impostazione aiuta a garantire il corretto funzionamento del framework sui progetti in produzione,
 * permettendo di abilitare solo su richiesta le funzionalità che potrebbero compromettere la compatibilità
 * e modificare il comportamento del framework.
 *
 * Default: disattivato.
 */
+ (void)setDevelopmentMode:(BOOL)enable;

/**
 * Returns YES if the development features are enabled.
 */
+ (BOOL)isDevelopmentModeEnabled;

@end
