//
//  DBLocalization.m
//  File version: 1.0.3
//  Last modified: 05/09/2018
//
//  Created by Davide Balistreri on 03/30/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBLocalization.h"
#import "DBFramework.h"

NSString *const kNotificaLinguaCorrenteAggiornata = @"kNotificaLinguaCorrenteAggiornata";

@interface DBLocalization ()

@property (strong, nonatomic) NSString *linguaCorrente;
@property (strong, nonatomic) NSString *linguaDefault;

@property (strong, nonatomic) NSDictionary *dictionaryLinguaCorrente;
@property (strong, nonatomic) NSDictionary *dictionaryLinguaDefault;

@end


@implementation DBLocalization

/// Localizza la stringa. Restituisce una stringa vuota se la stringa passata non è valida.
NSString *DBLocalizedString(NSString *stringa)
{
    return [NSString stringaLocalizzata:stringa];
}

/// Localizza la stringa in UPPERCASE. Restituisce una stringa vuota se la stringa passata non è valida.
NSString *DBLocalizedUppercaseString(NSString *stringa)
{
    return [NSString stringaLocalizzataUppercase:stringa];
}


+ (instancetype)sharedInstance
{
    @synchronized(self) {
        DBLocalization *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedLocalization"];
        
        if (!sharedInstance) {
            sharedInstance = [DBLocalization new];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedLocalization"];
        }
        
        return sharedInstance;
    }
}

/**
 * Imposta la lingua corrente dell'App, utilizzata dal metodo stringaLocalizzata.
 *
 * Se questa lingua non è impostata, viene utilizzata la localizzazione corrente del sistema.
 *
 * @param identifierLingua L'identifier della lingua da impostare come corrente.
 */
+ (void)impostaLinguaCorrente:(NSString *)identifierLingua
{
    // Carico le lingue precedentemente impostate negli User Defaults
    [self caricaParametriLingua];
    
    // Aggiorno la lingua corrente
    DBLocalization *sharedInstance = [DBLocalization sharedInstance];
    sharedInstance.linguaCorrente = identifierLingua;
    sharedInstance.dictionaryLinguaCorrente = nil;
    
    // Aggiorno le lingue impostate negli User Defaults
    [self salvaParametriLingua];
    
    // Notifico tutte le schermate che è stata cambiata la lingua corrente
    [DBNotificationCenter postNotificationWithName:kNotificaLinguaCorrenteAggiornata];
}

/**
 * Imposta la lingua di default dell'App, utilizzata dal metodo stringaLocalizzata quando non esiste una localizzazione per la lingua corrente.
 *
 * @param identifierLingua L'identifier della lingua da impostare come default.
 */
+ (void)impostaLinguaDefault:(NSString *)identifierLingua
{
    // Carico le lingue precedentemente impostate negli User Defaults
    [self caricaParametriLingua];
    
    // Aggiorno la lingua di default
    DBLocalization *sharedInstance = [DBLocalization sharedInstance];
    sharedInstance.linguaDefault = identifierLingua;
    sharedInstance.dictionaryLinguaDefault = nil;
    
    // Salvo la lingua negli User Defaults
    [self salvaParametriLingua];
    
    // Notifico tutte le schermate che è stata cambiata la lingua di default
    [DBNotificationCenter postNotificationWithName:kNotificaLinguaCorrenteAggiornata];
}

+ (void)salvaParametriLingua
{
    DBLocalization *sharedInstance = [DBLocalization sharedInstance];
    [[NSUserDefaults standardUserDefaults] setValue:sharedInstance.linguaCorrente forKey:@"DBLocalization.linguaCorrente"];
    [[NSUserDefaults standardUserDefaults] setValue:sharedInstance.linguaDefault  forKey:@"DBLocalization.linguaDefault"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)caricaParametriLingua
{
    DBLocalization *sharedInstance = [DBLocalization sharedInstance];
    sharedInstance.linguaCorrente = [[NSUserDefaults standardUserDefaults] valueForKey:@"DBLocalization.linguaCorrente"];
    sharedInstance.linguaDefault  = [[NSUserDefaults standardUserDefaults] valueForKey:@"DBLocalization.linguaDefault"];
}


/**
 * Restituisce l'identifier della lingua impostata manualmente.
 * Se non è stata impostata manualmente la lingua corrente, viene restituita la lingua di default.
 * Se non è stata impostata manualmente la lingua di default, viene restituita la lingua dell'app.
 */
+ (NSString *)identifierLinguaCorrente
{
    DBLocalization *sharedInstance = [DBLocalization sharedInstance];
    
    // Se ancora non è stata impostata la lingua, provo a caricarla dagli User Defaults
    if ([NSString isEmpty:sharedInstance.linguaCorrente]) {
        [self caricaParametriLingua];
    }
    
    // Se non è mai stata impostata/cambiata la lingua, utilizzo quella di default dell'app
    if ([NSString isEmpty:sharedInstance.linguaCorrente]) {
        return [self identifierLinguaApp];
    }
    
    return sharedInstance.linguaCorrente;
}

/**
 * Restituisce l'identifier della lingua impostata come default.
 */
+ (NSString *)identifierLinguaDefault
{
    DBLocalization *sharedInstance = [DBLocalization sharedInstance];
    
    // Se ancora non è stata impostata la lingua, provo a caricarla dagli User Defaults
    if ([NSString isEmpty:sharedInstance.linguaDefault]) {
        [DBLocalization caricaParametriLingua];
    }
    
    return sharedInstance.linguaDefault;
}

/**
 * Restituisce l'identifier della lingua utilizzata dall'app.
 * NB: Potrebbe essere diversa da quella del sistema operativo.
 */
+ (NSString *)identifierLinguaApp
{
    NSArray *preferredLanguages = [[NSBundle mainBundle] preferredLocalizations];
    NSString *locale = preferredLanguages.firstObject;
    NSDictionary *components = [NSLocale componentsFromLocaleIdentifier:locale];
    NSString *identifier = [components safeValueForKey:NSLocaleLanguageCode];
    
    return identifier;
}

/**
 * Restituisce l'identifier della lingua utilizzata dal sistema operativo.
 * NB: Potrebbe non essere inclusa nelle lingue disponibili e supportate dall'app.
 */
+ (NSString *)identifierLinguaSistema
{
    NSArray *preferredLanguages = [NSLocale preferredLanguages];
    NSString *locale = preferredLanguages.firstObject;
    NSDictionary *components = [NSLocale componentsFromLocaleIdentifier:locale];
    NSString *identifier = [components safeValueForKey:NSLocaleLanguageCode];
    
    return identifier;
}

+ (NSDictionary *)dictionaryLinguaCorrente
{
    DBLocalization *sharedInstance = [DBLocalization sharedInstance];
    
    if (sharedInstance.dictionaryLinguaCorrente) {
        // Utilizzo il file di lingue in memoria
        return sharedInstance.dictionaryLinguaCorrente;
    }
    
    // Carico in memoria il file di lingue richiesto
    sharedInstance.dictionaryLinguaCorrente = [self dictionaryLingua:[DBLocalization identifierLinguaCorrente]];
    return sharedInstance.dictionaryLinguaCorrente;
}

+ (NSDictionary *)dictionaryLinguaDefault
{
    DBLocalization *sharedInstance = [DBLocalization sharedInstance];
    
    if (sharedInstance.dictionaryLinguaDefault) {
        // Utilizzo il file di lingue in memoria
        return sharedInstance.dictionaryLinguaDefault;
    }
    
    // Carico in memoria il file di lingue richiesto
    sharedInstance.dictionaryLinguaDefault = [self dictionaryLingua:[DBLocalization identifierLinguaDefault]];
    return sharedInstance.dictionaryLinguaDefault;
}

#if DBFRAMEWORK_TARGET_IOS
/**
 * Esegue il download di un file Localizable.strings, lo struttura dentro il suo bundle
 * nella cartella Documenti e lo carica nell'app tra le lingue disponibili.
 * @param URL L'URL del file Localizable.strings da scaricare.
 * @param identifierLingua L'identifier della lingua da scaricare.
 */
+ (void)downloadLinguaConURL:(NSURL *)URL conIdentifierLingua:(NSString *)identifierLingua
{
    [self downloadLinguaConURL:URL conIdentifierLingua:identifierLingua sovrascrivi:NO];
}

/**
 * Esegue il download di un file Localizable.strings, lo struttura dentro il suo bundle
 * nella cartella Documenti e lo carica nell'app tra le lingue disponibili.
 * @param URL L'URL del file Localizable.strings da scaricare.
 * @param identifierLingua L'identifier della lingua da scaricare.
 * @param sovrascrivi Indica se bisogna sovrascrivere il file precedentemente scaricato.
 */
+ (void)downloadLinguaConURL:(NSURL *)URL conIdentifierLingua:(NSString *)identifierLingua sovrascrivi:(BOOL)sovrascrivi
{
    // Check
    if (!URL || [NSString isEmpty:identifierLingua]) return;
    
    NSString *nomeCartellaLingua = @"Localizations";
    NSString *nomeLingua = [NSString stringWithFormat:@"Localizable_%@.strings", [NSString safeString:identifierLingua]];
    
    NSURL *URLDocumenti = [NSFileManager URLCartellaDocumenti];
    NSURL *URLCartellaLingua = [URLDocumenti URLByAppendingPathComponent:nomeCartellaLingua isDirectory:YES];
    NSURL *URLLingua = [URLCartellaLingua URLByAppendingPathComponent:nomeLingua];
    
    [DBNetworking GETFileConURLString:[URL absoluteString] downloadPath:[URLLingua path] sovrascrivi:sovrascrivi completionBlock:^(DBNetworkingResponse * __nullable response) {
        if (response.success) {
            // Se è stata aggiornata una lingua attualmente in uso
            if ([identifierLingua isEqualToString:[self identifierLinguaCorrente]]) {
                // Invalido la lingua caricata in memoria
                DBLocalization *sharedInstance = [DBLocalization sharedInstance];
                sharedInstance.dictionaryLinguaCorrente = nil;
                
                // Notifico a tutte le schermate di aggiornare i testi
                [DBNotificationCenter postNotificationWithName:kNotificaLinguaCorrenteAggiornata];
            }
            else if ([identifierLingua isEqualToString:[self identifierLinguaDefault]]) {
                // Invalido la lingua caricata in memoria
                DBLocalization *sharedInstance = [DBLocalization sharedInstance];
                sharedInstance.dictionaryLinguaDefault = nil;
                
                // Notifico a tutte le schermate di aggiornare i testi
                [DBNotificationCenter postNotificationWithName:kNotificaLinguaCorrenteAggiornata];
            }
        }
    }];
}
#endif

+ (NSDictionary *)dictionaryLingua:(NSString *)identifierLingua
{
    // Controllo se è stato scaricato un file di lingue esterno
    NSString *nomeCartellaLingua = @"Localizations";
    NSString *nomeLingua = [NSString stringWithFormat:@"Localizable_%@.strings", [NSString safeString:identifierLingua]];
    
    NSURL *URLDocumenti = [NSFileManager URLCartellaDocumenti];
    NSURL *URLCartellaLingua = [URLDocumenti URLByAppendingPathComponent:nomeCartellaLingua isDirectory:YES];
    NSURL *URLLingua = [URLCartellaLingua URLByAppendingPathComponent:nomeLingua];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[URLLingua path]]) {
        // Prendo il file di lingua scaricato dall'esterno
        NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[URLLingua path]];
        return [NSDictionary safeDictionary:dictionary];
    }
    else {
        // Prendo il file di lingua fornito con l'applicazione
        NSString *pathLingua = [[NSBundle mainBundle] pathForResource:@"Localizable" ofType:@"strings" inDirectory:nil forLocalization:identifierLingua];
        NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:pathLingua];
        return [NSDictionary safeDictionary:dictionary];
    }
}

@end


@implementation NSString (DBLocalization)

/**
 * Utilizza l'istanza della stringa come chiave per cercare la sua localizzazione con la lingua corrente.
 *
 * Se non esiste alcuna localizzazione con la lingua corrente, viene usata la lingua di default.
 *
 * Se non esiste alcuna localizzazione neanche con la lingua di default, viene restituita l'istanza originale.
 *
 * Se nessuna delle due lingue (corrente e default) è impostata, il sistema cercherà automaticamente la versione inglese.
 */
- (NSString *)stringaLocalizzata
{
    NSDictionary *dictionaryLinguaCorrente = [DBLocalization dictionaryLinguaCorrente];
    NSString *stringaLocalizzata = [dictionaryLinguaCorrente safeValueForKey:self];
    
    // Check: se la stringa non esiste nella lingua corrente, provo a prenderla dalla lingua di default
    if (!stringaLocalizzata || [stringaLocalizzata isEqualToString:self]) {
        NSDictionary *dictionaryLinguaDefault = [DBLocalization dictionaryLinguaDefault];
        stringaLocalizzata = [dictionaryLinguaDefault safeValueForKey:self];
    }
    
    // Check: se la stringa non esiste neanche nella lingua di default, provo a prenderla con il metodo di sistema
    if (!stringaLocalizzata || [stringaLocalizzata isEqualToString:self]) {
        stringaLocalizzata = NSLocalizedString(self, nil);
    }
    
    // Se la stringa non esiste neanche nella lingua di default, resterà invariata
    return stringaLocalizzata;
}

/**
 * Restituisce la stringa localizzata del metodo stringaLocalizzata in UPPERCASE.
 */
- (NSString *)stringaLocalizzataUppercase
{
    return [[self stringaLocalizzata] uppercaseString];
}

/**
 * Metodo di classe che localizza la stringa, e restituisce una stringa vuota se la stringa passata non è valida.
 */
+ (NSString *)stringaLocalizzata:(NSString *)stringa
{
    NSString *stringaLocalizzata = [stringa stringaLocalizzata];
    return [NSString safeString:stringaLocalizzata];
}

/**
 * Metodo di classe che localizza la stringa in UPPERCASE, e restituisce una stringa vuota se la stringa passata non è valida.
 */
+ (NSString *)stringaLocalizzataUppercase:(NSString *)stringa
{
    NSString *stringaLocalizzata = [stringa stringaLocalizzataUppercase];
    return [NSString safeString:stringaLocalizzata];
}

@end
