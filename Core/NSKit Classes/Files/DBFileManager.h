//
//  DBFileManager.h
//  File version: 1.0.0
//  Last modified: 06/25/2017
//
//  Created by Davide Balistreri on 06/25/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBObject.h"

NS_ASSUME_NONNULL_BEGIN


/**
 * Class created to manage all files handled by the framework.
 *
 * It currently supports:
 * - Images
 * - Localizables (not yet)
 */

@interface DBFileManager : DBObject

/// Clears downloaded files older than 30 days
+ (void)clearLocalCachedFilesIfNeeded:(nullable void (^)(void))completionBlock;

/// Clears all downloaded files
+ (void)clearAllLocalCachedFiles:(nullable void (^)(void))completionBlock;

@end


/**
 * All images are stored in a specific folder inside the application documents directory.
 * The images are identified with a file name that represents their download URL, so they are all unique.
 *
 * @note
 * You can safely call 'clearLocalCachedFilesIfNeeded' to delete old cached images and free up some space.
 */

@interface DBFileManager (DBImagesExtension)

+ (BOOL)imageWithURLStringExists:(NSString *)urlString;


+ (BOOL)saveImageData:(NSData *)imageData withURLString:(NSString *)urlString;

+ (nullable UIImage *)imageWithURLString:(NSString *)urlString;

+ (BOOL)deleteImageWithURLString:(NSString *)urlString;

@end

NS_ASSUME_NONNULL_END
