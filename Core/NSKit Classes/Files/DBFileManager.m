//
//  DBFileManager.m
//  File version: 1.0.0
//  Last modified: 06/25/2017
//
//  Created by Davide Balistreri on 06/25/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFileManager.h"
#import "DBCentralManager.h"
#import "DBToolkit.h"

@implementation DBFileManager

// MARK: - Private methods

+ (NSString *)documentsLocalPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *urls = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *url = urls.firstObject;
    
#if DBFRAMEWORK_TARGET_OSX
    // Preso dall'estensione NSFileManager in DBToolkit
    // Da ricontrollare perché allora venivano gestiti come NSURL e adesso sono NSString
    url = [URL URLByAppendingPathComponent:[DBApplication nomeApplicazione] isDirectory:YES];
    
    // Se la sottocartella con il nome dell'App non esiste, la creo
    if ([self fileExistsAtPath:url.path isDirectory:nil] == NO) {
        [self createDirectoryAtURL:url withIntermediateDirectories:YES attributes:nil error:nil];
    }
#endif
    
    return url.path;
}

+ (BOOL)fileExistsAtPath:(NSString *)filePath
{
    if (filePath) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        BOOL exists = [fileManager fileExistsAtPath:filePath];
        return exists;
    }
    
    // Not valid
    return NO;
}

+ (BOOL)deleteFileAtPath:(NSString *)filePath
{
    if (filePath) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        __unused NSError *error = nil;
        BOOL success = [fileManager removeItemAtPath:filePath error:&error];
        
        return success;
    }
    
    // Not valid
    return NO;
}

+ (void)clearCachedFilesInPath:(NSString *)path withMaximumTimeInterval:(NSTimeInterval)maximumTimeInterval
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray<NSString *> *objects = [fileManager contentsOfDirectoryAtPath:path error:nil];
    
    for (NSString *object in objects) {
        NSString *objectPath = [path stringByAppendingPathComponent:object];
        
        BOOL isDirectory = NO;
        BOOL exists = [fileManager fileExistsAtPath:objectPath isDirectory:&isDirectory];
        
        if (isDirectory) {
            [self clearCachedFilesInPath:objectPath withMaximumTimeInterval:maximumTimeInterval];
        }
        else if (exists) {
            NSDictionary *attributes = [fileManager attributesOfItemAtPath:objectPath error:nil];
            NSDate *modificationDate = [attributes safeObjectForKey:NSFileModificationDate];
            
            if (modificationDate) {
                NSTimeInterval timeInterval = fabs(modificationDate.timeIntervalSinceNow);
                
                if (timeInterval > maximumTimeInterval) {
                    // NSLog(@"^ Questo file è più vecchio di %d secondi, lo elimino", (int)maximumTimeInterval);
                    [self deleteFileAtPath:objectPath];
                }
                else {
                    // NSLog(@"^ Questo file è recente, non lo elimino");
                }
            }
        }
    }
}


// MARK: - Public methods

/// Clears downloaded files older than 30 days
+ (void)clearLocalCachedFilesIfNeeded:(void (^)(void))completionBlock
{
    [DBThread eseguiBlockSuThreadInBackground:^{
        NSString *basePath = [self documentsLocalPath];
        NSTimeInterval maximumTimeInterval = 30 * (60 * 60 * 24); // 30 days
        
        [self clearCachedFilesInPath:basePath withMaximumTimeInterval:maximumTimeInterval];
        
        [DBThread eseguiBlock:^{
            if (completionBlock) {
                completionBlock();
            }
        }];
    }];
}

/// Clears all downloaded files
+ (void)clearAllLocalCachedFiles:(void (^)(void))completionBlock
{
    [DBThread eseguiBlockSuThreadInBackground:^{
        NSString *basePath = [self documentsLocalPath];
        NSTimeInterval maximumTimeInterval = -1; // All files
        
        [self clearCachedFilesInPath:basePath withMaximumTimeInterval:maximumTimeInterval];
        
        [DBThread eseguiBlock:^{
            if (completionBlock) {
                completionBlock();
            }
        }];
    }];
}

@end


@implementation DBFileManager (DBImagesExtension)

// MARK: - Private methods

+ (NSString *)imagesLocalPath
{
    NSString *imagesPathName = @"Images";
    
    NSString *documentsPath = [self documentsLocalPath];
    NSString *imagesPath = [documentsPath stringByAppendingPathComponent:imagesPathName];
    
    return imagesPath;
}

+ (BOOL)createImagesLocalPathIfNeeded
{
    NSString *path = [self imagesLocalPath];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL exists = [fileManager fileExistsAtPath:path isDirectory:nil];
    
    if (exists == NO) {
        NSError *error = nil;
        BOOL success = [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error || success == NO) {
            DBExtendedLog(@"Non sono riuscito a creare la cartella per il download delle immagini.\n", [error localizedDescription]);
            return NO;
        }
        else {
            NSLog(@"Cartella '%@' creata correttamente!", path);
        }
        
        // Escludo dal backup iCloud la cartella creata
        BOOL excluded = [fileManager escludiPathDaBackupiCloud:path];
        
        if (excluded == NO) {
            DBExtendedLog(@"Non sono riuscito ad escludere dal backup iCloud la cartella per il download delle immagini");
        }
        else {
            NSLog(@"La cartella è stata esclusa dal backup iCloud!");
        }
    }
    
    return YES;
}

+ (NSString *)imageLocalPathWithURLString:(NSString *)urlString
{
    if (!urlString) {
        // Not valid
        return nil;
    }
    
    NSString *outputFileName = [NSString stringaMD5:urlString];
    
    NSString *fileExtension = [urlString pathExtension];
    if (fileExtension.length > 0) {
        outputFileName = [outputFileName stringByAppendingFormat:@".%@", fileExtension];
    }
    
    NSString *imagesLocalPath = [self imagesLocalPath];
    
    NSString *path = [imagesLocalPath stringByAppendingPathComponent:outputFileName];
    return path;
}


// MARK: - Public methods

+ (BOOL)imageWithURLStringExists:(NSString *)urlString
{
    NSString *filePath = [self imageLocalPathWithURLString:urlString];
    
    BOOL exists = [self fileExistsAtPath:filePath];
    return exists;
}

+ (BOOL)saveImageData:(NSData *)imageData withURLString:(NSString *)urlString
{
    if (!imageData || !urlString) {
        // Not valid
        return NO;
    }
    
    NSString *filePath = [self imageLocalPathWithURLString:urlString];
    
    [self createImagesLocalPathIfNeeded];
    
    __unused NSError *error = nil;
    BOOL success = [imageData writeToFile:filePath options:NSDataWritingAtomic error:&error];
    
    return success;
}

+ (nullable UIImage *)imageWithURLString:(NSString *)urlString
{
    if (!urlString) {
        // Not valid
        return nil;
    }
    
    NSString *filePath = [self imageLocalPathWithURLString:urlString];
    
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    UIImage *image = [[UIImage alloc] initWithData:data];
    
    return image;
}

+ (BOOL)deleteImageWithURLString:(NSString *)urlString
{
    NSString *filePath = [self imageLocalPathWithURLString:urlString];
    
    BOOL success = [self deleteFileAtPath:filePath];
    return success;
}

@end
