//
//  DBUtility.m
//  File version: 1.0.0
//  Last modified: 03/27/2016
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBUtility.h"
#import "DBApplication.h"
#import "DBStringExtensions.h"

@interface DBUtility ()

@property (strong, nonatomic) UIColor *tintColor;

@end


@implementation DBUtility

+ (instancetype)sharedInstance
{
    @synchronized(self) {
        DBUtility *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedUtility"];
        
        if (!sharedInstance) {
            sharedInstance = [DBUtility new];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedUtility"];
        }
        
        return sharedInstance;
    }
}

+ (BOOL)randomBool
{
    int random = arc4random_uniform(99);
    return (random % 2) ? YES : NO;
}

+ (int)randomIntDa:(NSInteger)da a:(NSInteger)a
{
    u_int32_t random = arc4random_uniform(1 + parseInt(a - da));
    return parseInt(random + da);
}

+ (NSString *)randomStringConNumeroCaratteri:(NSInteger)numeroCaratteri
{
    NSMutableString *testo = [NSMutableString string];
    
    NSString *caratteri = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSInteger maxCount = caratteri.length - 1;
    
    for (NSUInteger counter = 0; counter < numeroCaratteri; counter++) {
        [testo appendFormat:@"%c", [caratteri characterAtIndex:[DBUtility randomIntDa:0 a:maxCount]]];
    }
    
    return testo;
}

+ (NSString *)randomStringLeggibileConNumeroCaratteri:(NSInteger)numeroCaratteri
{
    NSString *testo = @"";
    
    switch ([DBUtility randomIntDa:0 a:2]) {
        case 0:
            testo = @"Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che contenevano passaggi del Lorem Ipsum, e più recentemente da software di impaginazione come Aldus PageMaker, che includeva versioni del Lorem Ipsum.";
            break;
        case 1:
            testo = @"Al contrario di quanto si pensi, Lorem Ipsum non è semplicemente una sequenza casuale di caratteri. Risale ad un classico della letteratura latina del 45 AC, cosa che lo rende vecchio di 2000 anni. Richard McClintock, professore di latino al Hampden-Sydney College in Virginia, ha ricercato una delle più oscure parole latine, consectetur, da un passaggio del Lorem Ipsum e ha scoperto tra i vari testi in cui è citata, la fonte da cui è tratto il testo, le sezioni 1.10.32 and 1.10.33 del \"de Finibus Bonorum et Malorum\" di Cicerone. Questo testo è un trattato su teorie di etica, molto popolare nel Rinascimento. La prima riga del Lorem Ipsum, \"Lorem ipsum dolor sit amet...\", è tratta da un passaggio della sezione 1.10.32.";
            break;
        case 2:
            testo = @"È universalmente riconosciuto che un lettore che osserva il layout di una pagina viene distratto dal contenuto testuale se questo è leggibile. Lo scopo dell’utilizzo del Lorem Ipsum è che offre una normale distribuzione delle lettere (al contrario di quanto avviene se si utilizzano brevi frasi ripetute, ad esempio “testo qui”), apparendo come un normale blocco di testo leggibile. Molti software di impaginazione e di web design utilizzano Lorem Ipsum come testo modello. Molte versioni del testo sono state prodotte negli anni, a volte casualmente, a volte di proposito (ad esempio inserendo passaggi ironici).";
            break;
    }
    
    NSString *stringa = testo;
    
    while (stringa.length < numeroCaratteri) {
        stringa = [stringa stringByAppendingFormat:@" %@", testo];
    }
    
    return [stringa safeSubstringToIndex:numeroCaratteri];
}

/**
 * Restituisce il tintColor impostato per il Framework.
 */
+ (UIColor *)tintColor
{
    DBUtility *sharedInstance = [DBUtility sharedInstance];
    return sharedInstance.tintColor;
}

/**
 * Se impostato, alcune classi del Framework utilizzeranno questo colore per alcuni elementi.
 * @param tintColor Il colore da impostare.
 */
+ (void)impostaTintColor:(UIColor *)tintColor
{
    DBUtility *sharedInstance = [DBUtility sharedInstance];
    sharedInstance.tintColor = tintColor;
}

+ (void)correzioneLagInputFields
{
    UIWindow *mainWindow = [DBApplication mainWindow];
    
    // Preloads keyboard so there's no lag on initial keyboard appearance.
    UITextField *lagFreeField = [UITextField new];
    [mainWindow addSubview:lagFreeField];
    [lagFreeField becomeFirstResponder];
    [lagFreeField resignFirstResponder];
    [lagFreeField removeFromSuperview];
    
    UITextView *lagFreeView = [UITextView new];
    [mainWindow addSubview:lagFreeView];
    [lagFreeView becomeFirstResponder];
    [lagFreeView resignFirstResponder];
    [lagFreeView removeFromSuperview];
}

+ (void)setDevelopmentMode:(BOOL)enable
{
    NSNumber *number = [NSNumber numberWithBool:enable];
    [DBCentralManager setRisorsa:number conIdentifier:@"DBUtilityDevelopmentMode"];
}

+ (BOOL)isDevelopmentModeEnabled
{
    NSNumber *number = [DBCentralManager getRisorsa:@"DBUtilityDevelopmentMode"];
    
    if (number) {
        return number.boolValue;
    }
    
    return NO;
}

@end
