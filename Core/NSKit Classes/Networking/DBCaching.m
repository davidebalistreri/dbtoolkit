//
//  DBCaching.m
//  File version: 1.0.0
//  Last modified: 04/23/2016
//
//  Created by Davide Balistreri on 04/23/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBCaching.h"

#import "DBNetworkingRequest.h"
#import "DBNetworkingResponse.h"

@implementation DBCaching

+ (NSMutableDictionary *)sharedInstances
{
    @synchronized(self) {
        NSMutableDictionary *sharedInstances = [[NSUserDefaults standardUserDefaults] objectForKey:@"DBSharedCaching"];
        
        if (!sharedInstances) {
            sharedInstances = [NSMutableDictionary dictionary];
            [[NSUserDefaults standardUserDefaults] setObject:sharedInstances forKey:@"DBSharedCaching"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        return [sharedInstances mutableCopy];
    }
}

+ (void)cacheResponse:(DBNetworkingResponse *)response ofRequest:(DBNetworkingRequest *)request
{
    if (response.success == NO) {
        // Non è supportato il salvataggio delle risposte errate
        return;
    }
    
    NSString *key = [request toSerializedString];
    NSString *value = [response toSerializedString];
    
    NSMutableDictionary *cache = [DBCaching sharedInstances];
    [cache setValue:value forKey:key];
    
    [[NSUserDefaults standardUserDefaults] setObject:cache forKey:@"DBSharedCaching"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (DBNetworkingResponse *)cachedResponseOfRequest:(DBNetworkingRequest *)request
{
    NSMutableDictionary *cache = [DBCaching sharedInstances];
    NSString *key = [request toSerializedString];
    NSString *value = [cache valueForKey:key];
    
    if (value) {
        return [DBNetworkingResponse fromSerializedString:value];
    }
    
    return nil;
}

+ (void)clearLocalData
{
    
}

@end
