//
//  DBNetworkingRequest.m
//  File version: 1.0.1
//  Last modified: 09/10/2016
//
//  Created by Davide Balistreri on 03/15/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBNetworkingRequest.h"
#import "DBNetworking.h"

#import "DBObjectExtensions.h"
#import "DBStringExtensions.h"

#import "AFHTTPSessionManager.h"

@interface DBNetworkingRequest ()

// Oggetto richiesta
@property (strong, nonatomic) AFHTTPSessionManager *manager;
@property (strong, nonatomic) NSURLRequest *URLRequest;
@property (strong, nonatomic) NSURLSessionDataTask *dataTask;

@property (nonatomic) NSTimeInterval timestamp;
@property (nonatomic) BOOL modalitaDebug;

@property (copy) DBNetworkingCompletionBlock completionBlock;
@property (copy) DBNetworkingMultipartBlock  multipartBlock;

// Risposta del server
@property (strong, nonatomic) DBNetworkingResponse *response;

@end


@implementation DBNetworkingRequest

/// Override per confrontare due oggetti DBNetworkingRequest
- (BOOL)isEqual:(id)object
{
    if (![object isKindOfClass:[DBNetworkingRequest class]]) {
        return NO;
    }
    
    DBNetworkingRequest *richiesta = (DBNetworkingRequest *)object;
    
    if (![richiesta.method      isEqual:self.method])      return NO;
    if (![richiesta.contentType isEqual:self.contentType]) return NO;
    if (![richiesta.URLString   isEqual:self.URLString])   return NO;
    if (![richiesta.parameters  isEqual:self.parameters])  return NO;
    if (![richiesta.header      isEqual:self.header])      return NO;
    
    return YES;
}

@end


@implementation DBNetworkingRequest (Caching)

- (NSString *)toSerializedString
{
    NSMutableString *string = [NSMutableString string];
    [string appendFormat:@"%@|", [NSString safeString:self.contentType]];
    [string appendFormat:@"%@|", [NSString safeString:self.method]];
    [string appendFormat:@"%@|", [NSString safeString:self.URLString]];
    
    if ([NSArray isNotEmpty:self.parameters]) {
        NSData *dataParametri = [NSJSONSerialization dataWithJSONObject:self.parameters options:0 error:nil];
        NSString *stringaParametri = [[NSString alloc] initWithData:dataParametri encoding:NSUTF8StringEncoding];
        [string appendFormat:@"%@|", stringaParametri];
    }
    
    if ([NSArray isNotEmpty:self.header]) {
        NSData *dataHeader = [NSJSONSerialization dataWithJSONObject:self.header options:0 error:nil];
        NSString *stringaHeader = [[NSString alloc] initWithData:dataHeader encoding:NSUTF8StringEncoding];
        [string appendFormat:@"%@|", stringaHeader];
    }
    
    return [NSString stringWithString:string];
}

@end
