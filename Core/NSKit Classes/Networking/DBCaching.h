//
//  DBCaching.h
//  File version: 1.0.0
//  Last modified: 04/23/2016
//
//  Created by Davide Balistreri on 04/23/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@class DBNetworkingRequest, DBNetworkingResponse;

/**
 * Modulo che gestisce le chiamate DBNetworking e lo storage della cache DBDatabase.
 */
@interface DBCaching : NSObject

/// Salva nella cache la risposta alla richiesta specificata.
+ (void)cacheResponse:(DBNetworkingResponse *)response ofRequest:(DBNetworkingRequest *)request;

/// Restituisce l'eventuale risposta salvata per la richiesta specificata.
+ (DBNetworkingResponse *)cachedResponseOfRequest:(DBNetworkingRequest *)request;

/// Elimina la cache accumulata in locale.
+ (void)clearLocalData;

@end
