//
//  DBNetworkingResponse.h
//  File version: 1.0.0
//  Last modified: 04/23/2016
//
//  Created by Davide Balistreri on 04/01/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBObject.h"

/**
 * La risposta server alle richieste DBNetworking, restituita nei completionBlock.
 */
@interface DBNetworkingResponse : DBObject

/// L'oggetto di risposta restituito dal server.
@property (strong, nonatomic, nullable) id responseObject;

/// Il codice di risposta restituito dal server.
@property (nonatomic) NSInteger statusCode;

/// Boolean che indica se la richiesta è andata a buon fine.
@property (nonatomic) BOOL success;

/// L'eventuale errore restituito dal server o dal framework.
@property (strong, nonatomic, nullable) NSError *error;

@end


@interface DBNetworkingResponse (Caching)

- (nullable NSString *)toSerializedString;
+ (nullable DBNetworkingResponse *)fromSerializedString:(NSString * __nullable)string;

@end
