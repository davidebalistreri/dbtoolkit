//
//  DBNetworking.m
//  File version: 1.3.3
//  Last modified: 01/05/2018
//
//  Created by Davide Balistreri on 03/15/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBNetworking.h"
#import "DBFramework.h"
#import "DBCaching.h"

#import "AFNetworking.h"

// MARK: - DBNetworkingRequest private

@interface DBNetworkingRequest ()

@property (strong, nonatomic) AFHTTPSessionManager *manager;
@property (strong, nonatomic) NSURLRequest *URLRequest;
@property (strong, nonatomic) NSURLSessionDataTask *dataTask;

@property (nonatomic) NSTimeInterval timestamp;
@property (nonatomic) BOOL modalitaDebug;

@property (copy) DBNetworkingCompletionBlock completionBlock;
@property (copy) DBNetworkingMultipartBlock  multipartBlock;

// Risposta del server
@property (strong, nonatomic) DBNetworkingResponse *response;

@end


// MARK: - DBNetworkingResponse private

@interface DBNetworkingResponse ()

/// Risposta generica quando la richiesta web è andata a buon fine.
+ (DBNetworkingResponse *)successResponse;

/// Risposta generica quando la richiesta web fallisce.
+ (DBNetworkingResponse *)failureResponse;

/// Risposta generica quando la richiesta web fallisce.
+ (DBNetworkingResponse *)failureResponseConStatusCode:(NSInteger)statusCode conMessaggioErrore:(NSString *)messaggioErrore;

@end


// MARK: - DBNetworking Class

@interface DBNetworking ()

@property (nonatomic) BOOL debugMode;
@property (nonatomic) BOOL JSONRequestSerializer;
@property (nonatomic) BOOL cancellaRichiestePrecedenti;

@property (strong, nonatomic) NSDictionary *globalHeader;

@property (strong, nonatomic) NSMutableArray *pendingRequests;

@end


@implementation DBNetworking

+ (void)load
{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

+ (DBNetworking *)sharedInstance
{
    @synchronized(self) {
        DBNetworking *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedNetworking"];
        
        if (!sharedInstance) {
            sharedInstance = [DBNetworking new];
            sharedInstance.pendingRequests = [NSMutableArray array];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedNetworking"];
        }
        
        return sharedInstance;
    }
}

+ (void)setDebugMode:(BOOL)enabled
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    sharedInstance.debugMode = enabled;
}

/**
 * Fix temporaneo per impostare globalmente il formato JSON sul serializzatore delle richieste web.
 * La prossima versione di DBNetworking sarà più dinamica e non ci sarà bisogno di utilizzare questo metodo.
 */
+ (void)setJSONRequestSerializer:(BOOL)enabled
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    sharedInstance.JSONRequestSerializer = enabled;
}

/**
 * Fix temporaneo per bypassare il controllo dei certificati SSL invalidi.
 */
+ (void)setResponseSecurityValidation:(BOOL)validate
{
    AFSecurityPolicy *securityPolicy = [[AFHTTPRequestOperationManager manager] securityPolicy];
    
    if (validate) {
        securityPolicy.validatesDomainName = YES;
        securityPolicy.allowInvalidCertificates = NO;
    } else {
        securityPolicy.validatesDomainName = NO;
        securityPolicy.allowInvalidCertificates = YES;
    }
}

// FORBIDDEN METHOD
+ (void)abilitaCancellaRichiestePrecedenti:(BOOL)abilita
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    sharedInstance.cancellaRichiestePrecedenti = abilita;
}

+ (void)setGlobalHeader:(NSDictionary *)header
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    sharedInstance.globalHeader = header;
}

+ (NSDictionary *)globalHeader
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    return sharedInstance.globalHeader;
}

- (BOOL)attendiRichiesteSimili:(DBNetworkingRequest *)richiestaOriginale
{
    // Prendo le richieste ancora in attesa (escludendo quella originale)
    NSMutableArray *pendingRequests = [[self filtraRichiesteSimili:richiestaOriginale] mutableCopy];
    
    if (self.cancellaRichiestePrecedenti && pendingRequests.count > 0) {
        // Annullo tutte le altre richieste
        while (pendingRequests.count > 0) {
            DBNetworkingRequest *request = [pendingRequests firstObject];
            [request.dataTask cancel];
            
            if (request.completionBlock) {
                NSString *messaggioErrore = @"DBNetworking impostaCancellaRichiestePrecedenti attivo.";
                request.completionBlock([DBNetworkingResponse failureResponseConStatusCode:-1 conMessaggioErrore:messaggioErrore]);
            }
            
            [pendingRequests removeObject:request];
            [self.pendingRequests removeObject:request];
        }
    }
    else if (pendingRequests.count > 0) {
        // La richiesta deve restare in attesa di quelle precedenti
        return YES;
    }
    
    // La richiesta può essere effettuata
    return NO;
}

/**
 * Restituisce un'array di richieste simili a quella passata (non includendola nell'array restituito).
 * @param richiestaOriginale La richiesta originale per effettuare il filtro.
 */
- (NSArray *)filtraRichiesteSimili:(DBNetworkingRequest *)richiestaOriginale
{
    // Prendo le altre richieste ancora in attesa con lo stesso URLString
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"URLString == %@", richiestaOriginale.URLString];
    NSMutableArray *richiesteAttive = [[self.pendingRequests filteredArrayUsingPredicate:predicate] mutableCopy];
    
    // Escludo quella originale
    [richiesteAttive removeObject:richiestaOriginale];
    
    // Confronto più nel dettaglio le richieste ancora in attesa
    NSMutableArray *richiesteSimili = [NSMutableArray array];
    for (DBNetworkingRequest *richiesta in richiesteAttive) {
        if ([richiesta isEqual:richiestaOriginale]) {
            [richiesteSimili addObject:richiesta];
        }
    }
    
    return richiesteSimili;
}

- (void)terminaRichiesteSimili:(DBNetworkingRequest *)richiestaOriginale
{
    // Prendo le altre richieste ancora in attesa
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:YES];
    NSArray *richiesteAttive = [self filtraRichiesteSimili:richiestaOriginale];
    richiesteAttive = [richiesteAttive arrayByAddingObject:richiestaOriginale];
    
    // Riordino per data
    richiesteAttive = [richiesteAttive sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    NSMutableArray *mutableRichiesteAttive = [richiesteAttive mutableCopy];
    while (mutableRichiesteAttive.count > 0) {
        DBNetworkingRequest *richiesta = [mutableRichiesteAttive firstObject];
        
        if (richiesta.completionBlock) {
            // richiesta.completionBlock([richiestaOriginale.response copy]);
            richiesta.completionBlock(richiestaOriginale.response);
        }
        
        [mutableRichiesteAttive removeObject:richiesta];
        [self.pendingRequests removeObject:richiesta];
    }
}

+ (BOOL)isNetworkAvailable
{
    AFNetworkReachabilityStatus status = [[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus];
    return (status == AFNetworkReachabilityStatusNotReachable) ? NO : YES;
}

+ (BOOL)isNetworkReachableViaWiFi
{
    AFNetworkReachabilityStatus status = [[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus];
    return (status == AFNetworkReachabilityStatusReachableViaWiFi) ? YES : NO;
}


// MARK: Richieste JSON

+ (void)JSONGETConURLString:(NSString *)URLString parametri:(NSDictionary *)parametri completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    
    DBNetworkingRequest *richiesta = [sharedInstance requestConMetodo:@"GET" contentType:@"JSON" URLString:URLString parametri:parametri multipartBlock:nil completionBlock:completionBlock];
    
    [sharedInstance executeRequest:richiesta];
}

+ (void)JSONPOSTConURLString:(NSString *)URLString parametri:(NSDictionary *)parametri completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    
    DBNetworkingRequest *richiesta = [sharedInstance requestConMetodo:@"POST" contentType:@"JSON" URLString:URLString parametri:parametri multipartBlock:nil completionBlock:completionBlock];
    
    [sharedInstance executeRequest:richiesta];
}

+ (void)JSONDELETEConURLString:(NSString *)URLString parametri:(NSDictionary *)parametri completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    
    DBNetworkingRequest *richiesta = [sharedInstance requestConMetodo:@"DELETE" contentType:@"JSON" URLString:URLString parametri:parametri multipartBlock:nil completionBlock:completionBlock];
    
    [sharedInstance executeRequest:richiesta];
}

+ (void)JSONPUTConURLString:(NSString *)URLString parametri:(NSDictionary *)parametri completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    
    DBNetworkingRequest *richiesta = [sharedInstance requestConMetodo:@"PUT" contentType:@"JSON" URLString:URLString parametri:parametri multipartBlock:nil completionBlock:completionBlock];
    
    [sharedInstance executeRequest:richiesta];
}

+ (void)multipartJSONPOSTConURLString:(NSString *)URLString parametri:(NSDictionary *)parametri multipartBlock:(DBNetworkingMultipartBlock)multipartBlock completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    
    DBNetworkingRequest *richiesta = [sharedInstance requestConMetodo:@"POST" contentType:@"JSON" URLString:URLString parametri:parametri multipartBlock:multipartBlock completionBlock:completionBlock];
    
    [sharedInstance executeRequest:richiesta];
}

+ (void)multipartJSONPUTConURLString:(NSString *)URLString parametri:(NSDictionary *)parametri multipartBlock:(DBNetworkingMultipartBlock)multipartBlock completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    
    DBNetworkingRequest *richiesta = [sharedInstance requestConMetodo:@"PUT" contentType:@"JSON" URLString:URLString parametri:parametri multipartBlock:multipartBlock completionBlock:completionBlock];
    
    [sharedInstance executeRequest:richiesta];
}

// MARK: Altre richieste

+ (void)HTMLGETConURLString:(NSString *)URLString parametri:(NSDictionary *)parametri completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    
    DBNetworkingRequest *richiesta = [sharedInstance requestConMetodo:@"GET" contentType:@"HTML" URLString:URLString parametri:parametri multipartBlock:nil completionBlock:completionBlock];
    
    [sharedInstance executeRequest:richiesta];
}

+ (void)HTMLPOSTConURLString:(NSString *)URLString parametri:(NSDictionary *)parametri completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    
    DBNetworkingRequest *richiesta = [sharedInstance requestConMetodo:@"POST" contentType:@"HTML" URLString:URLString parametri:parametri multipartBlock:nil completionBlock:completionBlock];
    
    [sharedInstance executeRequest:richiesta];
}

// MARK: Immagini

+ (void)GETImmagineConURLString:(NSString *)URLString completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    [self GETImmagineConURLString:URLString sovrascrivi:NO completionBlock:completionBlock];
}

+ (void)GETImmagineConURLString:(NSString *)URLString sovrascrivi:(BOOL)sovrascrivi completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    // ***** Metodo da controllare e riadattare alla nuova gestione delle chiamate al WebService! *****
    
    // Check validità richiesta
    if ([NSString isEmpty:URLString]) {
        if (completionBlock) {
            NSString *messaggioErrore = @"DBNetworking richiesta GETImmagine non valida: URLString non specificato.";
            completionBlock([DBNetworkingResponse failureResponseConStatusCode:400 conMessaggioErrore:messaggioErrore]);
        }
        
        return;
    }
    
    BOOL exists = [DBFileManager imageWithURLStringExists:URLString];
    
    if (exists && sovrascrivi == NO) {
        // Immagine già scaricata
        if (completionBlock) {
            UIImage *immagine = [DBFileManager imageWithURLString:URLString];
            
            DBNetworkingResponse *response = [DBNetworkingResponse successResponse];
            response.responseObject = immagine;
            completionBlock(response);
        }
        
        return;
    }
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    request.URL = [NSURL URLWithString:URLString];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    request.timeoutInterval = 10.0;
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFImageResponseSerializer serializer];
    
    // In alcune circostanze le foto vengono inviate con questo formato
    operation.responseSerializer.acceptableContentTypes = [operation.responseSerializer.acceptableContentTypes setByAddingObject:@"multipart/form-data"];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        if ([responseObject isNotKindOfClass:[UIImage class]]) {
            // Si è verificato un errore durante la richiesta
            NSLog(@"GETImmagine Failure (%d)\n\n", (int)operation.response.statusCode);
            
            if (completionBlock) {
                DBNetworkingResponse *response = [DBNetworkingResponse failureResponse];
                response.responseObject = operation.responseObject;
                response.statusCode = operation.response.statusCode;
                response.error = operation.error;
                
                completionBlock(response);
            }
            
            return;
        }
        
        // Tutto ok
        NSLog(@"GETImmagine Success (%d)\n\n", (int)operation.response.statusCode);
        
        // Salvo l'immagine nella cartella Immagini
        NSData *imageData = operation.responseData;
        BOOL success = [DBFileManager saveImageData:imageData withURLString:URLString];
        
        if (success == NO) {
            if (completionBlock) {
                DBNetworkingResponse *response = [DBNetworkingResponse failureResponse];
                response.statusCode = operation.response.statusCode;
                response.error = operation.error;
                
                completionBlock(response);
            }
        }
        else {
            // Carico l'immagine appena salvata (il responseObject di AFNetworking non la rappresenta 1:1)
            UIImage *image = [DBFileManager imageWithURLString:URLString];
            
            if (completionBlock) {
                DBNetworkingResponse *response = [DBNetworkingResponse successResponse];
                response.responseObject = image;
                response.statusCode = operation.response.statusCode;
                
                completionBlock(response);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        // Si è verificato un errore durante la richiesta
        NSLog(@"Failure (%d): %@\n\n", (int) operation.response.statusCode, [error localizedDescription]);
        
        if (completionBlock) {
            DBNetworkingResponse *response = [DBNetworkingResponse failureResponse];
            response.responseObject = operation.responseObject;
            response.statusCode = operation.response.statusCode;
            response.error = error;
            
            completionBlock(response);
        }
    }];
    
    [operation start];
}

// MARK: File

+ (void)GETFileConURLString:(NSString *)URLString downloadPath:(NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    [self GETFileConURLString:URLString downloadPath:downloadPath sovrascrivi:sovrascrivi progressBlock:nil completionBlock:completionBlock];
}

+ (void)GETFileConURLString:(NSString *)URLString downloadPath:(NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi progressHUD:(BOOL)progressHUDAbilitato messaggioProgressHUD:(NSString *)messaggioProgressHUD completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBProgressHUD *progressHUD = nil;
    
    if (progressHUDAbilitato) {
        progressHUD = [DBProgressHUD progressHUDConMessaggio:[NSString safeString:messaggioProgressHUD]];
        progressHUD.mode = MBProgressHUDModeDeterminateHorizontalBar;
    }
    
    [self GETFileConURLString:URLString downloadPath:downloadPath sovrascrivi:sovrascrivi progressBlock:^(CGFloat progress) {
        if (progressHUDAbilitato) {
            progressHUD.progress = progress;
        }
        
    } completionBlock:^(DBNetworkingResponse * __nullable response) {
        if (progressHUDAbilitato) {
            [progressHUD rimuoviProgressHUD];
        }
        
        if (completionBlock) {
            completionBlock(response);
        }
    }];
}

+ (void)GETFileConURLString:(NSString *)URLString downloadPath:(NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi progressBlock:(DBNetworkingProgressBlock)progressBlock completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    // Check validità richiesta
    if ([NSString isEmpty:URLString] || [NSString isEmpty:downloadPath]) {
        if (completionBlock) {
            NSString *messaggioErrore = @"DBNetworking richiesta GETFile non valida: URLString non specificato.";
            completionBlock([DBNetworkingResponse failureResponseConStatusCode:400 conMessaggioErrore:messaggioErrore]);
        }
        
        return;
    }
    else if ([NSString isEmpty:downloadPath]) {
        if (completionBlock) {
            NSString *messaggioErrore = @"DBNetworking richiesta GETFile non valida: downloadPath non specificata.";
            completionBlock([DBNetworkingResponse failureResponseConStatusCode:400 conMessaggioErrore:messaggioErrore]);
        }
        
        return;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Controllo se il file è già stato scaricato
    if (sovrascrivi == NO) {
        BOOL giaScaricato = [fileManager fileExistsAtPath:downloadPath];
        
        if (giaScaricato) {
            // Bisogna ritornare il file come responseObject!
            NSLog(@"Il file '%@' è già stato scaricato", [downloadPath lastPathComponent]);
            
            if (completionBlock) {
                completionBlock([DBNetworkingResponse successResponse]);
            }
            
            return;
        }
    }
    
    // Controllo se la cartella di destinazione esiste
    NSString *cartellaDestinazione = [downloadPath stringByDeletingLastPathComponent];
    BOOL cartellaEsistente = NO;
    [fileManager fileExistsAtPath:cartellaDestinazione isDirectory:&cartellaEsistente];
    
    if (!cartellaEsistente) {
        NSError *error = nil;
        BOOL success = [fileManager createDirectoryAtPath:cartellaDestinazione withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error || !success) {
            DBLog(@"Non sono riuscito a creare la cartella per il download del file '", [downloadPath lastPathComponent], @"'.\n", [error localizedDescription]);
        }
        else {
            DBLog(@"Cartella di destinazione per il download del file '", [downloadPath lastPathComponent], @"' creata correttamente!");
        }
        
        // Escludo dal backup iCloud la cartella creata
        [fileManager escludiPathDaBackupiCloud:cartellaDestinazione];
    }
    
    // Scarico il file
    NSURL *URL = [NSURL URLWithString:URLString];
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    request.URL = URL;
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    request.timeoutInterval = 10.0;
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:downloadPath append:NO];
    
    if (progressBlock) {
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            CGFloat progress = (parseFloat(totalBytesRead) / parseFloat(totalBytesExpectedToRead));
            NSLog(@"Download del file '%@': %d%%", [downloadPath lastPathComponent], parseInt(progress * 100));
            
            progressBlock(progress);
        }];
    }
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"File '%@' scaricato correttamente nella cartella: '%@'", [URL lastPathComponent], downloadPath);
        [fileManager escludiURLDaBackupiCloud:[NSURL fileURLWithPath:downloadPath]];
        
        if (completionBlock) {
            DBNetworkingResponse *response = [DBNetworkingResponse successResponse];
            response.responseObject = responseObject;
            completionBlock(response);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Si è verificato un errore durante il download del file '%@': %@", [downloadPath lastPathComponent], [error localizedDescription]);
        [fileManager removeItemAtPath:downloadPath error:nil];
        
        if (completionBlock) {
            DBNetworkingResponse *response = [DBNetworkingResponse failureResponse];
            response.statusCode = operation.response.statusCode;
            response.error = error;
            completionBlock(response);
        }
    }];
    
    NSLog(@"Inizio il download del file '%@'", [downloadPath lastPathComponent]);
    [[NSOperationQueue currentQueue] addOperation:operation];
}

// MARK: - Richiesta web

- (void)executeRequest:(DBNetworkingRequest *)request
{
    // Check validità richiesta
    if ([NSString isEmpty:request.URLString]) {
        if (request.completionBlock) {
            NSString *messaggioErrore = @"DBNetworking richiesta non valida: URLString non specificato.";
            request.completionBlock([DBNetworkingResponse failureResponseConStatusCode:400 conMessaggioErrore:messaggioErrore]);
        }
        
        return;
    }
    
    // Aggiungo la richiesta alla coda
    [self.pendingRequests addObject:request];
    
    // Controllo se devo attendere la risposta delle precedenti richieste
    if ([self attendiRichiesteSimili:request]) {
        return;
    }
    
    NSDate *startDate = [NSDate date];
    
    request.dataTask = [request.manager dataTaskWithRequest:request.URLRequest completionHandler:^(NSURLResponse * _Nonnull response, id _Nullable responseObject, NSError * _Nullable error) {
        
        // Richiesta terminata
        
        // Parsing
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger statusCode = httpResponse.statusCode;
        
        if ([request.contentType isEqualToString:@"HTML"] && [responseObject isKindOfClass:[NSData class]]) {
            NSData *data = responseObject;
            NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            responseObject = string.trimmedString;
        }
        
        // Logging
        if (request.modalitaDebug) {
            // Constructing the output string
            NSMutableString *log = [NSMutableString string];
            
            // Request URL
            [log appendString:(request.multipartBlock) ? @"MULTIPART " : @""];
            [log appendFormat:@"%@ URL: ", request.method];
            [log appendStringLine:request.URLString];
            
            // Status code
            NSString *statusCodeDescription = [NSHTTPURLResponse localizedStringForStatusCode:statusCode];
            [log appendFormat:@"STATUS CODE: %ld ", (long)statusCode];
            [log appendStringLine:statusCodeDescription.capitalizedString];
            
            // Elapsed time
            double elapsedTime = [[NSDate adesso] secondiPassatiDallaData:startDate] * 1000.0;
            [log appendFormatLine:@"TIME: %.f ms", elapsedTime];
            
            // Headers
            if ([NSDictionary isNotEmpty:request.header]) {
                NSString *headersString = [NSString stringWithJSONObject:request.header];
                [log appendStringLine:@"HEADERS:"];
                [log appendStringLine:headersString];
            }
            
            // Parameters
            if ([NSDictionary isNotEmpty:request.parameters]) {
                NSString *parametersString = [NSString stringWithJSONObject:request.parameters];
                [log appendStringLine:@"PARAMETERS:"];
                [log appendStringLine:parametersString];
            }
            
            // Error
            if (error) {
                NSString *errorString = [error localizedDescription];
                [log appendStringLine:@"ERROR:"];
                [log appendStringLine:errorString];
            }
            
            // Response
            NSString *responseString = @"";
            
            if ([responseObject isKindOfClass:[NSString class]]) {
                responseString = responseObject;
            } else {
                responseString = [NSString stringWithJSONObject:responseObject];
            }
            
            if ([NSString isNotEmpty:responseString]) {
                [log appendStringLine:@"RESPONSE:"];
                [log appendStringLine:responseString];
            }
            
            // Printing the log string
            NSLog(@"\n%@\n", log);
        }
        
        if (error == nil) {
            // Success
            DBNetworkingResponse *response = [DBNetworkingResponse successResponse];
            response.responseObject = responseObject;
            response.statusCode = statusCode;
            request.response = response;
            
            [self terminaRichiesteSimili:request];
        }
        else if (request.dataTask.state != NSURLSessionTaskStateCanceling) {
            // Failure
            DBNetworkingResponse *response = [DBNetworkingResponse failureResponse];
            response.responseObject = responseObject;
            response.statusCode = statusCode;
            response.error = error;
            request.response = response;
            
            [self terminaRichiesteSimili:request];
        }
        
        if ([NSArray isEmpty:self.pendingRequests]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        }
    }];
    
    // Avvio la richiesta
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [request.dataTask resume];
}

- (DBNetworkingRequest *)requestConMetodo:(NSString *)metodo
                              contentType:(NSString *)contentType
                                URLString:(NSString *)URLString
                                parametri:(NSDictionary *)parametri
                           multipartBlock:(DBNetworkingMultipartBlock)multipartBlock
                          completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBNetworkingRequest *request = [DBNetworkingRequest new];
    request.method = metodo;
    request.contentType = contentType;
    request.URLString = URLString;
    request.parameters = parametri;
    request.header = [self.globalHeader copy];
    request.modalitaDebug = self.debugMode;
    request.timestamp = [NSDate timeIntervalSinceReferenceDate];
    request.completionBlock = completionBlock;
    request.multipartBlock = multipartBlock;
    
    // Preparo la chiamata
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    if (self.JSONRequestSerializer) {
        // Fix temporaneo
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    
    // Aggiungo l'identifier della lingua correntemente utilizzata dall'app
    // NB: Gli Header globali possono sovrascrivere questa impostazione automatica della lingua
    NSString *acceptLanguage = [DBLocalization identifierLinguaCorrente];
    [manager.requestSerializer setValue:acceptLanguage forHTTPHeaderField:@"Accept-Language"];
    
    // Aggiungo gli Header globali
    NSArray *allKeys   = request.header.allKeys;
    NSArray *allValues = request.header.allValues;
    for (NSInteger counter = 0; counter < allKeys.count; counter++) {
        [manager.requestSerializer setValue:allValues[counter] forHTTPHeaderField:allKeys[counter]];
    }
    
    if ([contentType isEqualToString:@"HTML"]) {
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    else if ([contentType isEqualToString:@"JSON"]) {
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json", @"text/html"]];
    
    NSMutableURLRequest *URLRequest = nil;
    
    //
    // Multipart che invia i parametri in formdata JSON mettendoli dentro il Dictionary 'data'
    //
    //    if (multipartBlock && [DBUtility isDevelopmentModeEnabled]) {
    //        URLRequest = [manager.requestSerializer multipartFormRequestWithMethod:metodo URLString:URLString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
    //
    //            // NSDictionary *jsonHeaders = @{@"Content-Disposition" : @"form-data; name=\"data\"", @"Content-Type" : @"application/json"};
    //
    //            // Body
    //            for (NSInteger counter = 0; counter < parametri.allKeys.count; counter++) {
    //                NSString *name = [parametri.allKeys objectAtIndex:counter];
    //                id object = [parametri.allValues objectAtIndex:counter];
    //
    //                if ([NSJSONSerialization isValidJSONObject:object]) {
    //                    NSError *error = nil;
    //                    NSData *data = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:&error];
    //                    [formData appendPartWithFormData:data name:name];
    //                }
    //            }
    //
    //            // NSData *parametersData = [NSJSONSerialization dataWithJSONObject:parametri options:0 error:nil];
    //            // [formData appendPartWithHeaders:jsonHeaders body:parametersData];
    //
    //            // User multipartBlock
    //            multipartBlock(formData);
    //
    //        } error:nil];
    //    }
    //    else
    
    if (multipartBlock) {
        URLRequest = [manager.requestSerializer multipartFormRequestWithMethod:metodo URLString:URLString parameters:parametri constructingBodyWithBlock:multipartBlock error:nil];
    }
    else {
        URLRequest = [manager.requestSerializer requestWithMethod:metodo URLString:URLString parameters:parametri error:nil];
    }
    
    request.manager = manager;
    request.URLRequest = URLRequest;
    
    return request;
}

@end


// MARK: - Caching

@implementation DBNetworking (Caching)

+ (void)JSONGETConURLString:(NSString *)URLString parametri:(NSDictionary *)parametri cachePolicy:(DBNetworkingCachePolicy)cachePolicy completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    
    __block DBNetworkingRequest *request = nil;
    
    request = [sharedInstance requestConMetodo:@"GET" contentType:@"JSON" URLString:URLString parametri:parametri multipartBlock:nil completionBlock:^(DBNetworkingResponse * __nullable response) {
        BOOL requestFailed = [response.error.domain isEqualToString:NSURLErrorDomain];
        
        if (cachePolicy == DBNetworkingCachePolicyReloadUsingLocalDataIfNeeded && requestFailed) {
            // La richiesta è fallita, restistuisco la cache locale
            DBNetworkingResponse *cachedResponse = [DBCaching cachedResponseOfRequest:request];
            if (cachedResponse && completionBlock) {
                // Cache locale trovata
                completionBlock(cachedResponse);
            }
            else if (completionBlock) {
                // Nessuna cache locale trovata, restituisco la risposta del server
                completionBlock(response);
            }
        }
        else {
            // Aggiorno la cache locale
            [DBCaching cacheResponse:response ofRequest:request];
            
            if (completionBlock) {
                // Restituisco la risposta del server
                completionBlock(response);
            }
        }
    }];
    
    DBNetworkingResponse *cachedResponse = [DBCaching cachedResponseOfRequest:request];
    if (cachePolicy == DBNetworkingCachePolicyReturnLocalDataElseLoad && cachedResponse && completionBlock) {
        // Utilizzo la cache locale
        completionBlock(cachedResponse);
    }
    else {
        // Chiedo i nuovi dati al server
        [sharedInstance executeRequest:request];
    }
}

+ (void)JSONPOSTConURLString:(NSString *)URLString parametri:(NSDictionary *)parametri cachePolicy:(DBNetworkingCachePolicy)cachePolicy completionBlock:(DBNetworkingCompletionBlock)completionBlock
{
    DBNetworking *sharedInstance = [DBNetworking sharedInstance];
    
    DBNetworkingRequest *request = nil;
    
    request = [sharedInstance requestConMetodo:@"POST" contentType:@"JSON" URLString:URLString parametri:parametri multipartBlock:nil completionBlock:^(DBNetworkingResponse * __nullable response) {
        if (cachePolicy == DBNetworkingCachePolicyReloadUsingLocalDataIfNeeded && response.statusCode < 0) {
            // La richiesta è fallita, restistuisco la cache locale
            DBNetworkingResponse *cachedResponse = [DBCaching cachedResponseOfRequest:request];
            if (cachedResponse && completionBlock) {
                // Cache locale trovata
                completionBlock(cachedResponse);
            }
            else if (completionBlock) {
                // Nessuna cache locale trovata, restituisco la risposta del server
                completionBlock(response);
            }
        }
        else {
            // Aggiorno la cache locale
            [DBCaching cacheResponse:response ofRequest:request];
            
            if (completionBlock) {
                // Restituisco la risposta del server
                completionBlock(response);
            }
        }
    }];
    
    DBNetworkingResponse *cachedResponse = [DBCaching cachedResponseOfRequest:request];
    if (cachePolicy == DBNetworkingCachePolicyReturnLocalDataElseLoad && cachedResponse && completionBlock) {
        // Utilizzo la cache locale
        completionBlock(cachedResponse);
    }
    else {
        // Chiedo i nuovi dati al server
        [sharedInstance executeRequest:request];
    }
}

@end
