//
//  DBNetworking.h
//  File version: 1.3.3
//  Last modified: 01/05/2018
//
//  Created by Davide Balistreri on 03/15/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#if DBFRAMEWORK_VERSION_EARLIER_THAN(__DBFRAMEWORK_0_4_0)
    #warning Bisogna adattare il codice DBNetworking alla nuova versione del Framework.
#endif


#import "DBNetworkingRequest.h"
#import "DBNetworkingResponse.h"

// Per il protocollo AFMultipartFormData
#import "AFURLRequestSerialization.h"


/**
 * Modulo per la gestione delle chiamate al server.
 */
@interface DBNetworking : NSObject

// MARK: - CompletionBlock

/**
 * Block da eseguire al termine delle richieste web.
 * @param response L'oggetto DBNetworkingResponse, che raccoglie tutte le informazioni necessarie per analizzare la risposta della chiamata.
 */
typedef void (^DBNetworkingCompletionBlock)(DBNetworkingResponse * __nullable response);

/**
 * Block da eseguire per comporre il form multipart.
 */
typedef void (^DBNetworkingMultipartBlock)(id <AFMultipartFormData> __nullable formData);

/**
 * Block per gestire la percentuale di completamento durante il download di un file.
 * @param progress La la percentuale di completamento del download, da 0.0 a 1.0.
 */
typedef void (^DBNetworkingProgressBlock)(CGFloat progress);


// MARK: - Caching

/**
 * Protocollo di cache da utilizzare per le chiamate DBNetworking.
 */
typedef NS_ENUM(NSUInteger, DBNetworkingCachePolicy) {
    /// Default. Scarica i dati dal server, non utilizza la cache locale.
    DBNetworkingCachePolicyReloadIgnoringLocalData,
    /// Scarica i dati dal server e aggiorna la cache locale se la chiamata va a buon fine, altrimenti utilizza la cache salvata in locale.
    DBNetworkingCachePolicyReloadUsingLocalDataIfNeeded,
    /// Se presente utilizza la cache locale, altrimenti scarica i dati dal server e aggiorna la cache locale.
    DBNetworkingCachePolicyReturnLocalDataElseLoad
};


// MARK: - Setup

/**
 * Attiva il debug delle chiamate nella console.
 */
+ (void)setDebugMode:(BOOL)enabled;


/**
 * Fix temporaneo per impostare globalmente il formato JSON sul serializzatore delle richieste web.
 * La prossima versione di DBNetworking sarà più dinamica e non ci sarà bisogno di utilizzare questo metodo.
 */
+ (void)setJSONRequestSerializer:(BOOL)enabled;

/**
 * Fix temporaneo per bypassare il controllo dei certificati SSL invalidi.
 */
+ (void)setResponseSecurityValidation:(BOOL)validate;


/**
 * Imposta l'Header da inviare al server in ogni richiesta effettuata.
 */
+ (void)setGlobalHeader:(nullable NSDictionary *)header;

/**
 * Restituisce l'Header che viene attualmente inviato al server ad ogni richiesta effettuata.
 */
+ (nullable NSDictionary *)globalHeader;


/**
 * Boolean che indica se il dispositivo è connesso ad internet.
 * @return YES se il dispositivo è connesso ad internet.
 */
+ (BOOL)isNetworkAvailable;

+ (BOOL)isNetworkReachableViaWiFi;


// MARK: - JSON requests

+ (void)JSONGETConURLString:(nullable NSString *)URLString parametri:(nullable NSDictionary *)parametri completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(jsonGet(urlString:parameters:completionBlock:));

+ (void)JSONPOSTConURLString:(nullable NSString *)URLString parametri:(nullable NSDictionary *)parametri completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(jsonPost(urlString:parameters:completionBlock:));

+ (void)JSONDELETEConURLString:(nullable NSString *)URLString parametri:(nullable NSDictionary *)parametri completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(jsonDelete(urlString:parameters:completionBlock:));

+ (void)JSONPUTConURLString:(nullable NSString *)URLString parametri:(nullable NSDictionary *)parametri completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(jsonPut(urlString:parameters:completionBlock:));


+ (void)multipartJSONPOSTConURLString:(nullable NSString *)URLString parametri:(nullable NSDictionary *)parametri multipartBlock:(nullable DBNetworkingMultipartBlock)multipartBlock completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(multipartJsonPost(urlString:parameters:multipartBlock:completionBlock:));

+ (void)multipartJSONPUTConURLString:(nullable NSString *)URLString parametri:(nullable NSDictionary *)parametri multipartBlock:(nullable DBNetworkingMultipartBlock)multipartBlock completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(multipartJsonPut(urlString:parameters:multipartBlock:completionBlock:));


// MARK: - Other requests

+ (void)HTMLGETConURLString:(nullable NSString *)URLString parametri:(nullable NSDictionary *)parametri completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(htmlGet(urlString:parameters:completionBlock:));

+ (void)HTMLPOSTConURLString:(nullable NSString *)URLString parametri:(nullable NSDictionary *)parametri completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(htmlPost(urlString:parameters:completionBlock:));


// MARK: - Image requests

+ (void)GETImmagineConURLString:(nullable NSString *)URLString completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(imageGet(urlString:completionBlock:));

+ (void)GETImmagineConURLString:(nullable NSString *)URLString sovrascrivi:(BOOL)sovrascrivi completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(imageGet(urlString:overwrite:completionBlock:));


// MARK: - File requests

+ (void)GETFileConURLString:(nullable NSString *)URLString downloadPath:(nullable NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(fileGet(urlString:downloadPath:overwrite:completionBlock:));

+ (void)GETFileConURLString:(nullable NSString *)URLString downloadPath:(nullable NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi progressHUD:(BOOL)progressHUDAbilitato messaggioProgressHUD:(nullable NSString *)messaggioProgressHUD completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(fileGet(urlString:downloadPath:overwrite:progressHud:progressHudMessage:completionBlock:));

+ (void)GETFileConURLString:(nullable NSString *)URLString downloadPath:(nullable NSString *)downloadPath sovrascrivi:(BOOL)sovrascrivi progressBlock:(nullable DBNetworkingProgressBlock)progressBlock completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(fileGet(urlString:downloadPath:overwrite:progressBlock:completionBlock:));

@end


// MARK: - Caching extentions

@interface DBNetworking (Caching)

+ (void)JSONGETConURLString:(nullable NSString *)URLString parametri:(nullable NSDictionary *)parametri cachePolicy:(DBNetworkingCachePolicy)cachePolicy completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(jsonGet(urlString:parameters:cachePolicy:completionBlock:));

+ (void)JSONPOSTConURLString:(nullable NSString *)URLString parametri:(nullable NSDictionary *)parametri cachePolicy:(DBNetworkingCachePolicy)cachePolicy completionBlock:(nullable DBNetworkingCompletionBlock)completionBlock
    NS_SWIFT_NAME(jsonPost(urlString:parameters:cachePolicy:completionBlock:));

@end
