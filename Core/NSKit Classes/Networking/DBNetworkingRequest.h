//
//  DBNetworkingRequest.h
//  File version: 1.0.1
//  Last modified: 09/10/2016
//
//  Created by Davide Balistreri on 03/15/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBObject.h"

/**
 * La richiesta da inviare al server per le chiamate DBNetworking.
 */
@interface DBNetworkingRequest : DBObject

@property (strong, nonatomic) NSString *method;
@property (strong, nonatomic) NSString *contentType;
@property (strong, nonatomic) NSString *URLString;
@property (strong, nonatomic) NSDictionary *parameters;
@property (strong, nonatomic) NSDictionary *header;

@end


@interface DBNetworkingRequest (Caching)

- (NSString *)toSerializedString;

@end
