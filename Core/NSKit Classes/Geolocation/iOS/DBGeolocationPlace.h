//
//  DBGeolocationPlace.h
//  File version: 1.0.0
//  Last modified: 02/09/2017
//
//  Created by Davide Balistreri on 02/09/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBObject.h"
#import <CoreLocation/CLLocation.h>

@interface DBGeolocationPlace : DBObject

@property (strong, nonatomic) NSString *name;

@property (strong, nonatomic) NSString *formattedAddress;

@property (nonatomic) CLLocationCoordinate2D coordinate;

@end
