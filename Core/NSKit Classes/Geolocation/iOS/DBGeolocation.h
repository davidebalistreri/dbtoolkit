//
//  DBGeolocation.h
//  File version: 1.0.2
//  Last modified: 02/09/2017
//
//  Created by Davide Balistreri on 03/27/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"
#import "DBGeolocationPlace.h"

#import <CoreLocation/CLLocation.h>
#import <CoreLocation/CLLocationManager.h>

extern NSString * __nonnull const kNotificaPosizioneUtenteAggiornata;


/**
 * Questa classe necessita dell'inserimento di una chiave nell'Info.plist, che indica la modalità di utilizzo GPS
 * richiesta al sistema iOS. Senza di questa, non funzionerà.
 *
 * NSLocationWhenInUseUsageDescription
 * NSLocationAlwaysUsageDescription
 */

@interface DBGeolocation : NSObject

typedef void (^DBGeolocationLocationBlock)(CLLocation * _Nullable location);
typedef void (^DBGeolocationAuthorizationBlock)(CLAuthorizationStatus authorizationStatus);


/// Returns YES if the geolocation service is enabled.
+ (BOOL)isGeolocationServiceEnabled;

/// Returns YES if the user has already chosen whether to authorize the access (or not).
+ (BOOL)isAuthorizationAsked;

/// Returns YES if the user has authorized the app.
+ (BOOL)isAuthorizationGranted;

/// Returns NO if the minimum requirements for geolocation are not met.
+ (BOOL)canGeolocate;


/**
 * Asks authorization for geolocation to the user, if needed.
 *
 * It will request both Always and WhenInUse authorizations,
 * but iOS will prompt to the user only the one which key is present in the Info.plist file.
 *
 * Its safe to call this method everytime you need to use the geolocation,
 * as it executes the completion block even if the authorization was already granted (or not).
 */
+ (void)requestAuthorizationWithCompletionBlock:(nullable DBGeolocationAuthorizationBlock)completionBlock;

+ (void)requestAuthorization;
+ (void)requestAlwaysAuthorization;
+ (void)requestWhenInUseAuthorization;


/// Returns YES if there's a geolocation request currently active.
+ (BOOL)isGeolocating;

/**
 * Calls the request current location method with default values for accuracy and timeout.
 *
 * Default accuracy: hundred meters
 *
 * Default timeout: 4 seconds
 */
+ (void)requestCurrentLocation:(nullable DBGeolocationLocationBlock)completionBlock;

/**
 * Begins receiving updates of user's current location.
 *
 * When a location with at least the specified accuracy is received, it will be returned within the completion block.
 *
 * @discussion
 * If a location with the specified accuracy can not be retrieved within the specified timeout time,
 * the last location received will be returned in the completion block.
 *
 * As soon as the completion block is called, user's location updates will be suspended.
 *
 * @note
 * It will automatically activate the location manager if needed, asking for user permission if it's the first time.
 *
 * @param accuracy The minimum desired accuracy of the user current location. You can use kCLLocationAccuracy constants.
 * @param timeout  The maximum timeout time after which the last location received will be returned.
 * @param completionBlock The completion block called when a valid location is received.
 */
+ (void)requestCurrentLocationWithAccuracy:(CLLocationAccuracy)accuracy timeout:(NSTimeInterval)timeout completionBlock:(nullable DBGeolocationLocationBlock)completionBlock;


+ (nullable CLLocation *)lastLocation;


/**
 * Imposta l'accuratezza del segnale GPS: valori più precisi richiedono un consumo energetico maggiore.
 * Se la geolocalizzazione non è attiva verrà attivata.
 * @param precisione La costante CLLocationAccuracy che definisce il livello di precisione richiesto.
 */
+ (void)impostaPrecisione:(CLLocationAccuracy)precisione;

+ (CLLocationCoordinate2D)posizioneCorrente;

+ (BOOL)isLocation:(CLLocationCoordinate2D)location equalToLocation:(CLLocationCoordinate2D)otherLocation;

+ (BOOL)isPosizioneValida:(CLLocationCoordinate2D)coordinate;
+ (BOOL)areCoordinateValide:(CLLocationCoordinate2D)coordinate DBFRAMEWORK_DEPRECATED(0.3.2, "utilizza il metodo +[DBGeolocation isPosizioneValida:]");

+ (CLLocationDistance)distanzaDa:(CLLocationCoordinate2D)daCoordinate a:(CLLocationCoordinate2D)aCoordinate;
+ (void)indirizzoConCoordinate:(CLLocationCoordinate2D)coordinate conCompletionBlock:(nullable void (^)(NSString * __nullable indirizzo, NSString * __nullable citta))completionBlock;
+ (void)coordinateConIndirizzo:(nullable NSString *)indirizzo conCitta:(nullable NSString *)citta conCompletionBlock:(nullable void (^)(CLLocationCoordinate2D coordinate))completionBlock;

@end


@interface DBGeolocation (DBExtended)

/**
 * Helper method for presenting an alert to the user when location manager can't acquire location because the authorization
 * status is not available (denied or restricted), and automatically open application's settings if accepted by the user.
 * You must provide all the strings of the alert box (title, message, open settings button title, close button title).
 */
+ (void)showAuthorizationNotAvailableAlertWithTitle:(nullable NSString *)title message:(nullable NSString *)message openSettingsTitle:(nullable NSString *)settings closeTitle:(nullable NSString *)close;

/**
 * Helper method for presenting an alert to the user when location manager can't acquire location because the authorization
 * status is not available (denied or restricted), and automatically open application's settings if accepted by the user.
 * You must provide all the strings of the alert box (title, message, open settings button title, close button title),
 * they will be used as identifiers to automatically localize given strings with the current language.
 */
+ (void)showAuthorizationNotAvailableAlertLocalizedWithTitle:(nullable NSString *)title message:(nullable NSString *)message openSettingsTitle:(nullable NSString *)settings closeTitle:(nullable NSString *)close;

@end


@interface DBGeolocation (DBStringExtensions)

+ (nullable NSString *)stringaPosizione:(CLLocationCoordinate2D)coordinate;
+ (nullable NSString *)stringaPosizione:(CLLocationCoordinate2D)coordinate conSeparatore:(nullable NSString *)separatore;
+ (nullable NSString *)stringaPosizioneLatitudine:(CLLocationCoordinate2D)coordinate;
+ (nullable NSString *)stringaPosizioneLongitudine:(CLLocationCoordinate2D)coordinate;

@end


@interface DBGeolocation (DBGeocoding)

/**
 * CompletionBlock per quando viene aggiornata la posizione dell'utente.
 */
typedef void (^DBGeolocationSearchBlock)(NSArray<DBGeolocationPlace *> * __nullable places);

+ (void)searchForPlacesWithQuery:(nullable NSString *)query completionBlock:(nullable DBGeolocationSearchBlock)completionBlock;

+ (void)searchForGooglePlacesWithQuery:(nullable NSString *)query applicationIdentifier:(nullable NSString *)appID completionBlock:(nullable DBGeolocationSearchBlock)completionBlock;

@end


@interface DBGeolocation (DBDeprecated)

/**
 * CompletionBlock per quando viene aggiornata la posizione dell'utente.
 * @param posizioneCorrente L'oggetto ricevuto in risposta.
 */
typedef void (^DBGeolocationDeprecatedBlock)(CLLocationCoordinate2D posizioneCorrente);


/**
 * Attiva la geolocalizzazione, effettuando la richiesta di permessi all'utente in base al valore impostato nell'Info.plist dell'App.
 * Continuerà ad aggiornare la posizione dell'utente, notificando ogni aggiornamento attraverso la chiave kNotificaPosizioneUtenteAggiornata.
 */
+ (void)avviaGeolocalizzazione;

/**
 * Attiva la geolocalizzazione, effettuando la richiesta di permessi all'utente in base al valore impostato nell'Info.plist dell'App.
 * Continuerà ad aggiornare la posizione dell'utente, notificando ogni aggiornamento eseguendo il DBGeolocationBlock e attraverso la chiave kNotificaPosizioneUtenteAggiornata.
 * @param locationBlock Il codice da eseguire ad ogni aggiornamento della posizione utente.
 */
+ (void)avviaGeolocalizzazioneConLocationBlock:(nullable DBGeolocationDeprecatedBlock)locationBlock;

/// Interrompe la geolocalizzazione.
+ (void)interrompiGeolocalizzazione;

@end
