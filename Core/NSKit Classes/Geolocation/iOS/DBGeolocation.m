//
//  DBGeolocation.m
//  File version: 1.0.2
//  Last modified: 02/09/2017
//
//  Created by Davide Balistreri on 03/27/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBGeolocation.h"
#import "DBFramework.h"

#import <AddressBookUI/AddressBookUI.h>

NSString * const kNotificaPosizioneUtenteAggiornata = @"kNotificaPosizioneUtenteAggiornata";

@interface DBGeolocationManager : DBObject <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocation *lastLocation;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (copy) DBGeolocationLocationBlock locationBlock;
@property (copy) DBGeolocationDeprecatedBlock deprecatedLocationBlock;

@property (copy) DBGeolocationAuthorizationBlock authorizationBlock;

@property (nonatomic, getter=isGeolocating) BOOL geolocating;

@end


@implementation DBGeolocationManager

+ (instancetype)sharedInstance
{
    @synchronized(self) {
        DBGeolocationManager *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedGeolocationManager"];
        
        if (!sharedInstance) {
            sharedInstance = [DBGeolocationManager new];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedGeolocationManager"];
        }
        
        return sharedInstance;
    }
}

+ (NSMutableArray *)sharedInstances
{
    @synchronized(self) {
        NSMutableArray *sharedInstances = [DBCentralManager getRisorsa:@"DBSharedGeolocationManagers"];
        
        if (!sharedInstances) {
            sharedInstances = [NSMutableArray array];
            [DBCentralManager setRisorsa:sharedInstances conIdentifier:@"DBSharedGeolocationManagers"];
        }
        
        return sharedInstances;
    }
}

- (void)setupObject
{
    [super setupObject];
    
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
}

- (BOOL)isGeolocationServiceEnabled
{
    return [CLLocationManager locationServicesEnabled];
}

- (BOOL)isAuthorizationAsked
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        return NO;
    }
    
    return YES;
}

- (BOOL)isAuthorizationGranted
{
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
            return NO;
            
        default:
            return YES;
    }
}

- (void)requestAuthorizationIfNeeded
{
    if ([DBGeolocation isAuthorizationGranted] == NO) {
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [self.locationManager requestAlwaysAuthorization];
        }
        
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
    }
    else {
        // Forcing the authorizations status update, to call the completion block
        CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
        [self locationManager:self.locationManager didChangeAuthorizationStatus:authorizationStatus];
    }
}

- (void)requestAlwaysAuthorizationIfNeeded
{
    if ([DBGeolocation isAuthorizationGranted] == NO) {
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [self.locationManager requestAlwaysAuthorization];
        }
    }
    else {
        // Forcing the authorizations status update, to call the completion block
        CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
        [self locationManager:self.locationManager didChangeAuthorizationStatus:authorizationStatus];
    }
}

- (void)requestWhenInUseAuthorizationIfNeeded
{
    if ([DBGeolocation isAuthorizationGranted] == NO) {
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
    }
    else {
        // Forcing the authorizations status update, to call the completion block
        CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
        [self locationManager:self.locationManager didChangeAuthorizationStatus:authorizationStatus];
    }
}


- (void)avviaGeolocalizzazione
{
    [self requestAuthorizationIfNeeded];
    
    self.geolocating = YES;
    [self.locationManager startUpdatingLocation];
}

- (void)interrompiGeolocalizzazione
{
    self.geolocating = NO;
    [self.locationManager stopUpdatingLocation];
}


// MARK: - CLLocationManager delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if (self.isGeolocating == NO) {
        // Not valid
        return;
    }
    
    NSLog(@"^^ LocationManager didUpdateLocation");
    
    CLLocation *location = [locations lastObject]; // Most recent location
    CLLocation *lastLocation = [location copy];
    
    // Updating instance last location
    self.lastLocation = lastLocation;
    
    // Updating global last location
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    sharedInstance.lastLocation = lastLocation;
    
    // Notifico l'aggiornamento della posizione corrente
    [DBNotificationCenter postNotificationWithName:kNotificaPosizioneUtenteAggiornata];
    
    if (self.deprecatedLocationBlock) {
        self.deprecatedLocationBlock(lastLocation.coordinate);
    }
    
    if (self.locationBlock) {
        self.locationBlock(lastLocation);
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusNotDetermined) {
        // Not valid
        return;
    }
    
    if (self.authorizationBlock) {
        self.authorizationBlock(status);
        self.authorizationBlock = nil;
    }
}

@end


@implementation DBGeolocation

+ (BOOL)isGeolocationServiceEnabled
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    return [sharedInstance isGeolocationServiceEnabled];
}

+ (BOOL)isAuthorizationAsked
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    return [sharedInstance isAuthorizationAsked];
}

+ (BOOL)isAuthorizationGranted
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    return [sharedInstance isAuthorizationGranted];
}

+ (BOOL)canGeolocate
{
    if ([DBGeolocation isGeolocationServiceEnabled] == NO) {
        return NO;
    }
    
    if ([DBGeolocation isAuthorizationGranted] == NO) {
        return NO;
    }
    
    return YES;
}

+ (void)requestAuthorizationWithCompletionBlock:(DBGeolocationAuthorizationBlock)completionBlock
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    sharedInstance.authorizationBlock = completionBlock;
    [sharedInstance requestAuthorizationIfNeeded];
}

+ (void)requestAuthorization
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    sharedInstance.authorizationBlock = nil;
    [sharedInstance requestAuthorizationIfNeeded];
}

+ (void)requestAlwaysAuthorization
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    sharedInstance.authorizationBlock = nil;
    [sharedInstance requestAlwaysAuthorizationIfNeeded];
}

+ (void)requestWhenInUseAuthorization
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    sharedInstance.authorizationBlock = nil;
    [sharedInstance requestWhenInUseAuthorizationIfNeeded];
}

+ (BOOL)isGeolocating
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    BOOL isGeolocating = sharedInstance.isGeolocating;
    
    // TODO: Trovare un modo per recuperare l'istanza di un Manager
    // e controllare nello specifico se quel Manager sta geolocalizzando,
    // altrimenti basta che un manager qualsiasi stia geolocalizzando
    // per far sembrare tutti gli altri 'attivi'
    
    NSArray *sharedInstances = [DBGeolocationManager sharedInstances];
    for (DBGeolocationManager *manager in sharedInstances) {
        if (manager.isGeolocating) {
            isGeolocating = YES;
        }
    }
    
    return isGeolocating;
}

+ (void)requestCurrentLocation:(DBGeolocationLocationBlock)completionBlock
{
    [self requestCurrentLocationWithAccuracy:kCLLocationAccuracyHundredMeters timeout:4.0 completionBlock:completionBlock];
}

+ (void)requestCurrentLocationWithAccuracy:(CLLocationAccuracy)accuracy timeout:(NSTimeInterval)timeout completionBlock:(DBGeolocationLocationBlock)completionBlock
{
    // Force quitting if the minimum requirements for geolocation are not met
    if ([DBGeolocation canGeolocate] == NO) {
        if (completionBlock) {
            completionBlock(nil);
        }
        
        return;
    }
    
    DBGeolocationManager *manager = [DBGeolocationManager new];
    manager.locationManager.desiredAccuracy = accuracy;
    
    NSMutableArray *managers = [DBGeolocationManager sharedInstances];
    [managers addObject:manager];
    
    __weak DBGeolocationManager *weakManager = manager;
    
    manager.locationBlock = ^(CLLocation * _Nullable location) {
        CLLocationAccuracy locationAccuracy = location.horizontalAccuracy;
        
        if (locationAccuracy > 0.0 && locationAccuracy <= accuracy) {
            // A location with the requested accuracy was found
            
            // Check if the geolocation is still active
            if (weakManager.isGeolocating) {
                [weakManager interrompiGeolocalizzazione];
                
                if (completionBlock) {
                    completionBlock(weakManager.lastLocation);
                }
                
                // We're done
                [managers removeObject:weakManager];
            }
        }
    };
    
    [manager avviaGeolocalizzazione];
    
    // Timeout handling
    [DBThread eseguiBlockConRitardo:timeout block:^{
        // Se la geolocalizzazione è ancora attiva, interrompo tutto e restituisco l'ultima posizione ricevuta
        if (weakManager.isGeolocating) {
            [weakManager interrompiGeolocalizzazione];
            
            if (completionBlock) {
                completionBlock(weakManager.lastLocation);
            }
            
            // We're done
            [managers removeObject:weakManager];
        }
    }];
}

+ (void)avviaGeolocalizzazione
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    [sharedInstance avviaGeolocalizzazione];
}

+ (void)avviaGeolocalizzazioneConLocationBlock:(DBGeolocationDeprecatedBlock)locationBlock
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    sharedInstance.deprecatedLocationBlock = locationBlock;
    [sharedInstance avviaGeolocalizzazione];
}

+ (void)interrompiGeolocalizzazione
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    sharedInstance.deprecatedLocationBlock = nil;
    [sharedInstance interrompiGeolocalizzazione];
}

+ (CLLocation *)lastLocation
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    return sharedInstance.lastLocation;
}

+ (void)impostaPrecisione:(CLLocationAccuracy)precisione
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    sharedInstance.locationManager.desiredAccuracy = precisione;
}

+ (CLLocationCoordinate2D)posizioneCorrente
{
    DBGeolocationManager *sharedInstance = [DBGeolocationManager sharedInstance];
    return sharedInstance.lastLocation.coordinate;
}

+ (BOOL)isLocation:(CLLocationCoordinate2D)location equalToLocation:(CLLocationCoordinate2D)otherLocation
{
    if (location.latitude == otherLocation.latitude && location.longitude == otherLocation.longitude)
        return YES;
    
    return NO;
}

+ (BOOL)isPosizioneValida:(CLLocationCoordinate2D)coordinate
{
    if (!CLLocationCoordinate2DIsValid(coordinate))
        return NO;
    
    if (coordinate.latitude == 0.0 && coordinate.longitude == 0.0)
        return NO;
    
    // Posizione valida
    return YES;
}

+ (BOOL)areCoordinateValide:(CLLocationCoordinate2D)coordinate
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.2, "utilizza il metodo +[DBGeolocation isPosizioneValida:]");
    return [self isPosizioneValida:coordinate];
}

+ (CLLocationDistance)distanzaDa:(CLLocationCoordinate2D)daCoordinate a:(CLLocationCoordinate2D)aCoordinate
{
    MKMapPoint mapPoint1 = MKMapPointForCoordinate(daCoordinate);
    MKMapPoint mapPoint2 = MKMapPointForCoordinate(aCoordinate);
    
    CLLocationDistance distance = MKMetersBetweenMapPoints(mapPoint1, mapPoint2);
    return distance;
}

+ (void)indirizzoConCoordinate:(CLLocationCoordinate2D)coordinate
            conCompletionBlock:(void (^)(NSString *indirizzo, NSString *citta))completionBlock
{
    CLGeocoder *geocoder = [CLGeocoder new];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks firstObject];
        NSDictionary *indirizzi = placemark.addressDictionary;
        NSString *citta = [indirizzi safeValueForKey:@"City"];
        NSString *indirizzo = [indirizzi safeValueForKey:@"Street"];
        if ([indirizzo length] == 0) indirizzo = [indirizzi safeValueForKey:@"Name"];
        completionBlock(indirizzo, citta);
    }];
}

+ (void)coordinateConIndirizzo:(NSString *)indirizzo conCitta:(NSString *)citta
            conCompletionBlock:(void (^)(CLLocationCoordinate2D coordinate))completionBlock
{
    CLGeocoder *geocoder = [CLGeocoder new];
    
    NSString *addressString = [NSString safeString:indirizzo];
    
    if ([NSString isNotEmpty:citta]) {
        addressString = [addressString stringByAppendingFormat:@", %@", [NSString safeString:citta]];
    }
    
    [geocoder geocodeAddressString:addressString completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks firstObject];
        CLLocationCoordinate2D coordinate = placemark.location.coordinate;
        
        // Controllo che siano valide
        if (![self isPosizioneValida:coordinate])
            coordinate = kCLLocationCoordinate2DInvalid;
        
        // Restituisco le coordinate
        if (completionBlock) completionBlock(coordinate);
    }];
}

@end


@implementation DBGeolocation (DBExtended)

/**
 * Helper method for presenting an alert to the user when location manager can't acquire location because the authorization
 * status is not available (denied or restricted), and automatically open application's settings if accepted by the user.
 * You must provide all the strings of the alert box (title, message, open settings button title, close button title).
 */
+ (void)showAuthorizationNotAvailableAlertWithTitle:(NSString *)title message:(NSString *)message openSettingsTitle:(NSString *)settings closeTitle:(NSString *)close
{
    if ([DBGeolocation isAuthorizationAsked] == NO || [DBGeolocation isAuthorizationGranted]) {
        // Not valid
        return;
    }
    
    [DBAlertView alertConTitolo:title conMessaggio:message conPulsanteAccetta:settings conAccettaBlock:^{
        [DBApplication openApplicationSettings];
    } conPulsanteRifiuta:close conRifiutaBlock:nil];
}

/**
 * Helper method for presenting an alert to the user when location manager can't acquire location because the authorization
 * status is not available (denied or restricted), and automatically open application's settings if accepted by the user.
 * You must provide all the strings of the alert box (title, message, open settings button title, close button title),
 * they will be used as identifiers to automatically localize given strings with the current language.
 */
+ (void)showAuthorizationNotAvailableAlertLocalizedWithTitle:(NSString *)title message:(NSString *)message openSettingsTitle:(NSString *)settings closeTitle:(NSString *)close
{
    [self showAuthorizationNotAvailableAlertWithTitle:DBLocalizedString(title) message:DBLocalizedString(message) openSettingsTitle:DBLocalizedString(settings) closeTitle:DBLocalizedString(close)];
}

@end


@implementation DBGeolocation (DBStringExtensions)

+ (NSString *)stringaPosizione:(CLLocationCoordinate2D)coordinate
{
    return [self stringaPosizione:coordinate conSeparatore:@", "];
}

+ (NSString *)stringaPosizione:(CLLocationCoordinate2D)coordinate conSeparatore:(NSString *)separatore
{
    NSString *stringa = [NSString stringWithFormat:@"%@%@%@",
                         [self stringaPosizioneLatitudine:coordinate],
                         separatore,
                         [self stringaPosizioneLongitudine:coordinate]];
    
    return [NSString safeString:stringa];
}

+ (NSString *)stringaPosizioneLatitudine:(CLLocationCoordinate2D)coordinate
{
    int latSeconds = parseInt(coordinate.latitude * 3600);
    int latDegrees = latSeconds / 3600;
    latSeconds     = abs(latSeconds % 3600);
    int latMinutes = latSeconds / 60;
    latSeconds    %= 60;
    
    NSString *stringa = [NSString stringWithFormat:@"%d°%d'%d\"%@",
                         abs(latDegrees), latMinutes, latSeconds, latDegrees >= 0 ? @"N" : @"S"];
    
    return [NSString safeString:stringa];
}

+ (NSString *)stringaPosizioneLongitudine:(CLLocationCoordinate2D)coordinate
{
    int lngSeconds = parseInt(coordinate.longitude * 3600);
    int lngDegrees = lngSeconds / 3600;
    lngSeconds     = abs(lngSeconds % 3600);
    int lngMinutes = lngSeconds / 60;
    lngSeconds    %= 60;
    
    
    NSString *stringa = [NSString stringWithFormat:@"%d°%d'%d\"%@",
                         abs(lngDegrees), lngMinutes, lngSeconds, lngDegrees >= 0 ? @"E" : @"W"];
    
    return [NSString safeString:stringa];
}

@end


@implementation DBGeolocation (DBGeocoding)

+ (void)searchForPlacesWithQuery:(NSString *)query completionBlock:(DBGeolocationSearchBlock)completionBlock
{
    if ([NSString isEmpty:query]) {
        // Not valid
        if (completionBlock) completionBlock(nil);
        return;
    }
    
    CLLocationCoordinate2D coordinate = [DBGeolocation posizioneCorrente];
    CLLocationDistance distance = 10000;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, distance, distance);
    
    MKLocalSearchRequest *request = [MKLocalSearchRequest new];
    request.naturalLanguageQuery = query;
    request.region = region;
    
    MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];
    [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        NSMutableArray *places = [NSMutableArray array];
        
        for (MKMapItem *item in response.mapItems) {
            MKPlacemark *placemark = item.placemark;
            DBGeolocationPlace *place = [self placeWithPlacemark:placemark];
            [places addObject:place];
        }
        
        if (completionBlock) {
            completionBlock(places);
        }
    }];
}

+ (void)searchForGooglePlacesWithQuery:(NSString *)query applicationIdentifier:(NSString *)appID completionBlock:(nullable DBGeolocationSearchBlock)completionBlock
{
    if ([NSString isEmpty:query]) {
        // Not valid
        if (completionBlock) completionBlock(nil);
        return;
    }
    
    NSString *URLString = @"https://maps.googleapis.com/maps/api/place/textsearch/json";
    
    // Parameters
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"key"] = [NSString safeString:appID];
    parameters[@"input"] = [query stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    // FIXME: Non funziona bene: cerca solo nel raggio selezionato
    CLLocationCoordinate2D coordinate = [DBGeolocation posizioneCorrente];
    if ([DBGeolocation isPosizioneValida:coordinate]) {
        NSString *location = [NSString stringWithFormat:@"%.7f,%.7f", coordinate.latitude, coordinate.longitude];
        parameters[@"location"] = location;
        parameters[@"radius"] = @"10000";
    }
    
    [DBNetworking JSONGETConURLString:URLString parametri:parameters completionBlock:^(DBNetworkingResponse * __nullable response) {
        
        // Response parsing
        NSMutableArray *places = [NSMutableArray array];
        
        if (response.success && [response.responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *responseObject = response.responseObject;
            NSArray *results = [responseObject safeArrayForKey:@"results"];
            
            for (NSDictionary *dictionary in results) {
                DBGeolocationPlace *place = [self placeWithGoogleDictionary:dictionary];
                [places addObject:place];
            }
        }
        
        if (completionBlock) {
            completionBlock(places);
        }
    }];
}

+ (DBGeolocationPlace *)placeWithPlacemark:(MKPlacemark *)placemark
{
    NSString *formattedAddress = ABCreateStringWithAddressDictionary(placemark.addressDictionary, YES);
    formattedAddress = [formattedAddress stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    
    DBGeolocationPlace *place = [DBGeolocationPlace new];
    place.name = placemark.name;
    place.formattedAddress = formattedAddress;
    place.coordinate = placemark.location.coordinate;
    
    return place;
}

+ (DBGeolocationPlace *)placeWithGoogleDictionary:(NSDictionary *)dictionary
{
    DBGeolocationPlace *place = [DBGeolocationPlace new];
    
    NSString *name = [dictionary safeStringForKey:@"name"];
    place.name = name.trimmedString;
    
    NSString *formattedAddress = [dictionary safeStringForKey:@"formatted_address"];
    formattedAddress = [formattedAddress stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    place.formattedAddress = formattedAddress.trimmedString;
    
    NSDictionary *geometry = [dictionary safeDictionaryForKey:@"geometry"];
    NSDictionary *location = [geometry safeDictionaryForKey:@"location"];
    CLLocationDegrees latitude  = [location safeDoubleForKey:@"lat"];
    CLLocationDegrees longitude = [location safeDoubleForKey:@"lng"];
    place.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
    return place;
}

@end
