//
//  DBGeolocationPlace.m
//  File version: 1.0.0
//  Last modified: 02/09/2017
//
//  Created by Davide Balistreri on 02/09/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBGeolocationPlace.h"

@implementation DBGeolocationPlace

- (id)copyWithZone:(NSZone *)zone
{
    DBGeolocationPlace *copy = [super copyWithZone:zone];
    
    if (copy) {
        copy.name = [self.name copy];
        copy.formattedAddress = [self.formattedAddress copy];
        copy.coordinate = self.coordinate;
    }
    
    return copy;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.formattedAddress = [aDecoder decodeObjectForKey:@"formattedAddress"];
        
        CGFloat latitude = [aDecoder decodeDoubleForKey:@"coordinate.latitude"];
        CGFloat longitude = [aDecoder decodeDoubleForKey:@"coordinate.longitude"];
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.formattedAddress forKey:@"formattedAddress"];
    [aCoder encodeDouble:self.coordinate.latitude forKey:@"coordinate.latitude"];
    [aCoder encodeDouble:self.coordinate.longitude forKey:@"coordinate.longitude"];
}

@end
