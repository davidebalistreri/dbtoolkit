//
//  DBNotificationCenter.m
//  File version: 1.0.0 beta
//  Last modified: 10/09/2016
//
//  Created by Davide Balistreri on 10/09/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBNotificationCenter.h"
#import "DBCentralManager.h"
#import "DBDictionaryExtensions.h"
#import "DBObjectExtensions.h"

@class DBNotificationCenterObserver;


@interface DBNotificationCenter ()

+ (void)removeObserver:(DBNotificationCenterObserver *)observer;

@end


@interface DBNotificationCenterObserver : NSObject

@property (weak, nonatomic) id target;

@property (strong, nonatomic) NSMutableDictionary<NSString *, NSMutableArray *> *names;

@end

@implementation DBNotificationCenterObserver

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.names = [NSMutableDictionary dictionary];
    }
    return self;
}

- (NSMutableArray *)observersWithName:(NSString *)name
{
    NSMutableArray *items = [self.names safeObjectForKey:name];
    
    if (!items) {
        items = [NSMutableArray array];
        [self.names setObject:items forKey:name];
    }
    
    return items;
}

- (void)addObserverForName:(NSString *)name withNotificationBlock:(DBNotificationCenterBlock)notificationBlock
{
    id object = [[NSNotificationCenter defaultCenter] addObserverForName:name object:nil queue:[NSOperationQueue currentQueue] usingBlock:^(NSNotification * _Nonnull note) {
        
        if (!self.target) {
            // Observer deallocated, it's not valid anymore
            // Debug: NSLog(@"^ Observer deallocated");
            [self removeAllObservers];
        }
        else if (notificationBlock) {
            // Debug: NSLog(@"^ Notification received!");
            notificationBlock(note);
        }
    }];
    
    NSMutableArray *observers = [self observersWithName:name];
    [observers addObject:object];
}

- (void)removeObserversForName:(NSString *)name
{
    NSArray *objects = [self.names safeArrayForKey:name];
    
    for (id object in objects) {
        [[NSNotificationCenter defaultCenter] removeObserver:object];
    }
    
    [self.names removeObjectForKey:name];
    
    if (self.names.count == 0) {
        // Dealloc
        [DBNotificationCenter removeObserver:self];
    }
}

- (void)removeAllObservers
{
    for (NSMutableArray *objects in self.names.allValues) {
        for (id object in objects) {
            [[NSNotificationCenter defaultCenter] removeObserver:object];
        }
    }
    
    [self.names removeAllObjects];
    
    // Dealloc
    [DBNotificationCenter removeObserver:self];
}

@end


@implementation DBNotificationCenter

+ (NSMutableArray<DBNotificationCenterObserver *> *)sharedInstances
{
    @synchronized(self) {
        NSMutableArray *sharedInstances = [DBCentralManager getRisorsa:@"DBSharedNotificationHandlers"];
        
        if (!sharedInstances) {
            sharedInstances = [NSMutableArray array];
            [DBCentralManager setRisorsa:sharedInstances conIdentifier:@"DBSharedNotificationHandlers"];
        }
        
        return sharedInstances;
    }
}

+ (DBNotificationCenterObserver *)observerWithTarget:(id)target
{
    NSMutableArray *sharedInstances = [self sharedInstances];
    
    for (DBNotificationCenterObserver *observer in sharedInstances) {
        if (target == observer.target) {
            return observer;
        }
    }
    
    return nil;
}

+ (void)addObserver:(id)observer
{
    NSMutableArray *sharedInstances = [self sharedInstances];
    // Debug: NSLog(@"^ Observers added: %lu + 1 = %lu", sharedInstances.count, sharedInstances.count + 1);
    [sharedInstances addObject:observer];
}

+ (void)removeObserver:(id)observer
{
    NSMutableArray *sharedInstances = [self sharedInstances];
    // Debug: NSLog(@"^ Observers removed: %lu - 1 = %lu", sharedInstances.count, sharedInstances.count - 1);
    [sharedInstances removeObject:observer];
}

// MARK: - Public methods

+ (void)postNotification:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

+ (void)addObserverOnTarget:(id)target forName:(NSString *)name withNotificationBlock:(DBNotificationCenterBlock)notificationBlock
{
    DBNotificationCenterObserver *observer = [self observerWithTarget:target];
    
    if (!observer) {
        // New observer
        observer = [DBNotificationCenterObserver new];
        observer.target = target;
        [self addObserver:observer];
    }
    
    [observer addObserverForName:name withNotificationBlock:notificationBlock];
}

+ (void)addObserverOnTarget:(id)target forName:(NSString *)name withSelector:(SEL)selector object:(id)object
{
    __weak NSObject *weakTarget = target;
    
    NSString *selectorString = NSStringFromSelector(selector);
    BOOL selectorHasObject = [selectorString containsString:@":"];
    
    [self addObserverOnTarget:target forName:name withNotificationBlock:^(NSNotification * _Nonnull notification) {
        
        if ([weakTarget respondsToSelector:selector]) {
            if (selectorHasObject) {
                [weakTarget safePerformSelector:selector withObject:notification];
            }
            else {
                [weakTarget safePerformSelector:selector];
            }
        }
    }];
}

+ (void)removeObserversOnTarget:(id)target forName:(NSString *)name
{
    if (target && name) {
        DBNotificationCenterObserver *observer = [self observerWithTarget:target];
        [observer removeObserversForName:name];
    }
}

+ (void)removeAllObserversOnTarget:(id)target
{
    if (target) {
        DBNotificationCenterObserver *observer = [self observerWithTarget:target];
        [observer removeAllObservers];
    }
}

@end


@implementation DBNotificationCenter (Extended)

+ (void)postNotificationWithName:(NSString *)name
{
    [self postNotificationWithName:name object:nil userInfo:nil];
}

+ (void)postNotificationWithName:(NSString *)name object:(nullable id)object
{
    [self postNotificationWithName:name object:object userInfo:nil];
}

+ (void)postNotificationWithName:(NSString *)name object:(nullable id)object userInfo:(nullable NSDictionary *)userInfo
{
    NSNotification *notification = [NSNotification notificationWithName:name object:object userInfo:userInfo];
    [self postNotification:notification];
}

@end
