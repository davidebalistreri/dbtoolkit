//
//  DBDevice.h
//  File version: 1.2.0
//  Last modified: 12/11/2016
//
//  Created by Davide Balistreri on 02/01/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@interface DBDevice : NSObject

// MARK: - Device model

/**
 * Device models.
 *
 * With this integer enumerator you can compare the models
 * using boolean operators (including < and >) and switch cases.
 */
typedef NS_ENUM(NSInteger, DBDeviceType) {
    DBDeviceTypeUndefined = 0,
    DBDeviceTypeSimulator = 1,
    
    DBDeviceTypeiPhone2g     = 100,
    DBDeviceTypeiPhone3g     = 101,
    DBDeviceTypeiPhone3gs    = 102,
    DBDeviceTypeiPhone4      = 103,
    DBDeviceTypeiPhone4s     = 104,
    DBDeviceTypeiPhone5      = 105,
    DBDeviceTypeiPhone5c     = 106,
    DBDeviceTypeiPhone5s     = 107,
    DBDeviceTypeiPhone6      = 108,
    DBDeviceTypeiPhone6Plus  = 109,
    DBDeviceTypeiPhone6s     = 110,
    DBDeviceTypeiPhone6sPlus = 111,
    DBDeviceTypeiPhoneSE     = 112,
    DBDeviceTypeiPhone7      = 113,
    DBDeviceTypeiPhone7Plus  = 114,
};

/// Returns the current device model.
+ (DBDeviceType)deviceType;


// MARK: - Device screen model

/**
 * Screen models grouped by size in inches.
 *
 * With this integer enumerator you can compare the models
 * using boolean operators (including < and >) and switch cases.
 */
typedef NS_ENUM(NSInteger, DBScreenType) {
    DBScreenTypeUndefined = 0,
    
    DBScreenType3dot5 = 100,
    DBScreenType4     = 101,
    DBScreenType4dot7 = 102,
    DBScreenType5dot5 = 103,
    DBScreenType5dot8 = 104,
};

/// Returns the current device screen model.
+ (DBScreenType)screenType;


// MARK: - Device

+ (BOOL)isiPhone;
+ (BOOL)isiPad;

+ (BOOL)isSimulator;

/// Restituisce la stringa del modello del dispositivo.
+ (NSString *)identifierModelloDispositivo;


// MARK: - Application

+ (BOOL)isiPhoneApp;
+ (BOOL)isiPadApp;

+ (BOOL)isPortrait;
+ (BOOL)isPortraitUp;
+ (BOOL)isPortraitUpsideDown;
+ (BOOL)isLandscape;
+ (BOOL)isLandscapeLeft;
+ (BOOL)isLandscapeRight;

+ (void)impostaOrientamento:(UIInterfaceOrientation)orientamento;

+ (BOOL)isApplicazioneAttiva;
+ (BOOL)isApplicazioneInBackground;


+ (CGFloat)screenBrightness;
+ (void)setScreenBrightness:(CGFloat)brightness;
+ (void)setScreenBrightness:(CGFloat)brightness animated:(BOOL)animated;


// MARK: - Operating system

/// Restituisce la stringa di versione del sistema operativo.
+ (NSString *)identifierVersioneiOS;

/// Restituisce la stringa della build del sistema operativo.
+ (NSString *)identifierBuildVersioneiOS;

/// Restituisce la stringa di versione del sistema operativo e del kernel.
+ (NSString *)identifierVersioneiOSCompleta;

/// Restituisce il numero di versione del sistema operativo.
+ (double)versioneiOS;

/**
 * Controlla se la versione di iOS in esecuzione è <b>esattamente</b> quella specificata.
 * @param versione Il numero della versione.
 */
+ (BOOL)isiOS:(double)versione;

/**
 * Controlla se la versione di iOS in esecuzione è <b>almeno</b> quella specificata.
 * @param versione Il numero della versione.
 */
+ (BOOL)isiOSMin:(double)versione;

/**
 * Controlla se la versione di iOS in esecuzione è <b>al massimo</b> quella specificata.
 * @param versione Il numero della versione.
 */
+ (BOOL)isiOSMax:(double)versione;

/**
 * Controlla se la versione major di iOS in esecuzione è <b>esattamente</b> quella specificata.
 * @param versione Il numero della versione.
 */
+ (BOOL)isiOSMajor:(int)versione;

@end
