//
//  DBDevice.m
//  File version: 1.2.0
//  Last modified: 12/11/2016
//
//  Created by Davide Balistreri on 02/01/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBDevice.h"
#import "DBThread.h"
#import <sys/sysctl.h>

@implementation DBDevice

// MARK: - Device model

/// Returns the current device model.
+ (DBDeviceType)deviceType
{
    NSString *identifier = [self identifierModelloDispositivo];
    
    if ([identifier hasPrefix:@"iPhone1,1"]) return DBDeviceTypeiPhone2g;
    if ([identifier hasPrefix:@"iPhone1,2"]) return DBDeviceTypeiPhone3g;
    if ([identifier hasPrefix:@"iPhone2"])   return DBDeviceTypeiPhone3gs;
    if ([identifier hasPrefix:@"iPhone3"])   return DBDeviceTypeiPhone4;
    if ([identifier hasPrefix:@"iPhone4"])   return DBDeviceTypeiPhone4s;
    if ([identifier hasPrefix:@"iPhone5,1"]) return DBDeviceTypeiPhone5;
    if ([identifier hasPrefix:@"iPhone5,2"]) return DBDeviceTypeiPhone5;
    if ([identifier hasPrefix:@"iPhone5,3"]) return DBDeviceTypeiPhone5c;
    if ([identifier hasPrefix:@"iPhone5,4"]) return DBDeviceTypeiPhone5c;
    if ([identifier hasPrefix:@"iPhone6"])   return DBDeviceTypeiPhone5s;
    if ([identifier hasPrefix:@"iPhone7,2"]) return DBDeviceTypeiPhone6;
    if ([identifier hasPrefix:@"iPhone7,1"]) return DBDeviceTypeiPhone6Plus;
    if ([identifier hasPrefix:@"iPhone8,1"]) return DBDeviceTypeiPhone6s;
    if ([identifier hasPrefix:@"iPhone8,2"]) return DBDeviceTypeiPhone6sPlus;
    if ([identifier hasPrefix:@"iPhone8,4"]) return DBDeviceTypeiPhoneSE;
    if ([identifier hasPrefix:@"iPhone9,1"]) return DBDeviceTypeiPhone7;
    if ([identifier hasPrefix:@"iPhone9,3"]) return DBDeviceTypeiPhone7;
    if ([identifier hasPrefix:@"iPhone9,2"]) return DBDeviceTypeiPhone7Plus;
    if ([identifier hasPrefix:@"iPhone9,4"]) return DBDeviceTypeiPhone7Plus;
    
    //    if ([identifier hasPrefix:@"iPod1,1"]) return @"iPod Touch 1G";
    //    if ([identifier hasPrefix:@"iPod2,1"]) return @"iPod Touch 2G";
    //    if ([identifier hasPrefix:@"iPod3,1"]) return @"iPod Touch 3G";
    //    if ([identifier hasPrefix:@"iPod4,1"]) return @"iPod Touch 4G";
    //    if ([identifier hasPrefix:@"iPod5,1"]) return @"iPod Touch 5G";
    //    if ([identifier hasPrefix:@"iPad1,1"]) return @"iPad";
    //    if ([identifier hasPrefix:@"iPad2,1"]) return @"iPad 2 (WiFi)";
    //    if ([identifier hasPrefix:@"iPad2,2"]) return @"iPad 2 (GSM)";
    //    if ([identifier hasPrefix:@"iPad2,3"]) return @"iPad 2 (CDMA)";
    //    if ([identifier hasPrefix:@"iPad2,4"]) return @"iPad 2 (WiFi)";
    //    if ([identifier hasPrefix:@"iPad2,5"]) return @"iPad Mini (WiFi)";
    //    if ([identifier hasPrefix:@"iPad2,6"]) return @"iPad Mini (GSM)";
    //    if ([identifier hasPrefix:@"iPad2,7"]) return @"iPad Mini (GSM+CDMA)";
    //    if ([identifier hasPrefix:@"iPad3,1"]) return @"iPad 3 (WiFi)";
    //    if ([identifier hasPrefix:@"iPad3,2"]) return @"iPad 3 (GSM+CDMA)";
    //    if ([identifier hasPrefix:@"iPad3,3"]) return @"iPad 3 (GSM)";
    //    if ([identifier hasPrefix:@"iPad3,4"]) return @"iPad 4 (WiFi)";
    //    if ([identifier hasPrefix:@"iPad3,5"]) return @"iPad 4 (GSM)";
    //    if ([identifier hasPrefix:@"iPad3,6"]) return @"iPad 4 (GSM+CDMA)";
    //    if ([identifier hasPrefix:@"iPad4,1"]) return @"iPad Air (WiFi)";
    //    if ([identifier hasPrefix:@"iPad4,2"]) return @"iPad Air (Cellular)";
    //    if ([identifier hasPrefix:@"iPad4,3"]) return @"iPad Air";
    //    if ([identifier hasPrefix:@"iPad4,4"]) return @"iPad Mini 2G (WiFi)";
    //    if ([identifier hasPrefix:@"iPad4,5"]) return @"iPad Mini 2G (Cellular)";
    //    if ([identifier hasPrefix:@"iPad4,6"]) return @"iPad Mini 2G";
    //    if ([identifier hasPrefix:@"iPad4,7"]) return @"iPad Mini 3 (WiFi)";
    //    if ([identifier hasPrefix:@"iPad4,8"]) return @"iPad Mini 3 (Cellular)";
    //    if ([identifier hasPrefix:@"iPad4,9"]) return @"iPad Mini 3 (China)";
    //    if ([identifier hasPrefix:@"iPad5,3"]) return @"iPad Air 2 (WiFi)";
    //    if ([identifier hasPrefix:@"iPad5,4"]) return @"iPad Air 2 (Cellular)";
    //    if ([identifier hasPrefix:@"AppleTV2,1"]) return @"Apple TV 2G";
    //    if ([identifier hasPrefix:@"AppleTV3,1"]) return @"Apple TV 3";
    //    if ([identifier hasPrefix:@"AppleTV3,2"]) return @"Apple TV 3 (2013)";
    if ([identifier hasPrefix:@"i386"])   return DBDeviceTypeSimulator;
    if ([identifier hasPrefix:@"x86_64"]) return DBDeviceTypeSimulator;
    
    return DBDeviceTypeUndefined;
}


// MARK: - Device screen model

/// Returns the current device screen model.
+ (DBScreenType)screenType
{
    CGRect frame = [[UIScreen mainScreen] bounds];
    
    if (frame.size.height == 480.0) {
        return DBScreenType3dot5;
    }
    else if (frame.size.height == 568.0) {
        return DBScreenType4;
    }
    else if (frame.size.height == 667.0) {
        return DBScreenType4dot7;
    }
    else if (frame.size.height == 736.0) {
        return DBScreenType5dot5;
    }
    else if (frame.size.height == 812.0) {
        return DBScreenType5dot8;
    }
    
    return DBScreenTypeUndefined;
}


// MARK: - Device

+ (BOOL)isiPhone
{
    return ([[[UIDevice currentDevice] model] hasPrefix:@"iPhone"]) ? YES : NO;
}

+ (BOOL)isiPad
{
    return ([[[UIDevice currentDevice] model] hasPrefix:@"iPad"]) ? YES : NO;
}

+ (BOOL)isSimulator
{
    return TARGET_OS_SIMULATOR;
}

+ (NSString *)identifierModelloDispositivo
{
    return [self systemInfoConNome:"hw.machine"];
}

+ (NSString *)systemInfoConNome:(char *)nome
{
    size_t size;
    sysctlbyname(nome, NULL, &size, NULL, 0);
    
    char *answer = malloc(size);
    sysctlbyname(nome, answer, &size, NULL, 0);
    
    NSString *results = [NSString stringWithCString:answer encoding: NSUTF8StringEncoding];
    
    free(answer);
    return results;
}


// MARK: - Application

+ (BOOL)isiPhoneApp
{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) ? YES : NO;
}

+ (BOOL)isiPadApp
{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? YES : NO;
}

+ (BOOL)isPortrait
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return UIInterfaceOrientationIsPortrait(orientation);
}

+ (BOOL)isPortraitUp
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationPortrait);
}

+ (BOOL)isPortraitUpsideDown
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationPortraitUpsideDown);
}

+ (BOOL)isLandscape
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return UIInterfaceOrientationIsLandscape(orientation);
}

+ (BOOL)isLandscapeLeft
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationLandscapeLeft);
}

+ (BOOL)isLandscapeRight
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationLandscapeRight);
}

+ (void)impostaOrientamento:(UIInterfaceOrientation)orientamento
{
    if (orientamento) {
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:orientamento] forKey:@"orientation"];
    }
}

+ (BOOL)isApplicazioneAttiva
{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    return (state == UIApplicationStateActive) ? YES : NO;
}

+ (BOOL)isApplicazioneInBackground
{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    return (state == UIApplicationStateBackground) ? YES : NO;
}

+ (CGFloat)screenBrightness
{
    return [[UIScreen mainScreen] brightness];
}

+ (void)setScreenBrightness:(CGFloat)brightness
{
    [self setScreenBrightness:brightness animated:YES];
}

+ (void)setScreenBrightness:(CGFloat)brightness animated:(BOOL)animated
{
    CGFloat safeBrightness = mantieniAllInterno(brightness, 0.0, 1.0);
    safeBrightness = floorConDecimali(brightness, 2);
    
    if (animated == NO) {
        [[UIScreen mainScreen] setBrightness:safeBrightness];
        return;
    }
    
    __block CGFloat currentBrightness = [self screenBrightness];
    
    [DBThread eseguiBlockSuThreadInBackground:^{
        // Animation parameters
        const CGFloat delta = 0.01;
        const CGFloat delay = 0.005;
        
        if (currentBrightness < safeBrightness) {
            // Increasing
            while (currentBrightness < safeBrightness) {
                currentBrightness += delta;
                
                [DBThread eseguiBlock:^{
                    [[UIScreen mainScreen] setBrightness:currentBrightness];
                }];
                
                [NSThread sleepForTimeInterval:delay];
            }
        }
        else if (currentBrightness > safeBrightness) {
            // Decreasing
            while (currentBrightness > safeBrightness) {
                currentBrightness -= delta;
                
                [DBThread eseguiBlock:^{
                    [[UIScreen mainScreen] setBrightness:currentBrightness];
                }];
                
                [NSThread sleepForTimeInterval:delay];
            }
        }
        
        [DBThread eseguiBlock:^{
            // Done
            [[UIScreen mainScreen] setBrightness:safeBrightness];
        }];
    }];
}


// MARK: - Operating system

+ (NSString *)identifierVersioneiOS
{
    return [[UIDevice currentDevice] systemVersion];
}

+ (NSString *)identifierBuildVersioneiOS
{
    int mib[2] = {CTL_KERN, KERN_OSVERSION};
    size_t size = 0;
    
    // Get the size for the buffer
    sysctl(mib, 2, NULL, &size, NULL, 0);
    
    char *answer = malloc(size);
    sysctl(mib, 2, answer, &size, NULL, 0);
    
    NSString *build = [NSString stringWithCString:answer encoding: NSUTF8StringEncoding];
    free(answer);
    
    return build;
}

+ (NSString *)identifierVersioneiOSCompleta
{
    return [NSString stringWithFormat:@"%@ (%@)", [self identifierVersioneiOS], [self identifierBuildVersioneiOS]];
}

+ (double)versioneiOS
{
    return [[self identifierVersioneiOS] doubleValue];
}

/**
 * Controlla se la versione di iOS in esecuzione è <b>esattamente</b> quella specificata.
 * @param versione Il numero della versione.
 */
+ (BOOL)isiOS:(double)versione
{
    return ([self versioneiOS] == versione) ? YES : NO;
}

/**
 * Controlla se la versione di iOS in esecuzione è <b>almeno</b> quella specificata.
 * @param versione Il numero della versione.
 */
+ (BOOL)isiOSMin:(double)versione
{
    return ([self versioneiOS] >= versione) ? YES : NO;
}

/**
 * Controlla se la versione di iOS in esecuzione è <b>al massimo</b> quella specificata.
 * @param versione Il numero della versione.
 */
+ (BOOL)isiOSMax:(double)versione
{
    return ([self versioneiOS] <= versione) ? YES : NO;
}

/**
 * Controlla se la versione major di iOS in esecuzione è <b>esattamente</b> quella specificata.
 * @param versione Il numero della versione.
 */
+ (BOOL)isiOSMajor:(int)versione
{
    return (parseInt([self versioneiOS]) == versione) ? YES : NO;
}

@end
