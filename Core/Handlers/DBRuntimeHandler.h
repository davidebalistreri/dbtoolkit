//
//  DBRuntimeHandler.h
//  File version: 1.1.0
//  Last modified: 03/14/2016
//
//  Created by Davide Balistreri on 03/13/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

/**
 * Questa classe permette di utilizzare le funzioni dinamiche Reflection di Objective-C.
 *
 * Alcuni riferimenti:
 * https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtPropertyIntrospection.html
 * https://developer.apple.com/library/ios/documentation/Cocoa/Conceptual/KeyValueObserving/Articles/KVOImplementation.html
 */

@interface NSObject (DBRuntimeHandler)

// MARK: - Oggetti

/**
 * Permette associare un nuovo oggetto all'istanza, o di sovrascriverlo, con una referenza debole (weak, assign).
 * <p>Al dealloc dell'istanza verrà automaticamente rilasciato.</p>
 */
- (void)associaOggettoWeak:(id)oggetto conSelector:(SEL)selector;

/**
 * Permette associare un nuovo oggetto all'istanza, o di sovrascriverlo, con una referenza forte (strong, retain).
 * <p>Al dealloc dell'istanza verrà automaticamente rilasciato.</p>
 */
- (void)associaOggettoStrong:(id)oggetto conSelector:(SEL)selector;

/**
 * Permette recuperare un oggetto precedentemente associato all'istanza.
 */
- (id)oggettoAssociatoConSelector:(SEL)selector;

// MARK: - Metodi

/**
 * Permette di controllare se un'istanza esegue l'override dell'implementazione di un metodo.
 */
- (BOOL)overridesSelector:(SEL)selector;

/**
 * Permette di controllare se un'istanza esegue l'override dell'implementazione di un metodo di una specifica superclasse.
 */
- (BOOL)overridesSelector:(SEL)selector ofClass:(Class)klass;

/**
 * Permette di controllare se una classe esegue l'override dell'implementazione di un metodo.
 */
+ (BOOL)overridesClassSelector:(SEL)selector;

/**
 * Permette di controllare se una classe esegue l'override dell'implementazione di un metodo di una specifica superclasse.
 */
+ (BOOL)overridesClassSelector:(SEL)selector ofClass:(Class)klass;

/**
 * Permette di cambiare a runtime l'implementazione di un metodo di classe.
 *
 * <b>Come funziona:</b>
 * <p>Se verrà richiamato il metodo specificato, sarà eseguito il codice dell'altro,
 * e se invece verrà richiamato l'altro, sarà eseguito il codice del metodo originale specificato.</p>
 *
 * <b>Precauzioni:</b>
 * <p>Questa funzione è particolarmente potente e pericolosa.</p>
 * <p>Una volta richiamata bisogna ricordarsi che, se si vuole richiamare il metodo originale, bisogna
 * richiamarlo con il nuovo metodo (perché sono stati scambiati), e viceversa.</p>
 *
 * <b>Effetti indesiderati:</b>
 * <p>Il valore di _cmd manterrà il nome del metodo originale.</p>
 */
+ (void)scambiaClassSelector:(SEL)selector conNuovoClassSelector:(SEL)nuovoSelector;

/**
 * Permette di cambiare a runtime l'implementazione di un metodo d'istanza.
 *
 * <b>Come funziona:</b> <br>
 * Se verrà richiamato il metodo specificato, sarà eseguito il codice dell'altro,
 * e se invece verrà richiamato l'altro, sarà eseguito il codice del metodo originale specificato.
 *
 * <b>Precauzioni:</b> <br>
 * Questa funzione è particolarmente potente e pericolosa. <br>
 * Una volta richiamata bisogna ricordarsi che, se si vuole richiamare il metodo originale, bisogna
 * richiamarlo con il nuovo metodo (perché sono stati scambiati), e viceversa.
 *
 * <b>Effetti indesiderati:</b> <br>
 * Il valore di _cmd manterrà il nome del metodo originale.
 */
+ (void)scambiaSelector:(SEL)selector conNuovoSelector:(SEL)nuovoSelector;

/**
 * Permette di cambiare a runtime l'implementazione di un metodo di classe, e di eseguirlo su un altro target.
 * Questa modifica sarà applicata a tutte le classi dello stesso tipo.
 *
 * <i>Approfondire il <b>Come funziona</b>, le <b>Precauzioni</b> e gli <b>Effetti indesiderati</b> leggendo la spiegazione del metodo +[NSObject scambiaClassSelector:conNuovoClassSelector:].</i>
 */
+ (void)scambiaClassSelector:(SEL)selector conNuovoClassSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget;

/**
 * Permette di cambiare a runtime l'implementazione di un metodo d'istanza, e di eseguirlo su un altro target.
 * Questa modifica sarà applicata a tutte le classi dello stesso tipo.
 *
 * <i>Approfondire il <b>Come funziona</b>, le <b>Precauzioni</b> e gli <b>Effetti indesiderati</b> leggendo la spiegazione del metodo +[NSObject scambiaSelector:conNuovoSelector:].</i>
 */
+ (void)scambiaSelector:(SEL)selector conNuovoSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget;

/**
 * Permette di cambiare a runtime l'implementazione di un metodo di classe, e di eseguirlo su un metodo d'istanza di un altro target.
 *
 * <i>Approfondire il <b>Come funziona</b>, le <b>Precauzioni</b> e gli <b>Effetti indesiderati</b> leggendo la spiegazione del metodo +[NSObject scambiaSelector:conNuovoSelector:].</i>
 */
+ (void)scambiaClassSelector:(SEL)selector conNuovoSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget;

/**
 * Permette di cambiare a runtime l'implementazione di un metodo d'istanza, e di eseguirlo su un metodo di classe di un altro target.
 *
 * <i>Approfondire il <b>Come funziona</b>, le <b>Precauzioni</b> e gli <b>Effetti indesiderati</b> leggendo la spiegazione del metodo +[NSObject scambiaSelector:conNuovoSelector:].</i>
 */
+ (void)scambiaSelector:(SEL)selector conNuovoClassSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget;

// MARK: - Classi

/**
 * Permette di cambiare a runtime la classe di un'istanza.
 * <p>Questa tecnologia si chiama <i>isa-swizzling</i> ed è utilizzata da Apple per implementare i KVO automatici.</p>
 * <p>Con questa tecnologia è possibile cambiare la classe di un oggetto dinamicamente, ad esempio estendendolo e
 * sovrascrivendo i suoi metodi e le sue proprietà, con la possibilità però di richiamare i metodi originali attraverso il super.</p>
 */
- (void)cambiaClasseIstanza:(Class)nuovaClasse;

@end


@interface NSObject (DBRuntimeHandlerEsteso)

// MARK: - Metodi estesi

/// <i>La descrizione completa di questo metodo è nel metodo di classe con lo stesso nome.</i>
- (void)scambiaClassSelector:(SEL)selector conNuovoClassSelector:(SEL)nuovoSelector;

/// <i>La descrizione completa di questo metodo è nel metodo di classe con lo stesso nome.</i>
- (void)scambiaSelector:(SEL)selector conNuovoSelector:(SEL)nuovoSelector;

/// <i>La descrizione completa di questo metodo è nel metodo di classe con lo stesso nome.</i>
- (void)scambiaClassSelector:(SEL)selector conNuovoClassSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget;

/// <i>La descrizione completa di questo metodo è nel metodo di classe con lo stesso nome.</i>
- (void)scambiaSelector:(SEL)selector conNuovoSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget;

/// <i>La descrizione completa di questo metodo è nel metodo di classe con lo stesso nome.</i>
- (void)scambiaClassSelector:(SEL)selector conNuovoSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget;

/// <i>La descrizione completa di questo metodo è nel metodo di classe con lo stesso nome.</i>
- (void)scambiaSelector:(SEL)selector conNuovoClassSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget;

@end
