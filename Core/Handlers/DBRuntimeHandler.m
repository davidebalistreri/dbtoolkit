//
//  DBRuntimeHandler.m
//  File version: 1.1.0
//  Last modified: 03/14/2016
//
//  Created by Davide Balistreri on 03/13/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBRuntimeHandler.h"

#import <objc/runtime.h>

@implementation NSObject (DBRuntimeHandler)

// MARK: - Oggetti

/**
 * Permette associare un nuovo oggetto all'istanza, o di sovrascriverlo, con una referenza debole (weak, assign).
 * <p>Al dealloc dell'istanza verrà automaticamente rilasciato.</p>
 */
- (void)associaOggettoWeak:(id)oggetto conSelector:(SEL)selector
{
    objc_setAssociatedObject(self, selector, oggetto, OBJC_ASSOCIATION_ASSIGN);
}

/**
 * Permette associare un nuovo oggetto all'istanza, o di sovrascriverlo, con una referenza forte (strong, retain).
 * <p>Al dealloc dell'istanza verrà automaticamente rilasciato.</p>
 */
- (void)associaOggettoStrong:(id)oggetto conSelector:(SEL)selector
{
    objc_setAssociatedObject(self, selector, oggetto, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

/**
 * Permette recuperare un oggetto precedentemente associato all'istanza.
 */
- (id)oggettoAssociatoConSelector:(SEL)selector
{
    return objc_getAssociatedObject(self, selector);
}

// MARK: - Metodi

/**
 * Permette di controllare se un'istanza esegue l'override dell'implementazione di un metodo.
 */
- (BOOL)overridesSelector:(SEL)selector
{
    Class classe  = [self class];
    Method metodo = class_getInstanceMethod(classe, selector);
    
    Class superclasse = [self superclass];
    
    while (superclasse != Nil) {
        Method supermetodo = class_getInstanceMethod(superclasse, selector);
        if (metodo != supermetodo && supermetodo) {
            // Il metodo dell'istanza è diverso da quello della superclasse
            return YES;
        }
        
        // Continuo la ricerca...
        superclasse = [superclasse superclass];
    }
    
    // L'istanza non esegue l'override del metodo specificato
    return NO;
}

/**
 * Permette di controllare se un'istanza esegue l'override dell'implementazione di un metodo di una specifica superclasse.
 */
- (BOOL)overridesSelector:(SEL)selector ofClass:(Class)klass
{
    Class classe1  = [self class];
    Method metodo1 = class_getInstanceMethod(classe1, selector);
    
    Class classe2  = klass;
    Method metodo2 = class_getInstanceMethod(classe2, selector);
    
    if (metodo1 != metodo2 && metodo2) {
        // Il metodo dell'istanza è diverso da quello della classe specificata
        return YES;
    }
    
    // L'istanza non esegue l'override del metodo specificato
    return NO;
}

/**
 * Permette di controllare se una classe esegue l'override dell'implementazione di un metodo.
 */
+ (BOOL)overridesClassSelector:(SEL)selector
{
    Class classe  = self;
    Method metodo = class_getClassMethod(classe, selector);
    
    Class superclasse = [self superclass];
    
    while (superclasse != Nil) {
        Method supermetodo = class_getClassMethod(superclasse, selector);
        if (metodo != supermetodo && supermetodo) {
            // Il metodo della classe è diverso da quello della classe specificata
            return YES;
        }
        
        // Continuo la ricerca...
        superclasse = [superclasse superclass];
    }
    
    // La classe non esegue l'override del metodo specificato
    return NO;
}

/**
 * Permette di controllare se una classe esegue l'override dell'implementazione di un metodo di una specifica superclasse.
 */
+ (BOOL)overridesClassSelector:(SEL)selector ofClass:(Class)klass
{
    Class classe1  = self;
    Method metodo1 = class_getClassMethod(classe1, selector);
    
    Class classe2  = klass;
    Method metodo2 = class_getClassMethod(classe2, selector);
    
    if (metodo1 != metodo2 && metodo2) {
        // Il metodo della classe è diverso da quello della classe specificata
        return YES;
    }
    
    // La classe non esegue l'override del metodo specificato
    return NO;
}

/**
 * Permette di cambiare l'implementazione di un metodo di classe o d'istanza.
 *
 * <b>Come funziona:</b><br>
 * Quando verrà richiamato il metodo specificato, sarà invece eseguito il codice dell'altro.
 * Al contrario se verrà richiamato l'altro, sarà eseguito il codice del metodo originale specificato.
 *
 * <b>Precauzioni:</b><br>
 * Questa funzione è particolarmente potente e pericolosa. <br>
 * Una volta richiamata bisogna ricordarsi che, se si vuole richiamare il metodo originale, bisogna
 * richiamarlo con il nome del nuovo metodo (perché sono stati scambiati), e viceversa.
 */
+ (void)scambiaMetodo:(Method)metodo conNuovoMetodo:(Method)nuovoMetodo
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (metodo && nuovoMetodo) {
            method_exchangeImplementations(metodo, nuovoMetodo);
        }
    });
}

+ (void)scambiaClassSelector:(SEL)selector conNuovoClassSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget
{
    Class _target      = [self class];
    Class _nuovoTarget = [nuovoTarget class];
    
    Method metodo = class_getClassMethod(_target, selector);
    Method nuovoMetodo = class_getClassMethod(_nuovoTarget, nuovoSelector);
    
    [self scambiaMetodo:metodo conNuovoMetodo:nuovoMetodo];
}

+ (void)scambiaSelector:(SEL)selector conNuovoSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget
{
    Class _target      = [self class];
    Class _nuovoTarget = [nuovoTarget class];
    
    Method metodo = class_getInstanceMethod(_target, selector);
    Method nuovoMetodo = class_getInstanceMethod(_nuovoTarget, nuovoSelector);
    
    [self scambiaMetodo:metodo conNuovoMetodo:nuovoMetodo];
}

+ (void)scambiaClassSelector:(SEL)selector conNuovoSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget
{
    Class _target      = [self class];
    Class _nuovoTarget = [nuovoTarget class];
    
    Method metodo = class_getClassMethod(_target, selector);
    Method nuovoMetodo = class_getInstanceMethod(_nuovoTarget, nuovoSelector);
    
    [self scambiaMetodo:metodo conNuovoMetodo:nuovoMetodo];
}

+ (void)scambiaSelector:(SEL)selector conNuovoClassSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget
{
    Class _target      = [self class];
    Class _nuovoTarget = [nuovoTarget class];
    
    Method metodo = class_getInstanceMethod(_target, selector);
    Method nuovoMetodo = class_getClassMethod(_nuovoTarget, nuovoSelector);
    
    [self scambiaMetodo:metodo conNuovoMetodo:nuovoMetodo];
}

+ (void)scambiaClassSelector:(SEL)selector conNuovoClassSelector:(SEL)nuovoSelector
{
    [self scambiaClassSelector:selector conNuovoClassSelector:nuovoSelector nuovoTarget:self];
}

+ (void)scambiaSelector:(SEL)selector conNuovoSelector:(SEL)nuovoSelector
{
    [self scambiaSelector:selector conNuovoSelector:nuovoSelector nuovoTarget:self];
}

// MARK: - Classi

- (void)cambiaClasseIstanza:(Class)nuovaClasse
{
    object_setClass(self, nuovaClasse);
}

@end


@implementation NSObject (DBRuntimeHandlerEsteso)

// MARK: - Metodi estesi

- (void)scambiaClassSelector:(SEL)selector conNuovoClassSelector:(SEL)nuovoSelector
{
    [[self class] scambiaClassSelector:selector conNuovoClassSelector:nuovoSelector];
}

- (void)scambiaSelector:(SEL)selector conNuovoSelector:(SEL)nuovoSelector
{
    [[self class] scambiaSelector:selector conNuovoSelector:nuovoSelector];
}

- (void)scambiaClassSelector:(SEL)selector conNuovoClassSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget
{
    [[self class] scambiaSelector:selector conNuovoClassSelector:nuovoSelector nuovoTarget:nuovoTarget];
}

- (void)scambiaSelector:(SEL)selector conNuovoSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget
{
    [[self class] scambiaSelector:selector conNuovoSelector:nuovoSelector nuovoTarget:nuovoTarget];
}

- (void)scambiaClassSelector:(SEL)selector conNuovoSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget
{
    [[self class] scambiaClassSelector:selector conNuovoSelector:nuovoSelector nuovoTarget:nuovoTarget];
}

- (void)scambiaSelector:(SEL)selector conNuovoClassSelector:(SEL)nuovoSelector nuovoTarget:(id)nuovoTarget
{
    [[self class] scambiaSelector:selector conNuovoClassSelector:nuovoSelector nuovoTarget:nuovoTarget];
}

@end


/// In costruzione...
@interface DBRuntimeSelectorScambiato : NSObject

@property (nonatomic, readonly) SEL selectorOriginale;
@property (nonatomic, readonly) SEL selectorScambiato;
@property (weak, nonatomic, readonly) id targetOriginale;
@property (weak, nonatomic, readonly) id targetScambiato;
@property (nonatomic, readonly) id classeOriginale;
@property (nonatomic, readonly) id classeScambiato;

@property (nonatomic) BOOL attivo;

- (void)ripristinaOriginale;

@end
