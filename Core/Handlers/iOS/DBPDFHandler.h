//
//  DBPDFHandler.h
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 02/25/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

typedef NS_ENUM(NSUInteger, DBPDFHandlerContentMode) {
    // Adatta ogni pagina per riempire il foglio, sia in larghezza che in altezza (default)
    DBPDFHandlerContentModeScaleToFill,
    // Adatta ogni pagina mantenendo le proporzioni e centrando tutti gli oggetti al suo interno
    DBPDFHandlerContentModeAspectFit,
    // Adatta ogni pagina mantenendo le proporzioni riempiendo la larghezza o l'altezza, alcuni oggetti potrebbero essere tagliati
    DBPDFHandlerContentModeAspectFill
};


@interface DBPDFHandler : NSObject

/// Imposta il colore da utilizzare per alcuni oggetti, ad esempio per il Footer
+ (void)impostaTintColor:(UIColor *)tintColor;

/// In percentuale da 0 a 100, imposta la percentuale del foglio da riservare al margine
+ (void)impostaPercentualeMargine:(NSInteger)percentualeMargine;

/// Imposta il frame del foglio A4 internazionale
+ (void)impostaFrameFoglioA4;

/// Imposta il frame del foglio Letter americano
+ (void)impostaFrameFoglioAmericano;

/// Imposta l'adattamento degli oggetti da stampare sul foglio
+ (void)impostaContentMode:(DBPDFHandlerContentMode)contentMode;

+ (void)impostaFooterConTesto:(NSString *)testo conFont:(UIFont *)font conLogo:(UIImage *)logo stampaNumeroPagine:(BOOL)stampaNumeroPagine;
+ (void)rimuoviFooter;

// Ancora da implementare
// + (NSString *)creaPDFFormatoA4ConVista:(UIView *)vista conNomeFile:(NSString *)nomeFile;

+ (NSString *)creaPDFFormatoA4ConVista:(UIView *)vista conNomeFile:(NSString *)nomeFile conPagine:(NSArray *)pagine;

+ (NSValue *)creaPaginaConRect:(CGRect)rect;

// Ancora da implementare
// + (NSString *)creaPDFFormatoA4ConTableViewController:(UITableViewController *)tableViewController conNomeFile:(NSString *)nomeFile;

@end
