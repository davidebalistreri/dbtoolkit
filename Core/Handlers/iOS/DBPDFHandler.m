//
//  DBPDFHandler.m
//  File version: 1.0.0
//  Last modified: 03/01/2016
//
//  Created by Davide Balistreri on 02/25/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBPDFHandler.h"
#import "DBFramework.h"

@interface DBPDFHandler ()

/// Colore utilizzato per alcuni oggetti, ad esempio per il Footer
@property (strong, nonatomic) UIColor *tintColor;

/// Adattamento dell'altezza degli oggetti
@property (nonatomic) CGFloat zoomAltezza;

/// Adattamento della larghezza degli oggetti
@property (nonatomic) CGFloat zoomLarghezza;

/// Adattamento degli oggetti da stampare sul foglio
@property (nonatomic) DBPDFHandlerContentMode contentMode;

/// Percentuale del foglio da riservare al margine
@property (nonatomic) CGFloat percentualeMargine;

/// Frame del foglio
@property (nonatomic) CGRect frameFoglio;

/// Frame del foglio comprensivo dei margini
@property (nonatomic) CGRect frameFoglioConMargini;

/* FOOTER */
@property (nonatomic, getter = isFooterAbilitato) BOOL footerAbilitato;
@property (nonatomic) BOOL footerStampaNumeroPagine;
@property (strong, nonatomic) UIFont *footerFont;
@property (strong, nonatomic) NSString *footerTesto;
@property (strong, nonatomic) UIImage *footerLogo;

@end


@implementation DBPDFHandler

DBPDFHandler *_dbSharedPDFHandler;
+ (DBPDFHandler *)sharedInstance
{
    @synchronized(self) {
        if (!_dbSharedPDFHandler) {
            _dbSharedPDFHandler = [DBPDFHandler new];
            
            // Inizializzazione
            [DBPDFHandler impostaContentMode:DBPDFHandlerContentModeScaleToFill];
            [DBPDFHandler impostaFrameFoglioA4];
            [DBPDFHandler impostaPercentualeMargine:5];
            [DBPDFHandler impostaTintColor:[DBUtility tintColor]];
        }
        
        return _dbSharedPDFHandler;
    }
}

// MARK: - Setup

/// Imposta il colore da utilizzare per alcuni oggetti, ad esempio per il Footer
+ (void)impostaTintColor:(UIColor *)tintColor
{
    [[DBPDFHandler sharedInstance] setTintColor:tintColor];
}

/// Imposta l'adattamento degli oggetti da stampare sul foglio
+ (void)impostaContentMode:(DBPDFHandlerContentMode)contentMode
{
    [[DBPDFHandler sharedInstance] setContentMode:contentMode];
}

/// In percentuale da 0 a 100, imposta la percentuale del foglio da riservare al margine
+ (void)impostaPercentualeMargine:(NSInteger)percentualeMargine
{
    [[DBPDFHandler sharedInstance] setPercentualeMargine:(percentualeMargine / 100.0)];
}

/// Imposta il frame del foglio A4 internazionale
+ (void)impostaFrameFoglioA4
{
    [[DBPDFHandler sharedInstance] setFrameFoglio:CGRectMake(0.0f, 0.0f, 595.0f, 842.0f)];
}

/// Imposta il frame del foglio Letter americano
+ (void)impostaFrameFoglioAmericano
{
    [[DBPDFHandler sharedInstance] setFrameFoglio:CGRectMake(0.0f, 0.0f, 595.0f, 700.0f)];
}

+ (void)impostaFooterConTesto:(NSString *)testo conFont:(UIFont *)font conLogo:(UIImage *)logo stampaNumeroPagine:(BOOL)stampaNumeroPagine
{
    DBPDFHandler *handler = [DBPDFHandler sharedInstance];
    handler.footerAbilitato = YES;
    handler.footerTesto = testo;
    handler.footerFont = font;
    handler.footerLogo = logo;
    handler.footerStampaNumeroPagine = stampaNumeroPagine;
}

+ (void)rimuoviFooter
{
    [[DBPDFHandler sharedInstance] setFooterAbilitato:NO];
}

+ (NSValue *)creaPaginaConRect:(CGRect)rect
{
    return [NSValue valueWithCGRect:rect];
}

// MARK: - Stampa

//+ (NSString *)creaPDFFormatoA4ConTableViewController:(UITableViewController *)tableViewController conNomeFile:(NSString *)nomeFile
//{
//    // Trasformo la TableView in una View, separandola per pagine
//
//    UITableView *tableView = tableViewController.tableView;
//    CGPoint offsetTableView = tableView.contentOffset;
//    tableView.contentOffset = CGPointMake(0, 0);
//    [tableView setNeedsDisplay];
//    CGRect frameVista = tableView.bounds;
//    CGRect framePagina = [self getFramePagina];
//
//    // Calcolo zoom per adattare la vista al foglio A4
//    _pdfHandlerZoom = 1.0f + ((framePagina.size.width - frameVista.size.width) / frameVista.size.width);
//
//    CGFloat altezzaPagina = (framePagina.size.height * 0.95) / _pdfHandlerZoom;
//    NSMutableArray *pagine = [NSMutableArray arrayWithObject:[NSValue valueWithCGRect:CGRectZero]];
//
//    UIView *view = [[UIView alloc] initWithFrame:tableView.bounds];
//
//    CGRect frameView = view.frame;
//    frameView.size.height = 0;
//
//    CGRect framePaginaAttuale = frameView;
//
//    for (NSInteger counterSections = 0; counterSections < [tableView numberOfSections]; counterSections++) {
//        // Header
//        UIView *header = [tableViewController tableView:tableView viewForHeaderInSection:counterSections];
//        CGRect frameHeader = header.frame;
//        frameHeader.origin.y = frameView.size.height;
//        frameHeader.size.height = [tableViewController tableView:tableView heightForHeaderInSection:counterSections];
//        header.frame = frameHeader;
//
//        [view addSubview:header];
//
//        frameView.size.height += frameHeader.size.height;
//
//        // Check frame pagina attuale
//        framePaginaAttuale.size.height += frameHeader.size.height;
//        if (framePaginaAttuale.size.height <= altezzaPagina) {
//            // Continuo sulla stessa pagina
//            NSInteger index = [pagine indexOfObject:[pagine lastObject]];
//            NSValue *value = [NSValue valueWithCGRect:framePaginaAttuale];
//            [pagine replaceObjectAtIndex:index withObject:value];
//        } else {
//            // Nuova pagina
//            framePaginaAttuale.origin.y += framePaginaAttuale.size.height - frameHeader.size.height;
//            framePaginaAttuale.size.height = frameHeader.size.height;
//            [pagine addObject:[NSValue valueWithCGRect:framePaginaAttuale]];
//        }
//
//        // Celle
//        for (NSInteger counterRows = 0; counterRows < [tableView numberOfRowsInSection:counterSections]; counterRows++) {
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:counterRows inSection:counterSections];
//
//            // Debug
//            // NSLog(@"Sezione %d Cella %d", (int) counterSections, (int) counterRows);
//
//            UIView *cella = [tableViewController tableView:tableView cellForRowAtIndexPath:indexPath];
//            CGRect frameCella = cella.frame;
//            frameCella.origin.y = frameView.size.height;
//            frameCella.size.height = [tableViewController tableView:tableView heightForRowAtIndexPath:indexPath];
//
//            frameView.size.height += frameCella.size.height;
//
//            // Check frame pagina attuale
//            framePaginaAttuale.size.height += frameCella.size.height;
//            if (framePaginaAttuale.size.height <= altezzaPagina) {
//                // Continuo sulla stessa pagina
//                NSInteger index = [pagine indexOfObject:[pagine lastObject]];
//                NSValue *value = [NSValue valueWithCGRect:framePaginaAttuale];
//                [pagine replaceObjectAtIndex:index withObject:value];
//            } else {
//                // Nuova pagina
//                framePaginaAttuale.origin.y += framePaginaAttuale.size.height - frameCella.size.height;
//                framePaginaAttuale.size.height = frameCella.size.height;
//                [pagine addObject:[NSValue valueWithCGRect:framePaginaAttuale]];
//
//                // Creo un nuovo header
//                UIView *replicaHeader = [tableViewController tableView:tableView viewForHeaderInSection:counterSections];
//                CGRect frameHeaderReplica = replicaHeader.frame;
//                frameHeaderReplica.origin.y = frameCella.origin.y;
//                frameHeaderReplica.size.height = [tableViewController tableView:tableView heightForHeaderInSection:counterSections];
//                replicaHeader.frame = frameHeaderReplica;
//
//                [view addSubview:replicaHeader];
//
//                frameView.size.height += frameHeaderReplica.size.height;
//                frameCella.origin.y += frameHeaderReplica.size.height;
//                framePaginaAttuale.size.height += frameHeaderReplica.size.height;
//            }
//
//            cella.frame = frameCella;
//            [view addSubview:cella];
//        }
//    }
//
//    view.frame = frameView;
//    tableView.contentOffset = offsetTableView;
//
//    return [self creaPDFFormatoA4ConVista:view conNomeFile:nomeFile conPagine:pagine];
//}

+ (NSString *)creaPDFFormatoA4ConVista:(UIView *)vista conNomeFile:(NSString *)nomeFile conPagine:(NSArray *)pagine
{
    DBPDFHandler *handler = [DBPDFHandler sharedInstance];
    
    CGRect frameFoglio = handler.frameFoglio;
    
    //    CGRect frameVista = vista.bounds;
    //
    //    // Calcolo zoom per adattare la vista al foglio A4
    //    _pdfHandlerZoom = 1.0f + ((framePagina.size.width - frameVista.size.width) / frameVista.size.width);
    
    NSInteger numeroPagine = pagine.count;
    
    // NSData contenente il PDF
    NSMutableData *data = [NSMutableData data];
    
    @autoreleasepool {
        // Points the pdf converter to the mutable data object and to the UIView to be converted
        UIGraphicsBeginPDFContextToData(data, frameFoglio, nil);
        
        for (NSInteger counter = 0; counter < numeroPagine; counter++) {
            
            // Calcolo il frame della pagina adattato al foglio A4
            CGRect framePagina = [pagine[counter] CGRectValue];
            
            // Calcolo i fattori di adattamento per la pagina corrente
            CGFloat marginiLarghezza = frameFoglio.size.width * (handler.percentualeMargine * 2.0);
            CGFloat marginiAltezza = frameFoglio.size.height * (handler.percentualeMargine * 2.0);
            handler.zoomLarghezza = (frameFoglio.size.width - marginiLarghezza) / framePagina.size.width;
            handler.zoomAltezza = (frameFoglio.size.height - marginiAltezza) / framePagina.size.height;
            
            if (handler.contentMode == DBPDFHandlerContentModeAspectFit) {
                handler.zoomLarghezza = fmin(handler.zoomLarghezza, handler.zoomAltezza);
                handler.zoomAltezza = handler.zoomLarghezza;
                
            } else if (handler.contentMode == DBPDFHandlerContentModeAspectFill) {
                handler.zoomLarghezza = fmax(handler.zoomLarghezza, handler.zoomAltezza);
                handler.zoomAltezza = handler.zoomLarghezza;
            }
            
            // Nascondo le parti fuori dal frame della pagina
            // CGFloat margine = framePagina.size.width * 0.1f;
            // CGFloat microMargine = 0.4f; // Copre mezzo pixel che viene fuori in stampa
            
            UIView *coverTop = [UIView new];
            // [coverTop setFrame:CGRectMake(-margine, -margine, framePagina.size.width +margine+margine+microMargine, framePagina.origin.y +margine+microMargine)];
            [coverTop setBackgroundColor:[UIColor whiteColor]];
            // [vista addSubview:coverTop];
            
            UIView *coverBottom = [UIView new];
            // [coverBottom setFrame:CGRectMake(-margine, framePagina.origin.y + framePagina.size.height -microMargine, framePagina.size.width +margine+margine+microMargine, frameVista.size.height +microMargine)];
            [coverBottom setBackgroundColor:[UIColor whiteColor]];
            // [vista addSubview:coverBottom];
            
            // Calcolo il frame della pagina adattato al foglio A4
            frameFoglio.origin.y = -(framePagina.origin.y * handler.zoomAltezza);
            
            // Aggiungo il footer
            UIView *footer = [self creaFooterConFrame:frameFoglio conNumeroPagina:counter+1 conPagineTotali:numeroPagine];
            [vista addSubview:footer];
            
            UIGraphicsBeginPDFPageWithInfo(frameFoglio, nil);
            [handler disegnaVista:vista conVistaParent:vista conNumeroPagina:counter+1];
            
            [footer removeFromSuperview];
            [coverTop removeFromSuperview];
            [coverBottom removeFromSuperview];
        }
        
        // Remove PDF rendering context
        UIGraphicsEndPDFContext();
    }
    
    // Retrieves the document directories from the iOS device
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentDirectory = [documentDirectories firstObject];
    NSString *documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:nomeFile];
    
    // Instructs the mutable data object to write its context to a file on disk
    [data writeToFile:documentDirectoryFilename atomically:YES];
    
    NSLog(@"documentDirectoryFileName: %@", documentDirectoryFilename);
    
    return documentDirectoryFilename;
}

//+ (NSString *)creaPDFFormatoA4ConVista:(UIView *)vista conNomeFile:(NSString *)nomeFile
//{
//    CGRect frameVista = vista.bounds;
//    CGRect framePagina = [self getFramePagina];
//
//    // Calcolo zoom per adattare la vista al foglio A4
//    _pdfHandlerZoom = 1.0f + ((framePagina.size.width - frameVista.size.width) / frameVista.size.width);
//
//    // Calcolo il numero di pagine del PDF in base all'altezza della vista
//    NSInteger numeroPagine = frameVista.size.height / framePagina.size.height;
//
//    // Creates a mutable data object for updating with binary data, like a byte array
//    NSMutableData *pdfData = [NSMutableData data];
//
//    @autoreleasepool {
//        // Points the pdf converter to the mutable data object and to the UIView to be converted
//        UIGraphicsBeginPDFContextToData(pdfData, framePagina, nil);
//
//        for (NSInteger counter = 0; counter < numeroPagine; counter++) {
//            UIGraphicsBeginPDFPageWithInfo(framePagina, nil);
//
//            UIView *footer = [self creaFooterConFrame:framePagina conNumeroPagina:counter+1 conPagineTotali:numeroPagine];
//            [vista addSubview:footer];
//            [self disegnaVista:vista conVistaParent:vista conNumeroPagina:counter+1];
//            [footer removeFromSuperview];
//
//            framePagina.origin.y -= framePagina.size.height;
//        }
//
//        // Remove PDF rendering context
//        UIGraphicsEndPDFContext();
//    }
//
//    // Retrieves the document directories from the iOS device
//    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//
//    NSString *documentDirectory = [documentDirectories firstObject];
//    NSString *documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:nomeFile];
//
//    // Instructs the mutable data object to write its context to a file on disk
//    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
//
//    NSLog(@"documentDirectoryFileName: %@", documentDirectoryFilename);
//
//    return documentDirectoryFilename;
//}

+ (UIView *)creaFooterConFrame:(CGRect)frame conNumeroPagina:(NSInteger)numeroPagina conPagineTotali:(NSInteger)pagineTotali
{
    DBPDFHandler *handler = [DBPDFHandler sharedInstance];
    
    if (!handler.isFooterAbilitato)
        return [UIView new];
    
    // Il footer viene calcolato con la risoluzione del foglio,
    // quindi andrà adattato alla risoluzione della pagina (la vista da stampare)
    //
    //    CGFloat marginiLarghezza = frame.size.width * (handler.percentualeMargine * 2.0);
    //    CGFloat marginiAltezza = frame.size.height * (handler.percentualeMargine * 2.0);
    //    frame.origin.x += marginiLarghezza / handler.zoomLarghezza;
    //    frame.origin.y += marginiAltezza / handler.zoomAltezza;
    //    frame.size.width  -= marginiLarghezza / handler.zoomLarghezza;
    //    frame.size.height -= marginiAltezza / handler.zoomAltezza;
    //
    
    // Vista principale
    CGFloat altezzaFooter = frame.size.height * 0.03;  // 3%
    CGRect frameFooter = [DBGeometry frame:frame conAltezza:altezzaFooter];
    // frameFooter.origin.y = fabs(frame.origin.y) + (frame.size.height - altezzaFooter);
    frameFooter.origin.y = frame.origin.y + frame.size.height - altezzaFooter;  // Prova
    
    frameFooter.origin.x /= handler.zoomLarghezza;
    frameFooter.origin.y /= handler.zoomAltezza;
    frameFooter.size.width  = 595 / handler.zoomLarghezza;
    frameFooter.size.height /= handler.zoomAltezza;
    
    UIView *viewFooter = [[UIView alloc] initWithFrame:frameFooter];
    viewFooter.backgroundColor = [UIColor yellowColor];
    
    // Linea orizzontale
    CGRect frameLinea = CGRectMake(0, altezzaFooter, frameFooter.size.width, 2.0f * handler.zoomAltezza);
    frameLinea.origin.y -= frameLinea.size.height;
    
    UILabel *labelLinea = [[UILabel alloc] initWithFrame:frameLinea];
    labelLinea.backgroundColor = handler.tintColor;
    [viewFooter addSubview:labelLinea];
    
    // Testo footer
    CGRect frameTesto = CGRectMake(0, 0, frameLinea.size.width, 20.0f);
    frameTesto.size.height = [@"Test" sizeWithAttributes:@{NSFontAttributeName:handler.footerFont}].height;
    frameTesto.origin = frameLinea.origin;
    frameTesto.origin.y -= frameTesto.size.height + (frameLinea.size.height * 2.0f);
    
    if (![NSString isEmpty:handler.footerTesto]) {
        UILabel *labelTesto = [[UILabel alloc] initWithFrame:frameTesto];
        labelTesto.text = handler.footerTesto;
        labelTesto.font = handler.footerFont;
        labelTesto.textColor = handler.tintColor;
        [viewFooter addSubview:labelTesto];
    }
    
    // Numero di pagina
    if (handler.footerStampaNumeroPagine) {
        UILabel *labelNumeroPagina = [[UILabel alloc] initWithFrame:frameTesto];
        labelNumeroPagina.text = [NSString stringWithFormat:@"%d/%d", (int) numeroPagina, (int) pagineTotali];
        labelNumeroPagina.font = handler.footerFont;
        labelNumeroPagina.textColor = handler.tintColor;
        labelNumeroPagina.textAlignment = NSTextAlignmentCenter;
        [viewFooter addSubview:labelNumeroPagina];
    }
    
    // Logo footer
    if (handler.footerLogo) {
        CGFloat heightLogo = frameLinea.origin.y + frameLinea.size.height - frameTesto.origin.y;
        CGRect frameLogo = CGRectMake(frameLinea.origin.x, 0, frameLinea.size.width * 0.5, heightLogo);
        frameLogo.size = CGSizeAspectFit(handler.footerLogo.size, frameLogo.size);
        frameLogo.origin.x += frameLinea.size.width - frameLogo.size.width;
        frameLogo.origin.y = frameLinea.origin.y + frameLinea.size.height - frameLogo.size.height;
        
        UIImageView *imageLogo = [[UIImageView alloc] initWithFrame:frameLogo];
        imageLogo.contentMode = UIViewContentModeScaleAspectFit;
        imageLogo.image = handler.footerLogo;
        [viewFooter addSubview:imageLogo];
        
        // Fix frame linea
        frameLinea.size.width -= 10.0f + frameLogo.size.width;
        labelLinea.frame = frameLinea;
    }
    
    return viewFooter;
}

- (void)disegnaVista:(UIView *)vista conVistaParent:(UIView *)vistaParent conNumeroPagina:(NSInteger)numeroPagina
{
    // Check visibilità della vista
    if (!vista || vista.hidden || vista.alpha <= 0.0f) return;
    
    CGRect frameFoglio = self.frameFoglio;
    CGRect frame = vista.frame;
    frame.origin = [DBGeometry calcolaOrigineReale:vista conVistaParent:vistaParent];
    
    // Check visibilità del frame della vista
    // if (framePagina.origin.x + framePagina.size.width <= 0.0f || framePagina.origin.y + framePagina.size.height <= 0.0f || framePagina.size.width <= 0.0f || framePagina.size.height <= 0.0f) return;
    
    @autoreleasepool {
        // Calcolo la correzione per adattare il frame al foglio A4, tenendo conto dei margini
        // CGFloat marginiA4 = frameFoglio.size.width * (self.percentualeMargine * 2.0f);
        // CGFloat larghezzaVistaA4 = vistaParent.bounds.size.width;
        
        // Aggiungo il margine destro
        CGFloat margineX = frameFoglio.size.width * self.percentualeMargine;
        frame.origin.x += margineX / self.zoomLarghezza;
        
        // AltezzaA4Fixed è il valore per calcolare il margine verticale, altrimenti non funziona... Ancora non abbiamo capito il perché!
        CGFloat altezzaA4fixed = frameFoglio.size.height - (frameFoglio.size.height * self.percentualeMargine) - 10.0f;
        
        CGFloat margineY = altezzaA4fixed * self.percentualeMargine;
        CGFloat numeroRipetizioni = (numeroPagina * 2.0f) - 1.0f;
        frame.origin.y += (margineY * numeroRipetizioni) / self.zoomAltezza;
        
        // Aggiungo i margini
        frame.origin.x *= self.zoomLarghezza;
        frame.origin.y *= self.zoomAltezza;
        frame.size.width *= self.zoomLarghezza;
        frame.size.height *= self.zoomAltezza;
        
        // Debug
        // NSLog(@"%@ %d", [vista class], (int)vista.subviews.count);
        
        if ([vista isKindOfClass:[UIButton class]]) {
            // ...
            
        } if ([vista isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (UIImageView *) vista;
            
            if (imageView.image) {
                // Bisogna tener conto del ContentMode dell'ImageView
                CGSize imageSize = imageView.image.size;
                CGSize frameSize = frame.size;
                
                CGSize frameResizedSize = frameSize;
                
                if (imageView.contentMode == UIViewContentModeScaleAspectFit)
                    frameResizedSize = CGSizeAspectFit(imageSize, frameSize);
                
                if (imageView.contentMode == UIViewContentModeScaleAspectFill)
                    frameResizedSize = CGSizeAspectFill(imageSize, frameSize);
                
                CGFloat differenzaX = frameSize.width - frameResizedSize.width;
                CGFloat differenzaY = frameSize.height - frameResizedSize.height;
                
                frame.origin.x += differenzaX / 2.0f;
                frame.origin.y += differenzaY / 2.0f;
                frame.size = frameResizedSize;
                
                [imageView.image drawInRect:frame];
            }
            
        } else if ([vista isKindOfClass:[UILabel class]] || [vista isMemberOfClass:[UITextView class]]) {
            UILabel *label = (UILabel *) vista;
            UITextView *textView = ([vista isMemberOfClass:[UITextView class]]) ? (UITextView *) vista : nil;
            
            // Border
            if (label.layer.borderWidth > 0.0f) {
                [[UIColor clearColor] setFill];
                [[UIColor colorWithCGColor:label.layer.borderColor] setStroke];
                CGContextStrokeRectWithWidth(UIGraphicsGetCurrentContext(), frame, (label.layer.borderWidth * 2.0f) * self.zoomLarghezza);
            }
            
            // Background
            if (label.backgroundColor && ![label.backgroundColor isEqual:[UIColor clearColor]]) {
                double mixedAlpha = fmin(label.alpha, label.backgroundColor.alpha);
                UIColor *coloreConAlpha = [label.backgroundColor colorWithAlphaComponent:mixedAlpha];
                [coloreConAlpha set];
                CGContextFillRect(UIGraphicsGetCurrentContext(), frame);
            }
            
            // Testo
            if (label.text.length > 0 && ![label.textColor isEqual:[UIColor clearColor]]) {
                
                // Per prendere i dati dalla TextView, deve essere "selectable"
                BOOL selectable;
                if (textView) {
                    selectable = textView.selectable;
                    textView.selectable = YES;
                }
                
                // Replico le caratteristiche della label originale
                NSString *text = label.text;
                if (!text) text = @"";
                
                UIColor *textColor = label.textColor;
                if (!textColor) textColor = [UIColor blackColor];
                
                UIFont *font = [UIFont fontWithName:label.font.fontName size:((label.font.pointSize - 0.5f) * fmin(self.zoomLarghezza, self.zoomAltezza))];
                
                NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
                [paragraphStyle setAlignment:label.textAlignment];
                
                NSDictionary *attributes = @{NSFontAttributeName:font, NSForegroundColorAttributeName:textColor, NSParagraphStyleAttributeName:paragraphStyle};
                
                if (textView) {
                    // Fix margine
                    CGFloat margineX = textView.contentInset.left;
                    CGFloat margineY = textView.contentInset.top;
                    
                    @try {
                        if ([DBDevice isiOSMin:7]) margineX += textView.textContainerInset.left;
                        if ([DBDevice isiOSMin:7]) margineX += textView.textContainer.lineFragmentPadding;
                        if ([DBDevice isiOSMin:7]) margineY += textView.textContainerInset.top;
                    } @catch (NSException *exception) {}
                    
                    frame.origin.x += margineX * self.zoomLarghezza;
                    frame.origin.y += margineX * self.zoomLarghezza;
                    frame.size.width  -= (margineY * self.zoomAltezza) * 2.0f;
                    frame.size.height -= (margineY * self.zoomAltezza) * 2.0f;
                    
                } else {
                    // Fix vertical alignment del testo
                    UIFont *fontCopy = [UIFont fontWithName:label.font.fontName size:(label.font.pointSize - 0.5f)];
                    CGFloat height = [text altezzaConFont:fontCopy conDimensioniMassime:label.frame.size];
                    
                    CGFloat differenza = label.frame.size.height - height;
                    frame.origin.y += (differenza / 2.0f) * self.zoomAltezza;
                }
                
                [text drawInRect:frame withAttributes:attributes];
                
                // Ripristino lo stato del "selectable" della TextView
                if (textView) textView.selectable = selectable;
                
                // Non considero le subviews di una TextView
                if (textView) return;
            }
        } else {
            // Background
            if (vista.backgroundColor && ![vista.backgroundColor isEqual:[UIColor clearColor]]) {
                [vista.backgroundColor set];
                CGContextFillRect(UIGraphicsGetCurrentContext(), frame);
            }
        }
    }
    
    // Se ci sono subview da disegnare
    NSArray *subviews = vista.subviews;
    if (subviews.count > 0) {
        for (id subview in subviews) {
            if ([subview isMemberOfClass:[UITableView class]]) {
                // Converto l'UITableView in una UIView, così verranno disegnate tutte le celle e potranno essere stampate
                UITableView *tableView = subview;
                UIView *tableSubview = [tableView creaView];
                tableSubview.frame = [subview frame];
                [tableView.superview addSubview:tableSubview];
                [self disegnaVista:tableSubview conVistaParent:vistaParent conNumeroPagina:numeroPagina];
                [tableSubview removeFromSuperview];
            } else {
                [self disegnaVista:subview conVistaParent:vistaParent conNumeroPagina:numeroPagina];
            }
        }
    }
}

@end
