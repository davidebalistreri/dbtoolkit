//
//  DBKeyboardHandler.h
//  File version: 1.3.0
//  Last modified: 06/17/2016
//
//  Created by Davide Balistreri on 12/11/2014
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#if DBFRAMEWORK_VERSION_EARLIER_THAN(__DBFRAMEWORK_0_7_0)
    #warning Bisogna adattare il codice DBKeyboardHandler alla nuova versione del Framework. \
    I metodi delegate sono cambiati.
#endif

@protocol DBKeyboardHandlerDelegate;


/**
 * Questo Handler permette di gestire un'insieme di InputField (TextField e TextView), semplificando alcuni scenari ricorrenti
 * come lo spostamento automatico premendo Invio, l'adattamento della schermata quando la tastiera si apre e ne copre una parte,
 * il controllo e la limitazione del testo immesso, e infine la gestione attraverso i delegate di ogni stato delle InputField.
 *
 * <b>Installazione:</b>
 * <p>Abilitare l'Handler nel <b>viewDidAppear</b> e disabilitarlo nel <b>viewWillDisappear</b>.</p>
 */
@interface DBKeyboardHandler : NSObject

NS_ASSUME_NONNULL_BEGIN

// MARK: - Metodi di classe

/**
 * Crea un nuovo Handler che si occuperà autonomamente della gestione delle InputField specificate.
 * Ogni operazione potrà essere controllata e autorizzata dal delegate, se specificato.
 *
 * @param view La vista su cui risiedono le InputField.
 *             <p>Sarà automaticamente ridimensionata quando la tastiera verrà aperta e chiusa, inoltre se la tastiera sarà
 *             aperta sarà possibile chiuderla tappando dove lo schermo è vuoto, al di fuori delle InputField.</p>
 * @param inputFields La lista delle InputField da controllare.
 * @param delegate Un delegate conforme al protocollo DBKeyboardHandlerDelegate.
 *
 * @return L'istanza del nuovo Handler.
 */
+ (DBKeyboardHandler *)handlerWithView:(UIView *)view inputFields:(nullable NSArray *)inputFields delegate:(nullable id<DBKeyboardHandlerDelegate>)delegate;

/**
 * Disabilita ed elimina gli Handler associati alla vista specificata.
 * @param view La vista con cui è stato configurato l'Handler.
 */
+ (void)destroyHandlerWithView:(UIView *)view;


// MARK: - Metodi d'istanza

// MARK: Gestione dell'Handler

/// La vista su cui risiedono le InputField.
@property (weak, nonatomic, readonly) UIView *view;

/// Le InputField gestite dall'Handler.
@property (strong, nonatomic) NSArray *inputFields;

/// Il delegate dell'Handler.
@property (weak, nonatomic) id<DBKeyboardHandlerDelegate> delegate;

/// L'InputField attualmente selezionata.
@property (weak, nonatomic, readonly) id selectedInputField;

/// Boolean che indica se la tastiera è attualmente aperta.
@property (nonatomic, readonly) BOOL isKeyboardOpen;


/// Abilita o disabilita l'Handler.
- (void)enableHandler:(BOOL)enable;

/// Disabilita ed elimina l'Handler dalla memoria.
- (void)destroyHandler;

/// Chiude l'eventuale tastiera attualmente aperta.
- (void)endEditing;


// MARK: Gestione delle InputField

/**
 * Imposta un delegate che si occuperà di gestire il comportamento della singola InputField specificata.
 * Verrà comunque chiamato prima il delegate generale (se impostato).
 */
- (void)inputField:(id)inputField setDelegate:(id<DBKeyboardHandlerDelegate>)delegate;

/**
 * Imposta il limite massimo di caratteri che può contenere l'InputField.
 * <p>Impostando -1 si disabilita il limite di caratteri.</p>
 */
- (void)inputField:(id)inputField setMaximumNumberOfCharacters:(NSInteger)maximumCharacters;

/**
 * Imposta i caratteri esclusivi che può contenere l'InputField.
 * <p>Impostando nil si disabilita il limite di caratteri.</p>
 */
- (void)inputField:(id)inputField setAllowedCharacters:(NSString *)allowedCharacters;

/**
 * Imposta i caratteri esclusivi che non può contenere l'InputField.
 * <p>Impostando nil si disabilita il limite di caratteri.</p>
 */
- (void)inputField:(id)inputField setForbiddenCharacters:(NSString *)forbiddenCharacters;


// MARK: Gestione delle animazioni

/// Comportamento della schermata quando viene selezionata un'InputField.
typedef NS_ENUM(NSInteger, DBKeyboardHandlerAnchorPoint) {
    /// Default. L'InputField attiva sarà posizionata sul centro della schermata.
    DBKeyboardHandlerAnchorPointCenter = 0,
    /// L'InputField attiva sarà posizionata sul punto più alto della schermata.
    DBKeyboardHandlerAnchorPointTop = 1,
    /// L'InputField attiva sarà posizionata sul punto più basso della schermata.
    DBKeyboardHandlerAnchorPointBottom = 2,
    /// Nessuna configurazione.
    DBKeyboardHandlerAnchorPointNotSet = -1,
};

/**
 * Comportamento globale della schermata quando viene selezionata un'InputField.
 * Default: DBKeyboardHandlerAnchorPointCenter.
 */
@property (nonatomic) DBKeyboardHandlerAnchorPoint anchorPoint;

/**
 * Se questo parametro è attivo il posizionamento globale sarà forzato sul punto di ancoraggio configurato,
 * bypassando i controlli che impediscono la schermata di non uscire da sopra e sotto.
 * Default: non attivo.
 */
@property (nonatomic) BOOL forceAnchorPoint;

/**
 * Margine globale applicato al punto di ancoraggio.
 * Default: zero.
 */
@property (nonatomic) CGFloat anchorPointMargin;

/**
 * Offset di correzione globale applicato al punto di ancoraggio.
 * Default: zero.
 */
@property (nonatomic) CGFloat anchorPointOffset;

/**
 * Parametro globale che stabilisce se eseguire l'animazione di apertura/chiusura della tastiera.
 * Default: attivo.
 */
@property (nonatomic) BOOL shouldAnimateViewWithKeyboard;


/// Imposta il punto di ancoraggio per un'InputField specifica.
- (void)inputField:(id)inputField setAnchorPoint:(DBKeyboardHandlerAnchorPoint)anchorPoint;

/// Imposta la forzatura del punto di ancoraggio per un'InputField specifica.
- (void)inputField:(id)inputField setForceAnchorPoint:(BOOL)forceAnchorPoint;

/// Imposta il margine applicato al punto di ancoraggio per un'InputField specifica.
- (void)inputField:(id)inputField setAnchorPointMargin:(CGFloat)anchorPointMargin;

/// Imposta l'offset di correzione applicato al punto di ancoraggio per un'InputField specifica.
- (void)inputField:(id)inputField setAnchorPointOffset:(CGFloat)anchorPointOffset;

NS_ASSUME_NONNULL_END

@end


@protocol DBKeyboardHandlerDelegate <NSObject>

@optional

NS_ASSUME_NONNULL_BEGIN

- (BOOL)handler:(DBKeyboardHandler *)handler inputFieldShouldBeginEditing:(id)inputField;
- (BOOL)handler:(DBKeyboardHandler *)handler inputField:(id)inputField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
- (BOOL)handler:(DBKeyboardHandler *)handler inputFieldShouldReturn:(id)inputField;

/**
 * Asks the delegate if the touched view triggered by the internal TapRecognizer should dismiss the keyboard.
 */
- (BOOL)handler:(DBKeyboardHandler *)handler touchedView:(__kindof UIView *)view shouldEndEditingOfInputField:(id)inputField;

- (void)handler:(DBKeyboardHandler *)handler inputFieldDidBeginEditing:(id)inputField;
- (void)handler:(DBKeyboardHandler *)handler inputFieldDidChangeText:(id)inputField;
- (void)handler:(DBKeyboardHandler *)handler inputFieldDidEndEditing:(id)inputField;

/**
 * Notifies the delegate when the keyboard is opening or closing, passing its frame and animation properties for an easier replication.
 *
 * Be careful: performing a custom animation could potentially break a user interaction, for example when tapping upon UITableViewCells.
 * To avoid this, use the options provided by this method, or add UIViewAnimationOptionAllowUserInteraction to your custom animation.
 */
- (void)handler:(DBKeyboardHandler *)handler inputFieldWillStartAnimation:(id)inputField duration:(NSTimeInterval)duration options:(UIViewAnimationOptions)options keyboardFrame:(CGRect)keyboardFrame;

/**
 * Notifies the delegate when the keyboard has been opened or closed, passing its frame.
 */
- (void)handler:(DBKeyboardHandler *)handler inputFieldDidEndAnimation:(id)inputField keyboardFrame:(CGRect)keyboardFrame;

- (void)handlerDidEndEditing:(DBKeyboardHandler *)handler;

NS_ASSUME_NONNULL_END

@end
