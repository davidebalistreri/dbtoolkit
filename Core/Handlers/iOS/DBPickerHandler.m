//
//  DBPickerHandler.m
//  File version: 1.1.0
//  Last modified: 04/15/2016
//
//  Created by Davide Balistreri on 11/13/2014
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBPickerHandler.h"
#import "DBFramework.h"

@interface DBPickerHandlerItem : DBObject

@property (strong, nonatomic) UITextField *inputField;
@property (strong, nonatomic) UIPickerView *pickerView;
@property (strong, nonatomic) id dataSource;

@end

@implementation DBPickerHandlerItem

@end


const NSString *DateTimePicker = @"DBPickerHandler+dateTimePicker";
const NSString *DatePicker     = @"DBPickerHandler+datePicker";
const NSString *TimePicker     = @"DBPickerHandler+timePicker";

@interface DBPickerHandler () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, getter = isEnabled) BOOL enabled;

@property (strong, nonatomic) NSMutableArray<DBPickerHandlerItem *> *items;

@end


@implementation DBPickerHandler

+ (NSMutableArray *)sharedInstances
{
    @synchronized(self) {
        NSMutableArray *sharedInstances = [DBCentralManager getRisorsa:@"DBSharedPickerHandlers"];
        
        if (!sharedInstances) {
            sharedInstances = [NSMutableArray array];
            [DBCentralManager setRisorsa:sharedInstances conIdentifier:@"DBSharedPickerHandlers"];
        }
        
        return sharedInstances;
    }
}

// MARK: - Metodi di classe

+ (DBPickerHandler *)handlerWithDelegate:(id<DBPickerHandlerDelegate>)delegate inputFields:(NSArray<UITextField *> *)inputFields
{
    // Istanzio il nuovo Handler
    DBPickerHandler *handler = [DBPickerHandler new];
    handler.delegate = delegate;
    handler.inputFields = [NSArray arrayWithArray:inputFields];
    [handler enableHandler:YES];
    
    // Lo aggiungo allo stack
    NSMutableArray *sharedInstances = [DBPickerHandler sharedInstances];
    [sharedInstances addObject:handler];
    
    return handler;
}

+ (void)destroyHandlerWithDelegate:(id<DBPickerHandlerDelegate>)delegate
{
    // Cerco tra gli Handler attualmente attivi
    NSMutableArray *sharedInstances = [DBPickerHandler sharedInstances];
    
    for (NSInteger counter = 0; counter < sharedInstances.count; counter++) {
        DBPickerHandler *handler = sharedInstances[counter];
        
        if ([handler.delegate isEqual:delegate]) {
            // Handler trovato, lo disabilito e rimuovo dallo stack di Handlers
            [handler destroyHandler];
            counter--;
        }
    }
}


// MARK: - Metodi d'istanza

// MARK: Gestione dell'Handler

@synthesize delegate = _delegate;
- (void)setDelegate:(id<DBPickerHandlerDelegate>)delegate
{
    _delegate = delegate;
}

- (NSArray<UITextField *> *)inputFields
{
    NSMutableArray *inputFields = [NSMutableArray array];
    
    for (DBPickerHandlerItem *item in self.items) {
        [inputFields safeAddObject:item.inputField];
    }
    
    return inputFields;
}

- (void)setInputFields:(NSArray<UITextField *> *)inputFields
{
    BOOL isEnabled = self.isEnabled;
    
    if (isEnabled) {
        // Rimuovo i Picker dalle vecchie InputFields
        [self enableHandler:NO];
    }
    
    // Creo nuovi Item
    NSMutableArray *items = [NSMutableArray array];
    
    for (UITextField *inputField in inputFields) {
        // Provo a riutilizzare il vecchio Item
        DBPickerHandlerItem *item = [self itemWithInputField:inputField];
        
        if (!item) {
            // Istanzio il nuovo Item
            item = [DBPickerHandlerItem new];
            item.inputField = inputField;
        }
        
        [items addObject:item];
    }
    
    // Rimuovo gli Item delle InputFields non più controllate
    for (DBPickerHandlerItem *item in self.items) {
        if ([items containsObject:item] == NO) {
            item.inputField.inputView = nil;
            item.pickerView = nil;
        }
    }
    
    self.items = items;
    
    if (isEnabled) {
        // Aggiungo i Picker alle nuove InputFields
        [self enableHandler:YES];
    }
}

@synthesize backgroundColor = _backgroundColor;
- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    _backgroundColor = backgroundColor;
    
    for (DBPickerHandlerItem *item in self.items) {
        item.pickerView.backgroundColor = backgroundColor;
    }
}

- (BOOL)isHandlerValid
{
    return (self.delegate) ? YES : NO;
}

- (void)enableHandler:(BOOL)enable
{
    self.enabled = enable;
    
    // Rimuovo i Picker dalle InputFields
    for (DBPickerHandlerItem *item in self.items) {
        if (enable) {
            item.inputField.inputView = item.pickerView;
            
            if ([item.inputField isKindOfClass:[UITextField class]]) {
                // Nascondo il pulsante per cancellare l'input
                item.inputField.clearButtonMode = UITextFieldViewModeNever;
                
                // Nascondo il cursore
                // item.inputField.tintColor = [UIColor clearColor];
            }
        }
        else {
            item.inputField.inputView = nil;
        }
    }
}

- (void)destroyHandler
{
    [self enableHandler:NO];
    
    NSMutableArray *sharedInstances = [DBPickerHandler sharedInstances];
    [sharedInstances removeObject:self];
}

- (void)informaDelegateConInputField:(id)inputField oggettoSelezionato:(id)oggetto conIndice:(NSInteger)indice;
{
    if ([self.delegate respondsToSelector:@selector(handler:inputField:didSelectObject:atIndex:)]) {
        [self.delegate handler:self inputField:inputField didSelectObject:oggetto atIndex:indice];
    }
    
    if ([self.delegate respondsToSelector:@selector(handler:pickerViewDidSelectItem:atIndex:inComponent:conInputField:)]) {
        DBFRAMEWORK_DEPRECATED_LOG(0.6.4, "utilizza il nuovo metodo -[DBPickerHandler handler:inputField:oggettoSelezionato:conIndice:]");
        
        DBFRAMEWORK_SUPPRESS_DEPRECATED_WARNING([self.delegate handler:self pickerViewDidSelectItem:oggetto atIndex:indice inComponent:0 conInputField:inputField]);
    }
}

- (void)aggiornaTestoInputField:(UITextField *)inputField
{
    DBPickerHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    UIPickerView *pickerView = item.pickerView;
    id dataSource = item.dataSource;
    
    if ([self dataSourceIsDatePicker:dataSource]) {
        UIDatePicker *datePicker = (id)pickerView;
        NSDate *data = datePicker.date;
        
        if ([dataSource isEqual:DatePicker]) {
            inputField.text = [NSDateFormatter stringaFormatoStandardData:data];
        }
        else if ([dataSource isEqual:TimePicker]) {
            inputField.text = [NSDateFormatter stringaFormatoStandardOra:data];
        }
        else if ([dataSource isEqual:DateTimePicker]) {
            inputField.text = [NSDateFormatter stringaFormatoStandardDataOra:data];
        }
    }
    else if (dataSource) {
        id oggetto = [dataSource safeObjectAtIndex:[pickerView selectedRowInComponent:0]];
        
        NSString *testo = [self privateTitoloOggetto:oggetto];
        inputField.text = testo;
    }
}

- (BOOL)dataSourceIsDatePicker:(id)dataSource
{
    if ([dataSource isEqual:DateTimePicker] || [dataSource isEqual:DatePicker] || [dataSource isEqual:TimePicker]) {
        return YES;
    }
    
    return NO;
}

- (DBPickerHandlerItem *)itemWithPickerView:(__kindof UIPickerView *)pickerView
{
    for (DBPickerHandlerItem *item in self.items) {
        if ([item.pickerView isEqual:pickerView]) {
            return item;
        }
    }
    
    return nil;
}

- (DBPickerHandlerItem *)itemWithInputField:(UITextField *)inputField
{
    for (DBPickerHandlerItem *item in self.items) {
        if ([item.inputField isEqual:inputField]) {
            return item;
        }
    }
    
    return nil;
}

// MARK: Gestione delle InputField

- (id)inputFieldDataSource:(UITextField *)inputField
{
    DBPickerHandlerItem *item = [self itemWithInputField:inputField];
    return item.dataSource;
}

/// Imposta o sostituisce il dataSource per l'InputField richiesta
- (void)inputField:(UITextField *)inputField setDataSource:(id)dataSource
{
    DBPickerHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    item.dataSource = dataSource;
    
    if ([NSObject isNotNull:dataSource]) {
        if ([self dataSourceIsDatePicker:dataSource]) {
            UIDatePicker *datePicker = [UIDatePicker new];
            datePicker.backgroundColor = self.backgroundColor;
            datePicker.locale = [NSLocale localeWithLocaleIdentifier:[DBLocalization identifierLinguaCorrente]];
            
            // Su iOS 13.4 viene introdotta una nuova modalità per il datepicker
            if (@available(iOS 13.4, *)) {
                datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
            }
            
            item.pickerView = (id)datePicker;
            
            if ([dataSource isNotEqual:DateTimePicker]) {
                datePicker.datePickerMode = ([dataSource isEqual:DatePicker]) ? UIDatePickerModeDate : UIDatePickerModeTime;
            }
            
            [datePicker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
            
            // Imposto il primo valore
            [self datePickerChanged:datePicker];
        }
        else {
            UIPickerView *pickerView = [UIPickerView new];
            pickerView.backgroundColor = self.backgroundColor;
            pickerView.delegate = self;
            item.pickerView = pickerView;
            
            // Imposto il primo valore
            [self pickerView:pickerView didSelectRow:0 inComponent:0];
        }
    }
    else {
        item.pickerView = nil;
    }
    
    if (self.isEnabled) {
        inputField.inputView = item.pickerView;
        [inputField reloadInputViews];
    }
    
    // Informo il delegate
    if ([self.delegate respondsToSelector:@selector(handler:inputField:didUpdateDataSource:)]) {
        [self.delegate handler:self inputField:inputField didUpdateDataSource:dataSource];
    }
}

- (void)inputField:(UITextField *)inputField setMinimumDate:(NSDate *)minimumDate maximumDate:(NSDate *)maximumDate
{
    DBPickerHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    UIDatePicker *datePicker = (id)item.pickerView;
    NSDate *currentDate = datePicker.date;
    
    // Aggiorno i vincoli del picker
    datePicker.minimumDate = minimumDate;
    datePicker.maximumDate = maximumDate;
    
    // Se la data precedentemente impostata non rispetta più i nuovi vincoli del picker, informo il delegate
    if (currentDate && [currentDate isEqualToDate:datePicker.date] == NO) {
        [self datePickerChanged:datePicker];
    }
}

- (void)inputField:(UITextField *)inputField setDate:(NSDate *)date
{
    DBPickerHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    if (!date) return;
    
    UIDatePicker *datePicker = (id)item.pickerView;
    datePicker.date = date;
    
    [self datePickerChanged:datePicker];
}

- (void)inputField:(UITextField *)inputField setMinuteInterval:(NSInteger)minuteInterval
{
    DBPickerHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    UIDatePicker *datePicker = (id)item.pickerView;
    datePicker.minuteInterval = minuteInterval;
    
    // Arrotondo il valore mostrato in base ai minuti di intervallo
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitMinute fromDate:datePicker.date];
    NSInteger minutes = [dateComponents minute];
    NSInteger minutesRounded = ( (NSInteger)(minutes / minuteInterval) ) * minuteInterval;
    NSDate *roundedDate = [[NSDate alloc] initWithTimeInterval:60.0 * (minutesRounded - minutes) sinceDate:datePicker.date];
    datePicker.date = roundedDate;
    
    [self datePickerChanged:datePicker];
}

- (void)inputField:(UITextField *)inputField selectObjectAtIndex:(NSInteger)index
{
    DBPickerHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    UIPickerView *pickerView = item.pickerView;
    id dataSource = item.dataSource;
    
    id object = [dataSource safeObjectAtIndex:index];
    if ([NSObject isNull:object]) return;
    
    if ([self dataSourceIsDatePicker:dataSource]) {
        // Ancora da implementare...
    }
    else {
        @try {
            // Aggiorno il Picker
            [pickerView selectRow:index inComponent:0 animated:YES];
            [self pickerView:pickerView didSelectRow:index inComponent:0];
        } @catch (NSException *exception) {}
    }
}

- (void)inputField:(UITextField *)inputField selectObject:(id)object
{
    if ([NSObject isNull:object]) return;
    
    DBPickerHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    UIPickerView *pickerView = item.pickerView;
    id dataSource = item.dataSource;
    
    NSUInteger index = 0;
    
    if ([self dataSourceIsDatePicker:dataSource]) {
        // Ancora da implementare...
    }
    else {
        index = [dataSource indexOfObject:object];
    }
    
    @try {
        // Aggiorno il Picker
        [pickerView selectRow:index inComponent:0 animated:YES];
        [self pickerView:pickerView didSelectRow:index inComponent:0];
    } @catch (NSException *exception) {}
}

- (id)inputFieldSelectedObject:(UITextField *)inputField
{
    DBPickerHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return nil;
    
    UIPickerView *pickerView = item.pickerView;
    id dataSource = item.dataSource;
    
    @try {
        if ([self dataSourceIsDatePicker:dataSource]) {
            UIDatePicker *datePicker = (id)pickerView;
            
            if ([dataSource isEqual:DatePicker]) {
                // Tolgo i secondi
                NSDateFormatter *dateFormatter = [NSDateFormatter new];
                [dateFormatter setDateFormat:@"dd/MM/yyyy"];
                
                return [dateFormatter dateFromString:[dateFormatter stringFromDate:datePicker.date]];
            }
            else {
                return datePicker.date;
            }
        }
        else {
            return [dataSource safeObjectAtIndex:[pickerView selectedRowInComponent:0]];
        }
    } @catch (NSException *exception) {}
    
    return nil;
}

- (NSInteger)inputFieldSelectedObjectIndex:(UITextField *)inputField
{
    DBPickerHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return NSNotFound;
    
    UIPickerView *pickerView = item.pickerView;
    id dataSource = item.dataSource;
    
    if ([self dataSourceIsDatePicker:dataSource]) {
        // Ancora da implementare...
    }
    else if (pickerView) {
        return [pickerView selectedRowInComponent:0];
    }
    
    return NSNotFound;
}

/// Cerca di capire il nome dell'oggetto controllando alcune proprietà utilizzate spesso
- (NSString *)privateTitoloOggetto:(id)oggetto
{
    if ([oggetto isKindOfClass:[NSString class]]) {
        return DBLocalizedString(oggetto);
    }
    
    NSString *titolo = @"";
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    
    if ([oggetto respondsToSelector:@selector(titolo)]) {
        titolo = [oggetto performSelector:@selector(titolo) withObject:nil];
    }
    else if ([oggetto respondsToSelector:@selector(title)]) {
        titolo = [oggetto performSelector:@selector(title) withObject:nil];
    }
    else if ([oggetto respondsToSelector:@selector(name)]) {
        titolo = [oggetto performSelector:@selector(name) withObject:nil];
    }
    else if ([oggetto respondsToSelector:@selector(nome)]) {
        titolo = [oggetto performSelector:@selector(nome) withObject:nil];
    }
    else if ([oggetto respondsToSelector:@selector(stringValue)]) {
        titolo = [oggetto stringValue];
    }
    
#pragma clang diagnostic pop
    
    return DBLocalizedString(titolo);
}

// MARK: - Delegate PickerView con NSArray

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    DBPickerHandlerItem *item = [self itemWithPickerView:pickerView];
    NSArray *dataSource = item.dataSource;
    return dataSource.count;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    DBPickerHandlerItem *item = [self itemWithPickerView:pickerView];
    NSArray *dataSource = item.dataSource;
    id object = [dataSource safeObjectAtIndex:row];
    
    if ([self.delegate respondsToSelector:@selector(handler:inputField:attributedTitleForObject:atIndex:)]) {
        return [self.delegate handler:self inputField:item.inputField attributedTitleForObject:object atIndex:row];
    }
    
    NSString *title = [self privateTitoloOggetto:object];
    
    if ([self.delegate respondsToSelector:@selector(handler:inputField:titleForObject:atIndex:)]) {
        title = [self.delegate handler:self inputField:item.inputField titleForObject:object atIndex:row];
    }
    
    title = [NSString cleanString:title];
    
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes safeSetValue:self.textColor forKey:NSForegroundColorAttributeName];
    [attributes safeSetValue:self.font forKey:NSFontAttributeName];
    
    return [[NSAttributedString alloc] initWithString:title attributes:attributes];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    @try {
        DBPickerHandlerItem *item = [self itemWithPickerView:pickerView];
        
        UITextField *inputField = item.inputField;
        NSArray *dataSource = item.dataSource;
        
        id oggetto = [dataSource safeObjectAtIndex:row];
        
        // Aggiorno l'InputField
        [self aggiornaTestoInputField:inputField];
        
        // Informo il delegate
        [self informaDelegateConInputField:inputField oggettoSelezionato:oggetto conIndice:row];
    } @catch (NSException *exception) {}
}

// MARK: - Delegate PickerView con NSDate

- (void)datePickerChanged:(UIDatePicker *)datePicker
{
    DBPickerHandlerItem *item = [self itemWithPickerView:datePicker];
    
    UITextField *inputField = item.inputField;
    NSDate *oggetto = datePicker.date;
    
    // Aggiorno l'InputField
    [self aggiornaTestoInputField:inputField];
    
    // Informo il delegate
    [self informaDelegateConInputField:inputField oggettoSelezionato:oggetto conIndice:0];
}

@end
