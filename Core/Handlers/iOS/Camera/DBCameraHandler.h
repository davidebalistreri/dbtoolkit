//
//  DBCameraHandler.h
//  File version: 1.0.1
//  Last modified: 09/14/2016
//
//  Created by Davide Balistreri on 05/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#import "DBCameraHandlerDefines.h"


@interface DBCameraHandler : NSObject

/**
 * Questa classe necessita dell'inserimento di una chiave nell'Info.plist,
 * dove lo sviluppatore spiega in che modo viene utilizzata la fotocamera dall'app.
 *
 * Senza l'inserimento di questa chiave, il framework potrebbe non funzionare e/o lanciare un'eccezione.
 *
 * NSCameraUsageDescription
 */


// MARK: - Configurazione

@property (weak, nonatomic) id<DBCameraHandlerDelegate> delegate;


// MARK: - Metodi di classe

+ (DBCameraHandler *)handlerWithView:(UIView *)view cameraDevice:(DBCameraDevice)cameraDevice outputQuality:(DBCameraOutputQuality)outputQuality;

/**
 * Disabilita ed elimina l'Handler associato alla vista specificata.
 * @param view La vista con cui è stato configurato l'Handler.
 */
+ (void)destroyHandlerWithView:(UIView *)view;


// MARK: - Metodi d'istanza

// MARK: Gestione dell'Handler

@property (weak, nonatomic, readonly) UIView *view;

@property (nonatomic) DBCameraDevice cameraDevice;

@property (nonatomic) DBCameraOutputQuality outputQuality;

@property (nonatomic) DBCameraFlashMode flashMode;


/// Abilita o disabilita l'Handler.
- (void)enableHandler:(BOOL)enable;

/// Disabilita ed elimina l'Handler dalla memoria.
- (void)destroyHandler;


// MARK: Gestione della camera

/// Cattura l'immagine visibile nell'anteprima live, con la stessa qualità e le stesse dimensioni.
- (void)capturePreviewImageWithCompletionBlock:(DBCameraHandlerImageBlock)completionBlock;

/// Cattura l'immagine scattando una foto, con qualità e dimensioni più alte dell'anteprima live.
- (void)captureOutputImageWithCompletionBlock:(DBCameraHandlerImageBlock)completionBlock;

/// Inizia a scrivere l'output video su un file.
- (void)startRecordingOutputVideoToFileURL:(NSURL *)fileURL;

/// Interrompe la scrittura dell'output video e salva il file eseguendo il completionBlock quando ha terminato.
- (void)stopRecordingOutputVideoWithCompletionBlock:(DBCameraHandlerVideoBlock)completionBlock;


// MARK: Media recognition

/**
 * Lista degli AVMetadataObjectType attualmente monitorati dalla fotocamera.
 *
 * @note
 * Gli AVMetadataObjectType sono oggetti NSString.
 */
@property (strong, nonatomic, readonly) NSArray<__kindof NSString *> *metadataObjectTypes;

- (void)startCapturingMetadataObjectTypes:(NSArray<__kindof NSString *> *)metadataObjectTypes;

- (void)stopCapturingMetadataObjectTypes:(NSArray<__kindof NSString *> *)metadataObjectTypes;

- (void)stopCapturingAllMetadataObjectTypes;

@end
