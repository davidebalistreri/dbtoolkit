//
//  DBPickerHandler.h
//  File version: 1.1.0
//  Last modified: 04/15/2016
//
//  Created by Davide Balistreri on 11/13/2014
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#if DBFRAMEWORK_VERSION_EARLIER_THAN(__DBFRAMEWORK_0_7_0)
    #warning Bisogna adattare il codice DBPickerHandler alla nuova versione del Framework.
#endif


@protocol DBPickerHandlerDelegate;

/// Impostare questo valore nel dataSource delle InputField di cui si vuole visualizzare il DatePicker (data e ora).
extern const NSString *DateTimePicker;

/// Impostare questo valore nel dataSource delle InputField di cui si vuole visualizzare il DatePicker (solo data).
extern const NSString *DatePicker;

/// Impostare questo valore nel dataSource delle InputField di cui si vuole visualizzare il DatePicker (solo ora).
extern const NSString *TimePicker;


/**
 * Questo Handler permette di gestire un'insieme di InputField. Descrizione da completare.
 *
 * <b>Installazione:</b>
 * <p>Abilitare l'Handler nel <b>viewDidLoad</b> e disabilitarlo nel <b>dealloc</b>.</p>
 */
@interface DBPickerHandler : NSObject

NS_ASSUME_NONNULL_BEGIN

// MARK: - Metodi di classe

/**
 * Crea un nuovo Handler che si occuperà della gestione dei Picker per le InputField specificate.
 *
 * Una volta creato l'Handler, imposta i DataSource di ogni InputField con il metodo
 * <code>-[DBPickerHandler inputField:setDataSource:]</code>.
 *
 * @param delegate La classe delegate conforme al protocollo DBPickerHandlerDelegate.
 * @param inputFields L'insieme di InputField da controllare.
 *
 * @return L'istanza del nuovo Handler.
 */
+ (DBPickerHandler *)handlerWithDelegate:(id<DBPickerHandlerDelegate>)delegate inputFields:(nullable NSArray<__kindof UITextField *> *)inputFields;

/**
 * Disabilita ed elimina l'Handler associato al delegate specificato.
 * @param delegate Il delegate con cui è stato configurato l'Handler.
 */
+ (void)destroyHandlerWithDelegate:(id<DBPickerHandlerDelegate>)delegate;


// MARK: - Metodi d'istanza

// MARK: Gestione dell'Handler

/// Il delegate dell'Handler.
@property (weak, nonatomic, readonly) id<DBPickerHandlerDelegate> delegate;

/// Le InputField gestite dall'Handler.
@property (strong, nonatomic) NSArray<__kindof UITextField *> *inputFields;

/// Il colore di background dei Picker.
@property (strong, nonatomic) UIColor *backgroundColor;

/// Il colore del testo dei Picker.
@property (strong, nonatomic) UIColor *textColor;

/// Ancora da implementare. Il font del testo dei Picker.
@property (strong, nonatomic) UIFont *font;


/// Abilita o disabilita l'Handler.
- (void)enableHandler:(BOOL)enable;

/// Disabilita ed elimina l'Handler dalla memoria.
- (void)destroyHandler;


// MARK: Gestione delle InputField

/// Restituisce il DataSource associato all'InputField specificata.
- (id)inputFieldDataSource:(__kindof UITextField *)inputField;

/// Sovrascrive il dataSource associato all'InputField specificata.
- (void)inputField:(__kindof UITextField *)inputField setDataSource:(nullable id)dataSource;


/// Imposta la data corrente del DatePicker associato all'InputField specificata.
- (void)inputField:(__kindof UITextField *)inputField setDate:(NSDate *)date;

/// Imposta la data minima e la data massima visualizzabile dal DatePicker associato all'InputField specificata.
- (void)inputField:(__kindof UITextField *)inputField setMinimumDate:(nullable NSDate *)minimumDate maximumDate:(nullable NSDate *)maximumDate;

/// Imposta l'intervallo di minuti tra una data e l'altra del DatePicker associato all'InputField specificata.
- (void)inputField:(__kindof UITextField *)inputField setMinuteInterval:(NSInteger)minuteInterval;


/// Permette di impostare l'oggetto selezionato nel Picker associato all'InputField specificata.
- (void)inputField:(__kindof UITextField *)inputField selectObject:(id)object;

/// Permette di impostare l'oggetto selezionato nel Picker associato all'InputField specificata.
- (void)inputField:(__kindof UITextField *)inputField selectObjectAtIndex:(NSInteger)index;


/// Restituisce l'oggetto attualmente selezionato nel Picker associato all'InputField specificata.
- (id)inputFieldSelectedObject:(__kindof UITextField *)inputField;

/// Restituisce l'indice dell'oggetto attualmente selezionato nel Picker associato all'InputField specificata.
- (NSInteger)inputFieldSelectedObjectIndex:(__kindof UITextField *)inputField;

NS_ASSUME_NONNULL_END

@end


@protocol DBPickerHandlerDelegate <NSObject>

@optional

NS_ASSUME_NONNULL_BEGIN

/**
 * Richiamato per personalizzare il nome degli elementi presenti nei DataSource delle InputField.
 */
- (NSString *)handler:(DBPickerHandler *)handler inputField:(__kindof UITextField *)inputField titleForObject:(id)object atIndex:(NSInteger)index;

/**
 * Richiamato per personalizzare il nome degli elementi presenti nei DataSource delle InputField.
 * Utilizzare questo metodo per personalizzare lo stile del titolo degli elementi.
 */
- (NSAttributedString *)handler:(DBPickerHandler *)handler inputField:(__kindof UITextField *)inputField attributedTitleForObject:(id)object atIndex:(NSInteger)index;

/// Informa il delegate quando viene selezionato un oggetto in un Picker di una InputField.
- (void)handler:(DBPickerHandler *)handler inputField:(__kindof UITextField *)inputField didSelectObject:(id)object atIndex:(NSInteger)index;

/**
 * Informa il delegate quando viene aggiornato il Data Source di una InputField,
 * utile per gestire i metodi asincroni.
 */
- (void)handler:(DBPickerHandler *)handler inputField:(__kindof UITextField *)inputField didUpdateDataSource:(id)dataSource;

- (void)handler:(DBPickerHandler *)handler pickerViewDidSelectItem:(id)item atIndex:(NSInteger)index inComponent:(NSInteger)component conInputField:(id)inputField
    DBFRAMEWORK_DEPRECATED(0.7.0, "utilizza il nuovo metodo -[DBPickerHandler handler:inputField:didSelectObject:atIndex:]");

NS_ASSUME_NONNULL_END

@end
