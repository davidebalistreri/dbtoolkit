//
//  DBKeyboardHandler.m
//  File version: 1.3.0
//  Last modified: 06/17/2016
//
//  Created by Davide Balistreri on 12/11/2014
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBKeyboardHandler.h"
#import "DBAccessoryTextField.h"
#import "DBFramework.h"


// MARK: - DBKeyboardHandlerItem

@interface DBKeyboardHandlerItem : DBObject

@property (strong, nonatomic) UITextField *inputField;
@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) NSNumber *maximumCharacters;
@property (strong, nonatomic) NSString *allowedCharacters;
@property (strong, nonatomic) NSString *forbiddenCharacters;

@property (strong, nonatomic) NSNumber *anchorPoint;
@property (strong, nonatomic) NSNumber *forceAnchorPoint;
@property (strong, nonatomic) NSNumber *anchorPointMargin;
@property (strong, nonatomic) NSNumber *anchorPointOffset;

@end

@implementation DBKeyboardHandlerItem

@end


@interface DBAccessoryTextField (DBKeyboardHandlerPrivateMethods)

- (void)keyboardHandlerDidUpdateAvailableSpace:(CGFloat)availableSpace withAnchorMargin:(CGFloat)anchorMargin;

@end


struct DBKeyboardHandlerAnimationInfo {
    BOOL isOpening;
    UIViewAnimationCurve animationCurve;
    NSTimeInterval duration;
    CGFloat coveredSpace;
    CGFloat availableSpace;
    CGFloat offset;
    CGRect keyboardFrame;
};

typedef struct DBKeyboardHandlerAnimationInfo DBKeyboardHandlerAnimationInfo;

DBKeyboardHandlerAnimationInfo DBKeyboardHandlerAnimationInfoMake() {
    // Empty animation info
    DBKeyboardHandlerAnimationInfo animationInfo;
    animationInfo.isOpening = NO;
    animationInfo.animationCurve = 0;
    animationInfo.duration       = 0.0;
    animationInfo.coveredSpace   = 0.0;
    animationInfo.availableSpace = 0.0;
    animationInfo.offset         = 0.0;
    animationInfo.keyboardFrame  = CGRectZero;
    return animationInfo;
}


// MARK: - DBKeyboardHandler

@interface DBKeyboardHandler () <UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate>

@property (nonatomic, getter = isEnabled) BOOL enabled;

@property (nonatomic) CGRect frameIniziale;
@property (nonatomic) UIEdgeInsets contentInsetIniziale;
@property (nonatomic) UIEdgeInsets scrollInsetIniziale;

/// Utilizzato per mantenere centrate le TextView durante l'inserimento del testo
@property (nonatomic) CGPoint ripristinaOffset;

@property (strong, nonatomic) NSDictionary *lastKeyboardInfo;

@property (strong, nonatomic) NSMutableArray<DBKeyboardHandlerItem *> *items;
@property (strong, nonatomic) DBKeyboardHandlerItem *selectedItem;

@property (strong, nonatomic) UITapGestureRecognizer *tapRecognizer;

@end


@implementation DBKeyboardHandler

+ (NSMutableArray *)sharedInstances
{
    @synchronized(self) {
        NSMutableArray *sharedInstances = [DBCentralManager getRisorsa:@"DBSharedKeyboardHandlers"];
        
        if (!sharedInstances) {
            sharedInstances = [NSMutableArray array];
            [DBCentralManager setRisorsa:sharedInstances conIdentifier:@"DBSharedKeyboardHandlers"];
        }
        
        return sharedInstances;
    }
}

// MARK: - Metodi di classe

+ (DBKeyboardHandler *)handlerWithView:(UIView *)view inputFields:(NSArray *)inputFields delegate:(id<DBKeyboardHandlerDelegate>)delegate
{
    // Istanzio il nuovo Handler
    DBKeyboardHandler *handler = [DBKeyboardHandler new];
    handler.shouldAnimateViewWithKeyboard = YES;
    handler.view = view;
    handler.inputFields = [NSArray arrayWithArray:inputFields];
    handler.delegate = delegate;
    [handler enableHandler:YES];
    
    // Lo aggiungo allo stack
    NSMutableArray *sharedInstances = [DBKeyboardHandler sharedInstances];
    [sharedInstances addObject:handler];
    
    return handler;
}

+ (void)destroyHandlerWithView:(UIView *)view
{
    // Cerco tra gli Handler attualmente attivi
    NSMutableArray *sharedInstances = [self sharedInstances];
    
    for (NSInteger counter = 0; counter < sharedInstances.count; counter++) {
        DBKeyboardHandler *handler = sharedInstances[counter];
        
        if ([handler.view isEqual:view]) {
            // Handler trovato, lo disabilito e rimuovo dallo stack di Handlers
            [handler destroyHandler];
            counter--;
        }
    }
}


// MARK: - Metodi d'istanza

@synthesize view = _view;
- (void)setView:(id)view
{
    _view = view;
    
    if ([view isKindOfClass:[UIScrollView class]]) {
        UIScrollView *scrollView = view;
        
        // Configuro la chiusura della tastiera durante lo scroll
        if (scrollView.keyboardDismissMode == UIScrollViewKeyboardDismissModeNone) {
            scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        }
    }
}

- (id)selectedInputField
{
    return self.selectedItem.inputField;
}

@synthesize isKeyboardOpen = _isKeyboardOpen;
- (void)setIsKeyboardOpen:(BOOL)isKeyboardOpen
{
    _isKeyboardOpen = isKeyboardOpen;
}

- (NSArray<UITextField *> *)inputFields
{
    NSMutableArray *inputFields = [NSMutableArray array];
    
    for (DBKeyboardHandlerItem *item in self.items) {
        [inputFields safeAddObject:item.inputField];
    }
    
    return inputFields;
}

- (void)setInputFields:(NSArray *)inputFields
{
    BOOL isEnabled = self.isEnabled;
    
    if (isEnabled) {
        // Invalido la gestione delle vecchie InputFields
        [self enableHandler:NO];
    }
    
    NSMutableArray *items = [NSMutableArray array];
    
    for (UITextField *inputField in inputFields) {
        // Provo a riutilizzare il vecchio Item
        DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
        
        if (!item) {
            // Istanzio il nuovo Item
            item = [DBKeyboardHandlerItem new];
            item.inputField = inputField;
        }
        
        [items addObject:item];
    }
    
    self.items = items;
    
    for (UITextField *inputField in inputFields) {
        if ([inputField isKindOfClass:[DBAccessoryTextField class]]) {
            // Automatic linking
            DBAccessoryTextField *accessoryTextField = (id)inputField;
            accessoryTextField.keyboardHandler = self;
        }
    }
    
    if (isEnabled) {
        // Riabilito l'Handler con gli Item aggiornati
        [self enableHandler:YES];
    }
}

- (BOOL)isHandlerValid
{
    return (self.view) ? YES : NO;
}

- (void)enableHandler:(BOOL)enable
{
    self.enabled = enable;
    
    [self abilitaTapRecognizer:enable];
    
    for (DBKeyboardHandlerItem *item in self.items) {
        UITextField *inputField = item.inputField;
        
        if (enable) {
            inputField.delegate = self;
            
            if ([inputField isKindOfClass:[UITextField class]]) {
                // UITextField purtroppo non fornisce il metodo delegate "didChange"
                [inputField addTarget:self action:@selector(inputFieldDidChangeText:) forControlEvents:UIControlEventEditingChanged];
            }
        }
        else {
            inputField.delegate = nil;
            
            if ([inputField isKindOfClass:[UITextField class]]) {
                // UITextField purtroppo non fornisce il metodo delegate "didChange"
                [inputField removeTarget:self action:@selector(inputFieldDidChangeText:) forControlEvents:UIControlEventEditingChanged];
            }
        }
    }
    
    if (enable) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tastieraAperta:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tastieraChiusa:) name:UIKeyboardWillHideNotification object:nil];
    }
    else {
        [self endEditing];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    }
}

- (void)abilitaTapRecognizer:(BOOL)abilita
{
    if (abilita && !self.tapRecognizer) {
        self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureRecognizer:)];
        self.tapRecognizer.delegate = self;
        self.tapRecognizer.cancelsTouchesInView = NO;
        
        UIView *view = [self viewSuper];
        [view addGestureRecognizer:self.tapRecognizer];
    }
    else if (!abilita && self.tapRecognizer) {
        [self.tapRecognizer.view removeGestureRecognizer:self.tapRecognizer];
        self.tapRecognizer = nil;
    }
}

- (UIView *)viewSuper
{
    UIView *view = self.view;
    UIView *superview = view.superview;
    
    if ([DBUtility isDevelopmentModeEnabled]) {
        // Aumento il raggio di azione a tutta la Window
        UIWindow *window = view.window;
        UIWindow *mainWindow = [DBApplication mainWindow];
        
        if (window) {
            return window;
        }
        else if (mainWindow) {
            return mainWindow;
        }
    }
    
    if (superview) {
        return superview;
    }
    else {
        return view;
    }
}

- (void)destroyHandler
{
    [self enableHandler:NO];
    
    NSMutableArray *sharedInstances = [DBKeyboardHandler sharedInstances];
    [sharedInstances removeObject:self];
}

/// Chiude la tastiera attualmente aperta
- (void)endEditing
{
    // Se una InputField è selezionata
    if (self.selectedItem) {
        DBKeyboardHandlerItem *selectedItem = self.selectedItem;
        UITextField *selectedInputField = selectedItem.inputField;
        
        [selectedInputField resignFirstResponder];
        
        // Notifico il delegate che una InputField ha terminato la sua modalità modifica
        if ([self.delegate respondsToSelector:@selector(handler:inputFieldDidEndEditing:)]) {
            [self.delegate handler:self inputFieldDidEndEditing:selectedInputField];
        }
        
        // Notifico il delegate specifico che l'InputField ha terminato la sua modalità modifica
        if ([selectedItem.delegate respondsToSelector:@selector(handler:inputFieldDidEndEditing:)]) {
            [selectedItem.delegate handler:self inputFieldDidEndEditing:selectedInputField];
        }
        
        // Notifico il delegate che tutte le InputField hanno terminato la loro modalità di modifica
        if ([self.delegate respondsToSelector:@selector(handlerDidEndEditing:)]) {
            [self.delegate handlerDidEndEditing:self];
        }
        
        self.selectedItem = nil;
    }
}

// MARK: Gestione delle InputField

- (void)inputField:(id)inputField setDelegate:(id<DBKeyboardHandlerDelegate>)delegate
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    item.delegate = delegate;
}

- (void)inputField:(id)inputField setMaximumNumberOfCharacters:(NSInteger)maximumCharacters
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    item.maximumCharacters = [NSNumber numberWithInteger:maximumCharacters];
}

- (void)inputField:(id)inputField setAllowedCharacters:(NSString *)allowedCharacters
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    item.allowedCharacters = allowedCharacters;
}

- (void)inputField:(id)inputField setForbiddenCharacters:(NSString *)forbiddenCharacters
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    item.forbiddenCharacters = forbiddenCharacters;
}

@synthesize shouldAnimateViewWithKeyboard = _shouldAnimateViewWithKeyboard;
- (void)setShouldAnimateViewWithKeyboard:(BOOL)shouldAnimateViewWithKeyboard
{
    if (shouldAnimateViewWithKeyboard && self.selectedItem) {
        [self handleKeyboardNotification:nil isOpening:YES];
    }
    else if (shouldAnimateViewWithKeyboard == NO) {
        [self handleKeyboardNotification:nil isOpening:NO];
    }
    
    _shouldAnimateViewWithKeyboard = shouldAnimateViewWithKeyboard;
}

// MARK: Gestione delle animazioni (metodi interni)

- (DBKeyboardHandlerAnchorPoint)anchorPointWithInputField:(id)inputField
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    
    if (!item || !item.anchorPoint)
        return self.anchorPoint;
    
    DBKeyboardHandlerAnchorPoint anchorPoint = item.anchorPoint.integerValue;
    return anchorPoint;
}

- (BOOL)forceAnchorPointWithInputField:(id)inputField
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    
    if (!item || !item.forceAnchorPoint)
        return self.forceAnchorPoint;
    
    BOOL forceAnchorPoint = item.forceAnchorPoint.boolValue;
    return forceAnchorPoint;
}

- (CGFloat)anchorPointMarginWithInputField:(id)inputField
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    
    if (!item || !item.anchorPointMargin)
        return self.anchorPointMargin;
    
    CGFloat anchorPointMargin = item.anchorPointMargin.doubleValue;
    return anchorPointMargin;
}

- (CGFloat)anchorPointOffsetWithInputField:(id)inputField
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    
    if (!item || !item.anchorPointOffset)
        return self.anchorPointOffset;
    
    CGFloat anchorPointOffset = item.anchorPointOffset.doubleValue;
    return anchorPointOffset;
}

// MARK: Gestione delle animazioni (metodi esterni)

/// Imposta il punto di ancoraggio per un'InputField specifica.
- (void)inputField:(id)inputField setAnchorPoint:(DBKeyboardHandlerAnchorPoint)anchorPoint
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    item.anchorPoint = [NSNumber numberWithInteger:anchorPoint];
    
    // Gestire il cambiamento a runtime...
}

/// Imposta la forzatura del punto di ancoraggio per un'InputField specifica.
- (void)inputField:(id)inputField setForceAnchorPoint:(BOOL)forceAnchorPoint
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    item.forceAnchorPoint = [NSNumber numberWithBool:forceAnchorPoint];
    
    // Gestire il cambiamento a runtime...
}

/// Imposta il margine applicato al punto di ancoraggio per un'InputField specifica.
- (void)inputField:(id)inputField setAnchorPointMargin:(CGFloat)anchorPointMargin
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    item.anchorPointMargin = [NSNumber numberWithDouble:anchorPointMargin];
    
    // Gestire il cambiamento a runtime...
}

/// Imposta l'offset di correzione applicato al punto di ancoraggio per un'InputField specifica.
- (void)inputField:(id)inputField setAnchorPointOffset:(CGFloat)anchorPointOffset
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    item.anchorPointOffset = [NSNumber numberWithDouble:anchorPointOffset];
    
    // Gestire il cambiamento a runtime...
}

// MARK: - Metodi interni

- (DBKeyboardHandlerItem *)itemWithInputField:(UITextField *)inputField
{
    for (DBKeyboardHandlerItem *item in self.items) {
        if ([item.inputField isEqual:inputField]) {
            return item;
        }
    }
    
    return nil;
}

- (CGSize)contentSizeScrollView
{
    CGSize contentSize = CGSizeZero;
    
    if ([self.view isKindOfClass:[UIScrollView class]]) {
        UIScrollView *scrollView = (UIScrollView *) self.view;
        contentSize = scrollView.contentSize;
        
        // Check frame contenuto Scroll View
        // if (contentSize.height == 0.0)
        //     contentSize = CGSizeMake(scrollView.bounds.size.width, scrollView.contentInset.bottom);
        
        if (contentSize.height <= 0.0) {
            contentSize = CGSizeMake(scrollView.bounds.size.width, scrollView.bounds.size.height);
        }
        
        // contentSize.height += scrollView.contentInset.top;
        // contentSize.height += scrollView.contentInset.bottom;
    }
    
    return contentSize;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // Controllo se l'InputField può avviare la modalità di modifica
    DBKeyboardHandlerItem *item = [self itemWithInputField:textField];
    if (!item) return NO;
    
    // Se precedentemente era selezionata un'altra InputField
    if (self.selectedItem && ![self.selectedItem isEqual:item]) {
        // La tastiera è stata cambiata dall'utente, senza premere il tasto invio
        DBKeyboardHandlerItem *selectedItem = self.selectedItem;
        UITextField *selectedInputField = selectedItem.inputField;
        
        // Chiudo l'InputField precedente (per eseguire l'animazione della tastiera aperta/chiusa)
        [selectedInputField resignFirstResponder];
        
        // Notifico il delegate che l'InputField precedente ha terminato la sua modalità modifica
        if ([self.delegate respondsToSelector:@selector(handler:inputFieldDidEndEditing:)]) {
            [self.delegate handler:self inputFieldDidEndEditing:selectedInputField];
        }
        
        // Notifico il delegate specifico che l'InputField precedente ha terminato la sua modalità modifica
        if ([selectedItem.delegate respondsToSelector:@selector(handler:inputFieldDidEndEditing:)]) {
            [selectedItem.delegate handler:self inputFieldDidEndEditing:selectedInputField];
        }
        
        self.selectedItem = nil;
    }
    // Se la tastiera è quella attualmente selezionata
    else if ([self.selectedItem isEqual:item]) {
        // Questo ELSE viene eseguito sempre, per via della riga [textField becomeFirstResponder] a fine metodo
        return YES;
    }
    
    // Controllo se l'InputField può avviare la modalità di modifica
    
    // Prima chiedo al delegate generale dell'Handler (se presente)
    if ([self.delegate respondsToSelector:@selector(handler:inputFieldShouldBeginEditing:)]) {
        if ([self.delegate handler:self inputFieldShouldBeginEditing:textField] == NO) {
            // Non può avviare la modalità di modifica
            return NO;
        }
    }
    
    // Poi chiedo al delegate specifico della InputField (se presente)
    if ([item.delegate respondsToSelector:@selector(handler:inputFieldShouldBeginEditing:)]) {
        if ([item.delegate handler:self inputFieldShouldBeginEditing:textField] == NO) {
            // Non può avviare la modalità di modifica
            return NO;
        }
    }
    
    self.selectedItem = item;
    
    // Imposto l'InputField corrente anche se è già impostata (per eseguire l'animazione della tastiera aperta/chiusa)
    [textField becomeFirstResponder];
    
    // Notifico il delegate che l'InputField ha iniziato la sua modalità modifica
    if ([self.delegate respondsToSelector:@selector(handler:inputFieldDidBeginEditing:)]) {
        [self.delegate handler:self inputFieldDidBeginEditing:textField];
    }
    
    // Notifico il delegate specifico che l'InputField ha iniziato la sua modalità modifica
    if ([item.delegate respondsToSelector:@selector(handler:inputFieldDidBeginEditing:)]) {
        [item.delegate handler:self inputFieldDidBeginEditing:textField];
    }
    
    // Gestisco lo scroll automatico della ScrollView
    if ([self.view isKindOfClass:[UIScrollView class]]) {
        // Disabilitato perché deve funzionare senza!
        // UIScrollView *scrollView = self.view;
        // [scrollView setContentOffset:scrollView.contentOffset animated:NO];
    }
    
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    // Utilizzo la stessa logica delle UITextFields
    return [self textFieldShouldBeginEditing:(UITextField *)textView];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:textField];
    if (!item) return NO;
    
    // Prima chiedo al delegate generale dell'Handler
    if ([self.delegate respondsToSelector:@selector(handler:inputField:shouldChangeCharactersInRange:replacementString:)]) {
        if ([self.delegate handler:self inputField:textField shouldChangeCharactersInRange:range replacementString:string] == NO) {
            // Non può aggiornare il testo
            return NO;
        }
    }
    
    // Poi chiedo al delegate specifico dell'InputField
    if ([item.delegate respondsToSelector:@selector(handler:inputField:shouldChangeCharactersInRange:replacementString:)]) {
        if ([item.delegate handler:self inputField:textField shouldChangeCharactersInRange:range replacementString:string] == NO) {
            // Non può aggiornare il testo
            return NO;
        }
    }
    
    // Check caratteri proibiti
    if (item.forbiddenCharacters && [string contieneCaratteri:item.forbiddenCharacters]) {
        // La nuova stringa contiene caratteri non consentiti
        return NO;
    }
    
    // Check caratteri permessi
    if (item.allowedCharacters && [string contieneCaratteriDiversiDa:item.allowedCharacters]) {
        // La nuova stringa contiene caratteri non consentiti
        return NO;
    }
    
    // Check limite caratteri
    if (item.maximumCharacters) {
        NSInteger maximumCharacters = item.maximumCharacters.integerValue;
        if (maximumCharacters < 0 || maximumCharacters == NSNotFound) {
            // Non è stato impostato un limite di caratteri
            return YES;
        }
        
        NSInteger insertDelta = string.length - range.length;
        
        if (textField.text.length + insertDelta > maximumCharacters) {
            // Limite di caratteri raggiunto
            return NO;
        }
    }
    
    // Tutto ok
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    /**
     * L'animazione di sistema quando viene inserito del testo non funziona bene ed è decentrata.
     * Questo codice permette di animare correttamente la vista seguendo il cursore man mano che il testo cambia.
     */
    UIScrollView *scrollView = ([self.view isKindOfClass:[UIScrollView class]]) ? (id)self.view : nil;
    
    if (scrollView) {
        self.ripristinaOffset = scrollView.contentOffset;
    }
    else {
        self.ripristinaOffset = CGPointZero;
    }
    
    // Utilizzo la stessa logica delle UITextFields
    return [self textField:(id)textView shouldChangeCharactersInRange:range replacementString:text];
}

- (void)inputFieldDidChangeText:(id)inputField
{
    DBKeyboardHandlerItem *item = [self itemWithInputField:inputField];
    if (!item) return;
    
    // Notifico il delegate che il testo è stato cambiato
    if ([self.delegate respondsToSelector:@selector(handler:inputFieldDidChangeText:)]) {
        [self.delegate handler:self inputFieldDidChangeText:inputField];
    }
    
    // Notifico il delegate specifico che il testo è stato cambiato
    if ([item.delegate respondsToSelector:@selector(handler:inputFieldDidChangeText:)]) {
        [item.delegate handler:self inputFieldDidChangeText:inputField];
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    /**
     * L'animazione di sistema quando viene inserito del testo non funziona bene ed è decentrata.
     * Questo codice permette di animare correttamente la vista seguendo il cursore man mano che il testo cambia.
     */
    UIScrollView *scrollView = ([self.view isKindOfClass:[UIScrollView class]]) ? (id)self.view : nil;
    
    if (scrollView) {
        // Ripristino la posizione originale della ScrollView
        [scrollView setContentOffset:self.ripristinaOffset animated:NO];
        [scrollView layoutIfNeeded];
        [textView layoutIfNeeded];
        
        // Eseguo l'animazione per centrare la TextView sul cursore
        [self handleKeyboardNotification:nil isOpening:YES];
    }
    
    // Utilizzo la stessa logica delle UITextFields
    return [self inputFieldDidChangeText:textView];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // Controllo se l'InputField può terminare la modalità di modifica premendo il tasto Invio
    DBKeyboardHandlerItem *item = [self itemWithInputField:textField];
    if (!item) return NO;
    
    // Chiedo al delegate se la InputField può terminare la modalità di modifica premendo il tasto Invio
    if ([self.delegate respondsToSelector:@selector(handler:inputFieldShouldReturn:)]) {
        if ([self.delegate handler:self inputFieldShouldReturn:textField] == NO) {
            // Non può terminare la modalità di modifica
            return NO;
        }
    }
    
    // Chiedo al delegate specifico se la InputField può terminare la modalità di modifica premendo il tasto Invio
    if ([item.delegate respondsToSelector:@selector(handler:inputFieldShouldReturn:)]) {
        if ([item.delegate handler:self inputFieldShouldReturn:textField] == NO) {
            // Non può terminare la modalità di modifica
            return NO;
        }
    }
    
    // Seleziono l'InputField successiva
    NSInteger index = [self.inputFields indexOfObject:textField];
    UITextField *nextTextField = [self.inputFields safeObjectAtIndex:(index + 1)];
    [nextTextField becomeFirstResponder];
    
    // Se l'InputField successiva è disabilitata o inesistente, chiudo la tastiera
    if ([nextTextField isFirstResponder] == NO) {
        [self endEditing];
    }
    
    return NO;
}

- (void)tastieraAperta:(NSNotification *)notification
{
    if (self.selectedItem) {
        DBKeyboardHandlerItem *selectedItem = self.selectedItem;
        UITextField *selectedInputField = selectedItem.inputField;
        
        [selectedInputField layoutIfNeeded];
        
        if ([selectedInputField isKindOfClass:[UITextView class]]) {
            // Alcuni elementi grafici e di sistema devono ancora impostare le loro proprietà,
            // per questo serve un piccolo ritardo prima di eseguire l'animazione
            [DBThread eseguiBlock:^{
                [self handleKeyboardNotification:notification isOpening:YES];
            }];
        }
        else {
            [self handleKeyboardNotification:notification isOpening:YES];
        }
    }
}

- (void)tastieraChiusa:(NSNotification *)notification
{
    if (self.selectedItem) {
        [self handleKeyboardNotification:notification isOpening:NO];
    }
}

- (CGPoint)calcolaOrigineReale:(UIView *)vista
{
    CGPoint origine = vista.frame.origin;
    
    UIView *superview = vista.superview;
    while (superview && ![superview isEqual:self.view]) {
        origine.x += superview.frame.origin.x;
        origine.y += superview.frame.origin.y;
        superview = superview.superview;
    }
    
    return origine;
}

- (void)handleKeyboardNotification:(NSNotification *)notification isOpening:(BOOL)isOpening
{
    if ([self isHandlerValid] == NO) {
        [self destroyHandler];
        return;
    }
    
    self.isKeyboardOpen = isOpening;
    
    if ([notification userInfo]) {
        // Aggiorno le info sulla tastiera
        self.lastKeyboardInfo = [notification userInfo];
    }
    
    DBKeyboardHandlerItem *item = self.selectedItem;
    id inputField = item.inputField;
    
    
    if (!inputField) {
        // Invalid state
        return;
    }
    
    NSDictionary *keyboardInfo = self.lastKeyboardInfo;
    
    DBKeyboardHandlerAnimationInfo animationInfo = DBKeyboardHandlerAnimationInfoMake();
    animationInfo.isOpening = isOpening;
    
    UIViewAnimationCurve animationCurve = [keyboardInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    animationInfo.animationCurve = animationCurve;
    
    NSTimeInterval duration = [keyboardInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    animationInfo.duration = duration;
    
    CGRect keyboardFrame = [[keyboardInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    animationInfo.keyboardFrame = keyboardFrame;
    
    [self calculateAnimationInfo:&animationInfo withInputField:inputField];
    
    if ([inputField isKindOfClass:[DBAccessoryTextField class]]) {
        [self handleAccessoryTextField:inputField withAnimationInfo:animationInfo];
    }
    
    [self performAnimation:animationInfo withInputField:inputField];
}

- (void)calculateAnimationInfo:(DBKeyboardHandlerAnimationInfo *)animationInfo withInputField:(id)inputField
{
    UIView *view = self.view;
    UIScrollView *scrollView = ([self.view isKindOfClass:[UIScrollView class]]) ? (id)self.view : nil;
    
    
    // Devo calcolare le porzioni di schermo che cambiano quando si apre la tastiera
    __unused CGRect frameTastiera = animationInfo->keyboardFrame;
    
    // Per 'schermo' intendo la root view del view controller che ospita le input fields
    UIViewController *rootViewController = view.parentViewController;
    UIView *rootView = rootViewController.view;
    
    
    // Calcolo l'altezza del contenuto
    CGFloat altezzaContenuto = view.frame.size.height;
    
    if (scrollView) {
        altezzaContenuto = scrollView.contentSize.height;
        
        if (altezzaContenuto <= 0.0) {
            altezzaContenuto = scrollView.contentSizeIdeale.height;
        }
    }
    
    // Calcolo lo spazio visibile rimanente sullo schermo con la tastiera aperta
    CGFloat spazioRimanente = [self calculateAvailableSpaceInParentView:rootView withAnimationInfo:*animationInfo inputField:inputField];
    // Se l'altezza del contenuto è inferiore alla porzione di schermo rimanente
    spazioRimanente = fmin(spazioRimanente, altezzaContenuto);
    // Il punto centrale dello spazio rimanente
    CGFloat metaSpazioRimanente = spazioRimanente / 2.0;
    
    // Limite per non far uscire la View dall'alto
    CGFloat limiteSuperiore = metaSpazioRimanente;
    // Limite per non far uscire la View dal basso
    CGFloat limiteInferiore = altezzaContenuto - metaSpazioRimanente;
    
    
    // Calcolo lo spazio coperto dalla tastiera aperta
    CGFloat coperturaTastiera = [self calculateCoveredSpaceInParentView:rootView withAnimationInfo:*animationInfo inputField:inputField];
    
    
    // InputField
    CGRect frameInputField = [inputField frame];
    frameInputField.origin = [self calcolaOrigineReale:inputField];
    
    CGFloat metaAltezzaInputField = frameInputField.size.height / 2.0;
    CGFloat centroInputField = CGRectGetMidY(frameInputField);
    
    
    // Calcolo lo spostamento della schermata
    CGFloat nuovaOrigine = 0.0;
    
    // Se riesco, prendo il centro in base posizione attuale del cursore
    if ([inputField isKindOfClass:[UITextView class]]) {
        UITextView *textView = inputField;
        CGRect frameCursore = [inputField caretRectForPosition:textView.selectedTextRange.start];
        centroInputField = frameInputField.origin.y + CGRectGetMidY(frameCursore);
    }
    
    // Utilizzo la configurazione specifica dell'InputField o, se assente, quella globale
    DBKeyboardHandlerAnchorPoint anchorPoint = [self anchorPointWithInputField:inputField];
    BOOL forceAnchorPoint     = [self forceAnchorPointWithInputField:inputField];
    CGFloat anchorPointMargin = [self anchorPointMarginWithInputField:inputField];
    CGFloat anchorPointOffset = [self anchorPointOffsetWithInputField:inputField];
    
    if (anchorPoint == DBKeyboardHandlerAnchorPointTop) {
        // Correggo il centro dell'InputField per permettere di agganciarla in alto
        centroInputField += metaSpazioRimanente - metaAltezzaInputField - anchorPointMargin;
    }
    else if (anchorPoint == DBKeyboardHandlerAnchorPointBottom) {
        // Correggo il centro dell'InputField per permettere di agganciarla in basso
        centroInputField -= metaSpazioRimanente - metaAltezzaInputField - anchorPointMargin;
    }
    
    // Aggiungo l'offset di correzione (negativo perché sposta il centro)
    centroInputField += -anchorPointOffset;
    
    // Calcolo il nuovo centro dell'InputField
    nuovaOrigine = centroInputField - limiteSuperiore;
    
    if (forceAnchorPoint == NO) {
        if (centroInputField < limiteSuperiore) {
            // Bisogna agganciare il frame in alto perché il centro dell'InputField farebbe uscire la schermata dall'alto
            nuovaOrigine = 0.0;
        }
        else if (centroInputField > limiteInferiore) {
            // Bisogna agganciare il frame in alto perché il centro dell'InputField farebbe uscire la schermata dal basso
            nuovaOrigine = limiteInferiore - limiteSuperiore;
        }
    }
    
    animationInfo->availableSpace = spazioRimanente;
    animationInfo->coveredSpace = coperturaTastiera;
    animationInfo->offset = nuovaOrigine;
}

- (CGFloat)calculateAvailableSpaceInParentView:(UIView *)parentView withAnimationInfo:(DBKeyboardHandlerAnimationInfo)animationInfo inputField:(id)inputField
{
    UIView *view = self.view;
    UIScrollView *scrollView = ([self.view isKindOfClass:[UIScrollView class]]) ? (id)self.view : nil;
    
    // Devo calcolare le porzioni di schermo che cambiano quando si apre la tastiera
    CGRect frameTastiera = animationInfo.keyboardFrame;
    
    // Calcolo lo spazio visibile rimanente sullo parentView con la tastiera aperta
    CGFloat spazioRimanente = parentView.frame.size.height - frameTastiera.size.height;
    
    // La view potrebbe non essere attaccata al lato superiore della parentView
    if ([view isNotEqual:parentView] && [view isDescendantOfView:parentView]) {
        CGPoint realOrigin = [DBGeometry calcolaOrigineReale:view conVistaParent:parentView];
        spazioRimanente -= realOrigin.y;
        // OLD: spazioRimanente -= view.frame.origin.y;
    }
    
    if (scrollView) {
        // La scroll view potrebbe avere un inset che altera l'inizio reale del suo contenuto
        spazioRimanente -= scrollView.contentInset.top;
    }
    
    spazioRimanente = fmax(0.0, spazioRimanente);
    
    return spazioRimanente;
}

- (CGFloat)calculateCoveredSpaceInParentView:(UIView *)parentView withAnimationInfo:(DBKeyboardHandlerAnimationInfo)animationInfo inputField:(id)inputField
{
    UIView *view = self.view;
    __unused UIScrollView *scrollView = ([self.view isKindOfClass:[UIScrollView class]]) ? (id)self.view : nil;
    
    // Devo calcolare le porzioni di schermo che cambiano quando si apre la tastiera
    CGRect frameTastiera = animationInfo.keyboardFrame;
    
    // Calcolo lo spazio coperto dalla tastiera aperta
    CGFloat coperturaTastiera = frameTastiera.size.height;
    
    // La view potrebbe non essere attaccata al lato inferiore della parentView
    if ([view isNotEqual:parentView] && [view isDescendantOfView:parentView]) {
        // Misuro la distanza dal punto più basso della view al punto più basso della parentView
        CGFloat distanzaDalFondo = parentView.frame.size.height - view.frame.origin.y - view.frame.size.height;
        coperturaTastiera -= fmax(0.0, distanzaDalFondo);
    }
    
    // Considero valida la copertura della tastiera solo se effettivamente esiste
    coperturaTastiera = fmax(0.0, coperturaTastiera);
    
    return coperturaTastiera;
}

- (void)performAnimation:(DBKeyboardHandlerAnimationInfo)animationInfo withInputField:(id)inputField
{
    UIView *view = self.view;
    UIScrollView *scrollView = ([self.view isKindOfClass:[UIScrollView class]]) ? (id)self.view : nil;
    
    // Valutare se serve ancora
    if (CGRectIsEmpty(self.frameIniziale)) {
        // Disabilitato perché causa problemi di animazione
        // e perché dovrebbe funzionare lo stesso!
        // [view layoutIfNeeded];
        
        // Imposto il frame iniziale, per la chiusura della tastiera
        self.frameIniziale = view.frame;
        
        if (scrollView) {
            // Imposto il contentInset iniziale
            self.contentInsetIniziale = scrollView.contentInset;
            self.scrollInsetIniziale = scrollView.scrollIndicatorInsets;
        }
    }
    // Fine valutare se serve ancora
    
    // Parameters
    UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction | (animationInfo.animationCurve << 16);
    
    // Informo il delegate che l'animazione sta per iniziare
    if ([self.delegate respondsToSelector:@selector(handler:inputFieldWillStartAnimation:duration:options:keyboardFrame:)]) {
        if (animationInfo.isOpening) {
            // Will open notification
            [self.delegate handler:self inputFieldWillStartAnimation:inputField
                          duration:animationInfo.duration
                           options:options
                     keyboardFrame:animationInfo.keyboardFrame];
        }
        else {
            // Will close notification
            [self.delegate handler:self inputFieldWillStartAnimation:inputField
                          duration:animationInfo.duration
                           options:options
                     keyboardFrame:CGRectZero];
        }
    }
    
    if (self.shouldAnimateViewWithKeyboard) {
        // Necessario altrimenti il campo di testo fa una strana animazione del suo contenuto
        [view layoutIfNeeded];
    }
    
    // Animazione
    [UIView animateWithDuration:animationInfo.duration delay:0 options:options animations:^{
        
        if (self.shouldAnimateViewWithKeyboard == NO) {
            // No animation
            return;
        }
        
        if (scrollView) {
            UIEdgeInsets contentInset = self.contentInsetIniziale;
            UIEdgeInsets scrollInset = self.scrollInsetIniziale;
            
            if (animationInfo.isOpening) {
                // Ridimensiono la porzione sfruttabile della ScrollView
                contentInset.bottom += animationInfo.coveredSpace;
                scrollInset.bottom += animationInfo.coveredSpace;
                [scrollView setContentInset:contentInset];
                [scrollView setScrollIndicatorInsets:scrollInset];
                
                // Sposto l'InputField sul punto di ancoraggio
                // PS: Questa operazione deve essere effettuata dopo aver cambiato gli insets
                CGPoint contentOffset = CGPointMake(0.0, -contentInset.top);
                contentOffset.y += animationInfo.offset;
                [scrollView setContentOffset:contentOffset];
            }
            else {
                [scrollView setContentInset:contentInset];
                [scrollView setScrollIndicatorInsets:scrollInset];
            }
        }
        else {
            CGRect frame = self.frameIniziale;
            
            if (animationInfo.isOpening) {
                // Sposto l'InputField sul punto di ancoraggio
                frame.origin.y -= animationInfo.offset;
            }
            
            view.frame = frame;
            
            // Necessario per rendere fluida e progressiva l'animazione,
            // altrimenti la view fa uno strano scatto verso la direzione sbagliata
            [view layoutIfNeeded];
        }
        
    } completion:^(BOOL finished) {
        // Informo il delegate che l'animazione è terminata
        if ([self.delegate respondsToSelector:@selector(handler:inputFieldDidEndAnimation:keyboardFrame:)]) {
            if (animationInfo.isOpening) {
                // Did open notification
                [self.delegate handler:self inputFieldDidEndAnimation:inputField keyboardFrame:animationInfo.keyboardFrame];
            } else {
                // Did close notification
                [self.delegate handler:self inputFieldDidEndAnimation:inputField keyboardFrame:CGRectZero];
            }
        }
    }];
}

- (void)handleAccessoryTextField:(DBAccessoryTextField *)textField withAnimationInfo:(DBKeyboardHandlerAnimationInfo)animationInfo
{
    if (textField.keyboardHandler == self &&
        [textField respondsToSelector:@selector(keyboardHandlerDidUpdateAvailableSpace:withAnchorMargin:)]) {
        // Calcolo lo spazio disponibile dal fondo della text field all'inizio della tastiera,
        // togliendo fin da subito i margini impostati dall'utente
        
        CGRect frameTastiera = [textField frame];
        frameTastiera.origin = [self calcolaOrigineReale:textField];
        frameTastiera.origin = [DBGeometry calcolaOrigineReale:textField conVistaParent:textField.presentingView];
        
        // DBKeyboardHandlerAnchorPoint anchorPoint = [self anchorPointWithInputField:textField];
        CGFloat anchorMargin = frameTastiera.origin.y - animationInfo.offset;
        
        CGFloat availableSpace = [self calculateAvailableSpaceInParentView:textField.presentingView withAnimationInfo:animationInfo inputField:textField];
        
        if (self.shouldAnimateViewWithKeyboard) {
            availableSpace -= anchorMargin;
        }
        
        if ([textField.superview isEqual:textField.presentingView]) {
            availableSpace -= frameTastiera.size.height;
        }
        
        availableSpace = fmax(0.0, availableSpace);
        
        [textField keyboardHandlerDidUpdateAvailableSpace:availableSpace withAnchorMargin:anchorMargin];
    }
}

// MARK: - Gesture Recognizers

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    // Controllo se è stato toccato un oggetto interattivo (come un'InputField o un pulsante)
    if (self.selectedInputField) {
        if ([self.delegate respondsToSelector:@selector(handler:touchedView:shouldEndEditingOfInputField:)]) {
            if ([self.delegate handler:self touchedView:touch.view shouldEndEditingOfInputField:self.selectedInputField]) {
                // Il delegate ha deciso di terminare la modalità di modifica
                return YES;
            }
            else {
                // Il delegate ha deciso di NON terminare la modalità di modifica
                return NO;
            }
        }
        
        if ([self.selectedInputField isKindOfClass:[DBAccessoryTextField class]]) {
            DBAccessoryTextField *textField = self.selectedInputField;
            if ([touch.view isDescendantOfView:textField.containerView]) {
                // Non bisogna terminare la modalità di modifica
                return NO;
            }
        }
        
        if ([touch.view isNotKindOfClasses:@[[UIControl class], [UITextView class]]]) {
            // Oggetto non interattivo: potrebbe chiudere la tastiera
            return YES;
        }
    }
    
    // Lascio eseguire l'azione all'oggetto toccato
    return NO;
}

- (void)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
    // Tap su un oggetto non interattivo: chiudo la tastiera
    [self endEditing];
}

@end
