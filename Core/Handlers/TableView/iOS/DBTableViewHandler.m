//
//  DBTableViewHandler.m
//  File version: 1.0.0
//  Last modified: 03/08/2016
//
//  Created by Davide Balistreri on 03/08/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBTableViewHandler.h"
#import "DBDeprecated.h"

@interface DBTableViewHandler () <UITableViewDelegate, UITableViewDataSource>

@end


@implementation DBTableViewHandler

+ (NSMutableArray *)sharedInstances
{
    @synchronized(self) {
        NSMutableArray *sharedInstances = [DBCentralManager getRisorsa:@"DBSharedTableViewHandlers"];
        
        if (!sharedInstances) {
            sharedInstances = [NSMutableArray array];
            [DBCentralManager setRisorsa:sharedInstances conIdentifier:@"DBSharedTableViewHandlers"];
        }
        
        return sharedInstances;
    }
}

// MARK: - Metodi di classe

+ (DBTableViewHandler *)handlerWithTableView:(UITableView *)tableView dataSource:(id)dataSource delegate:(id<DBTableViewHandlerDelegate>)delegate
{
    DBTableViewHandler *handler = [DBTableViewHandler new];
    handler.tableView = tableView;
    handler.delegate = delegate;
    handler.dataSource = dataSource;
    handler.useSelfSizingCells = YES;
    handler.hidesEmptyCellsSeparator = YES;
    
    // Lo aggiungo allo stack
    NSMutableArray *sharedInstances = [self sharedInstances];
    [sharedInstances addObject:handler];
    
    return handler;
}

+ (void)destroyHandlerWithTableView:(UITableView *)tableView
{
    // Cerco tra gli Handler attualmente attivi
    NSMutableArray *sharedInstances = [self sharedInstances];
    
    // Cerco tutti gli Handlers che controllano la TableView specificata e li rimuovo
    for (NSInteger counter = 0; counter < sharedInstances.count; counter++) {
        DBTableViewHandler *handler = sharedInstances[counter];
        
        if ([handler.tableView isEqual:tableView]) {
            handler.delegate = nil;
            handler.dataSource = nil;
            handler.tableView = nil;
            
            [sharedInstances removeObject:handler];
            counter--;
        }
    }
}

// MARK: - Public methods

//@synthesize dataSource = _dataSource;
//- (void)setDataSource:(id)dataSource
//{
//    _dataSource = dataSource;
//    [self.tableView reloadData];
//}

@synthesize tableView = _tableView;
- (void)setTableView:(UITableView *)tableView
{
    if (_tableView) {
        // Rimuovo i riferimenti alla vecchia TableView
        _tableView.delegate = nil;
        _tableView.dataSource = nil;
    }
    
    _tableView = tableView;
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    // Refresh
    [self setHidesEmptyCellsSeparator:self.hidesEmptyCellsSeparator];
}

@synthesize hidesEmptyCellsSeparator = _hidesEmptyCellsSeparator;
- (void)setHidesEmptyCellsSeparator:(BOOL)hidesEmptyCellsSeparator
{
    _hidesEmptyCellsSeparator = hidesEmptyCellsSeparator;
    self.tableView.tableFooterView = (hidesEmptyCellsSeparator) ? [UIView new] : nil;
}

// MARK: - TableView Delegate

/**
 * Questo metodo esteso permette di conoscere automaticamente il numero di sezioni da mostrare.
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Se il delegate specificato dallo sviluppatore vuole gestire questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(numberOfSectionsInTableView:)]) {
        return [self.delegate numberOfSectionsInTableView:tableView];
    }
    
    // L'esecuzione di questo metodo indica che è stato richiesto alla TableView di ricaricare i suoi dati
    if ([self.delegate respondsToSelector:@selector(handlerTableViewAggiornata:)]) {
        // Retrocompatibilità
        DBFRAMEWORK_DEPRECATED_CUSTOM_LOG("-[DBTableViewHandlerDelegate handlerTableViewAggiornata:]", 0.7.0, "utilizza il metodo -[DBTableViewHandlerDelegate handlerDidReloadTableView:]");
        
        DBFRAMEWORK_SUPPRESS_DEPRECATED_WARNING([(id)self.delegate handlerTableViewAggiornata:self]);
    }
    
    if ([self.delegate respondsToSelector:@selector(handlerDidReloadTableView:)]) {
        [self.delegate handlerDidReloadTableView:self];
    }
    
    if (self.dataSource && [self.dataSource isMemberOfClass:[DBDataSource class]]) {
        // Gestisco le sezioni del DBDataSource
        return [self.dataSource numeroSezioni];
    }
    else if ([self.dataSource isKindOfClass:[NSArray class]]) {
        // Considero il dataSource una sezione unica
        return 1;
    }
    
    // Caso ancora non contemplato
    return 1;
}

/**
 * Questo metodo esteso permette di conoscere automaticamente il numero di celle della sezione da mostrare.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Se il delegate specificato dallo sviluppatore vuole gestire questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:numberOfRowsInSection:)]) {
        return [self.delegate tableView:tableView numberOfRowsInSection:section];
    }
    
    if (self.dataSource && [self.dataSource isMemberOfClass:[DBDataSource class]]) {
        // Gestisco le sezioni del DBDataSource
        return [self.dataSource numeroOggettiNellaSezioneConIndice:section];
    }
    else if ([self.dataSource isKindOfClass:[NSArray class]]) {
        // Considero il dataSource una sezione unica
        return [self.dataSource count];
    }
    
    // Caso ancora non contemplato
    return 0;
}

/**
 * Questo metodo esteso permette di conoscere automaticamente il nome della sezione da mostrare.
 */
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    // Se il delegate specificato dallo sviluppatore vuole gestire questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:titleForHeaderInSection:)]) {
        return [self.delegate tableView:tableView titleForHeaderInSection:section];
    }
    
    if (self.dataSource && [self.dataSource isKindOfClass:[DBDataSource class]]) {
        // Gestisco le sezioni del DBDataSource
        return [self.dataSource nomeSezioneConIndice:section];
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // Se il delegate specificato dallo sviluppatore vuole gestire questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:viewForHeaderInSection:)]) {
        UIView *headerView = [self.delegate tableView:tableView viewForHeaderInSection:section];
        headerView.userInteractionEnabled = NO;
        return headerView;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // Se il delegate specificato dallo sviluppatore vuole gestire questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:viewForFooterInSection:)]) {
        UIView *footerView = [self.delegate tableView:tableView viewForFooterInSection:section];
        footerView.userInteractionEnabled = NO;
        return footerView;
    }
    
    return nil;
}


// MARK: Variable heights

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    // Se il delegate specificato dallo sviluppatore vuole gestire questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:heightForHeaderInSection:)]) {
        return [self.delegate tableView:tableView heightForHeaderInSection:section];
    }
    
    if (self.useSelfSizingCells) {
        // Automatic header height
        UIView *headerView = [self tableView:tableView viewForHeaderInSection:section];
        
        if (headerView) {
            CGSize size = [headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize withHorizontalFittingPriority:UILayoutPriorityDefaultLow verticalFittingPriority:UILayoutPriorityDefaultHigh];
            return size.height;
        }
    }
    
    // Default
    return 0.0; // tableView.sectionHeaderHeight;
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section
//{
//    // Se il delegate specificato dallo sviluppatore vuole gestire interamente questa parte, non verrà eseguito altro codice
//    if ([self.delegate respondsToSelector:@selector(tableView:estimatedHeightForHeaderInSection:)]) {
//        return [self.delegate tableView:tableView estimatedHeightForHeaderInSection:section];
//    }
//    
//    // Default
//    return tableView.sectionHeaderHeight;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Se il delegate specificato dallo sviluppatore vuole gestire interamente questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:heightForRowAtIndexPath:)]) {
        return [self.delegate tableView:tableView heightForRowAtIndexPath:indexPath];
    }
    
    if (self.useSelfSizingCells) {
        return UITableViewAutomaticDimension;
    }
    
    // Default
    return tableView.rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Se il delegate specificato dallo sviluppatore vuole gestire interamente questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:estimatedHeightForRowAtIndexPath:)]) {
        return [self.delegate tableView:tableView estimatedHeightForRowAtIndexPath:indexPath];
    }
    
    // Default
    return tableView.rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    // Se il delegate specificato dallo sviluppatore vuole gestire interamente questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:heightForFooterInSection:)]) {
        return [self.delegate tableView:tableView heightForFooterInSection:section];
    }
    
    if (self.useSelfSizingCells) {
        // Automatic footer height
        UIView *footerView = [self tableView:tableView viewForFooterInSection:section];
        
        if (footerView) {
            CGSize size = [footerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize withHorizontalFittingPriority:UILayoutPriorityDefaultLow verticalFittingPriority:UILayoutPriorityDefaultHigh];
            return size.height;
        }
    }
    
    // Default
    return 0.0; // tableView.sectionFooterHeight;
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section
//{
//    // Se il delegate specificato dallo sviluppatore vuole gestire interamente questa parte, non verrà eseguito altro codice
//    if ([self.delegate respondsToSelector:@selector(tableView:estimatedHeightForFooterInSection:)]) {
//        return [self.delegate tableView:tableView estimatedHeightForFooterInSection:section];
//    }
//    
//    // Default
//    return tableView.sectionFooterHeight;
//}


// MARK: Cell setup

/**
 * Questo metodo esteso permette di passare automaticamente l'oggetto relativo alla cella da mostrare.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Se il delegate specificato dallo sviluppatore vuole gestire interamente questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:cellForRowAtIndexPath:)]) {
        return [self.delegate tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    
    id oggetto = nil;
    
    if (self.dataSource && [self.dataSource isMemberOfClass:[DBDataSource class]]) {
        // Gestisco le sezioni del DBDataSource
        oggetto = [self.dataSource oggettoConIndice:indexPath.row nellaSezioneConIndice:indexPath.section];
    }
    else if ([self.dataSource isKindOfClass:[NSArray class]]) {
        // Considero il dataSource una sezione unica
        oggetto = [self.dataSource safeObjectAtIndex:indexPath.row];
    }
    
    DBTableViewCell *cella = nil;
    
    // Se il delegate vuole gestire questa parte, non verrà istanziata automaticamente la cella
    if ([self.delegate respondsToSelector:@selector(handler:cellaPerOggetto:conIndexPath:)]) {
        // Retrocompatibilità
        DBFRAMEWORK_DEPRECATED_CUSTOM_LOG("-[DBTableViewHandlerDelegate handler:cellaPerOggetto:conIndexPath:]", 0.7.0, "utilizza il metodo -[DBTableViewHandlerDelegate handler:cellForObject:atIndexPath:]");
        
        DBFRAMEWORK_SUPPRESS_DEPRECATED_WARNING(cella = [(id)self.delegate handler:self cellaPerOggetto:oggetto conIndexPath:indexPath]);
    }
    
    if ([self.delegate respondsToSelector:@selector(handler:cellForObject:atIndexPath:)]) {
        DBFRAMEWORK_SUPPRESS_DEPRECATED_WARNING(cella = (id)[self.delegate handler:self cellForObject:oggetto atIndexPath:indexPath]);
    }
    
    // Se non è stata istanziata alcuna cella
    if (!cella) {
        // Prevenzione dai crash
        cella = [DBTableViewCell new];
    }
    
    // Se la cella istanziata supporta le funzionalità DBTableViewCell
    if ([cella isKindOfClass:[DBTableViewCell class]]) {
        // Aggiorno le informazioni della cella
        cella.oggetto = oggetto;
        cella.indexPath = indexPath;
        [cella aggiornaCella:NO];
        [cella layoutIfNeeded];
    }
    
    // Restituisco la cella
    return cella;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Se il delegate specificato dallo sviluppatore vuole gestire interamente questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:willDisplayCell:forRowAtIndexPath:)]) {
        [self.delegate tableView:tableView willDisplayCell:cell forRowAtIndexPath:indexPath];
    }
}

/**
 * Questo metodo esteso permette di passare automaticamente l'oggetto relativo alla cella selezionata.
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Se il delegate specificato dallo sviluppatore vuole gestire questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)]) {
        return [self.delegate tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
    
    id oggetto = nil;
    
    if (self.dataSource && [self.dataSource isMemberOfClass:[DBDataSource class]]) {
        // Gestisco le sezioni del DBDataSource
        oggetto = [self.dataSource oggettoConIndice:indexPath.row nellaSezioneConIndice:indexPath.section];
    }
    else if ([self.dataSource isKindOfClass:[NSArray class]]) {
        // Considero il dataSource una sezione unica
        oggetto = [self.dataSource safeObjectAtIndex:indexPath.row];
    }
    
    self.selectedIndexPath = indexPath;
    
    UITableViewCell *cella = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([self.delegate respondsToSelector:@selector(handler:cellaSelezionata:conOggetto:conIndexPath:)]) {
        // Retrocompatibilità
        DBFRAMEWORK_DEPRECATED_CUSTOM_LOG("-[DBTableViewHandlerDelegate handler:cellaSelezionata:conOggetto:conIndexPath:]", 0.7.0, "utilizza il metodo -[DBTableViewHandlerDelegate handler:didSelectCell:withObject:atIndexPath:]");
        
        DBFRAMEWORK_SUPPRESS_DEPRECATED_WARNING([(id)self.delegate handler:self cellaSelezionata:(id)cella conOggetto:oggetto conIndexPath:indexPath]);
    }
    
    if ([self.delegate respondsToSelector:@selector(handler:didSelectCell:withObject:atIndexPath:)]) {
        [self.delegate handler:self didSelectCell:cella withObject:oggetto atIndexPath:indexPath];
    }
}


// MARK: Cell editing

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // NB: Non funziona su Swift
    
    // Se il delegate specificato dallo sviluppatore vuole gestire questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:canEditRowAtIndexPath:)]) {
        return [self.delegate tableView:tableView canEditRowAtIndexPath:indexPath];
    }
    
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // NB: Non funziona su Swift
    
    // Se il delegate specificato dallo sviluppatore vuole gestire questa parte, non verrà eseguito altro codice
    if ([self.delegate respondsToSelector:@selector(tableView:commitEditingStyle:forRowAtIndexPath:)]) {
        return [self.delegate tableView:tableView commitEditingStyle:editingStyle forRowAtIndexPath:indexPath];
    }
}

@end
