//
//  DBTableViewHandler.h
//  File version: 1.0.0
//  Last modified: 03/08/2016
//
//  Created by Davide Balistreri on 03/08/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"
#import "DBDataSource.h"
#import "DBTableViewCell.h"

// DBTableViewHandlerDelegate protocol declaration
#import "DBTableViewHandlerDefines.h"


/**
 * Questa classe permette di gestire una TableView e il suo DataSource, semplificando alcuni scenari ricorrenti.
 */
@interface DBTableViewHandler : NSObject <DBTableViewHandlerDelegate>

// MARK: - Metodi di classe

/// Crea un nuovo Handler che si occuperà di gestire la TableView specificata e il suo DataSource, assegnandogli un delegate
+ (nonnull DBTableViewHandler *)handlerWithTableView:(nullable UITableView *)tableView dataSource:(nullable id)dataSource delegate:(nullable id<DBTableViewHandlerDelegate>)delegate;

/**
 * Disabilita ed elimina gli Handler associati alla TableView specificata.
 * @param tableView La TableView con cui è stato configurato l'Handler.
 */
+ (void)destroyHandlerWithTableView:(nullable UITableView *)tableView;


// MARK: - Metodi d'istanza

/** 
 * Delegate conforme al protocollo DBTableViewHandlerDelegate.
 *
 * Il delegate può implementare i metodi UITableViewDataSource e UITableViewDelegate per gestire la TableView manualmente,
 * ma sconsiglio questo approccio perché rende inutile l'utilizzo di questo Handler.
 */
@property (nullable, weak, nonatomic) id<DBTableViewHandlerDelegate> delegate;

/// La TableView da gestire
@property (nullable, strong, nonatomic) UITableView *tableView;

/**
 * La fonte dati della TableView.
 * Attualmente è possibile impostare due tipi di dataSource: NSArray e DBDataSource.
 */
@property (nullable, strong, nonatomic) id dataSource;

/**
 * L'IndexPath dell'ultima cella selezionata.
 */
@property (nullable, weak, nonatomic) NSIndexPath *selectedIndexPath;

/**
 * Permette di gestire automaticamente l'altezza delle celle in base alla loro dimensione.
 * Default: attivato.
 */
@property (nonatomic) BOOL useSelfSizingCells;

/**
 * Permette di nascondere automaticamente il separatore dalle celle vuote.
 * Default: attivato.
 */
@property (nonatomic) BOOL hidesEmptyCellsSeparator;

@end
