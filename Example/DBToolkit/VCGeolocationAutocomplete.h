//
//  VCGeolocationAutocomplete.h
//  DBToolkit
//
//  Created by Davide Balistreri on 08/02/17.
//  Copyright © 2017 Davide Balistreri. All rights reserved.
//

#import "TableViewController.h"

@protocol VCGeolocationAutocompleteDelegate;


@interface VCGeolocationAutocomplete : TableViewController

@property (weak, nonatomic) id<VCGeolocationAutocompleteDelegate> delegate;

- (void)searchForPlacesWithText:(NSString *)text;

// MARK: - Outlets

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end


@protocol VCGeolocationAutocompleteDelegate <NSObject>

@optional

/// Richiamato quando l'utente seleziona una posizione (si può terminare la modalità di modifica).
- (void)didSelectPlace:(DBGeolocationPlace *)place;

@end
