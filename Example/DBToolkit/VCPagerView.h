//
//  VCPagerView.h
//  DBToolkit
//
//  Created by Davide Balistreri on 03/08/16.
//  Copyright © 2016 Davide Balistreri. All rights reserved.
//

#import "ViewController.h"

@interface VCPagerView : ViewController

@property (weak, nonatomic) IBOutlet DBPagerView *pagerView;

@end
