//
//  VCPagerView.m
//  DBToolkit
//
//  Created by Davide Balistreri on 03/08/16.
//  Copyright © 2016 Davide Balistreri. All rights reserved.
//

#import "VCPagerView.h"
#import "VCMenu.h"

@interface VCPagerView () <DBPagerViewDelegate>

@end


@implementation VCPagerView

- (void)impostaSchermata
{
    self.pagerView.delegate = self;
    
    self.pagerView.unselectedColor    = [UIColor lightGrayColor];
    self.pagerView.selectedColor      = [UIColor redColor];
    self.pagerView.pageIndicatorColor = [UIColor redColor];
    self.pagerView.backgroundColor    = [UIColor colorWithBlack:0.05 alpha:1.0];
    
    // Non funzionano bene e non funzionano sul page indicator
    // self.pagerView.tabViewInset = UIEdgeInsetsMake(5.0, 20.0, 20.0, 5.0);
    
    // Dots page indicator
    // self.pagerView.pageIndicator = [DBPagerViewDotsPageIndicator indicator];
    self.pagerView.pageIndicator.contentInset = UIEdgeInsetsMake(10.0, 5.0, 10.0, 30.0);
    // self.pagerView.pageIndicator.backgroundColor = [UIColor whiteColor];
    // self.pagerView.tabViewPosition = DBPagerViewTabViewPositionBottom;
    // self.pagerView.pageIndicatorPosition = DBPagerViewPageIndicatorPositionAwayFromTabView;
    // self.pagerView.contentViewInset = UIEdgeInsetsMake(36.0, 0.0, 0.0, 0.0);
    // self.pagerView.bounces = YES;
    self.pagerView.tabItemStyle = DBPagerViewTabItemStyleFillWidth;
    
    NSInteger pages = [DBUtility randomIntDa:1 a:8];
    for (NSInteger counter = 0; counter < pages; counter++) {
        VCMenu *viewController = [self.storyboard instantiateViewControllerWithClass:[VCMenu class]];
        [self.pagerView addPageWithViewController:viewController];
    }
}

// MARK: - PagerView delegate

- (void)pagerView:(DBPagerView *)pagerView didSelectPage:(DBPagerViewPage *)page
{
    NSLog(@"PagerView didSelectPage: %d", (int)page.index);
}

@end
