//
//  VCBluetooth.m
//  DBToolkit
//
//  Created by Davide Balistreri on 04/04/17.
//  Copyright © 2017 Davide Balistreri. All rights reserved.
//

#import "VCBluetooth.h"
#import "DBBluetooth.h"

@implementation VCBluetooth

// MARK: - Setup

- (void)impostaSchermata
{
    self.dataSource = [NSMutableArray array];
    
    [DBNotificationCenter addObserverOnTarget:self forName:DBBluetoothDidChangeState withSelector:@selector(bluetoothDidChangeState) object:nil];
    
    [DBNotificationCenter addObserverOnTarget:self forName:DBBluetoothDidFindPeripheralNotification withSelector:@selector(bluetoothDidFindPeripheral:) object:nil];
    
    [DBBluetooth scanForPeripheralsNearby];
}

// MARK: - Bluetooth delegate

- (void)bluetoothDidChangeState
{
    if ([DBBluetooth isBluetoothPoweredOn]) {
        [DBProgressHUD toastConMessaggio:@"Bluetooth powered!"];
    }
    else {
        [DBProgressHUD toastConMessaggio:@"Bluetooth turned off!"];
    }
}

- (void)bluetoothDidFindPeripheral:(NSNotification *)notification
{
    DBBluetoothPeripheral *peripheral = notification.object;
    
    if ([self.dataSource containsObject:peripheral] == NO) {
        if (peripheral.peripheral.name) {
            [self.dataSource addObject:peripheral];
            [self.tableView reloadData];
        }
    }
}

// MARK: - TableView delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if ([object isKindOfClass:[DBBluetoothPeripheral class]]) {
        DBBluetoothPeripheral *peripheral = object;
        
        NSString *title = peripheral.peripheral.name;
        cell.textLabel.text = title;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectCell:(UITableViewCell *)cell withObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
