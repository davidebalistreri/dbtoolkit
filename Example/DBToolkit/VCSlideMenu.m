//
//  VCSlideMenu.m
//  DBToolkit
//
//  Created by Davide Balistreri on 28/07/16.
//  Copyright © 2016 Davide Balistreri. All rights reserved.
//

#import "VCSlideMenu.h"
#import "VCMenu.h"

@interface VCSlideMenu ()

@end


@implementation VCSlideMenu

- (void)impostaSchermata
{
    DBDataSource *dataSource = [DBDataSource new];
    
    NSString *leftMenu = @"Left Menu";
    [dataSource aggiungiSezioneConNome:leftMenu];
    
    NSDictionary *addLeftMenu = @{@"title":@"Aggiungi Left Menu", @"identifier":@"addLeft"};
    [dataSource aggiungiOggetto:addLeftMenu nellaSezioneConIndice:0];
    NSDictionary *removeLeftMenu = @{@"title":@"Rimuovi Left Menu", @"identifier":@"removeLeft"};
    [dataSource aggiungiOggetto:removeLeftMenu nellaSezioneConIndice:0];
    
    
    NSString *rightMenu = @"Right Menu";
    [dataSource aggiungiSezioneConNome:rightMenu];
    
    NSDictionary *addRightMenu = @{@"title":@"Aggiungi Right Menu", @"identifier":@"addRight"};
    [dataSource aggiungiOggetto:addRightMenu nellaSezioneConIndice:1];
    NSDictionary *removeRightMenu = @{@"title":@"Rimuovi Right Menu", @"identifier":@"removeRight"};
    [dataSource aggiungiOggetto:removeRightMenu nellaSezioneConIndice:1];
    
    
    NSString *leftMenuOpeningMode = @"Left Menu Mode";
    [dataSource aggiungiSezioneConNome:leftMenuOpeningMode];
    
    NSDictionary *modeLeft1 = @{@"title":@"Default DBSlideMenuOpeningModeAlongOpeningSide", @"identifier":@"modeLeft1"};
    [dataSource aggiungiOggetto:modeLeft1 nellaSezioneConIndice:2];
    NSDictionary *modeLeft2 = @{@"title":@"DBSlideMenuOpeningModeAlongMainController", @"identifier":@"modeLeft2"};
    [dataSource aggiungiOggetto:modeLeft2 nellaSezioneConIndice:2];
    NSDictionary *modeLeft3 = @{@"title":@"DBSlideMenuOpeningModeParallax", @"identifier":@"modeLeft3"};
    [dataSource aggiungiOggetto:modeLeft3 nellaSezioneConIndice:2];
    NSDictionary *modeLeft4 = @{@"title":@"DBSlideMenuOpeningModeAboveMainController", @"identifier":@"modeLeft4"};
    [dataSource aggiungiOggetto:modeLeft4 nellaSezioneConIndice:2];
    
    
    NSString *rightMenuOpeningMode = @"Right Menu Mode";
    [dataSource aggiungiSezioneConNome:rightMenuOpeningMode];
    
    NSDictionary *modeRight1 = @{@"title":@"Default DBSlideMenuOpeningModeAlongOpeningSide", @"identifier":@"modeRight1"};
    [dataSource aggiungiOggetto:modeRight1 nellaSezioneConIndice:3];
    NSDictionary *modeRight2 = @{@"title":@"DBSlideMenuOpeningModeAlongMainController", @"identifier":@"modeRight2"};
    [dataSource aggiungiOggetto:modeRight2 nellaSezioneConIndice:3];
    NSDictionary *modeRight3 = @{@"title":@"DBSlideMenuOpeningModeParallax", @"identifier":@"modeRight3"};
    [dataSource aggiungiOggetto:modeRight3 nellaSezioneConIndice:3];
    NSDictionary *modeRight4 = @{@"title":@"DBSlideMenuOpeningModeAboveMainController", @"identifier":@"modeRight4"};
    [dataSource aggiungiOggetto:modeRight4 nellaSezioneConIndice:3];
    
    
    NSString *leftMenuStatusBarMode = @"Left Menu StatusBar Mode";
    [dataSource aggiungiSezioneConNome:leftMenuStatusBarMode];
    
    NSDictionary *statusLeft1 = @{@"title":@"DBSlideMenuStatusBarModeDefault", @"identifier":@"statusLeft1"};
    [dataSource aggiungiOggetto:statusLeft1 nellaSezioneConIndice:4];
    NSDictionary *statusLeft2 = @{@"title":@"DBSlideMenuStatusBarModeOverlay", @"identifier":@"statusLeft2"};
    [dataSource aggiungiOggetto:statusLeft2 nellaSezioneConIndice:4];
    NSDictionary *statusLeft3 = @{@"title":@"DBSlideMenuStatusBarModeAlongOpeningSide", @"identifier":@"statusLeft3"};
    [dataSource aggiungiOggetto:statusLeft3 nellaSezioneConIndice:4];
    
    
    NSString *rightMenuStatusBarMode = @"Right Menu StatusBar Mode";
    [dataSource aggiungiSezioneConNome:rightMenuStatusBarMode];
    
    NSDictionary *statusRight1 = @{@"title":@"DBSlideMenuStatusBarModeDefault", @"identifier":@"statusRight1"};
    [dataSource aggiungiOggetto:statusRight1 nellaSezioneConIndice:5];
    NSDictionary *statusRight2 = @{@"title":@"DBSlideMenuStatusBarModeOverlay", @"identifier":@"statusRight2"};
    [dataSource aggiungiOggetto:statusRight2 nellaSezioneConIndice:5];
    NSDictionary *statusRight3 = @{@"title":@"DBSlideMenuStatusBarModeAlongOpeningSide", @"identifier":@"statusRight3"};
    [dataSource aggiungiOggetto:statusRight3 nellaSezioneConIndice:5];
    
    self.dataSource = dataSource;
}

#pragma mark - TableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if ([object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dictionary = object;
        NSString *title = [dictionary safeStringForKey:@"title"];
        
        cell.textLabel.text = title;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectCell:(UITableViewCell *)cell withObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    if ([object isNotKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    NSDictionary *dictionary = object;
    NSString *identifier = [dictionary safeStringForKey:@"identifier"];
    
    VCMenu *menu = [self.storyboard instantiateViewControllerWithClass:[VCMenu class]];
    
    // Left Menu
    if ([identifier isEqualToString:@"addLeft"]) {
        [[DBSlideMenu sharedInstance] setMainController:self.navigationController];
        [[DBSlideMenu sharedInstance] setLeftMenuController:menu];
    }
    else if ([identifier isEqualToString:@"removeLeft"]) {
        [[DBSlideMenu sharedInstance] setLeftMenuController:nil];
    }
    // Right Menu
    else if ([identifier isEqualToString:@"addRight"]) {
        [[DBSlideMenu sharedInstance] setRightMenuController:menu];
        [[DBSlideMenu sharedInstance] setMainController:self.navigationController];
    }
    else if ([identifier isEqualToString:@"removeRight"]) {
        [[DBSlideMenu sharedInstance] setRightMenuController:nil];
    }
    // Left Menu Mode
    else if ([identifier isEqualToString:@"modeLeft1"]) {
        [[DBSlideMenu sharedInstance] setLeftMenuOpeningMode:DBSlideMenuOpeningModeAlongOpeningSide];
    }
    else if ([identifier isEqualToString:@"modeLeft2"]) {
        [[DBSlideMenu sharedInstance] setLeftMenuOpeningMode:DBSlideMenuOpeningModeAlongMainController];
    }
    else if ([identifier isEqualToString:@"modeLeft3"]) {
        [[DBSlideMenu sharedInstance] setLeftMenuOpeningMode:DBSlideMenuOpeningModeParallax];
    }
    else if ([identifier isEqualToString:@"modeLeft4"]) {
        [[DBSlideMenu sharedInstance] setLeftMenuOpeningMode:DBSlideMenuOpeningModeAboveMainController];
    }
    // Right Menu Mode
    else if ([identifier isEqualToString:@"modeRight1"]) {
        [[DBSlideMenu sharedInstance] setRightMenuOpeningMode:DBSlideMenuOpeningModeAlongOpeningSide];
    }
    else if ([identifier isEqualToString:@"modeRight2"]) {
        [[DBSlideMenu sharedInstance] setRightMenuOpeningMode:DBSlideMenuOpeningModeAlongMainController];
    }
    else if ([identifier isEqualToString:@"modeRight3"]) {
        [[DBSlideMenu sharedInstance] setRightMenuOpeningMode:DBSlideMenuOpeningModeParallax];
    }
    else if ([identifier isEqualToString:@"modeRight4"]) {
        [[DBSlideMenu sharedInstance] setRightMenuOpeningMode:DBSlideMenuOpeningModeAboveMainController];
    }
    // Left Menu StatusBar Mode
    else if ([identifier isEqualToString:@"statusLeft1"]) {
        [[DBSlideMenu sharedInstance] setLeftMenuStatusBarMode:DBSlideMenuStatusBarModeDefault];
    }
    else if ([identifier isEqualToString:@"statusLeft2"]) {
        [[DBSlideMenu sharedInstance] setLeftMenuStatusBarMode:DBSlideMenuStatusBarModeOverlay];
    }
    else if ([identifier isEqualToString:@"statusLeft3"]) {
        [[DBSlideMenu sharedInstance] setLeftMenuStatusBarMode:DBSlideMenuStatusBarModeAlongOpeningSide];
    }
    // Right Menu StatusBar Mode
    else if ([identifier isEqualToString:@"statusRight1"]) {
        [[DBSlideMenu sharedInstance] setRightMenuStatusBarMode:DBSlideMenuStatusBarModeDefault];
    }
    else if ([identifier isEqualToString:@"statusRight2"]) {
        [[DBSlideMenu sharedInstance] setRightMenuStatusBarMode:DBSlideMenuStatusBarModeOverlay];
    }
    else if ([identifier isEqualToString:@"statusRight3"]) {
        [[DBSlideMenu sharedInstance] setRightMenuStatusBarMode:DBSlideMenuStatusBarModeAlongOpeningSide];
    }
}

@end
