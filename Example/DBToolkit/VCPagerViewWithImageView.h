//
//  VCPagerViewWithImageView.h
//  DBToolkit
//
//  Created by Davide Balistreri on 02/03/17.
//  Copyright © 2017 Davide Balistreri. All rights reserved.
//

#import "ViewController.h"

@interface VCPagerViewWithImageView : ViewController

// MARK: - Outlets

@property (weak, nonatomic) IBOutlet DBPagerView *pagerView;

@end
