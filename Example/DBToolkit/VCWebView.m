//
//  VCWebView.m
//  DBToolkit
//
//  Created by Davide Balistreri on 24/03/17.
//  Copyright © 2017 Davide Balistreri. All rights reserved.
//

#import "VCWebView.h"

@interface VCWebView () <DBWebViewDelegate>

@end


@implementation VCWebView

- (void)impostaSchermata
{
    // Web view
    self.webView.delegate = self;
    
    [self.webView loadURLString:@"http://www.google.com/"];
}

@end
