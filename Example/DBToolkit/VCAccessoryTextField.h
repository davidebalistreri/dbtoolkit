//
//  VCAccessoryTextField.h
//  DBToolkit
//
//  Created by Davide Balistreri on 05/12/16.
//  Copyright © 2016 Davide Balistreri. All rights reserved.
//

#import "ViewController.h"

@interface VCAccessoryTextField : ViewController

#pragma mark - Outlets

@property (weak, nonatomic) IBOutlet DBScrollView *scrollView;

@property (weak, nonatomic) IBOutlet DBAccessoryTextField *textField1;
@property (weak, nonatomic) IBOutlet DBAccessoryTextField *textField2;
@property (weak, nonatomic) IBOutlet DBAccessoryTextField *textField3;
@property (weak, nonatomic) IBOutlet DBAccessoryTextField *textField4;
@property (weak, nonatomic) IBOutlet DBAccessoryTextField *textField5;
@property (weak, nonatomic) IBOutlet DBAccessoryTextField *textField6;

#pragma mark - Collections

@property (strong, nonatomic) IBOutletCollection(DBAccessoryTextField) NSArray *textFields;

@end
