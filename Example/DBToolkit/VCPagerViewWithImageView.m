//
//  VCPagerViewWithImageView.m
//  DBToolkit
//
//  Created by Davide Balistreri on 02/03/17.
//  Copyright © 2017 Davide Balistreri. All rights reserved.
//

#import "VCPagerViewWithImageView.h"

@implementation VCPagerViewWithImageView

- (void)impostaSchermata
{
    NSMutableArray *dataSource = [NSMutableArray array];
    [dataSource addObject:@"https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-490620.jpg"];
    [dataSource addObject:@"https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-447891.jpg"];
    [dataSource addObject:@"https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-275329.jpg"];
    [dataSource addObject:@"https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-486228.jpg"];
    [dataSource addObject:@"https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-485744.jpg"];
    [dataSource addObject:@"https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-485331.jpg"];
    [dataSource addObject:@"https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-481635.jpg"];
    [dataSource addObject:@"https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-480302.jpg"];
    [dataSource addObject:@"https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-480876.jpg"];
    [dataSource addObject:@"https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-275302.jpg"];
    [dataSource addObject:@"https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-478155.jpg"];
    
    UIImage *placeholderImage = [UIImage imageNamed:@"Placeholder"];
    
    for (NSString *urlString in dataSource) {
        DBImageView *imageView = [DBImageView new];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        [imageView setClipsToBounds:YES];
        
        [imageView setImageWithURLString:urlString placeholderImage:placeholderImage];
        
        [self.pagerView addPageWithView:imageView withTitle:nil];
    }
    
    self.pagerView.tabViewHidden   = YES;
    self.pagerView.tabViewPosition = DBPagerViewTabViewPositionBottom;
    self.pagerView.pageIndicator   = [DBPagerViewDotsPageIndicator indicator];
    
    self.pagerView.selectedColor   = [UIColor whiteColor];
    self.pagerView.unselectedColor = [UIColor colorWithWhite:1.0 alpha:0.4];
}

@end
