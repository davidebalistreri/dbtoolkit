//
//  VCSlideMenu.h
//  DBToolkit
//
//  Created by Davide Balistreri on 28/07/16.
//  Copyright © 2016 Davide Balistreri. All rights reserved.
//

#import "TableViewController.h"

@interface VCSlideMenu : TableViewController

#pragma mark - Outlets

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
