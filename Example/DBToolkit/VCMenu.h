//
//  VCMenu.h
//  DBToolkit
//
//  Created by Davide Balistreri on 28/07/16.
//  Copyright © 2016 Davide Balistreri. All rights reserved.
//

#import "TableViewController.h"

@interface VCMenu : TableViewController

#pragma mark - Outlets

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
