//
//  AppDelegate.h
//  DBToolkit
//
//  Created by Davide Balistreri on 07/26/2016.
//  Copyright (c) 2016 Davide Balistreri. All rights reserved.
//

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
