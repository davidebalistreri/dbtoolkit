//
//  VCHome.m
//  DBToolkit
//
//  Created by Davide Balistreri on 07/26/2016.
//  Copyright (c) 2016 Davide Balistreri. All rights reserved.
//

#import "VCHome.h"

@interface VCHome ()

@end


@implementation VCHome

- (void)impostaSchermata
{
    DBDataSource *dataSource = [DBDataSource new];
    
    NSString *uiClasses = @"UIKit Classes";
    [dataSource aggiungiSezioneConNome:uiClasses];
    
    NSDictionary *slideMenu = @{@"title":@"Slide Menu", @"class":@"VCSlideMenu"};
    [dataSource aggiungiOggetto:slideMenu nellaSezioneConIndice:0];
    NSDictionary *pagerView = @{@"title":@"PagerView", @"class":@"VCPagerView"};
    [dataSource aggiungiOggetto:pagerView nellaSezioneConIndice:0];
    NSDictionary *pagerViewWithImageView = @{@"title":@"PagerView with DBImageView", @"class":@"VCPagerViewWithImageView"};
    [dataSource aggiungiOggetto:pagerViewWithImageView nellaSezioneConIndice:0];
    NSDictionary *accessoryTextField = @{@"title":@"Accessory Text Field", @"class":@"VCAccessoryTextField"};
    [dataSource aggiungiOggetto:accessoryTextField nellaSezioneConIndice:0];
    NSDictionary *geolocation = @{@"title":@"Geolocation", @"class":@"VCGeolocation"};
    [dataSource aggiungiOggetto:geolocation nellaSezioneConIndice:0];
    NSDictionary *webView = @{@"title":@"WebView", @"class":@"VCWebView"};
    [dataSource aggiungiOggetto:webView nellaSezioneConIndice:0];
    NSDictionary *bluetooth = @{@"title":@"Bluetooth", @"class":@"VCBluetooth"};
    [dataSource aggiungiOggetto:bluetooth nellaSezioneConIndice:0];
    
    self.dataSource = dataSource;
}

#pragma mark - TableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if ([object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dictionary = object;
        NSString *title = [dictionary safeStringForKey:@"title"];
        
        cell.textLabel.text = title;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectCell:(UITableViewCell *)cell withObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    if ([object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dictionary = object;
        NSString *class = [dictionary safeStringForKey:@"class"];
        
        UIViewController *next = [self.storyboard instantiateViewControllerWithIdentifier:class];
        [self.navigationController pushViewController:next animated:YES];
    }
}

@end
