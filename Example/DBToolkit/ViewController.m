//
//  ViewController.m
//  DBToolkit
//
//  Created by Davide Balistreri on 28/07/16.
//  Copyright © 2016 Davide Balistreri. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *title = NSStringFromClass([self class]);
    title = [title stringByReplacingOccurrencesOfString:@"VC" withString:@""];
    self.navigationItem.title = title;
}

@end
