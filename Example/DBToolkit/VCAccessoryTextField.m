//
//  VCAccessoryTextField.m
//  DBToolkit
//
//  Created by Davide Balistreri on 05/12/16.
//  Copyright © 2016 Davide Balistreri. All rights reserved.
//

#import "VCAccessoryTextField.h"

@interface VCAccessoryTextField () <DBKeyboardHandlerDelegate>

@property (strong, nonatomic) NSMutableArray *viewControllers;

@end


@implementation VCAccessoryTextField

- (void)impostaSchermata
{
    self.viewControllers = [NSMutableArray array];
    
    for (DBAccessoryTextField *textField in self.textFields) {
        
        // Content
        NSString *identifier = nil;
        
        switch ([DBUtility randomIntDa:1 a:2]) {
            case 1:
                identifier = @"VCProva3";
                break;
            case 2:
                identifier = @"VCMenu";
                break;
        }
        
        textField.placeholder = identifier;
        
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
        textField.accessoryView = viewController.view;
        
        // To keep the view controller alive (strong reference)
        [self.viewControllers addObject:viewController];
        
        // Style
        switch ([DBUtility randomIntDa:1 a:2]) {
            case 1:
                textField.placeholder = [textField.placeholder stringByAppendingString:@" ExpandSize"];
                textField.accessoryViewSize = DBAccessoryViewExpandSize;
                break;
            case 2:
                textField.placeholder = [textField.placeholder stringByAppendingString:@" RoundedCorners"];
                textField.accessoryViewSize = DBAccessoryViewAutomaticSize;
                textField.accessoryViewInsets = UIEdgeInsetsMake(20.0, 0.0, 0.0, 0.0);
                [textField.accessoryView aggiungiAngoliArrotondatiConRaggio:8.0];
                break;
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [DBKeyboardHandler handlerWithView:self.scrollView inputFields:self.textFields delegate:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [DBKeyboardHandler destroyHandlerWithView:self.scrollView];
}

// MARK: - DBKeyboardHandler delegate

- (void)handler:(DBKeyboardHandler *)handler inputFieldDidBeginEditing:(id)inputField
{
    self.scrollView.scrollEnabled = NO;
}

- (void)handler:(DBKeyboardHandler *)handler inputFieldDidEndEditing:(id)inputField
{
    self.scrollView.scrollEnabled = YES;
}

@end
