//
//  VCBluetooth.h
//  DBToolkit
//
//  Created by Davide Balistreri on 04/04/17.
//  Copyright © 2017 Davide Balistreri. All rights reserved.
//

#import "TableViewController.h"

@interface VCBluetooth : TableViewController

#pragma mark - Outlets

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
