//
//  VCMenu.m
//  DBToolkit
//
//  Created by Davide Balistreri on 28/07/16.
//  Copyright © 2016 Davide Balistreri. All rights reserved.
//

#import "VCMenu.h"

@interface VCMenu ()

@end


@implementation VCMenu

- (void)inizializzaSchermata
{
    self.title = [DBUtility randomStringLeggibileConNumeroCaratteri:[DBUtility randomIntDa:10 a:30]];
    // self.title = @"Ciao";
}

#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.textLabel.text = [NSString stringWithFormat:@"Cell %02d", (int)indexPath.row + 1];
    // cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

@end
