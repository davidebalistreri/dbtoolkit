//
//  main.m
//  DBToolkit
//
//  Created by Davide Balistreri on 07/26/2016.
//  Copyright (c) 2016 Davide Balistreri. All rights reserved.
//

@import UIKit;
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
