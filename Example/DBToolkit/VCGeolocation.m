//
//  VCGeolocation.m
//  DBToolkit
//
//  Created by Davide Balistreri on 08/02/17.
//  Copyright © 2017 Davide Balistreri. All rights reserved.
//

#import "VCGeolocation.h"
#import "VCGeolocationAutocomplete.h"

@interface VCGeolocation () <VCGeolocationAutocompleteDelegate, DBKeyboardHandlerDelegate>

@property (strong, nonatomic) VCGeolocationAutocomplete *vcGeolocationAutocomplete;

@end


@implementation VCGeolocation

- (void)impostaSchermata
{
    // Location autocomplete
    self.vcGeolocationAutocomplete = [self.storyboard instantiateViewControllerWithClass:[VCGeolocationAutocomplete class]];
    self.vcGeolocationAutocomplete.delegate = self;
    
    self.textField.accessoryView = self.vcGeolocationAutocomplete.view;
    self.textField.presentingView = self.view;
    self.textField.accessoryViewSize = DBAccessoryViewExpandSize;
    // self.textField.accessoryViewInsets = UIEdgeInsetsMake(20.0, 20.0, 5.0, 20.0);
    
    // UI
    [self.containerTextField aggiungiAngoliArrotondatiConRaggio:6.0];
    [self.containerTextField aggiungiBordoConColore:[UIColor lightGrayColor] conSpessore:1.0];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    DBKeyboardHandler *keyboardHandler = [DBKeyboardHandler handlerWithView:self.view inputFields:@[self.textField] delegate:self];
    [keyboardHandler inputField:self.textField setAnchorPointMargin:20.0];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [DBKeyboardHandler destroyHandlerWithView:self.view];
}

// MARK: DBKeyboardHandler delegate

- (void)handler:(DBKeyboardHandler *)handler inputFieldDidChangeText:(id)inputField
{
    if (inputField == self.textField) {
        [self.vcGeolocationAutocomplete searchForPlacesWithText:self.textField.text];
    }
}

@end
