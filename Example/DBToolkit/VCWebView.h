//
//  VCWebView.h
//  DBToolkit
//
//  Created by Davide Balistreri on 24/03/17.
//  Copyright © 2017 Davide Balistreri. All rights reserved.
//

#import "ViewController.h"

@interface VCWebView : ViewController

// MARK: - Outlets

@property (weak, nonatomic) IBOutlet DBWebView *webView;

@end
