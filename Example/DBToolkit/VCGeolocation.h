//
//  VCGeolocation.h
//  DBToolkit
//
//  Created by Davide Balistreri on 08/02/17.
//  Copyright © 2017 Davide Balistreri. All rights reserved.
//

#import "ViewController.h"

@interface VCGeolocation : ViewController

@property (weak, nonatomic) IBOutlet DBAccessoryTextField *textField;
@property (weak, nonatomic) IBOutlet UIView *containerTextField;

@end
