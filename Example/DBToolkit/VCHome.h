//
//  VCHome.h
//  DBToolkit
//
//  Created by Davide Balistreri on 07/26/2016.
//  Copyright (c) 2016 Davide Balistreri. All rights reserved.
//

#import "TableViewController.h"

@interface VCHome : TableViewController

#pragma mark - Outlets

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
