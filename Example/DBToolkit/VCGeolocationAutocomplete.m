//
//  VCGeolocationAutocomplete.m
//  DBToolkit
//
//  Created by Davide Balistreri on 08/02/17.
//  Copyright © 2017 Davide Balistreri. All rights reserved.
//

#import "VCGeolocationAutocomplete.h"

@interface VCGeolocationAutocomplete ()

@property (strong, nonatomic) NSString *searchText;

@end


@implementation VCGeolocationAutocomplete

- (void)impostaSchermata
{
    self.useSelfSizingCells = YES;
}

- (void)aggiornaSchermata:(BOOL)animazione
{
    [self.tableView reloadData:animazione];
}

- (void)aggiornaDataSource
{
    if (self.searchText.length < 2) {
        self.dataSource = [NSArray array];
        [self aggiornaSchermata:YES];
    }
    else {
        __weak VCGeolocationAutocomplete *weakSelf = self;
        
        [DBGeolocation searchForPlacesWithQuery:self.searchText completionBlock:^(NSArray<DBGeolocationPlace *> * _Nullable places) {
            // Parse if needed
            weakSelf.dataSource = places;
            [weakSelf aggiornaSchermata:YES];
        }];
    }
}

// MARK: - Public methods

- (void)searchForPlacesWithText:(NSString *)text
{
    // Check if the text has changed
    if ([text isNotEqualToString:self.searchText]) {
        self.searchText = [text copy];
        
        if ([NSString isEmpty:text]) {
            // Bypass the data source update delay
            [self aggiornaDataSource];
        }
        else {
            // Scheduling next data source update
            [DBThread eseguiBlockConRitardo:0.3 block:^{
                // Check if address is the same
                if ([text isEqualToString:self.searchText]) {
                    [self aggiornaDataSource];
                }
            }];
        }
    }
}

// MARK: - TableView delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    DBGeolocationPlace *place = object;
    cell.textLabel.text = place.name;
    cell.detailTextLabel.text = place.formattedAddress;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectCell:(UITableViewCell *)cell withObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(didSelectPlace:)]) {
        [self.delegate didSelectPlace:object];
    }
}

@end
