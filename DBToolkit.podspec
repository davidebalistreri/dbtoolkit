#
# Be sure to run `pod lib lint DBToolkit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'DBToolkit'
    s.version          = '0.9.0'
    s.summary          = 'Group of features created to simplify Apple developers\' life'
    s.description      = <<-DESC
    Group of features created to simplify Apple developers\' life.
    DESC
    s.homepage         = 'http://davidebalistreri.it/'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'Davide Balistreri' => 'davidebalistreri@hotmail.it' }
    s.source           = { :git => 'https://davidebalistreri@bitbucket.org/davidebalistreri/dbtoolkit.git', :tag => s.version.to_s }
    
    s.requires_arc = true
    s.ios.deployment_target = '8.0'
    s.osx.deployment_target = '10.9'
    
    s.source_files  = 'Core/**/**/**/*'
    s.exclude_files = 'Core/Development/**/*', 'Core/Deprecated/**/*'
    # s.prefix_header_contents = '#import "PrefixHeader.h"'
    # s.public_header_files = 'Core/DBFramework.h'
    # s.private_header_files = 'Core/Frameworks/*.h'
    
    s.ios.exclude_files = 'Core/**/OSX/**/*', 'Core/**/**/OSX/**/*'
    s.osx.exclude_files = 'Core/**/iOS/**/*', 'Core/**/**/iOS/**/*'
    
    # s.resource_bundles = {
    #   'DBToolkit' => ['Core/Resources/**/*']
    # }
    
    s.preserve_paths = 'Core'
    
    # s.dependency 'AFNetworking', '~> 2.6.3'
    # s.osx.dependency 'BFNavigationController', '1.1.0'
    s.osx.dependency 'JNWCollectionView', '1.4'
    s.osx.dependency 'BFPageControl', '1.0.1'
    s.osx.dependency 'BMExtendablePageController', '0.1.1'
    
    s.framework = 'WebKit'
    # s.dependency 'MBProgressHUD'
    # s.dependency 'SMCalloutView'
    
    s.xcconfig = { 'LD_RUNPATH_SEARCH_PATHS' => '$(inherited) $(FRAMEWORK_SEARCH_PATHS)' }
    
    # s.subspec 'FacebookKit' do |ss|
    #     ss.ios.dependency 'FBSDKCoreKit', '~> 4.34.0'
    #     ss.ios.dependency 'FBSDKLoginKit', '~> 4.34.0'
    #     ss.ios.dependency 'FBSDKShareKit', '~> 4.34.0'
    #     ss.xcconfig = { 'OTHER_CFLAGS' => '$(inherited) -DDBFRAMEWORK_SUBSPEC_FACEBOOKKIT' }
    # end
    
    # s.subspec 'GoogleKit' do |ss|
    #     ss.ios.dependency 'Google/SignIn', '3.0.3'
    #     ss.xcconfig = { 'OTHER_CFLAGS' => '$(inherited) -DDBFRAMEWORK_SUBSPEC_GOOGLEKIT' }
    # end
end
